from OFS.Folder import Folder
from OFS.Image import File
from OFS.Image import cookId
from OFS.DTMLDocument import DTMLDocument
from Globals import DTMLFile, InitializeClass,package_home
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from AccessControl import getSecurityManager
from Products.PageTemplates.PageTemplate import PageTemplate
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate
from AccessControl import ClassSecurityInfo
from difflib import Differ
from pprint import pprint
from Products.ZCatalog.CatalogPathAwareness import CatalogAware

try:
 from Products.ImageArchive.ImageArchive import manage_AddImageZogiLib
except:
 print "no images"


from threading import Thread
import shutil
import tempfile
import os.path
import urllib
import logging
import time
try:
    from Products.ECHO_content.ECHO_collection import ECHO_basis
except:
    print "ECHO Elements not imported"
    class ECHO_basis:
             """leer"""
             manage_options=()

        
def sortv(x,y):
    return cmp(x[0],y[0])

tdir = "/tmp/downloadVersionedFiles"

class generateDownloadZip:
    """generateDownloadSet"""

    def __init__(self,folderObject,url):
        """init downloadzip"""
        self.folder=folderObject
        self.done=None
        self.response=""
        self.url=url
        
    def __call__(self):
        """call generate download zip"""
        storeTempDir=tempfile.tempdir
        tempfile.tempdir=tdir

        tmpPath=tempfile.mktemp()
        tmpZip=tempfile.mktemp()+".gtz"
        tmpFn=os.path.split(tmpZip)[1]
        
        if not os.path.exists(tempfile.tempdir):
            os.mkdir(tempfile.tempdir) 

        if not os.path.exists(tmpPath):
                    os.mkdir(tmpPath) 
            
        tempfile.tempdir=storeTempDir
        self.response="<h3>1. step: getting the files</h3>"

        for files in self.folder.ZopeFind(self.folder,obj_metatypes=['versionedFile']):
            lastV=files[1].getLastVersion()
            self.response+=str("<p>Get File: %s<br>\n"%lastV.title)

            savePath=os.path.join(tmpPath,lastV.title)
            fh=file(savePath,"w")
            logging.debug("writing file %1 data in %s"%(lastV.title,savePath))
            # loop to get OFS.Image.File data
            data=lastV.data
            if isinstance(data, str):
                fh.write(data)
            else:
                while data is not None:
                    fh.write(data.data)
                    data=data.next

            fh.close()

        self.response+="<h3>2. step: creating the downloadable file</h3>"
        self.response+="<p>Create gtar<br>"
        self.response+="<p>This can take a while....<br>\n"

        fh=os.popen2("tar zcvf %s %s/*"%(tmpZip,tmpPath),1)[1]
        self.response+="<br>"
        for c in fh.read():
            self.response+=c
            if c==")":
                self.response+="<br>\n"
                    
                            

        
        shutil.rmtree(tmpPath)

        self.response+="<p>finished<br>\n"

        len=os.stat(tmpZip)[6]
        downloadUrl=self.url+"/downloadSet"
        self.response+="""<h1><a href="downloadSet?fn=%s">Click here for download ( %i Byte)</a></h1>\n"""%(tmpFn,len)
        self.response+="""<p>The file you receive is a tar (gnutar) compressed file, after unpacking you will find a new folder <emph>tmp</emph> where the files are stored in.</p>"""
        self.response+="""<p>The file will be stored for a while, you can download it later, the URL is:</p>
                    <p><a href="downloadSet?fn=%s">%s?fn=%s</a></h1>\n"""%(tmpFn,downloadUrl,tmpFn)

        self.done=True

 
    def getResult(self):
        """get result"""
        return self.response

    def isDone(self):
        if self.done:
            return True
        else:
            return False
        

class versionedFileFolder(Folder,ECHO_basis):
    """Folder with versioned files"""

    
    meta_type = "versionedFileFolder"

    security= ClassSecurityInfo()
    security.declareProtected('AUTHENTICATED_USER','addFileForm')
    filesMetaType=['versionedFile']
    if ECHO_basis:
        optTMP= Folder.manage_options+ECHO_basis.manage_options
    else:
        optTMP= Folder.manage_options

    manage_options =optTMP+(
                {'label':'Generate Index.html','action':'generateIndexHTML'},
                {'label':'Generate Image Index.html','action':'generateIndexHTML_image'},
                {'label':'Generate history_template.html','action':'generateHistoryHTML'},
                {'label':'Generate addFileForm','action':'generateAddFileForm'},
                {'label':'Import Folder','action':'importFolderForm'},
                {'label':'Export Folder','action':'exportFolder'},
                {'label':'Position of version number','action':'changeHistoryFileNamesForm'},
                )

            
    def changeHistoryFileNamesForm(self):
        """change position of version num"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','changeHistoryFileNamesForm.zpt')).__of__(self)
        return pt()
    
    
    def changeHistoryFileNames(self,positionVersionNum="front",RESPONSE=None):
        """change position of version num"""
        
        
        
        versions=self.ZopeFind(self,obj_metatypes=['versionedFileObject'],search_sub=1)
        
        if not (getattr(self,'positionVersionNum','front')==positionVersionNum):

            for version in versions:
                                
                if positionVersionNum=="front":
        
                    titleTmp=os.path.splitext(version[1].title)
                    titleTmp2="_".join(titleTmp[0].split("_")[0:-1])
                    if len(titleTmp)>1:
                        id=titleTmp[0].split("_")[-1]+"_"+titleTmp2+"."+titleTmp[1]
                    else:
                        id=titleTmp[0].split("_")[-1]+"_"+titleTmp2

                else:
                    titleTmp="_".join(version[1].getId().split("_")[1:])
                    tmp=os.path.splitext(titleTmp)
                    if len(tmp)>1:
                        id=tmp[0]+"_"+version[1].getId().split("_")[0]+tmp[1]
                    else:
                        id=tmp[0]+"_"+version[1].getId().split("_")[0]
                
                
                
                version[1].aq_parent.manage_renameObjects(ids=[version[1].getId()],new_ids=[id])
                version[1].title=id
                
                
        self.positionVersionNum=positionVersionNum        
        if RESPONSE:
            RESPONSE.redirect("manage_main")
        
        
        
    def importFolderForm(self):
        """form fuer folder import"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','importFolderForm.zpt')).__of__(self)
        return pt()
        
    def importFolder(self,path,comment="",author=None,lockedBy=None,RESPONSE=None):
        """importiere inhalt eines folders"""

        for fileName in os.listdir(path):
            if os.path.isfile(os.path.join(path,fileName)):
                manage_addVersionedFile(self,fileName,'','')
                id=fileName
                ob=self._getOb(fileName)
                ob.title=id
                file2=file(os.path.join(path,fileName))
        
                obj=ob.manage_addVersionedFileObject(id,comment,author,file2,content_type='')

        if RESPONSE:
            RESPONSE.redirect(self.REQUEST['URL1'])

    zipThreads={}
    zipThreads2={}

    def refreshTxt(self):
        """txt fuer refresh"""
        tn=self.REQUEST.SESSION['threadName']
        return """ 2;url=%s?repeat=%s """%(self.absolute_url()+"/exportFolder",tn)

    def exportFolder(self,repeat=None):
        """exportiert alle akutellen files des folders"""
        threadName=repeat
        
        downloadZip=generateDownloadZip(self,self.absolute_url())
        downloadZip()
        return downloadZip.getResult()
       ##  if not threadName or threadName=="":
##             threadStart=generateDownloadZip(self,self.absolute_url())
##             thread=Thread(target=threadStart)
            
##             thread.start()

                    
##             self.zipThreads[thread.getName()[0:]]=threadStart
##             self.zipThreads2[thread.getName()[0:]]=thread
##             self.REQUEST.SESSION['threadName']=thread.getName()[0:]
##             wait_template=self.aq_parent.ZopeFind(self.aq_parent,obj_ids=['zip_wait_template'])
##             if wait_template:
##                 return wait_template[0][1]()
##             pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','zip_wait.zpt')).__of__(self)
##             return pt()
                
##         else:
##             self.REQUEST.SESSION['threadName']=threadName

##             if (self.zipThreads[threadName].getResult()==None):

##                 wait_template=self.aq_parent.ZopeFind(self.aq_parent,obj_ids=['wait_template'])
##                 if wait_template:
##                     return wait_template[0][1]()

##                 pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','zip_wait.zpt')).__of__(self)
##                 return pt()
##             else:
##                 if self.zipThreads[threadName].isDone():
##                     self.REQUEST.SESSION['result']=self.zipThreads[threadName].getResult()
##                     self.zipThreads2[threadName].join()
##                     del(self.zipThreads2[threadName])
##                     del(self.zipThreads[threadName])
##                     pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','zip_result.zpt')).__of__(self)
##                     return pt()

##                 else:
##                     self.REQUEST.SESSION['result']=self.zipThreads[threadName].getResult()
##                     self.REQUEST.SESSION['threadName']=threadName
##                     pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','zip_wait_result.zpt')).__of__(self)
##                     return pt()

    def downloadSet(self,fn):
            """download prepared set"""
            filename=os.path.join(tdir,fn)

            
            self.REQUEST.RESPONSE.setHeader("Content-Disposition","""attachement; filename="%s" """%"downloadFileFolder.tgz")
            self.REQUEST.RESPONSE.setHeader("Content-Type","application/octet-stream")
            len=os.stat(filename)[6]
            self.REQUEST.RESPONSE.setHeader("Content-Length",len)
            images=file(filename).read()
            self.REQUEST.RESPONSE.write(images)
            self.REQUEST.RESPONSE.close()

        

    def helpDownload(self):
        """download help"""
        
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','helpDownload')).__of__(self)
        return pt()
    
    def generateIndexHTML_image(self,RESPONSE=None):
        """lege standard index.html an"""


        if not self.ZopeFind(self,obj_ids=['index.html']):
            zt=ZopePageTemplate('index.html')
            self._setObject('index.html',zt)
            default_content_fn = os.path.join(package_home(globals()),
                                               'zpt/versionFileFolderMain_image.zpt')
            text = open(default_content_fn).read()
            zt.pt_edit(text, 'text/html')

        else:
            return "already exists!"
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    
    def generateAddFileForm(self,RESPONSE=None):
        """lege standard addfileform an"""
        #TODO: write generateaddfileform only a dummy at them moment

        if not self.ZopeFind(self,obj_ids=['addFileForm.html']):
            zt=ZopePageTemplate('addFileForm.html')
            self._setObject('addFileForm.html',zt)
            default_content_fn = os.path.join(package_home(globals()),
                                               'zpt/fileAdd_template.zpt')
            text = open(default_content_fn).read()
            zt.pt_edit(text, 'text/html')

        else:
            return "already exists!"
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def generateIndexHTML(self,RESPONSE=None):
        """lege standard index.html an"""


        if not self.ZopeFind(self,obj_ids=['index.html']):
            zt=ZopePageTemplate('index.html')
            self._setObject('index.html',zt)
            default_content_fn = os.path.join(package_home(globals()),
                                               'zpt/versionFileFolderMain.zpt')
            text = open(default_content_fn).read()
            zt.pt_edit(text, 'text/html')

        else:
            return "already exists!"
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def generateHistoryHTML(self,RESPONSE=None):
        """lege standard index.html an"""

        

        if not self.ZopeFind(self,obj_ids=['history_template.html']):
            zt=ZopePageTemplate('history_template.html')
            self._setObject('history_template.html',zt)
            default_content_fn = os.path.join(package_home(globals()),
                                               'zpt/versionHistory.zpt')
            text = open(default_content_fn).read()
            zt.pt_edit(text, 'text/html')

        else:
            return "already exists!"
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    
       

    def getVersionedFiles(self,sortField='title'):
        """get all versioned files"""

        def sortName(x,y):
            return cmp(x[1].title.lower(),y[1].title.lower())

        def sortDate(x,y):
            return cmp(y[1].getLastVersion().getTime(),x[1].getLastVersion().getTime())

        
        def sortComment(x,y):

        
            
         try:
            xc=getattr(x[1],'comment','ZZZZZZZZZZZZZ').lower()
         except:
            xc='ZZZZZZZZZZZZZ'.lower()
         try:
            yc=getattr(y[1],'comment','ZZZZZZZZZZZZZ').lower()
         except:
            yc='ZZZZZZZZZZZZZ'.lower()


         if (xc=='') or (xc=='ZZZZZZZZZZZZZ'.lower()):
             
             try:
                 xc=x[1].getLastVersion().getVComment().lower()
             except:
                 xc='ZZZZZZZZZZZZZ'.lower()
                 
         if (yc=='') or (yc=='ZZZZZZZZZZZZZ'.lower()):
             try:
                 yc=y[1].getLastVersion().getVComment().lower()
             except:
                 yc='ZZZZZZZZZZZZZ'.lower()

         
         return cmp(xc,yc)

        def sortAuthor(x,y):
            
            return cmp(x[1].getLastVersion().lastEditor().lower(),y[1].getLastVersion().lastEditor().lower())
        
        versionedFiles=self.ZopeFind(self,obj_metatypes=self.filesMetaType)
        
        
        if sortField=='title':
            versionedFiles.sort(sortName)
        elif sortField=='date':
            versionedFiles.sort(sortDate)
        elif sortField=='author':
            versionedFiles.sort(sortAuthor)
        elif sortField=='comment':
            versionedFiles.sort(sortComment)

        return versionedFiles


    def header_html(self):
        """zusaetzlicher header"""
        ext=self.ZopeFind(self,obj_ids=["header.html"])
        if ext:
            return ext[0][1]()
        else:
            return ""


    security.declareProtected('View','index_html')
    def index_html(self):
        """main"""
        ext=self.ZopeFind(self,obj_ids=["index.html"])
        if ext:
            return ext[0][1]()
        
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','versionFileFolderMain')).__of__(self)
        return pt()



    def addFileForm(self):
        """add a file"""
        ext=getattr(self,'addFileForm.html',None)
        logging.error("Found %s"%ext)
        if ext:
            out=ext
        else:
        
            out=DTMLFile('dtml/newFileAdd', globals(),Kind='VersionedFileObject',kind='versionedFileObject',version='1').__of__(self)
        return out()


    def addFile(self,vC,file,author,newName='',content_type='',RESPONSE=None):
        """ add a new file"""
        if newName=='':
            filename=file.filename
            id=filename[max(filename.rfind('/'),
                                    filename.rfind('\\'),
                                    filename.rfind(':'),
                                    )+1:]

        else:
            id=newName
        
        vC=self.REQUEST.form['vC']
        manage_addVersionedFile(self,id,'','')
        #if (getattr(self,'commentNonEmpty',0)==1) and vC.strip()=="":
            
            
        ob=self._getOb(id)
        ob.title=id
        file2=file
        
        obj=ob.manage_addVersionedFileObject(id,vC,author,file2,content_type=content_type)
        self.REQUEST.SESSION['objID']=ob.getId()
        self.REQUEST.SESSION['objID_parent']=None

        if obj.getSize()==0:
            pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','errorUploadFile')).__of__(self)
            return pt()
        
        RESPONSE.redirect(self.REQUEST['URL1'])


    def deleteEmptyObject(self,submit,RESPONSE=None):
        """deleteemptyobject"""
        if submit=="delete it":
            if self.REQUEST.SESSION['objID_parent']:
                obj=getattr(self,self.REQUEST.SESSION['objID_parent'])

            else:
                obj=self
            obj.manage_delObjects([self.REQUEST.SESSION['objID']])

        RESPONSE.redirect(self.REQUEST['URL1'])
        
        
manage_addVersionedFileFolderForm=DTMLFile('dtml/folderAdd', globals())


def manage_addVersionedFileFolder(self, id, title='',
                     createPublic=0,
                     createUserF=0,
                     REQUEST=None):
    """Add a new Folder object with id *id*.

    If the 'createPublic' and 'createUserF' parameters are set to any true
    value, an 'index_html' and a 'UserFolder' objects are created respectively
    in the new folder.
    """
    ob=versionedFileFolder()
    ob.id=str(id)
    ob.title=title
    self._setObject(id, ob)
    ob=self._getOb(id)

    checkPermission=getSecurityManager().checkPermission

    if createUserF:
        if not checkPermission('Add User Folders', ob):
            raise Unauthorized, (
                  'You are not authorized to add User Folders.'
                  )
        ob.manage_addUserFolder()

  
    if REQUEST is not None:
        return self.manage_main(self, REQUEST, update_menu=1)



class versionedFileObject(File):
    """File Object im Folder"""
    security= ClassSecurityInfo()
    meta_type = "versionedFileObject"
    
    manage_editForm  =DTMLFile('dtml/fileEdit',globals(),
                               Kind='File',kind='file')
    manage_editForm._setName('manage_editForm')


    
 
    security.declarePublic('getTitle')

    def getTitle(self):
        """get title"""
        return self.title
 
    security.declarePublic('getVComment')
    def getVComment(self):
        """get the comment of this file"""
        if not hasattr(self,'vComment') or (not self.vComment) or (self.vComment.lstrip()==""):
            return "Add comment"

        else:
            return self.vComment
            
    def manageVCommentForm(self):
        """add a comment"""

        self.REQUEST.SESSION['refer']=self.REQUEST['HTTP_REFERER']


        
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addVComment')).__of__(self)
        return pt()

    def manageVComment(self,text,comment_author,submit,REQUEST=None):
        """manage comments"""
        if submit =='change':
            if text=='':
                self.vComment=None
            else:
                self.vComment=text
                self.vComment_author=comment_author

                self.vComment_date=time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())

        if self.REQUEST.SESSION.has_key('refer'):

            return REQUEST.RESPONSE.redirect(self.REQUEST.SESSION['refer'])
        return REQUEST.RESPONSE.redirect(self.aq_parent.absolute_url()+"/history")
    
    
    security.declarePublic('getVersionComment')
    def getVersionComment(self):
        """getversioncomment"""
        return self.versionComment
    
    security.declarePublic('getTime')

    def getTime(self):
        """getTime"""
        #return self.bobobase_modification_time().ISO()
        if hasattr(self,'time'):
            return time.strftime("%Y-%m-%d %H:%M:%S",self.time)
        elif hasattr(self,'timefixed'):
                return self.timefixed
        else:
            setattr(self,'timefixed',self.bobobase_modification_time().ISO())
            return self.bobobase_modification_time().ISO()



    

    def download(self,REQUEST=None,RESPONSE=None):
        """download and lock"""
        
        self.REQUEST.RESPONSE.setHeader("Content-Disposition","""attachement; filename=%s"""%self.getId())
        self.REQUEST.RESPONSE.setHeader("Content-Type","application/octet-stream")
        #try:
        #       txt=self.index_html()
        #except:
        #       txt=self.index_html(REQUEST,RESPONSE)
        #
        #self.REQUEST.RESPONSE.setHeader("Content-Length","str(len(txt)+1000)")
        
        self.content_type="application/octet-stream"
        self.REQUEST.RESPONSE.redirect(self.absolute_url())
        #txt=urllib.urlopen(self.absolute_url()).read()
        #self.REQUEST.RESPONSE.write(txt)
        

        #self.REQUEST.close()
    
    security.declareProtected('AUTHENTICATED_USER','downloadLocked')    
    def downloadLocked(self):
        """download and lock"""
        
        
        if repr(self.REQUEST['AUTHENTICATED_USER'])=='Anonymous User':
            return "please login first"
        if not self.aq_parent.lockedBy=="":
            return "cannot be locked because is already locked by %s"%self.lockedBy
        self.aq_parent.lockedBy=self.REQUEST['AUTHENTICATED_USER']

        self.content_type="application/octet-stream"
        self.REQUEST.RESPONSE.redirect(self.absolute_url())
    
    def setVersionNumber(self,versionNumber):
        """set version"""
        self.versionNumber=versionNumber


    security.declarePublic('getVersionNumber')                                              

    def getVersionNumber(self):
        """get version"""
        return self.versionNumber


   

    security.declarePublic('lastEditor')                                                

    def lastEditor(self):
        """last Editor"""
        if hasattr(self,'author'):
            try:
                ret=self.author.replace("-","\n")
            except:#old version of versionded file sometimes stored the user object and not only the name the following corrects this
                ret=str(self.author).replace("-","\n")
            ret=ret.replace("\r","\n")
            return ret.lstrip().rstrip()

        else:
            jar=self._p_jar
            oid=self._p_oid

            if jar is None or oid is None: return None

            return jar.db().history(oid)[0]['user_name']

    
    
        
manage_addVersionedFileObjectForm=DTMLFile('dtml/fileAdd', globals(),Kind='VersionedFileObject',kind='versionedFileObject', version='1')

def manage_addVersionedFileObject(self,id,vC='',author='', file='',title='',precondition='', content_type='',
                   REQUEST=None):
    """Add a new File object.

    Creates a new File object 'id' with the contents of 'file'"""

    id=str(id)
    title=str(title)
    content_type=str(content_type)
    precondition=str(precondition)
    
    id, title = cookId(id, title, file)

    self=self.this()

    # First, we create the file without data:
    self._setObject(id, versionedFileObject(id,title,'',content_type, precondition))
    self._getOb(id).versionComment=str(vC)
    self._getOb(id).time=time.localtime()
    
    setattr(self._getOb(id),'author',author)
    
    # Now we "upload" the data.  By doing this in two steps, we
    # can use a database trick to make the upload more efficient.
    if file:
        self._getOb(id).manage_upload(file)
    if content_type:
        self._getOb(id).content_type=content_type

    if REQUEST is not None:
        REQUEST['RESPONSE'].redirect(self.absolute_url()+'/manage_main')




class versionedFile(CatalogAware,Folder):
    """Versioniertes File"""

    default_catalog='fileCatalog'
    
    security= ClassSecurityInfo()   
    
    security.declarePublic('getTitle')
    def getTitle(self):
        """get title"""
        return self.title
    
    def PrincipiaSearchSource(self):
           """Return cataloguable key for ourselves."""
           return str(self)
       
    def __init__(self, id, title, lockedBy,author):
        """init"""
        self.id=id
        self.title=title
        self.lockedBy=lockedBy
        self.author=author

    def manageImagesForm(self):
        """manage Images attached to the file"""

        self.REQUEST.SESSION['refer']=self.REQUEST['HTTP_REFERER']
        
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','manageImage')).__of__(self)
        return pt()

    def manageImages(self,imageUrl=None,caption=None,REQUEST=None):
        """manage URL"""
        if imageUrl and (not imageUrl==""):
            manage_AddImageZogiLib(self,libPath=imageUrl,caption=caption)

        if self.REQUEST.SESSION.has_key('refer'):

            return REQUEST.RESPONSE.redirect(self.REQUEST.SESSION['refer'])
        return REQUEST.RESPONSE.redirect(self.aq_parent.absolute_url())
    
    
    
    def changeImages(self,caption=None,submit=None,id=None,REQUEST=None):
        """manage URL"""
        if submit=="change caption":
            image=self.ZopeFind(self,obj_ids=[id])
            if image:
                image[0][1].caption=caption[0:]
        
        elif submit=="delete":
            image=self.ZopeFind(self,obj_ids=[id])
            if image:
                self.manage_delObjects([image[0][1].getId()])


        if self.REQUEST.SESSION.has_key('refer'):

            return REQUEST.RESPONSE.redirect(self.REQUEST.SESSION['refer'])
        return REQUEST.RESPONSE.redirect(self.aq_parent.absolute_url())
    
        


    def getImages(self):
        """get Images"""
        images=self.ZopeFind(self,obj_metatypes=["ImageZogiLib"])
        if not images:
            return None
        else:
            return images
                             
    security.declarePublic('getComment')
    def getComment(self):
        """get the comment of this file"""
        if not hasattr(self,'comment') or (not self.comment) or (self.comment.lstrip()==""):
            return "Add comment"

        else:
            return self.comment
            
        
    meta_type="versionedFile"

    def manageCommentForm(self):
        """add a comment"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addComment')).__of__(self)
        return pt()

    def manageComment(self,text,comment_author,submit,REQUEST=None):
        """manage comments"""
        if submit =='change':
            if text=='':
                self.comment=None
            else:
                self.comment=text
                self.comment_author=comment_author

                self.comment_date=time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())

        return REQUEST.RESPONSE.redirect(self.aq_parent.absolute_url())
        
    security.declarePublic('getLastChangeDate')
    
    def getLastChangeDate(self):
        """get last change date"""
        lv=self.getLastVersion()
        time=lv.getTime()
        return time
    
    def getLastEditor(self):
        """get last change date"""
        lv=self.getLastVersion()
        le=lv.lastEditor()
        return le
    
    def getLockedBy(self):
        """get locked by"""
        return str(self.lockedBy)
        
    security.declarePublic('getLastVersion')
    def getLastVersion(self):
        """Last Version"""
        tmp=0
        lastVersion=None
        
        
        for version in self.ZopeFind(self):
            
            if hasattr(version[1],'versionNumber'):
                
                if int(version[1].versionNumber) > tmp:
                    tmp=int(version[1].versionNumber,)
                    lastVersion=version[1]
            if lastVersion==None:
                lastVersion=version[1]
                lastVersion.versionNumber=1
        return lastVersion
    
    def getVersionNr(self,nr):
        """Last Version"""
        tmp=0
        lastVersion=None
        
        
        for version in self.ZopeFind(self):
            
            if hasattr(version[1],'versionNumber'):
                
                if int(version[1].versionNumber) ==nr :
                    return version[1]
           
        return None
    def diff(self,data):
        """differenz between lastversion and data"""
        d=Differ()
        data=data.rstrip()
        try:
            tmp=self.getLastVersion().data.rstrip()
        except:
            tmp=str(self.getLastVersion().data).rstrip()
        #print "XX",data,tmp

        try:
                l=list(d.compare(data.splitlines(1),tmp.splitlines(1)))
        except:
                try:
                        l=list(d.compare(repr(data).splitlines(1),tmp.splitlines(1)))
                except:
                        return 9999,[]

        plus=0
        minus=0

        for a in l:
            if a[0]=='+':
                plus+=1
            if a[0]=='-':
                minus+=1
        
        
        return max([plus,minus]),l
    security.declarePublic('index_html')
    def index_html(self):
        """main view"""
        lastVersion=self.getLastVersion()
        #return "File:"+self.title+"  Version:%i"%lastVersion.versionNumber," modified:",lastVersion.bobobase_modification_time()," size:",lastVersion.getSize(),"modified by:",lastVersion.lastEditor()
        return "File: %s Version:%i modified:%s size:%s modified by:%s"%(self.title,lastVersion.versionNumber,lastVersion.getTime(),lastVersion.getSize(),lastVersion.lastEditor())


     
    security.declarePublic('getVersion')                                              
    def getVersion(self):
        tmp=0
        for version in self.ZopeFind(self):
            
            if hasattr(version[1],'versionNumber'):
                
                if int(version[1].versionNumber) > tmp:
                    tmp=int(version[1].versionNumber,)
        return tmp+1


    security.declareProtected('AUTHENTICATED_USER','unlock')

    def history(self):
        """history"""  

        ext=self.ZopeFind(self.aq_parent,obj_ids=["history_template.html"])
        if ext:
            return getattr(self,ext[0][1].getId())()
        
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','versionHistory')).__of__(self)
        return pt()

    def getVersions(self):
        """get all versions"""
        ret=[]
        for version in self.ZopeFind(self):
            if hasattr(version[1],'versionNumber'):
                ret.append((version[1].versionNumber,version[1]))
        ret.sort(sortv)
        return ret

    security.declareProtected('AUTHENTICATED_USER','forceunlock')   
    def forceunlock(self,RESPONSE=None):
        """unlock"""
        #safe who had the lock
        if self.lockedBy:
            self.brokenLock=str(self.lockedBy)
        else:
            self.brokenLock=""
        self.lockedBy=''
        return self.brokenLock

    security.declareProtected('AUTHENTICATED_USER','unlock')   
    def unlock(self,RESPONSE):
        """unlock"""
        if str(self.lockedBy) in [str(self.REQUEST['AUTHENTICATED_USER'])]:
            self.lockedBy=''
            RESPONSE.redirect(self.REQUEST['HTTP_REFERER'])
        else:
            return "Sorry, not locked by you! (%s,%s)"%(self.lockedBy,self.REQUEST['AUTHENTICATED_USER'])
        

    
    security.declareProtected('AUTHENTICATED_USER','addVersionedFileObjectForm')

    def addVersionedFileObjectForm(self):
        """add a new version"""
        
        if str(self.REQUEST['AUTHENTICATED_USER']) in ["Anonymous User"]:
            return "please login first"
        if (self.lockedBy==self.REQUEST['AUTHENTICATED_USER']) or (self.lockedBy==''):
            ext=self.ZopeFind(self.aq_parent,obj_ids=["addNewVersion.dtml"])
            if ext:
                return ext[0][1]('',globals(),version=self.getVersion(),lastComment=self.getLastVersion().getVersionComment(),AUTHENTICATED_USER=self.REQUEST.AUTHENTICATED_USER)
            else:
                out=DTMLFile('dtml/fileAdd', globals(),Kind='VersionedFileObject',kind='versionedFileObject',version=self.getVersion()).__of__(self)
                return out()
        else:
            return "Sorry file is locked by somebody else"
        
    def manage_addVersionedFileObject(self,id,vC,author,file='',title='',precondition='', content_type='',changeName='no',newName='', RESPONSE=None):
        """add"""
        try: #der ganze vC unsinn muss ueberarbeitet werden
            vC=self.REQUEST['vC']
        except:
            pass
        
        author=self.REQUEST['author']
        
        if changeName=="yes":
            filename=file.filename
            self.title=filename[max(filename.rfind('/'),
                        filename.rfind('\\'),
                        filename.rfind(':'),
                        )+1:]


        if not newName=='':
            self.title=newName[0:]
        
        

        
        
        positionVersionNum=getattr(self,'positionVersionNum','front')
        
        if positionVersionNum=='front':
            id="V%i"%self.getVersion()+"_"+self.title
        else:
            tmp=os.path.splitext(self.title)
            if len(tmp)>1:
                id=tmp[0]+"_V%i"%self.getVersion()+tmp[1]
            else:
                id=tmp[0]+"_V%i"%self.getVersion()
            
        
        manage_addVersionedFileObject(self,id,vC,author,file,id,precondition, content_type)
        objs=self.ZopeFind(self,obj_ids=[id])[0][1].setVersionNumber(int(self.getVersion()))
        self.REQUEST.SESSION['objID_parent']=self.getId()

        if getattr(self,'defaultFileCatalog',None):
            
            self.reindex_object()
            
        if RESPONSE:
            obj=self.ZopeFind(self,obj_ids=[id])[0][1]
            if obj.getSize()==0:
                self.REQUEST.SESSION['objID']=obj.getId()
                pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','errorUploadFile')).__of__(self)
                return pt()

            else:
                RESPONSE.redirect(self.REQUEST['URL2'])

        else:
            return self.ZopeFind(self,obj_ids=[id])[0][1]
        
    security.declareProtected('AUTHENTICATED_USER','downloadLocked')

    def download(self):
        """download and lock"""

        self.REQUEST.RESPONSE.setHeader("Content-Disposition","""attachement; filename=%s"""%self.getLastVersion().getId())
        self.REQUEST.RESPONSE.setHeader("Content-Type","application/octet-stream")
      
        #try:   
        #       txt=self.getLastVersion.index_html()
        #except:
        #       txt=self.getLastVersion.index_html(REQUEST,RESPONSE)

        #self.REQUEST.RESPONSE.setHeader("Content-Length","str(len(txt)+1000)")
        
        self.content_type="application/octet-stream"
        #self.REQUEST.RESPONSE.write("bl")
        #self.REQUEST.RESPONSE.write(txt)
        #self.REQUEST.close()

        #self.getLastVersion().content_type="application/octet-stream"
        self.REQUEST.RESPONSE.redirect(self.REQUEST['URL1']+'/'+self.getId()+'/'+self.getLastVersion().getId())
    
    security.declareProtected('AUTHENTICATED_USER','downloadLocked')    
    def downloadLocked(self):
        """download and lock"""

        if repr(self.REQUEST['AUTHENTICATED_USER'])=='Anonymous User':
            return "please login first"
        if not self.lockedBy=="":
            return "cannot be locked because is already locked by %s"%self.lockedBy
        self.lockedBy=self.REQUEST['AUTHENTICATED_USER']
        self.getLastVersion().content_type="application/octet-stream"
        self.REQUEST.RESPONSE.redirect(self.REQUEST['URL1']+'/'+self.getId()+'/'+self.getLastVersion().getId())
    
def manage_addVersionedFileForm(self):
    """interface for adding the OSAS_root"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addVersionedFile.zpt')).__of__(self)
    return pt()

def manage_addVersionedFile(self,id,title,lockedBy, author=None, RESPONSE=None):
    """add the OSAS_root"""
    newObj=versionedFile(id,title,lockedBy,author)
    self._setObject(id,newObj)
   
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')


InitializeClass(versionedFile)
InitializeClass(versionedFileFolder)
