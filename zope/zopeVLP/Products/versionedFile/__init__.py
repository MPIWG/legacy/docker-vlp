import versionedFile
import extVersionedFile

def initialize(context):
    """initialize versionedFile"""

    context.registerClass(
        versionedFile.versionedFileFolder,
        constructors = (
          versionedFile.manage_addVersionedFileFolderForm,
          versionedFile.manage_addVersionedFileFolder
          )
        )

    context.registerClass(
        versionedFile.versionedFile,
        constructors = (
          versionedFile.manage_addVersionedFileForm,
          versionedFile.manage_addVersionedFile
          )
        )
    
    context.registerClass(
        versionedFile.versionedFileObject,
        constructors = (
          versionedFile.manage_addVersionedFileObjectForm,
          versionedFile.manage_addVersionedFileObject
          )
        )
    
    context.registerClass(
        extVersionedFile.extVersionedFileFolder,
        constructors = (
          extVersionedFile.manage_addextVersionedFileFolderForm,
          extVersionedFile.manage_addextVersionedFileFolder
          )
        )

    context.registerClass(
        extVersionedFile.extVersionedFile,
        constructors = (
          extVersionedFile.manage_addextVersionedFileForm,
          extVersionedFile.manage_addextVersionedFile
          )
        )
    
    context.registerClass(
        extVersionedFile.extVersionedFileObject,
        constructors = (
          extVersionedFile.manage_addextVersionedFileObjectForm,
          extVersionedFile.manage_addextVersionedFileObject
          )
        )
    

    
