"""Extension of the Userfolder for MPIWG purposes"""
from AccessControl.User import User, UserFolder
import types

class MPIWGUser( User ):
    """MPIWG USER """


    def __init__(self, name, password, roles, domains, email):
        " init MPIWGUser "

        self.name = name
        self.__ = password
        self.roles = roles
        self.domains = domains
        self.email = email

    def getEmail(self):
        " email attribut eines user objektes "
        return self.email

    def getPassword(self):
        " __ attribut eines user objektes  "
        return self.__


class MPIWGUserFolder( UserFolder ):
    " MPIWG User Folder"""

    meta_type = 'MPIWGUserFolder'
    def manage_user(self):
        """manage user"""
        
        
    def addUser(self, user, password, group="", domains="", email="",
RESPONSE=None):
        " validate addUserForm "
        user = str(user)
        if type(group)==types.StringType:       
            group=[group]                      
        if type(domains)==types.StringType:    # wie bei group
            domains=[domains]

        #if user and password and password==password2 and (user not in self.aq_parent.acl_users.getUserNames()):
        if user and password:
            self.aq_parent.acl_users._doAddUser(user, password, group,
domains, email)
        else:
            #TODO: make sure that user doesn't exist also in LDAP
            return "ERROR: user already exist"
        if RESPONSE is not None:
            RESPONSE.redirect('index_html')
    """
    def editUser(self, user, olduser, password, group, domains,
    realname, comments, RESPONSE=None):
        " edit user "
        <aehnlich wie addUser>
    """

    # siehe auch:  User.py -->  _doAddUser()  /  _doEditUser

    def getUser(self,name):
        """get User"""
        return self.data.get(name,None)
    
    def _doAddUser(self, user, password, roles, domains, email=""):
        " add User "
        userObject = MPIWGUser(user, password, roles, domains, email)
        self.data[user] = userObject

    def _doEditUser(self, user, password, roles, domains, email=""):
        " edit User "
        userObject = self.data[user]
        userObject.__ = password
        userObject.roles = roles
        userObject.domains = domains
        userObject.email = email


def manage_addMPIWGUserFolderForm(self, RESPONSE=None):
    " userfolder  add form "
    #no user entries necessessary
    RESPONSE.redirect('manage_addMPIWGUserFolder')

def manage_addMPIWGUserFolder(self, RESPONSE=None):
    " add UF "
    ufolder = MPIWGUserFolder()
    self._setObject('acl_users', ufolder)

    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')