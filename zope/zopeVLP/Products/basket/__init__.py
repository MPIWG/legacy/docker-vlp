import basket
import groups
import MPIWGUserFolder

def initialize(context):
    """initialize Basket"""
    context.registerClass(
        basket.Basket,
        constructors = (
          basket.manage_addBasketForm,
          basket.manage_addBasket
          )
        )

    context.registerClass(
        basket.BasketFolder,
        constructors = (
          basket.manage_addBasketFolderForm,
          basket.manage_addBasketFolder
          )
        )
    context.registerClass(
        basket.BasketXRef,
        constructors = (
          basket.manage_addBasketXRefForm,
          basket.manage_addBasketXRef
          )
        )

    context.registerClass(
        basket.BasketText,
        constructors = (
          basket.manage_addBasketTextForm,
          basket.manage_addBasketText
          )
        )
        
    context.registerClass(
        basket.BasketInternalLink,
        constructors = (
          basket.manage_addBasketInternalLinkForm,
          basket.manage_addBasketInternalLink
          )
        )
    
    context.registerClass(
        groups.GroupFolder,
        constructors = (
          groups.manage_addGroupFolderForm,
          groups.manage_addGroupFolder
          )
        )

    context.registerClass(
        groups.Group,
        constructors = (
          groups.manage_addGroupForm,
          groups.manage_addGroup
          )
        )
       
    context.registerClass(
        MPIWGUserFolder.MPIWGUserFolder,
        constructors = (
          MPIWGUserFolder.manage_addMPIWGUserFolderForm,
          MPIWGUserFolder.manage_addMPIWGUserFolder
          )
        )
       
       