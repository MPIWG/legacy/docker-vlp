"""gruppenverwaltung fuer baskets, benutzer koennen verschiedenen gruppen zugeordnet werden"""

from OFS.Folder import Folder
from OFS.SimpleItem import SimpleItem
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Globals import package_home
import os.path
from types import *

from AccessControl import SecurityManagement

import smtplib

def sendForm(self,fromaddr,subject,content,nextPage="index_html",RESPONSE=None):
                """sendform"""
                fromaddr=fromaddr.strip("\r\n\t") # sicherstellen dass keine zusaetzlichen headerzeilen eingefuegt werden
                subject=subject.strip("\r\n\t") # sicherstellen dass keine zusaetzlichen headerzeilen eingefuegt werden
                toaddrs=self.toAddrs

                subject="%s %s"%(self.subjectAdd,subject)
                
                msg = ("From: %s\r\nSubject: %s\r\nTo: %s\r\n\r\n"
                       % (fromaddr, subject, ", ".join(toaddrs)))
                server = smtplib.SMTP("mail.mpiwg-berlin.mpg.de")
                #server.set_debuglevel(1)
                msg=msg+content
                server.sendmail(fromaddr, toaddrs, msg)
                server.quit()

                if RESPONSE:
                        RESPONSE.redirect(nextPage)
class GroupFolder(Folder):
    """Folder which contains groups"""
    
    meta_type="GroupFolder"

    def registerNewUser(self,user,password,email='',forward_to=None,RESPONSE=None):
        """register a new user to the system, username and password, will be stored in the acl_users folder,
        additional information are added here."""
        
        #TODO: check if user already exists in LDAP
        #TODO: email is not registered yet, because MPIWGUserfolder is not working with CookieCrumbler
        #print [x for x in repr(self.acl_users.getUserNames())]
        
        if not (len(user.split("@")) ==2):
            pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddUserErrorInvalidEmail.zpt')).__of__(self)
            return pt(user=user)
            
            
        if user in repr(self.acl_users.getUserNames()):
            
            pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddUserErrorAlreadyExists.zpt')).__of__(self)
            return pt(user=user)
        
        if user and password:
            #self.acl_users.addUser(user,password,email)
            self.aq_parent.acl_users._doAddUser(user, password, '', '')
            
        add="user=%s"%user
        if forward_to.find("?")>-1:
            #already parameters
            add="&"+add
        else:
            add="?"+add
        
        if RESPONSE and forward_to:
            RESPONSE.redirect(forward_to+add)
            
    def isMemberOf(self,group):
        user=SecurityManagement.getSecurityManager().getUser()
        
        if user and not (str(user)=='Anonymous User'):
            return self.checkMembership(str(user), group)
        return False
    
    def addKommaListOfUsersToGroup(self,users,group,remove=None,REQUEST=None,RESPONSE=None):
        """add users to a group, users in komma separeted list"""
        usersList=[x.lstrip().rstrip() for x in users.split(",")]
        
        self.addUsersToGroups(usersList,group,remove)

        if RESPONSE:
            RESPONSE.redirect(REQUEST['HTTP_REFERER'])
    
    def addUsersToGroups(self,users,groups,remove=None):
        """add users to groups
        @param users: list of usernames
        @param groups: list of groupnames
        @param remove: default None, if remove is set the actual
         user list of the groups will be replaced by users otherwise users will be added. 
         if remove= protected then the actualuser can not be deleted.
        """
      
        if type(groups) is not ListType:
            groups=[groups]
        
        for user in users:
            self.addUserToGroups(user,groups)
        
        if remove:
            for group in groups:
                for user in self.getGroupsAndUsers().get(group,None):
                    if (user not in users):
                        if not ((remove=="protected") and (user==self.getActualUserName())):
                            self.removeUserFromGroups(user,group)
        
    def removeUserFromGroups(self,user,groups):
        '''
        remove a user from a group
        @param user: username
        @param groups: list of groupnames
        '''
          
        #make sure that list of groups
        print "remove",user,repr(groups)
        
        if type(groups) is not ListType:
            groups=[groups]
            
        groupObjects=self.ZopeFind(self,obj_ids=groups)
        #group doesn't exist, return with false
        if len(groupObjects)<1:
            return False
        
        #otherwise add user to group
        for groupObject in groupObjects:
            groupObject[1].removeUser(user)
            
    def addUserToGroups(self,user,groups):
        '''
        Add a user to a group
        @param user: username
        @param groups: list of groupnames
        '''
        
        #make sure that list of groups
        
        if type(groups) is not ListType:
            groups=[groups]
            
        groupObjects=self.ZopeFind(self,obj_ids=groups)
        #group doesn't exist, return with false
        if len(groupObjects)<1:
            return False
        
        #otherwise add user to group
        for groupObject in groupObjects:
            groupObject[1].addUser(user)
        
        return True
    
    def addGroupsToGroups(self,newGroups,groups):
        """
        add Groups to groups
        @param unewGroups: list of groups to be added
        @param groups: list of groupnames
        """
        
        for group in newGroups:
            self.addGroupToGroups(group,groups)
        
        
    def addGroupToGroups(self,group,groups):
        '''
        Add a user to a group
        @param user: username
        @param groups: list of groupnames
        '''
        
        #make sure that list of groups
        
        if type(groups) is not ListType:
            groups=[groups]
            
        groupObjects=self.ZopeFind(self,obj_ids=groups)
        #group doesn't exist, return with false
        if len(groupObjects)<1:
            return False
        
        #otherwise add user to group
        for groupObject in groupObjects:
            groupObject[1].addGroup(group)
        
        return True
    
    def checkMembership(self,user,group):
        '''Check if user is member of group
        
        @param user:
        @param group:
        '''
        print "checking publish for user:",user,group,self.aq_parent.getId()
        ret=False
        
        groups=self.ZopeFind(self,obj_metatypes=['Group'],obj_ids=[group])
        #group doesn't exist
        
        if group=="publish":
            #special group, member is allowed to publish basket, at the moment all members of the admin group at root level
            #member of publish
            
            return  self.users.groupFolder.checkMembership(user,'admin')
            
        if len(groups)<1:
            ret=False
        else:
            ret=groups[0][1].isInGroup(user)
        
        if (not ret) and (self.aq_parent.getId()!='users'):
            #teste ob mitglied in super group
            return self.users.groupFolder.checkMembership(user,'admin')
        else:
            return ret
            
    def returnUsers(self):
        """return all users of groups"""
        
        users={}
        for group in self.ZopeFind(self,obj_metatypes=['Group']):
            for user in group[1].getUsers():
                if users.has_key(user):
                    users[user].append(group[0])
                else:
                    users[user]=[group[0]]
        
        return users
    
    def getGroupsAndUsers(self):
        """return all groups and their users as dict"""
        
        users={}
        for group in self.ZopeFind(self,obj_metatypes=['Group']):
            for user in group[1].getUsers():
                if users.has_key(group[0]):
                    if not (user in users[group[0]]):
                        users[group[0]].append(user)
                else:
                    users[group[0]]=[user]
        return users
                    
def manage_addGroupFolderForm(self):
    """form for adding a groupFolder"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addGroupFolder.zpt')).__of__(self)
    return pt()

def manage_addGroupFolder(self,id,RESPONSE=None):
    """add a group folder object"""

  
    newObj=GroupFolder(id)
    
    self._setObject(id,newObj)
    ob=self._getOb(id)
    
    
    #group folder always contains admin,editor,user
     
    manage_addGroup(ob,'admin') 
    manage_addGroup(ob,'editor') 
    manage_addGroup(ob,'user') 
    
    #actual user is always editor and admin
    user=str(self.REQUEST['AUTHENTICATED_USER'])
    
    ob.addUsersToGroups([user],['admin','editor'])
    
    #admin of supergrous are always admins of subgroups
    
    ob.addGroupsToGroups(['admin'],['admin'])
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
     

class Group(SimpleItem):
    """Group"""

    meta_type="Group"
    
    
    manage_options=SimpleItem.manage_options+(
        {'label':'change group members','action':'changeUsersInGroupForm'},
        )
    
    def __init__(self,id,description):
        """init group"""
        self.id=id
        self.description=description
        self.userList=[]
        self.groupList=[]
        
    def addUser(self,username):
        '''
        add an username to the group        
        @param username:username
        '''
        #make sure that entry is persistent
        list=self.userList
        list.append(username)
        self.userList=list[0:]
        
    def removeUser(self,username):
        """
        remove an user from group
        """
        list=self.userList
        list.remove(username)
        self.userList=list[0:]
        
        
    def addGroup(self,group):
        '''
        add a group to the group
        @param group: group to be added
        '''
        #make sure that entry is persistent
        list=self.groupList
        list.append(group)
        self.groupList=list[0:]
        
        
    def getGroups(self):
        """get groups, whose members are automatically members of this group"""
        return self.groupList
    
    def getUsers(self):
        """ get Userlist """
        return self.userList
    
    def changeUsersInGroupForm(self):
        """html form for changing the users"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','changeUsersInGroup.zpt')).__of__(self)
        return pt()
    
    def changeUsersInGroup(self,userList,RESPONSE=None):
        """change group"""
        self.userList=userList.split(",")[0:]
    
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
            
    def isInGroup(self,username):
        '''
        check if username is in group
        @param username: username
        '''
        
        #special handling of group 'user' all users are in user
        
        #TODO: only user which can log in are in 'user'
        if self.id=="user":
            return True
        
        #check first if user is in userList of this group
        
        if username in self.userList:
            return True
        
        #check now if user is member of one of the groups in grouplist
        
        groupFolders=self.ZopeFind(self.aq_parent,obj_metatypes=['GroupFolder'])
        #no groupFolder im parent
        
        if len(groupFolders)<1:
            return False
        
        #checke all groups in grouplist
        for group in self.ZopeFind(groupFolders[0][1],obj_metatype=['Group'],obj_ids=[self.groupList]):
            if group[1].isInGroup(username):
                return True
        
        return False
        

        
def manage_addGroupForm(self):
    """form for adding a groupFolder"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addGroup.zpt')).__of__(self)
    return pt()

def manage_addGroup(self,id,description='',RESPONSE=None):
    """add a group folder object"""

    
    newObj=Group(id,description)
    
    self._setObject(id,newObj)
    ob=self._getOb(id)
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
  