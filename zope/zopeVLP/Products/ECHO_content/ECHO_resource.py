import urlparse
import string
import tempfile
import zipfile
import re
import os,shutil
import OFS.Image
from types import *
from OFS.Cache import Cacheable
from OFS.Image import Image
from Globals import DTMLFile
from OFS.Folder import Folder
from OFS.SimpleItem import SimpleItem
from AccessControl import ClassSecurityInfo
from AccessControl.User import UserFolder
from Globals import InitializeClass
from Globals import DTMLFile
import Globals
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate,manage_addPageTemplate
from Globals import Persistent, package_home
from Acquisition import Implicit
from Products.ZCatalog.CatalogPathAwareness import CatalogAware
from Products.ZCTextIndex.ZCTextIndex import manage_addLexicon
try:
	from Products.MetaDataProvider.MetaDataClient import MetaDataClient
except:
	print "no metadata services"
import urllib
import urllib2
import cgi
import smtplib
import time
# from Ft.Xml.Domlette import NonvalidatingReader
# from Ft.Xml.Domlette import PrettyPrint, Print
# from Ft.Xml import EMPTY_NAMESPACE
# import Ft.Xml.XPath

import cStringIO

import sys

try:
        from psycopg import libpq
except:
        try:
                from pyPgSQL import libpq
        except:
                print "ECHO_collection: Warning - No libpq imported!"
                
import xml.dom.minidom

import urllib
import xml.dom.minidom
import ECHO_helpers
from ECHO_helpers import *


from ECHO_movie import *
import vlp_xmlhelpers #TODO: vlp elemente aus echo herausnehmen
import xmlrpclib

import logging

class ECHO_resource(CatalogAware,Folder,Persistent,ECHO_basis):
    """ECHO Ressource"""
    security=ClassSecurityInfo()
    meta_type='ECHO_resource'
    default_catalog='resourceCatalog'
    
#    viewClassificationList=viewClassificationListMaster

    def PrincipiaSearchSource(self):
           """Return cataloguable key for ourselves."""
           return str(self)

    getSubCols = ECHO_helpers.getSubCols

    def index_meta(self,RESPONSE=None):
        """ gibt das im metalink gespeicher xml-file zurueck"""
        url = self.metalink
        txt=""
        logging.debug("ml:%s"%url)
        try:
            page = urllib.urlopen(url);
            if RESPONSE:
                RESPONSE.setHeader("Content-Type","text/xml")
            
            txt = page.read();
        except:
            logging.error("Cannot read metadata of: %s"%self.getId())
        
        return txt
        
    def reindex(self):
        """generate fields for indexing and reindex"""
        
        #TODO: korrigieren des metalink pfades konfigurierbar machen
        splitted= [x for x in urlparse.urlparse(self.metalink)]
        splitted[1]="foxridge.mpiwg-berlin.mpg.de"[0:]
        
        if splitted[0]=="http":
            self.metalink=urlparse.urlunparse(splitted)
        

        self.fullTextUrl=self.getFullTextXML(noredirect="yes")
        
        #TODO: korrigieren relative url to absoluter url volltexten, konfigurierbar?
        splitted= [x for x in urlparse.urlparse(self.fullTextUrl)]
        if splitted[0]=="":
            splitted[0]="http"
            splitted[1]="foxridge.mpiwg-berlin.mpg.de"[0:]
            
            self.fullTextUrl=urlparse.urlunparse(splitted)
            
        self.imagePath=self.getImagePath()
        
        self.reindex_object()
  
  
    security.declareProtected('View','createPDF')
    def createPDF(self,RESPONSE=None,local=None,dpi=150):
            """erzeuge pdf file"""
            pages=1
            dpi=float(dpi)
            imagePath=self.getImagePath().replace("/mpiwg/online","")
            
            
            image="http://nausikaa2.mpiwg-berlin.mpg.de/digitallibrary/servlet/Scaler?fn="+imagePath+"&dw=%i&dh=%i&pn=%i"
            xmlInfo="http://nausikaa2.mpiwg-berlin.mpg.de/digitallibrary/dlInfo-xml.jsp?fn="+imagePath

            dom=xml.dom.minidom.parse(urllib.urlopen(xmlInfo))
            for parameter in dom.getElementsByTagName('parameter'):
                    if parameter.getAttribute('name')=="pt":
                            pages=int(parameter.getAttribute('value'))
                            break
            

            tempdir="/tmp/archivesImageServer"
            if not os.path.exists(tempdir):
                    os.mkdir(tempdir) 
  
            tmpPath=tempfile.mkdtemp(dir=tempdir)
            

            tmpZip=tempfile.mktemp(dir=tempdir)

            tmpFn=os.path.split(tmpZip)[1]


    

            if RESPONSE:
                    RESPONSE.setHeader("Content-Type","text/html")
                    RESPONSE.write("<h1>I am creating  the pdf</h1>")
                    txt="<h3>1. step: getting the images( %i pages)</h3>"%pages
                    RESPONSE.write(txt)

            c=canvas.Canvas(tmpZip)
            for i in range(1,pages+1):
                    if RESPONSE:
                            RESPONSE.write(str("<p>Get Page: %i<br>\n"%i))
                    faktor=dpi/72.0
                    
                    fn=tmpPath+"/%i"%i

                    width,height=A4
                    #print image%(width*faktor,height*faktor,i)
                    url=urllib.urlopen(image%(width*faktor,height*faktor,i)).read()
                    fh=file(fn,"w")
                    fh.write(url)
                    fh.close()

    

                    c.drawImage(fn,0,0,width=width,height=height)
                    c.showPage()
            c.save()
            if RESPONSE:
                    RESPONSE.write("<p>finished<br>\n")

            if RESPONSE:
                    len=os.stat(tmpZip)[6]
                    downloadUrl=self.absolute_url()+"/downloadPDF"
                    RESPONSE.write("""<h1><a href="downloadPDF?fn=%s">Click here for download ( %i Byte)</a></h1>\n"""%(tmpFn,len))
                    RESPONSE.write("""<p>The file will be stored for a while, you can download it later, the URL is:</p>
                    <p><a href="downloadPDF?fn=%s">%s?fn=%s</a></h1>\n"""%(tmpFn,downloadUrl,tmpFn))
                    RESPONSE.close()


    def downloadPDF(self,fn):
            """download prepared set"""
            filename="/tmp/archivesImageServer/"+fn
            namePDF=self.getId()+".pdf"
            self.REQUEST.RESPONSE.setHeader("Content-Disposition","""attachement; filename="%s" """%namePDF)
            self.REQUEST.RESPONSE.setHeader("Content-Type","application/octet-stream")
            len=os.stat(filename)[6]
            self.REQUEST.RESPONSE.setHeader("Content-Length",len)
            images=file(filename).read()
            self.REQUEST.RESPONSE.write(images)
            self.REQUEST.RESPONSE.close()


    def getRDF(self,urn=None):
            """rdf"""
            ap = self.getArchivePathFromMetadata()
         
       
            ret=getRDFDescription(self,self.link,urn=urn,ap=ap)
           
            return ret+self.createSubElementRDF(urn=urn) 

  
    def changeAccessRightForm(self,preselect=None):
        """change the access rights"""
        
        pt=zptFile(self, 'zpt/ChangeECHO_resourceAccessRight.zpt')
        return pt(preselect=preselect)
    
    def changeAccessRight(self):
        """changeRights"""
        argv=self.REQUEST.form
        self.setAccessRightXML(argv["%s_xml"%self.getId()])
        self.accessRight=argv["%s_echo"%self.getId()]
         
        self.REQUEST.RESPONSE.redirect('manage_main')
        
    
    def getAccessRightSelectorHTML(self,outlook="select",xmldominant=None,preselect=None):
            """htmlselector, xmldominant= in der regel wird der wert des xmlfiles ausgegeben ausser er existiert nicht"""
            values=['','free','MPIWG']
            
            if preselect:#set all to the preselected
                ar=(preselect,preselect)
            else:#else set to actual value
                ar=self.getAccessRightMD()
                
            if outlook=="select":
                    ret="""<select name="%s_xml">"""%self.getId()
                    ret1="""<select name="%s_echo">"""%self.getId()
                    for value in values:
                            if value==ar[0]:
                                    ret+="<option selected>%s</option>"%value
                            else:
                                    ret+="<option>%s</option>"%value
                            
                            if value==ar[1]:
                                    ret1+="<option selected>%s</option>"%value
                            else:
                                    ret1+="<option>%s</option>"%value
                    
                    
                    if not xmldominant:
                        return ret+"</select>",ret1+"</select>"
                    else:
                        if ar[0] is not None:
                            return ret+"</select>"
                        else:
                            return "<p>No xml file (only local selection):"+ret1

            else:
                    ret=""
                    ret1=""
                    for value in values:
                            
                            if value==ar[0]:
                                    ret+="""<input type="radio" name="%s_xml" value="%s" checked>%s"""%(self.getId(),value,value)
                            else:
                                    ret+="""<input type="radio" name="%s_xml" value="%s">%s"""%(self.getId(),value,value)
                                    
                            if value==ar[1]:
                                    ret1+="""<input type="radio" name="%s_echo" value="%s" checked>%s"""%(self.getId(),value,value)
                            else:
                                    ret1+="""<input type="radio" name="%s_echo" value="%s">%s"""%(self.getId(),value,value)

                    if not xmldominant:                        
                        return ret,ret1
                    else:                         
                        if ar[0]:
                            return ret
                        else:
                            return "<p>No xml file (only local selection)</p>"+ret1



    def copyAccessRightsFromMD(self):
            """kopiere rechte aus den metadaten"""
            self.accessRight=self.getAccessRightMD()[0]
            
            
            
            
    
    def getAccessRightMD(self):
                """set accessright"""
                url=self.metalink

                try:
                    urllib.urlopen(url)
                except:
                    logger("ECHO_Resource (getAccessRightMD)", logging.INFO,"%s (%s)"%sys.exc_info()[0:2])

                    return None,getattr(self,'accessRight','')
                
            
                try:
                        dom = NonvalidatingReader.parseUri(url)
                except:
                        logger("ECHO_Resource (getAccessRightMD)", logging.INFO,"%s (%s)"%sys.exc_info()[0:2])
                        return (None,"Cannot parse: "+url+"<br>"+"%s (%s)"%sys.exc_info()[0:2])


                accessright=dom.xpath("//meta/access-conditions/access/@type")
                if accessright:
                    accessright=accessright[0].value
                if str(accessright)=='institution':
                    tmp=dom.xpath("//meta/access-conditions/access/name")
                    if tmp:
                        accessright=getTextFromNode(tmp[0])
                    
                if not accessright:
                    accessright=""
                
                return accessright,getattr(self,'accessRight','')             

    def changeAccessRightMD(self,accessright,RESPONSE=None):
            """change the rights - not user anymore"""
            #TODO: check if method still needed
            params="accessright=%s"%accessright


            #print ECHO_helpers.urlopen(self.absolute_url()+'/setAccessRightXML'+'?'+params).read()

            tries=0
            for i in range(10):
                    x=ECHO_helpers.urlopen('http://echo.mpiwg-berlin.mpg.de/echo_nav/storage/downloadExternalXML?index_meta_url=%s&xml_url=%s'%(self.metalink,self.absolute_url()+'/setAccessRightXML'+urllib.quote('?'+params))).read()
                    if x=="ok":
                           
                            break;
                 
            
          
            if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

                
    def setAccessRightXML(self,accessright):
                """set accessright"""
                url=self.metalink
                accessrights=['MPIWG','free']#allowes rights
                
                if accessright =='':
                    """do nothing"""
                    return ""
                    
                    
                try:
                        geturl=""
                        for line in ECHO_helpers.urlopen(url).readlines():
                                geturl=geturl+line 


                except:
                        return (None,"Cannot open: "+url)

                try:
                        dom=xml.dom.minidom.parseString(geturl)
                        root=dom.getElementsByTagName('resource')[0]
                except:
                        return (None,"Cannot parse: "+url+"<br>"+geturl)

                metamains=dom.getElementsByTagName('meta')
                
                if not metamains:
                        nodenew=dom.createElement('meta')
                        root.appendChild(nodenew)
                        metamain=nodenew
                else:
                        metamain=metamains[0]
                
                
                metanodes=metamain.getElementsByTagName('access-conditions')

                if not metanodes:
                        nodenew=dom.createElement('access-conditions')
                        metamain.appendChild(nodenew)
                        metanode=nodenew
                else:
                        metanode=metanodes[0]

                accesses=metanode.getElementsByTagName('access')

                #delete old
                if accesses:
                     metanode.removeChild(accesses[0]).unlink()

                #create new
                    
                nodenew2=dom.createElement('access')
                metanode.appendChild(nodenew2)
                metanode2=nodenew2
                
                attribute=metanode2.getAttribute('type')
                
                if accessright=="free":
                    
                     metanode2.setAttribute('type','free')
                
                elif accessright.upper()=='MPIWG':
                        metanode2.setAttribute('type','institution')
                        nodenew4=dom.createElement('name')
                        metanodetext=dom.createTextNode('MPIWG')
                        nodenew4.appendChild(metanodetext)
                        nodenew2.appendChild(nodenew4)
                #print dom.toxml().encode('utf-8')
                string= encodeRPC(dom.toxml().encode('utf-8'))
                
                #TODO: make server configurable
                server=xmlrpclib.Server("http://foxridge.mpiwg-berlin.mpg.de/server")

                path=urlparse.urlparse(self.metalink)[2]
         
                server.writeMetaDataFile(path,string,"yes")
        
    def setStartPageForm(self):
            """Form for changing the startpage"""

            
            pt=zptFile(self, 'zpt/ChangeECHO_resourceStartPage.zpt')
            pt.content_type="text/html"
            return pt()
    

    def createImageUrl(self,pn=1):
            """create ImageUrl"""

            
            resourcepath=readFieldFromXML_xpath(self.metalink,'//resource/archive-path')
            
            digiliburlprefix=readFieldFromXML(self.metalink,'texttool','digiliburlprefix')
            images=readFieldFromXML(self.metalink,'texttool','image')


            if (not resourcepath) or (not digiliburlprefix) or (not images):
                    logger("ECHO (createImageUrl)",logging.ERROR,"Cannot create ImageUrl for %s"%self.absolute_url())
                    return None
            resourcepath=resourcepath.replace('/mpiwg/online','')
            if not digiliburlprefix: digiliburlprefix="http://echo.mpiwg-berlin.mpg.de/zogilib?"

            if (not images) or (not resourcepath): return None

            return "%sfn=%s&pn=%i"%(digiliburlprefix,resourcepath+"/"+images,pn)
    
    def copyTitleToInfoXML(self,RESPONSE=None):
            """copy title from the resource"""
            presentationXML=readFieldFromXML(self.metalink,'texttool','presentation')
            resourcepath=readFieldFromXML_xpath(self.metalink,'//resource/archive-path')
            if (not presentationXML) or (not resourcepath): 
                if RESPONSE:
                        RESPONSE.write("Error: %s\n"%self.getId())
                else:
                        return None,self.absolute_url()

            try:
                    fh=file(os.path.join(resourcepath,presentationXML),'w')
                    fh.write("""<info>
                    <author></author>
                    <title>%s</title>
                    <date></date>
                    <display>yes</display>
                    </info>"""%self.title)
                    fh.close()
                    return 1,self.getId()
            except:
                    if RESPONSE:
                            RESPONSE.write("Error: %s\n"%self.getId())
                    else:
                            return None,self.absolute_url()

            
    def setStartPage(self,startpage=None,RESPONSE=None):
            """set start page, if no startpage defined use the generic one of the resource"""

            if (not (type(startpage)==StringType)):
                    if ("__generic" in startpage): # checke ob generic in der liste
                            startpage=self.absolute_url()+"/startpage_html"
                    elif ("__firstPage" in startpage): # checke ob generic in der liste
                            startpage=self.createImageUrl()
                            
            if (not startpage):
                    startpage=self.absolute_url()+"/startpage_html"
            elif (startpage=="__generic"):
                    startpage=self.absolute_url()+"/startpage_html"
            elif (startpage=="__firstPage"):
                        startpage=self.createImageUrl()
    
            params="startpage=%s"%startpage
            #print 'http://xserve02.mpiwg-berlin.mpg.de:18880/echo_nav/storage/downloadExternalXML?index_meta_url=%s&xml_url=%s'%(self.metalink,self.absolute_url()+'/newMetaXML'+urllib.quote('?'+params))
               
            tries=0
            for i in range(10):
                    x=ECHO_helpers.urlopen('http://xserve02.mpiwg-berlin.mpg.de:18880/echo_nav/storage/downloadExternalXML?index_meta_url=%s&xml_url=%s'%(self.metalink,self.absolute_url()+'/newMetaXML'+urllib.quote('?'+params))).read()
                    if x=="ok":
                           
                            break;
                 
            
            path=self.metalink
            
            path=re.sub(self.REQUEST['SERVER_URL'],'',path)
            path=re.sub('http://'+self.REQUEST['HTTP_HOST'],'',path)
            
            path=re.sub('http://foxridge.mpiwg-berlin.mpg.de:8080','',path) # falls foxridge als server
            path=re.sub('http://foxridge.mpiwg-berlin.mpg.de','',path) # falls foxridge als server
            path=re.sub('http://foxridge.rz-berlin.mpg.de:8080','',path) # falls foxridge als server
            path=re.sub('http://content.mpiwg-berlin.mpg.de','',path) # falls content als server
            path=re.sub('http://foxridge.rz-berlin.mpg.de','',path) # falls foxridge als server
            path=re.sub('http://vision.mpiwg-berlin.mpg.de','',path) # falls vision als server
            path=re.sub('http://xserve02.mpiwg-berlin.mpg.de:18880','',path) # falls vision als server
            path=re.sub('http://echo.mpiwg-berlin.mpg.de','',path) # falls echo
            path=re.sub('/index.meta','',path) 


            ECHO_helpers.urlopen("http://nausikaa2.rz-berlin.mpg.de:86/cgi-bin/toc/admin/reg.cgi?path=%s"%path).readlines()

            if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

    def changeViewerTemplateSetForm(self):
            """change the viewer template set"""
            pt=zptFile(self, 'zpt/changeResourceViewerTemplateSet')
            return pt()


    def setLink(self, link=None):
        """change link field"""
        if link is not None:
            self.link = link
            
   
    def getTextToolsField(self,name,default=''):
        """Lese Textoolsfelder aus index.meta im path aus"""
        
        try:
            dom=xml.dom.minidom.parse(self.metalink)
            node=dom.getElementsByTagName('texttool')[0] #getNode
            subnode=node.getElementsByTagName(name)[0]

            # bei text wird nur der Folder gebraucht
            if name=="text":
                splitted=getText(subnode.childNodes).split("/")
                return splitted[len(splitted)-2]
            else:
                return getText(subnode.childNodes)
        except:
            return default
   

    def changeViewerTemplateSet(self,project,xslt,thumbtemplate,topbar,digiLibTemplate,digiliburlprefix,RESPONSE=None):
            """changeit"""

            paramList=['project','startpage','xslt','thumbtemplate','topbar','digiLibTemplate','digiliburlprefix']

            
            #writeMetadata(self.metalink,self.metaDataHash,project,None,xslt,thumbtemplate,topbar,digiLibTemplate)

            params="project=%s&xslt=%s&thumbtemplate=%s&topbar=%s&digiLibTemplate=%s&digiliburlprefix=%s"%(project,xslt,thumbtemplate,topbar,digiLibTemplate,digiliburlprefix)

            try:
                tries=0
                for i in range(10):
                        x=ECHO_helpers.urlopen('http://echo.mpiwg-berlin.mpg.de/echo_nav/storage/downloadExternalXML?index_meta_url=%s&xml_url=%s'%(self.metalink,self.absolute_url()+'/newMetaXML'+urllib.quote('?'+params))).read()
                        if x=="ok":
                           
                            break;
                 
            except:
                logger("ECHO_ressource (changeViewerTemplateSet)", logging.ERROR,"%s (%s)"%sys.exc_info()[0:2])          
                logger("ECHO_ressource (changeViewerTemplateSet)", logging.ERROR,'http://echo.mpiwg-berlin.mpg.de/echo_nav/storage/downloadExternalXML?index_meta_url=%s&xml_url=%s'%(self.metalink,self.absolute_url()+'/newMetaXML'+urllib.quote('?'+params)))
            #print self.absolute_url()+'/newMetaXML'+urllib.quote'?'+params)
            # hack Pfad auf die Dokumente
            path=self.metalink
            
            

            path=re.sub('/index.meta','',path) 

            #TODO: direct access to the file system necessary, fix that also xmlrpc to the server where the index file is stored is possible    
            parsedUrl=urlparse.urlparse(path)
            path=parsedUrl[2]

            try:        
                return ECHO_helpers.urlopen("http://nausikaa2.rz-berlin.mpg.de:86/cgi-bin/toc/admin/reg.cgi?path=%s"%path).readlines()
            except:
                logger("ECHO_Resource (changeViewerTemplateSet)", logging.INFO,"%s (%s)"%sys.exc_info()[0:2])
                logger("ECHO_Resource (changeViewerTemplateSet)", logging.INFO,"http://nausikaa2.rz-berlin.mpg.de:86/cgi-bin/toc/admin/reg.cgi?path=%s"%path)

            if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

    

    security.declarePublic('content_html')      
    def content_html(self):
        """template fuer content"""
        return ECHO_basis.content_html(self,'resource')
    
#    def getViewClassification(self):
#        if hasattr(self,'viewClassification'):
#            return self.viewClassification
#        else:
#            return ""

    def getFullTextXML(self,noredirect=None):
            """getFullTextXML; gives the FullText as an XML Document, and <error></error> if somthing goes wrong."""

            try:
                    #logger("ECHO Fulltext",logging.INFO,"open %s"%self.metalink)
                    fh=ECHO_helpers.urlopen(self.metalink)
                    #logger("ECHO Fulltext",logging.INFO,"opened %s"%self.metalink)
                    dom=xml.dom.minidom.parse(fh)
                    texttools=dom.getElementsByTagName('texttool')
                    text=texttools[0].getElementsByTagName('text')
                    texturl=getText(text[0].childNodes)

                    #logger("ECHO Fulltext",logging.INFO,"found %s"%texturl)
                    fh.close()
                    #logger("ECHO Fulltext",logging.INFO,"closed fh")
                    #keine url
                    if not (texturl.split(":")[0] in ['http','ftp','file']): 
                        if not noredirect:
                            return file(texturl).read()
                        else:
                            return texturl

                    if not noredirect:
                            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')  
                            logger("ECHO Fulltext",logging.INFO,"redirect to:%s"%texturl)
                            self.REQUEST.RESPONSE.redirect(texturl)
                    else:
                            return texturl
            except:

                    if not noredirect:
                            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
                            self.REQUEST.RESPONSE.write("<error>no fulltext available</error>")
                    else:
                            return "<error>no fulltext available</error>"

    def getImagePath(self):
            """gibt pfad zum image aus"""
            return self.getImageView(noredirect="yes",onlyPath="yes")
    
    def getArchivePathFromMetadata(self):
    	try:
    		archivePath=readFieldFromXML_xpath(self.metalink,"//resource/archive-path")
    		return archivePath
    	except:
			return ""
	
    def getImageView(self,noredirect=None,onlyPath=None):
            """getImages; give Imageviewr  and <error></error> if somthing goes wrong."""
            try:
            	    archivePath=readFieldFromXML_xpath(self.metalink,"//resource/archive-path")
                  
                    archivepath=re.sub('/mpiwg/online/','',archivepath) 
                    imageurl="http://echo.mpiwg-berlin.mpg.de/zogilib?fn="+archivepath+"/"+imagetemp
                    fh.close()

                    if not noredirect:
                            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')  
                            self.REQUEST.RESPONSE.redirect(imageurl)
                    else:
                            if not onlyPath:
                                    return imageurl
                            else:
                                    return archivepath+"/"+imagetemp
            except:

                    if not noredirect:
                            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
                            self.REQUEST.RESPONSE.write("<error>no fulltext available</error>")
                    else:
                            return "<error>no images available</error>"

    
    def getCopyrightsHTML(self):
            """gib (link auf copyright link, mediatyp, institution, copyrightType, label von copyrightType) aus"""
            
            if hasattr(self,'copyrightModel'):
                    obj=self.copyrightModel
                    
            else:
                    return "ERROR"
            ret=[]
            
            for copyright in obj.getCopyrights(): #copyright - media / partner / copyrightID
                    
                    
                    try:
                        if hasattr(self.copyrightTypes,copyright[2]):
                             copyrightTypeObj=getattr(self.copyrightTypes,copyright[2])
                             link="copyrightTypes/"+copyright[2]+'/copyright.html'
                        else:
                             copyrightTypeObj=getattr(obj,copyright[2])
                             link="copyrightModel/"+copyright[2]+'/copyright.html'
                             
                        label=copyrightTypeObj.label
                        url=getattr(copyrightTypeObj, 'url', '')
                            
                        if url!='':
                                 ret.append((url,copyright[0],copyright[1],copyright[2],label))
                        else:
                                if hasattr(copyrightTypeObj, 'copyright.html'):
                                     ret.append(("""%s?partner=%s"""%(link,copyright[1]),copyright[0],copyright[1],copyright[2],label))
                                else:
                                     ret.append(('empty',copyright[0],copyright[1],copyright[2],label))
                    except:
                            """nothing"""
                    
            return ret
            
    def getInstitutionsHTML(self):
                """gibt Liste der foerdernden Institutionen aus"""
                
                if hasattr(self,'support'):
                        obj=self.support
                        ret=obj.getSupporter()
                        return ret
                else:
                        return ''
                        
    def getOwnerOriginalsHTML(self):
               """gibt Liste der foerdernden Institutionen aus"""
               
               if hasattr(self,'ownerOriginal'):
                       obj=self.ownerOriginal
                       ret=obj.getOwner()
                       return ret
               else:
                       return ''
    
    def getDigiCopyByHTML(self):
               """gibt Liste der foerdernden Institutionen aus"""
               
               if hasattr(self,'digiCopyBy'):
                       obj=self.digiCopyBy
                       ret=obj.getDigiCopyBy()
                       return ret
               else:
                       return ''                    
    
    def getCredits(self):
        """Ausgabe der credits"""
        if self.credits:
            return self.credits
        else:
            return []


    def getStorageManagerResourceURL(self):
        """get the link to storage"""
        urlbase=self.getStorageManagerURL();
        
        #now get the path from the metadatalink
        
        path = self.correctPath(self.getMetaDataLink())
        if path is None:
            return ""
        else:
            path=path.replace("index.meta","")
            return urlbase+path
        
        
    def correctPath(self,path):
        #take only the path of the url which starts with /permanent or /experimental
        
        rs= re.search("/permanent/(.*)", path);
        if rs is not None:
            txt="permanent/"+rs.group(1)
        else:
            rs= re.search("/experimental/(.*)", path);
            if rs is not None:
                txt="experimental"+rs.group(1)
            else:
                return None
        
        return txt
        
    def __init__(self,id,link,metalink,resourceID,title,label,description,contentType,renderingType,copyrightType,responsible,credits,weight,coords):

        self.id = id
        """Festlegen der ID"""
        
        self.label = label
        self.link= link
        self.metalink=metalink
        self.title=title
        self.weight=weight
        self.credits=toList(credits)
        self.description=description
        self.contentType=contentType
        self.copyrightType=copyrightType
        self.renderingType=renderingType
        self.responsible=responsible
        self.resourceID=resourceID
        
        if coords:
            coordsnew=[ string.split(x,",") for x in coords]
        else:
            coordsnew=[]
        
        self.coords=coordsnew
#       self.viewClassification=""



    def getContentType(self):
            try:
                    return normalizeCt(self.contentType)
            except:
                    return ""

    def getCopyrightType(self):
            try:
                    return self.copyrightType
            except:
                    return ""

    def getRenderingType(self):
            try:
                    return self.renderingType
            except:
                    return ""

    def ECHO_resource_config(self):
        """Main configuration"""

        if not hasattr(self,'weight'):
            self.weight=""

        pt=zptFile(self, 'zpt/ChangeECHO_resource.zpt')
        return pt()
    

    def ECHO_resource_config_main(self):
        """Main configuration"""
        if not hasattr(self,'weight'):
            self.weight=""
        pt=zptFile(self, 'zpt/ChangeECHO_resource_main.zpt')
        return pt()

    def ECHO_resource_config_coords(self):
        """Coords configuration """
        pt=zptFile(self, 'zpt/ChangeECHO_resource_coords.zpt')
        return pt()

    def ECHO_resource_config_credits(self):
        """Main configuration"""
        pt=zptFile(self, 'zpt/ChangeECHO_resource_credits.zpt')
        return pt()

    def ECHO_resource_config_metadata(self):
        """Main configuration"""
        if (hasattr(self,'metadata')) and not (hasattr(self,'metaDataHash')):
                self.metaDataHash={}
                self.contentType=self.bib_type
                for data in self.metadata:
                        data_neu=re.sub('-','_',data)
                        self.metaDataHash[data_neu]=getattr(self,data)[0:]

        
        pt=zptFile(self, 'zpt/ChangeECHO_resource_metadata.zpt')
        return pt()


    
    def changeViewer(self,newViewer):
        """set newViewer to the url of the new viewer
        only if metalink is set, otherwise it gives false
        """
        
        texterUrl="http://nausikaa2.mpiwg-berlin.mpg.de/digitallibrary/servlet/Texter?fn=/"
        
        vl,msg=self.checkValidityOfMetaLink()
        
        if not vl: #ungueltiger link, versuche neuen
            newStr=re.match(".*/mpiwg/online/(.*)",self.metalink) # suche pfad hinter /mpiwg/online 
            oldLink=self.metalink
            
            if newStr:
                self.metalink=texterUrl+newStr.group(1)
                vl,msg=self.checkValidityOfMetaLink()
            else:
                logging.error("change Viewer XX(mew Metadatafile) %s"%self.metalink)
                vl=False
                
            if not vl: # geht immer noch nicht, dann setzte wieder zurueck
                self.metalink=oldLink
            
            logging.info("change Viewer (mew Metadatafile) %s"%self.metalink)
           
        if vl:
            self.newViewer=newViewer
            return vl,msg
        else:
            return vl,msg
    
    def checkValidityOfMetaLink(self):
        """checks if the metalink is valid xml"""
        
        ml=self.metalink
        
        try:
            txt=urllib.urlopen(ml)

        except:
            return False,"Cannot open: %s)"%ml
        
        txt.close()        
        try:
            dom = NonvalidatingReader.parseUri(ml)
        except:
            return False, "Connot parse: %s)"%ml
        
        if len(dom.xpath("//texttool"))==0:
            return False, "No texttools: %s)"%ml
            
        return True,""
        
    def changeECHO_resource_main(self,metalink,link,title,label,description,contentType,renderingType,weight,resourceID,newViewer='',RESPONSE=None):
        """Aenderung der Properties"""
        self.resourceID=resourceID
        self.title=title
        self.label=label
        self.description=description

        self.contentType=contentType
        self.renderingType=renderingType
        self.weight=weight
        
        self.link=link
        self.metalink=metalink
        
        self.newViewer=newViewer
        
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def changeECHO_resource_coords(self,RESPONSE=None):
        """Aenderung der Properties - coords"""
        #return self.REQUEST
        for area in self.getMapAreas():
            id = area.getId()
            if self.REQUEST.has_key('del.'+id):
                # delete this area
                self._delObject(id)
                # return to same menu
                if RESPONSE is not None:
                    RESPONSE.redirect('ECHO_resource_config_coords')
                return
            # modify this area
            coordstring = self.REQUEST.get('coords.'+id, '')
            coords = string.split(coordstring, ',')
            angle = self.REQUEST.get('angle.'+id, '0')
            type = self.REQUEST.get('type.'+id, 'area')
            if len(coords) == 4:
                area.setCoordString(coordstring)
                area.setAngle(angle)
                area.setType(type)
        # return to main menu    
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def changeECHO_resource_credits(self,credits,responsible,copyrightType,RESPONSE=None):
        """Aenderung der Properties"""
        self.credits=credits
        self.responsible=responsible
        self.copyrightType=copyrightType
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def changeECHO_resource_metadata_local(self,RESPONSE=None):
            """change metadata"""
            tags=self.findTagsFromMapping(normalizeCt(self.contentType))
            for field in tags[1]:
                    self.metaDataHash[self.getFieldTag(tags,field)]=self.REQUEST.form[self.getFieldTag(tags,field)][0:]
                        

                    
            if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')

    def changeECHO_resource_metadata(self,RESPONSE=None):
            """change metadata"""
            tags=self.findTagsFromMapping(normalizeCt(self.contentType))
            self.OSAS_meta={}
            for field in tags[1]:
                    try:
                            self.metaDataHash[self.getFieldTag(tags,field)]=self.REQUEST.form[self.getFieldTag(tags,field)]
                            self.OSAS_meta[self.getFieldTag(tags,field)]=self.REQUEST.form['OSAS_%s'%self.getFieldTag(tags,field)]
                    except:
                            logger("ECHO_collection (changeECHO_resource_metadata)",logging.ERROR,"%s (%s)"%sys.exc_info()[0:2])

            logging.debug("changeECHO_resource_metadata:"+repr(self.newMetaXML()))
            
            
            mc = MetaDataClient("http://content.mpiwg-berlin.mpg.de/metaDataServer") 
            x=mc.writeMetaDataFile(self.getMetaDataLink(),self.newMetaXML(),compressed=None)
                           
            logging.debug("changeECHO_resource_metadata (xlrpc return):"+repr(x))
            

            if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')

    

    def getMDValueSimpleSearchField(self):
            """returns value for simple search"""
            
            try:
                return " ".join([self.getMDValue('title',generic="yes"),
                             self.getMDValue('author',generic="yes"),
                             self.getMDValue('year',generic="yes"),self.getTitle()])
            except:
                
                #logger(" ECHO_collection(getMDSimpleSearchField)",logging.INFO," ".join([self.getMDValue('title',generic="yes"),
                 #            self.getMDValue('author',generic="yes"),
                  #           self.getMDValue('year',generic="yes"),'']))
                return ""
                             
    def getMDValue(self,fieldNameTest,empty=None,generic=None):
            """get md values"""
            #TODO: cache mappinghash
            
            fieldName=None
            if generic:
                if self.contentType:
                    ct=self.contentType.replace(" ","_").lower()
                    ct=self.contentType.replace("-","_").lower()
                else:
                    logger("ECHO_collection (getMDValue)", logging.INFO, "no_content type for %s"%self.getId())
                    ct=""
                    
                #caching
                if not hasattr(self,'_v_mapHash'): #noch keine cachevariable
                    self._v_mapHash={}
                
                

                tmp=self._v_mapHash.get(ct,None) 
                if tmp: #teste ob type schon existiert
                    fieldName=tmp.get(fieldNameTest,None)
                else:
                    self._v_mapHash[ct]={}
                    

                #noch nicht gecached    
                if not fieldName and hasattr(self.standardMD,ct):    
                    fieldName=getattr(self.standardMD,ct).generateMappingHash()[fieldNameTest][0]
                    self._v_mapHash[ct][fieldNameTest]=fieldName
            if not fieldName:
                fieldName=fieldNameTest  
            if not empty:
                           #FIXME: warum gibt es manchmal kein metadatahas
                try:
                    
                    ret =self.metaDataHash.get(fieldName,'!!NOT USED HERE in Type: %s'%normalizeCt(self.contentType))
                except:
                    logger("ECHO (line 1069)",logging.ERROR,"no md hash for %s"%self.getId()) 
                    return ""
                  
            else:

                   
                   ret= self.metaDataHash.get(fieldNameTest,empty)
               
            return unicodify(ret)

    getFieldValue=getMDValue #depricated

    def newMetaXML(self,project=None,startpage=None,xslt=None,thumbtemplate=None,topbar=None,digiLibTemplate=None,digiliburlprefix=None):
            """new index.meta"""
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')

            if not hasattr(self,'metaDataHash'):
                
                    self.copyIndex_meta2echo_resource()
            try:


                    return writeMetadata(self.metalink,self.OSAS_meta,project,startpage,xslt,thumbtemplate,topbar,digiLibTemplate,digiliburlprefix=digiliburlprefix)
            except:
                    #print self.metalink,self.metaDataHash,project,startpage,xslt,thumbtemplate,topbar,digiLibTemplate,digiliburlprefix

                    return writeMetadata(self.metalink,self.metaDataHash,project,startpage,xslt,thumbtemplate,topbar,digiLibTemplate,digiliburlprefix=digiliburlprefix)

    def showMetaDataXML(self,project=None,startpage=None,xslt=None,thumbtemplate=None,topbar=None,digiLibTemplate=None):
            """return bibliographical metadata as stored in the object"""
            
            try:
                    
                    return writeMetadata(self.metalink,self.metaDataHash,project,startpage,xslt,thumbtemplate,topbar,digiLibTemplate,xmlfrag="yes")
            except:
                    return "<error> no metadata stored</error>"
    
    def getXQueryMetaData(self,xquery):
                '''
                execute an xquery on the metadata and returns a list of tuples, 
                each tuple represent the content of the node as text and the whole node as xml-fragment
                @param xquery: xquery auf den metadaten
                '''
                
                try:
                        md=writeMetadata(self.metalink,self.metaDataHash,xmlfrag="yes")
                except:
                        return None

                dom = NonvalidatingReader.parseString(md,"http://www.mpiwg-berlin.mpg.de/")
                
                results=Ft.Xml.XPath.Evaluate(xquery, contextNode=dom)
            
                ret=[]
                for result in results:
                    
                    buf = cStringIO.StringIO()
                    PrettyPrint(result, stream=buf)
                    str = buf.getvalue()[0:]
                    buf.close()
                    ret.append((getTextFromNode(result),str)) 
                        
                return ret
        
    def getMetaDataXML(self,project=None,startpage=None,xslt=None,thumbtemplate=None,topbar=None,digiLibTemplate=None):
            """prints out metadata as stored in the echo environment, format is the index.meta format"""
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
            return writeMetadata(self.metalink,self.metaDataHash)

    def getMetaDataLink(self):
        """get link to the meta data"""
        return self.metalink
    
    def changeECHO_resource(self,metalink,link,title,label,description,contentType,responsible,weight,coords=None,credits=None,RESPONSE=None):
        """Aenderung der Properties"""
        
        setECHO_collectionInformation(self,title,label,description,contentType,responsible,credits,weight)
        self.link=link
        self.metalink=metalink
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
            
            
    manage_options = (
        {'label':'Main Config','action':'ECHO_resource_config_main'},
        {'label':'Change Local Metadata','action':'ECHO_resource_config_metadata'},
        {'label':'Sync Metadata with Repository','action':'ECHO_getResourceMD'},
        {'label':'Graphic Coords','action':'ECHO_graphicEntry'},
        {'label':'Change TemplateSets and Image Viewer','action':'changeViewerTemplateSetForm'},
        {'label':'set/change startpage','action':'setStartPageForm'},
        {'label':'Copy MD for indexing and search','action':'copySearchFields'},
        {'label':'Change AccessRights','action':'changeAccessRightForm'}, 
        ) + Folder.manage_options


    def isDefinedInThisSet(self,fields,field):
            """checks if field is defined in fields"""
            if (fields[0].has_key(field)) and not (fields[0][field]==""):
                    return 1
            else:
                  
                    return 0
            
    def getFieldLabel(self,fields,field):
        """get labels"""
        try:
            ret =fields[0][field]
            if ret == "":
                return field
            else:
                return ret
        except:
            return field



    def getFieldTag(self,fields,field):
        """get labels"""
        try:
            ret =fields[0][field]
            if ret == "":
                return field
            else:
                return ret
        except:
            return field

    

    def getMetaDataHash(self):
            """md hash"""
            return self.metaDataHash
    
    def setFieldValue(self,field,value):
        """get value"""
        #TODO: remove setFieldValue
        
        if not hasattr(self,'metaDataHash'):
                setattr(self,'metaDataHash',{})
        self.metaDataHash[field]=value[0:]

    def copySearchFields(self):
            """copys metadatafields to the object"""
            fields=['author','title','year']
            
            for field in fields:
                    setattr(self,'MD_'+field,self.getMDValue(field,generic="yes"))
    

    def findLabelsFromMapping(self,referenceType):
        """gib hash mit label -> generic zurueck"""
        #return {},[]

        temp=self.ZopeFind(self.standardMD)

        if referenceType=="":
                referenceType="book"
        
        
        bibdata={}
        retdata={}
        fields=[]
        fieldlist=self.standardMD.fieldList

        tags=self.findTagsFromMapping(normalizeCt(self.contentType))
        self.referencetypes=tags[2]
        for referenceTypeF in self.referencetypes:
                
                
                if normalizeCt(referenceTypeF[1].title) == normalizeCt(referenceType.lower()):

                        try:
                                bibdata[referenceTypeF[1].title]=referenceTypeF[1].fields
                                referenceType=referenceTypeF[1].title
                        except:
                                bibdata[referenceType]=referenceTypeF[1].fields
                

                        bibdata['data']=referenceTypeF[1]
                        fields=bibdata[referenceType]
                        for field in fieldlist:
                            retdata[field]=referenceTypeF[1].getValue(field)[1]

        return retdata,fieldlist,temp,fields

    def findTagsFromMapping(self,referenceType):
        """gib hash mit label -> generic zurueck"""
        

        if referenceType=="":
                referenceType="book"
        
        temp =  self.ZopeFind(self.standardMD)[0:]

        
        #self.referencetypes=temp[0:]
        
        
    

        bibdata={}
        retdata={}
        fieldlist=self.standardMD.fieldList
        fields=[]
        for referenceTypeF in temp:
                #print referenceType
        
                if normalizeCt(referenceTypeF[1].title) == normalizeCt(referenceType.lower()):
                        try:
                                bibdata[referenceTypeF[1].title]=referenceTypeF[1].fields
                                referenceType=referenceTypeF[1].title
                        except:
                                bibdata[referenceType]=referenceTypeF[1].fields
                        bibdata['data']=referenceTypeF[1]
                        fields=bibdata[referenceType]
                        for field in fieldlist:
                            retdata[field]=referenceTypeF[1].getValue(field)[0]
        
        return retdata,fieldlist,temp,fields

    
    security.declarePublic('copyIndex_meta2echo_resource') # has to be change, presentlyset because of OSAS koordination
    def copyIndex_meta2echo_resource(self,RESPONSE=None):
            """copy MD von Index_meta to the echo_resource"""
            
            (metadict, error)=readMetadata(self.metalink)


            
            self.metaDataHash={}
            if not error=="": #Fehler beim Auslesen des Metafiles
                    
                    return "ERROR:",error,self.absolute_url()
            
            self.contentType=normalizeCt(metadict['bib_type'])[0:]
            fields=self.findTagsFromMapping(normalizeCt(self.contentType))
            
            #fields=self.findLabelsFromMapping(normalizeCt(self.contentType))
            for field in fields[1]:
                    
                    if self.isDefinedInThisSet(fields,field):
                            #print urllib.unquote(metadict.get(self.getFieldTag(fields,field),''))
                            self.setFieldValue(self.getFieldTag(fields,field),metadict.get(self.getFieldTag(fields,field),''))

            
            
            if RESPONSE:
                    return RESPONSE.redirect('manage_main')
            
            return "OK:",self.absolute_url(),normalizeCt(self.contentType)
        
    def ECHO_getResourceMD(self,template="yes",back=None,overwrite="no"):
        """Einlesen der Metadaten und Anlegen dieser Metadaten als Informationen zur Resource"""
        (metadict, error)=readMetadata(self.metalink)
        
        if back:
            self.REQUEST.SESSION['back']=back

        if not error=="": #Fehler beim Auslesen des Metafiles
                return "ERROR:",error
        
        if (not self.contentType) or (overwrite=="yes"):
                self.contentType=normalizeCt(normalizeCt(metadict['bib_type']).lower())
                
        if not (normalizeCt(metadict['bib_type']).lower()==normalizeCt(self.contentType).lower()):
                self.REQUEST.SESSION['contentStorage']=normalizeCt(metadict['bib_type'])
                self.REQUEST.SESSION['contentZope']=normalizeCt(self.contentType)

                return zptFile(self, 'zpt/ECHO_getResourceMDErrorContentType.zpt')()

        #self.REQUEST.SESSION['metadict']=metadict
        
        
        

        self.REQUEST.SESSION['diffs']=checkDiffs(self,metadict)


        if template=="yes":
                #pt=PageTemplateFile(os.path.join(package_home(globals()), 'zpt/ECHO_resourceMD.zpt'),metadict=metadict).__of__(self)
                pt=zptFile(self, 'zpt/ECHO_resourceMD.zpt')
                
                return pt(metadict=metadict)

        

    
    
    def ECHO_getMD(self,item):
        """Ausgabe der MD"""
        return getattr(self,item)

    def checkRDF(self,path):
            """check if pdf in the path"""
            try:
                    for fileName in os.listdir(path):
                            if os.path.splitext(fileName)[1]==".pdf":
                                    return os.path.join(path,fileName)
                    return None
            except:
                    return None
            
            
    security.declareProtected('View','index_html')
    def index_html(self):
        """standard page"""
        pdf=self.checkRDF(self.link)
        if pdf:
                fh=file(pdf,'r').read()
                self.REQUEST.RESPONSE.setHeader('Content-Type','application/pdf')
                self.REQUEST.RESPONSE.write(fh)
                self.REQUEST.RESPONSE.close()
                return 
            
        if getattr(self,'newViewer',''): #benutze den neuen viewer
            url=self.newViewer+'url='+self.metalink+"&mode=texttool"
            return self.REQUEST.RESPONSE.redirect(url)
            
        return self.REQUEST.RESPONSE.redirect(self.link)

    def index2_html(self):
        """standard page"""
        pdf=self.checkRDF(self.link)
        if pdf:
                fh=file(pdf,'r').read()
                self.REQUEST.RESPONSE.setHeader('Content-Type','application/pdf')
                self.REQUEST.RESPONSE.write(fh)
                self.REQUEST.RESPONSE.close()
		return

	if getattr(self,'newViewer',''): #benutze den neuen viewer
            url=self.newViewer+'url='+self.metalink+"&mode=texttool"
            return self.REQUEST.RESPONSE.redirect(url)
	return self.REQUEST
	return self.REQUEST.RESPONSE.redirect(self.link)

    def startpage_html(self):
            """prints out a startpage for a resource for use e.g. in the BVE"""

            # suche ob startpage.html in dem Ordner vorhanden ist, dann wir diese angezeigt

            sp=self.ZopeFind(self,obj_ids=['startpage.html'])

            if sp:
                    return sp[1]()

            #pruefen ob irgendwo ein template
            if hasattr(self,'startpage_index_template'): 
                    return self.startpage_index_template()

            #generisches template ausgeben
            
            pt=zptFile(self, 'zpt/ECHO_startpage_index_template_standard.zpt')
            pt.content_type="text/html"
            return pt()

    def toc_html(self):
            
            sp=self.ZopeFind(self,obj_ids=['toc.html'])
                    
            if sp:
                    return sp[0][1]()


    security.declarePublic('generate_label') # has to be change, presentlyset because of OSAS koordination

    def generate_label(self):
        """Erzeugt_standard_Label aus Template"""
        pt=getattr(self,"label_template_"+normalizeCt(self.contentType).lower())

        self.label=pt()[0:]
        return pt()

    security.declarePublic('generate_title') # has to be change, presentlyset because of OSAS koordination

    def generate_title(self,RESPONSE=None):
        """Erzeugt_standard_Label aus Template"""
        pt=getattr(self,"label_template_"+normalizeCt(self.contentType).lower())

        self.title=pt()[0:]
        
        return pt()

Globals.InitializeClass(ECHO_resource)

def manage_addECHO_resourceForm(self):
        """Form for adding a ressource"""
        pt=zptFile(self, 'zpt/AddECHO_resourceForm.zpt')
        return pt()



def manage_addECHO_resource(self,id,title,label,description="",responsible="",link="",metalink="",weight="",copyrightType=None,resourceID=None,contentType=None,renderingType=None,credits=None,coords=None,RESPONSE=None):
    """addresource"""

    newObj=ECHO_resource(id,link,metalink,resourceID,title,label,description,contentType,renderingType,copyrightType,responsible,credits,weight,coords)

    self._setObject(id,newObj)

    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
 