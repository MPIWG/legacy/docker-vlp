"""New version of the produc started February, 8th. Without scientific classification, use content-type for further classification."""
"""Echo collection provides the classes for the ECHO content web-site.

class ECHO_collection is the basis class for an ECHO collection.

class ECHO_resource contains information on ECHO resources (e.g. an Display environment for Metadata

class ECHO_externalLink contains information on externalLinks


"""
try:
        from reportlab.pdfgen import canvas
        from reportlab.lib.pagesizes import A4
except:
        print "PDF generation will not work"
     
import urlparse
import string
import tempfile
import zipfile
import re
import os,shutil
import OFS.Image
from types import *
from OFS.Cache import Cacheable
from OFS.Image import Image
from Globals import DTMLFile
from OFS.Folder import Folder
from OFS.SimpleItem import SimpleItem
from AccessControl import ClassSecurityInfo
from AccessControl.User import UserFolder
from Globals import InitializeClass
from Globals import DTMLFile
import Globals
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate,manage_addPageTemplate
from Globals import Persistent, package_home
from Acquisition import Implicit
from Products.ZCatalog.CatalogPathAwareness import CatalogAware
from Products.ZCTextIndex.ZCTextIndex import manage_addLexicon
try:
	from Products.MetaDataProvider.MetaDataClient import MetaDataClient
except:
	print "no metadataclient"
import urllib
import urllib2
import cgi
import smtplib
import time
#from Ft.Xml.Domlette import NonvalidatingReader
#from Ft.Xml.Domlette import PrettyPrint, Print
#from Ft.Xml import EMPTY_NAMESPACE
#
#import Ft.Xml.XPath
import cStringIO

import sys

try:
        from psycopg import libpq
except:
        try:
                from pyPgSQL import libpq
        except:
                print "ECHO_collection: Warning - No libpq imported!"
                
import xml.dom.minidom

import urllib
import xml.dom.minidom
import ECHO_helpers
from ECHO_helpers import *

try:
 from ECHO_language import *
except:
    print "no echo_language"
    class ECHO_language:
        """ leere Klasse"""
        pass

from ECHO_movie import *
import vlp_xmlhelpers #TODO: vlp elemente aus echo herausnehmen
import xmlrpclib

import logging

#ersetzt logging.info
def logger(txt,method,txt2):
    """logging"""
    logging.info(txt+ txt2)


def setECHO_collectionInformation(self,title,label,description,contentType,responsible,credits,weight,coordstrs=""):

        """Allegemeine Informationen zu einer ECHO Collection"""

        self.label = label
        self.title=title
        self.description=description
        self.contentType=contentType
        self.responsible=responsible
        self.credits=toList(credits)
        self.weight=weight

import ECHO_resource
from ECHO_resource import manage_addECHO_resource

class ECHO_resource(ECHO_resource.ECHO_resource):
    """depricated use class in ECHO_resource.py"""


class ECHO_partner(Image,Persistent,ECHO_basis):
    """ECHO Partner"""

    meta_type="ECHO_partner"

    def __init__(self, id, title,url, file, copyrightType, person, email, country, color, content_type='', precondition=''):
        self.__name__=id
        self.title=title
        self.url=url
        self.person=person
        self.email=email
        self.country=country
        self.color=color
        self.precondition=precondition
        self.copyrightType=copyrightType
        data, size = self._read_data(file)
        content_type=self._get_content_type(file, data, id, content_type)
        self.update_data(data, content_type, size)

    manage_options = Image.manage_options+(
        {'label':'Partner Information','action':'ECHO_partner_config'},
        )

    
    def changeECHO_partner(self,url,copyrightType,person, email, country, color, RESPONSE=None):
        """Change main information"""
        self.url=url
        self.person=person
        self.email=email
        self.country=country
        self.color=color
        self.copyrightType=copyrightType
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
            
            

    def ECHO_partner_config(self):
        """Main configuration"""
        if not hasattr(self,'url'):
            self.url=""
        pt=zptFile(self, 'zpt/ChangeECHO_partner.zpt')
        return pt()

        
manage_addECHO_partnerForm=DTMLFile('dtml/ECHO_partnerAdd',globals(),
                             Kind='ECHO_partner',kind='ECHO_partner')



def manage_addECHO_partner(self, id, url, person, email, country, color, file=None, copyrightType='', title='', precondition='', content_type='',
                    REQUEST=None):
    """
    Add a new ECHO_partner object.

    Creates a new ECHO_partner object 'id' with the contents of 'file'.
    Based on Image.manage_addImage
    """

    id=str(id)
    title=str(title)
    content_type=str(content_type)
    precondition=str(precondition)

    id, title = OFS.Image.cookId(id, title, file)

    self=self.this()

    # First, we create the image without data:
    self._setObject(id, ECHO_partner(id,title,url,'',copyrightType, person, email, country, color, content_type, precondition))

    # Now we "upload" the data.  By doing this in two steps, we
    # can use a database trick to make the upload more efficient.
    if file:
        self._getOb(id).manage_upload(file)
    if content_type:
        self._getOb(id).content_type=content_type

    if REQUEST is not None:
        try:    url=self.DestinationURL()
        except: url=REQUEST['URL1']
        REQUEST.RESPONSE.redirect('%s/manage_main' % url)
    return id

class ECHO_locale(ZopePageTemplate):
        """localisierung"""

        meta_type="ECHO_locale"

        def __init__(self,id,lang,title,label,text=None,content_type=None):
            self.lang=lang
            self.title=title
            self.label=label
            # default content
            if not text:
                text = open(self._default_content_fn).read()
                content_type = 'text/html'
            self.pt_edit(text, content_type)
            self.id=id

        manage_options = ZopePageTemplate.manage_options+(
                {'label':'Main Config','action':'change_ECHO_localeForm'},
                )

        def change_ECHO_localeForm(self):
                """change form"""
                pt=zptFile(self, 'zpt/ChangeECHO_localeForm.zpt')
                return pt()
        
        def change_ECHO_locale(self,lang,title,label,text=None,content_type=None,RESPONSE=None):
                """change echo locale"""
                self.lang=lang
                self.title=title
                self.label=label
                if not text is None:
                        if content_type is None:
                            content_type = self.content_type
                        self.pt_edit(text, content_type)

                if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

                
def manage_addECHO_localeForm(self):
         """Form for adding"""
         pt=zptFile(self, 'zpt/AddECHO_localeForm.zpt')
         return pt()

def manage_addECHO_locale(self,lang,title,label,text=None,content_type=None,RESPONSE=None):
        """add echo locale"""

        id="locale_"+lang
        
        self._setObject(id, ECHO_locale(id,lang,title,label,text,content_type))
        if RESPONSE is not None:
                RESPONSE.redirect('manage_main')


class ECHO_copyright(Folder,ECHO_basis):
        """Copyright informationen"""
        meta_type="ECHO_copyright"
        
        
        
        def __init__(self,id,title,copyrights):
                """init"""
                self.title=title
                self.id=id
                self.copyrights=copyrights[0:]

        def getCopyrights(self):
                """return coyprights"""
                return self.copyrights
                
        manage_options = Folder.manage_options+(
                {'label':'Main Config','action':'ECHO_copyright_configForm'},
                )


        def ECHO_copyright_configForm(self):
                """change form"""
                pt=zptFile(self, 'zpt/ChangeECHO_copyright')
                pt.content_type="text/html"
                return pt()

        def ECHO_copyright_config(self,title,RESPONSE=None):
                """change"""
                self.title=title
                self.copyrights=[]
                self.copyrights=getCopyrightsFromForm(self,self.REQUEST.form)[0:]

                if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

                
def manage_addECHO_copyrightForm(self):
    """Form for adding"""
    pt=zptFile(self, 'zpt/AddECHO_copyright.zpt')
    return pt()

def manage_addECHO_copyright(self, id,title,RESPONSE=None):
        """add the copyright"""

        meta_type="ECHO_copyright"
        
        #first generate copyrights as list of tripels (mediaType,partner,copyrightType)
        
                

        self._setObject(id, ECHO_copyright(id, title,getCopyrightsFromForm(self,self.REQUEST.form)))

        if RESPONSE is not None:
                RESPONSE.redirect('manage_main')
        
class ECHO_layoutTemplate(ZopePageTemplate,ECHO_basis):
    """Create a layout Template for different purposes"""

    meta_type="ECHO_layoutTemplate"

    def __init__(self, id, text=None, content_type=None,EchoType=None):
        self.id = str(id)

        self.ZBindings_edit(self._default_bindings)
        if text is None:
            self._default_content_fn = os.path.join(package_home(globals()),'zpt','ECHO_%s_template_standard.zpt'%EchoType)
            text = open(self._default_content_fn).read()
        if content_type is None:
            content_type = self.content_type
        self.pt_edit(text, content_type)


def manage_addECHO_layoutTemplateForm(self):
    """Form for adding"""
    pt=zptFile(self, 'zpt/AddECHO_layoutTemplate.zpt')
    return pt()


def manage_addECHO_layoutTemplate(self, EchoType,title=None,REQUEST=None):
    "Add a Page Template with optional file content."
    if type(EchoType)==StringType:
        EchoTypes=[EchoType]
    else:
        EchoTypes=EchoType
        
    for singleType in EchoTypes:

        id = str(singleType)+"_template"
        if REQUEST is None:
            self._setObject(id, ECHO_layoutTemplate(id, text,EchoType=singleType))
            ob = getattr(self, id)
            
            if title:
                ob.pt_setTitle(title)
            return ob
        else:
            file = REQUEST.form.get('file')
            headers = getattr(file, 'headers', None)
            if headers is None or not file.filename:
                zpt = ECHO_layoutTemplate(id,EchoType=singleType)
            else:
                zpt = ECHO_layoutTemplate(id, file, headers.get('content_type'))

            self._setObject(id, zpt)
            ob = getattr(self, id)
            if title:
                ob.pt_setTitle(title)

            try:
                u = self.DestinationURL()
            except AttributeError:
                u = REQUEST['URL1']

            
    REQUEST.RESPONSE.redirect(u+'/manage_main')
    return ''

class ECHO_fullText(ZopePageTemplate,ECHO_basis,ECHO_language):
        """echo fulltext in xml"""

        meta_type="ECHO_fullText"

        
        def getPage(self,nr='1'):
                """get page n"""
                #return self().encode('latin-1','ignore')
                try:
                   dom=xml.dom.minidom.parseString(self())
                except:
                   dom=xml.dom.minidom.parseString(self.utf8ify(self()))
                pages=dom.getElementsByTagName('page')
                
                return pages[int(nr)-1].toxml()
        
# Product registration and Add support
manage_addECHO_fullTextForm = PageTemplateFile(
    'zpt/AddECHO_fullText.zpt', globals())

def manage_addECHO_fullText(self, id, title=None, text=None,
                           REQUEST=None, submit=None):
    "Add a Page Template with optional file content."

    id = str(id)
    if REQUEST is None:
        self._setObject(id, ECHO_fullText(id, text))
        ob = getattr(self, id)
        if title:
            ob.pt_setTitle(title)
        return ob
    else:
        file = REQUEST.form.get('file')
        headers = getattr(file, 'headers', None)
        if headers is None or not file.filename:
            zpt = ECHO_fullText(id)
        else:
            zpt = ECHO_fullText(id, file, headers.get('content_type'))

        self._setObject(id, zpt)

        try:
            u = self.DestinationURL()
        except AttributeError:
            u = REQUEST['URL1']

        if submit == " Add and Edit ":
            u = "%s/%s" % (u, urllib.quote(id))
        REQUEST.RESPONSE.redirect(u+'/manage_main')
    return ''




class ECHO_externalLink(Folder,ECHO_basis):
    """Link zu einer externen Ressource"""
    security=ClassSecurityInfo()
    meta_type='ECHO_externalLink'

    security.declarePublic('content_html')
    def content_html(self):
        """template fuer content"""
        ret= ECHO_basis.content_html(self,'externalLink')
        
        if type(ret) is StringType:
            return ret.decode('utf-8')
        else:
           return ret
        
        
    
    def __init__(self,id,link,title,label,description,contentType,responsible,credits,weight,coords,linkType):

        self.id = id
        """Festlegen der ID"""

        self.credits=toList(credits)
        self.label = label
        self.link= link
        self.title=title
        self.weight=weight
        self.description=description
        self.contentType=contentType
        self.responsible=responsible
        coordsnew=[ string.split(x,",") for x in coords]
        self.coords=coordsnew
        self.linkType = linkType        # Linktypen 'otherPresentation','external'
        

    def ECHO_externalLink_config(self):
        """Main configuration"""

        if not hasattr(self,'weight'):
            self.weight=""
        if not hasattr(self,'coords'):
            
            self.coords=['']


        pt=zptFile(self, 'zpt/ChangeECHO_externalLink.zpt')
        return pt()
    

    def changeECHO_externalLink(self,link,title,label,description,contentType,responsible,weight,coords=None,credits=None,RESPONSE=None,linkType='otherPresentation'):
        """Aenderung der Properties"""
        setECHO_collectionInformation(self,title,label,description,contentType,responsible,credits,weight)

        self.link=link
        self.linkType = linkType
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
            
    def getLinkType(self):
        """ return linkType """
        if hasattr(self,"linkType"):
                return self.linkType
        else :
                return 'otherPresentation'

    def setLinkType(self,type):
        """ set linkType """
        self.linkType = type
        
    def checkLink(self):
        """ returns tuple (isWorking,Error) """        
        try:
            urllib2.urlopen(self.link)
            return (True, '')
        except urllib2.HTTPError, e:
            return (False, e.code)
        except urllib2.URLError, e:
            return (False, str(e.reason))
        except:
            return (False, 'unknown Error')
            
            
    manage_options = (
        {'label':'Main Config','action':'ECHO_externalLink_config'},
        {'label':'Graphic Coords','action':'ECHO_graphicEntry'},
        )+Folder.manage_options

        
    def index_html(self):
        """standard page"""
        
        return self.REQUEST.RESPONSE.redirect(self.link)
            

def manage_addECHO_externalLinkForm(self):
        """Form for external Links"""
        pt=zptFile(self, 'zpt/AddECHO_externalLinkForm.zpt')
        return pt()


def manage_addECHO_externalLink(self,id,title,label,description,contentType,responsible,link,weight,coords=None,credits=None,RESPONSE=None,linkType='otherPresentation'):
    """Add an external Link"""

    newObj=ECHO_externalLink(id,link,title,label,description,contentType,responsible,credits,weight,coords,linkType)

    self._setObject(id,newObj)

    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
 

class ECHO_link(ECHO_externalLink):
        """external_link"""

        meta_type="ECHO_link"
        
        manage_options = ECHO_externalLink.manage_options+(
                {'label':'add links config','action':'ECHO_link_addLinksForm'},
                )
        
        def ECHO_link_addLinksForm(self):
            """Main configuration"""
        
            pt=zptFile(self, 'zpt/ChangeECHO_link_addLinks.zpt')
            return pt()
    
        def ECHO_link_addLinks(self,addLinks,RESPONSE):
            """add links"""
            self.addLinks=addLinks
            if RESPONSE is not None:
                RESPONSE.redirect('manage_main')
  
        def content_html(self):
                """template fuer link"""
        

                if hasattr(self,"link_template"):
                        ret=ECHO_basis.content_html(self,'link')

                else:
                        ret=ECHO_basis.content_html(self,'collection')


                #return ret
                return ret
#            
#            
#                try:
#                    return ret.decode('utf-8')
#                except:
#                    try: 
#                        return ret.decode('latin-1')
#                    except:
#                
#                        return ret
#        
        def getCopyrightsHTML(self):
            """gib (link auf copyright link, mediatyp, institution, copyrightType, label von copyrightType) aus"""
            
            if hasattr(self,'copyrightModel'):
                    obj=self.copyrightModel
                    
            else:
                    return "ERROR"
            ret=[]
            
            for copyright in obj.getCopyrights(): #copyright - media / partner / copyrightID
                    
                    
                    try:
                        if hasattr(self.copyrightTypes,copyright[2]):
                             copyrightTypeObj=getattr(self.copyrightTypes,copyright[2])
                             link="copyrightTypes/"+copyright[2]+'/copyright.html'
                        else:
                             copyrightTypeObj=getattr(obj,copyright[2])
                             link="copyrightModel/"+copyright[2]+'/copyright.html'
                             
                        label=copyrightTypeObj.label
                        url=getattr(copyrightTypeObj, 'url', '')
                            
                        if url!='':
                                 ret.append((url,copyright[0],copyright[1],copyright[2],label))
                        else:
                                if hasattr(copyrightTypeObj, 'copyright.html'):
                                     ret.append(("""%s?partner=%s"""%(link,copyright[1]),copyright[0],copyright[1],copyright[2],label))
                                else:
                                     ret.append(('empty',copyright[0],copyright[1],copyright[2],label))
                    except:
                            """nothing"""
                    
            return ret
            
        def getInstitutionsHTML(self):
                """gibt Liste der foerdernden Institutionen aus"""
                
                if hasattr(self,'support'):
                        obj=self.support
                        ret=obj.getSupporter()
                        return ret
                else:
                        return ''
                        
        def getOwnerOriginalsHTML(self):
               """gibt Liste der foerdernden Institutionen aus"""
               
               if hasattr(self,'ownerOriginal'):
                       obj=self.ownerOriginal
                       ret=obj.getOwner()
                       return ret
               else:
                       return ''
    
        def getDigiCopyByHTML(self):
               """gibt Liste der foerdernden Institutionen aus"""
               
               if hasattr(self,'digiCopyBy'):
                       obj=self.digiCopyBy
                       ret=obj.getDigiCopyBy()
                       return ret
               else:
                       return ''
                
        def index_html(self):
                """standard link"""
                if self.link:
                 splitted=self.link.split("?")
                 if len(splitted)>1:
                        params=cgi.parse_qs(splitted[1])

                        for x in params.keys():
                                if type(params[x]) is ListType:
                                        params[x]=params[x][0]
        

                 else:
                        params={}
                
                 if getattr(self,'addLinks','yes')=="yes":
                     params['backLink']=self.aq_parent.absolute_url()
                     params['startLink']=splitted[0]+"?"+urllib.urlencode(params)
                

                 return self.REQUEST.RESPONSE.redirect(splitted[0]+"?"+urllib.urlencode(params))
                else:
                 return ""

def manage_addECHO_linkForm(self):
        """Form for external Links"""
        pt=zptFile(self, 'zpt/AddECHO_linkForm.zpt')
        return pt()


def manage_addECHO_link(self,id,title,label,description="",contentType="",responsible="",link="",weight="",coords=[],credits=None,linkType="external",RESPONSE=None):
    """Add an external Link"""

    newObj=ECHO_link(id,link,title,label,description,contentType,responsible,credits,weight,coords,linkType)

    self._setObject(id,newObj)
    
    getattr(self,id).addLinks='no'
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
        

class ECHO_collection(CatalogAware, Folder, Persistent, Implicit, Cacheable,ECHO_basis):
    """ECHO Collection"""

    management_page_charset="utf-8"
    security=ClassSecurityInfo()
    meta_type='ECHO_collection'
    default_catalog='resourceCatalog'
    
#    viewClassificationList=viewClassificationListMaster
    displayTypes=displayTypes

    path="/mpiwg/online/permanent/shipbuilding"

    
    def exportImportObjects_html(self,RESPONSE):
        """ImportObject"""
        pt=zptFile(self, 'zpt/exportImportObjects.zpt')
        pt.content_type="text/html"
        return pt()
        
    def importObjects_html(self,RESPONSE):
        """ImportObject"""
        pt=zptFile(self, 'zpt/importObjects.zpt')
        pt.content_type="text/html"
        return pt()
   
    def importObjects(self,zfile,RESPONSE=None,REQUEST=None):
        """import object from file"""
        
        zf=zipfile.ZipFile(zfile,"r")
        tmpdir=tempfile.mkdtemp() # create tempdir
        ret=None
        
        for name in zf.namelist():
               ds,fn=tempfile.mkstemp()
               tf=file(fn,"w")
               x=zf.read(name)
               tf.write(x)
               tf.close()
               try:
                   self._importObjectFromFile(fn,set_owner=1)
               except:
                   if not ret:
                       ret=""
                   ret+="Cannot import: %s (Already existing?)<br>"%name
	       os.close(ds)
               os.remove(fn)

        zf.close()

        if ret:
            return """<html><body>%s</body></html>"""%ret
        if RESPONSE:
            RESPONSE.redirect(self.absolute_url())
        
    def exportObjects_html(self,RESPONSE):
        """Export objects"""
        
        pt=zptFile(self, 'zpt/exportObjects.zpt')
        pt.content_type="text/html"
        return pt()
    
    def exportObjects(self,ids,RESPONSE=None):
        """export objects with type id"""
        if not (type(ids) is ListType):
            ids=[ids]
        
        tmpdir=tempfile.mkdtemp() # create tempdir
        objs=self.ZopeFind(self,obj_ids=ids)
        tmpfile=tempfile.mkstemp()[1]

        zf=zipfile.ZipFile(tmpfile,"w")
        for obj in objs:

            f = os.path.join(tmpdir, '%s.zexp' %obj[0])
            #print E.absolute_url()

            obj[1]._p_jar.exportFile(obj[1]._p_oid, f)

            zf.write(f,obj[0])
        
        zf.close()
        shutil.rmtree(tmpdir)
        if RESPONSE:
            RESPONSE.setHeader("Content-Type","application/octet-stream")
            len=os.stat(tmpfile)[6]
            RESPONSE.setHeader("Content-Length",len)
            RESPONSE.setHeader("Content-Disposition","""attachement; filename=%s"""%"export.zip")
            fh=file(tmpfile)
            for x in fh.read():
                RESPONSE.write(x)
            fh.close()
            
    def getMDValueSimpleSearchField(self):
          """returns value for simple search"""
          return " ".join([self.title,self.description])
                             
    def getTitleAndLabel(self):
        """gibt title und label zurueck"""
        return (getattr(self,'title',''),getattr(self,'label',''))

    def localizeObjects(self):
            """localize all objects"""
            contents=self.ZopeFind(self,obj_metatypes=['ECHO_externalLink','ECHO_link','ECHO_mapText'])

            find=self.ZopeFind(self,obj_ids=('locale_en'))
            if not find:
                self.manage_addECHO_locale("en",'','')
            for content in contents:

                    if content[1].meta_type in ['ECHO_link','ECHO_externalLink']:

                            find=content[1].ZopeFind(content[1],obj_metatypes=('ECHO_mapText'))
                            if find:
                                    root=find[0][1]

                                    locale=find[0][1].ZopeFind(find[0][1],obj_ids=('locale_en'))
                            else:
                                    root=None
                    else:
                            root=content[1]
                            locale=content[1].ZopeFind(content[1],obj_ids=('locale_en'))
                    if root and not locale:
                            root.manage_addECHO_locale("en",'','')
                    
            pt=zptFile(self, 'zpt/localizeObjects.zpt')
            return pt()


    def localize(self,REQUEST,RESPONSE):
            """localize"""
            for key in REQUEST.form.keys():
                    splitted=key.split("!")

                    if splitted[0]=="" or splitted[0]=="en" or splitted[0]=="title" or splitted[0]=="label":
                            if splitted[0]=="en":
                                    setattr(self.locale_en,splitted[1],REQUEST.form[key])
                            else:
                                    setattr(self,splitted[0],REQUEST.form[key])
                    else:
                            obj=getattr(self,splitted[0])

                            if obj.meta_type=="ECHO_mapText":
                                    if splitted[1]=="en":
                                            obj.locale_en.pt_edit(REQUEST.form[key],obj.locale_en.content_type)
                                    else:
                                            obj.pt_edit(REQUEST.form[key],obj.content_type)
                            else:
                                    text=obj.ZopeFind(obj,obj_metatypes=['ECHO_mapText'])
                                    if splitted[1]=="en":
                                            
                                            text[0][1].locale_en.pt_edit(REQUEST.form[key],text[0][1].locale_en.content_type)
                                    else:
                                            text[0][1].pt_edit(REQUEST.form[key],text[0][1].content_type)
            if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')


    def getRDF(self,urn=None):
            """rdf of the collection"""
            
            contents=self.ZopeFind(self,obj_metatypes=['ECHO_group','ECHO_resource','ECHO_collection'])

            ret=getRDFDescription(self,self.absolute_url(),urn=urn)
            
            if not urn:
                    urn=self.absolute_url()
                    
            li="""<rdf:li rdf:resource="%s" />\n"""

            
            for content in contents:
		try:
                    ret+=content[1].getRDF()+"\n"
                except:
		    logging.error("getrdf: "+repr(content[1].getRDF()))
		    try:
		    	ret+=self.unicodify(content[1].getRDF())+"\n"
		    except:
			logging.error("--still cannot do it")
		    	ret+=repr(content[1].getRDF())+"\n"
            ret+="""<rdf:Seq rdf:about="%s">\n"""%urn
            for content in contents:
                    nurn=content[1].absolute_url()
                    ret+=li%nurn
            return ret+"</rdf:Seq>"
            

    
    def changeLabels(self):
                """change form"""
                pt=zptFile(self, 'zpt/changeLabelsForm')
                pt.content_type="text/html"
                return pt()

    def changeTitles(self):
                """change form"""
                pt=zptFile(self, 'zpt/changeTitleForm')
                pt.content_type="text/html"
                return pt()

    def changeWeights(self):
                """change form"""
                pt=zptFile(self, 'zpt/changeWeightForm')
                pt.content_type="text/html"
                return pt()

    def changeMetaDataLinks(self):
                """change form"""
                pt=zptFile(self, 'zpt/changeMetaDataLinkForm')
                pt.content_type="text/html"
                return pt()
            
    def changeAccessRightsCollectionForm(self,preselect=None):
        """change access rights of all resources in this collection"""

        pt=zptFile(self, 'zpt/changeAccessRightsCollectionForm')
        pt.content_type="text/html"
        return pt(preselect=preselect)
        
        
    def changeAccessRightsCollection(self):
            """change"""
            ret=""
            argv=self.REQUEST.form
            
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'])
            for resource in resources:
                   
                    if argv.has_key(resource[1].getId()+'_xml'):
                            ret+=resource[1].getId()+"   "+argv[resource[1].getId()+'_xml']+"</br>"
                            resource[1].setAccessRightXML(argv[resource[1].getId()+'_xml'])
                            resource[1].accessRight=argv[resource[1].getId()+'_xml']
                    elif argv.has_key(resource[1].getId()+'_echo'):
                            ret+="only local:"+resource[1].getId()+"   "+argv[resource[1].getId()+'_echo']+"</br>"
                            resource[1].accessRight=argv[resource[1].getId()+'_echo']
                    else:
                            ret+="ERROR:" +resource[0]
            return ret

    def changeMetaDataLinkInCollection(self):
            """change all lables of a collection"""
            ret=""
            argv=self.REQUEST.form
            
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'])
            for resource in resources:
                   
                    try:
                            ret+=resource[1].getId()+"   "+argv[resource[1].getId()]+"</br>"
                            resource[1].metalink=argv[resource[1].getId()][0:]
                    except:
                            pass
            return ret

    def changeMetaDataLinkInCollection(self):
            """change all lables of a collection"""
            ret=""
            argv=self.REQUEST.form
            
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'])
            for resource in resources:
                   
                    try:
                            ret+=resource[1].getId()+"   "+argv[resource[1].getId()]+"</br>"
                            resource[1].metalink=argv[resource[1].getId()][0:]
                    except:
                            pass
            return ret

    def changeWeightsInCollection(self):
            """change all lables of a collection"""
            ret=""
            argv=self.REQUEST.form
            
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_pageTemplate','ECHO_movie','ECHO_resource','ECHO_collection','ECHO_link','ECHO_externalLink'])
            for resource in resources:
                   
                    try:
                            ret+=resource[1].getId()+"   "+argv[resource[1].getId()]+"</br>"
                            resource[1].weight=argv[resource[1].getId()][0:]
                    except:
                            pass
            return ret

    def changeTitlesInCollection(self):
            """change all lables of a collection"""
            ret=""
            argv=self.REQUEST.form
            
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_pageTemplate','ECHO_movie','ECHO_resource','ECHO_collection','ECHO_link','ECHO_externalLink'])
            for resource in resources:
                   
                    try:
                            ret+=resource[1].getId()+"   "+argv[resource[1].getId()]+"</br>"
                            resource[1].title=argv[resource[1].getId()][0:]
                    except:
                            pass
            return ret
    
    def changeLabelsInCollection(self):
            """change all lables of a collection"""
            ret=""
            argv=self.REQUEST.form
            
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_pageTemplate','ECHO_movie','ECHO_resource','ECHO_collection','ECHO_link','ECHO_externalLink'])
            for resource in resources:
                   
                    try:
                            ret+=resource[1].getId()+"   "+argv[resource[1].getId()]+"</br>"
                            resource[1].label=argv[resource[1].getId()][0:]
                    except:
                            pass
            return ret
       
    def importCollection(self,path=None,RESPONSE=None):
        """liest verzeichnisse aus dem pfad und legt sie dann als objekte in den ordner"""
        
        if path is None:
                pt=zptFile(self, 'zpt/importCollection.zpt')
                return pt()

        files=os.listdir(path)
        ret=""
        for fileName in files:
            
            if fileName:
                
                tempPath=re.sub("/mpiwg/online","",path)
                link="http://echo.mpiwg-berlin.mpg.de/zogilib_book?fn="+tempPath+"/"+fileName+"/pageimg"
                
                metalink=self.path+"/"+fileName+"/index.meta"
                try:

                        #link="http://nausikaa2.mpiwg-berlin.mpg.de/cgi-bin/toc/toc.x.cgi?dir="+fileName+"&step=thumb"
                        
                        newObj=ECHO_resource(fileName,link,metalink,fileName,fileName,fileName,'generated','book','','','','','','')
                        self._setObject(fileName,newObj)
                
                        genObj=getattr(self,fileName)
                        #genObj.createIndexFile()
                        ret+="OK:"+fileName+"<br/>"
                except:
                        print "ERROR"
                        ret+="ERROR:"+fileName+"<br/>"

        return ret

        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    def changeViewerTemplateSetsForm(self):
            """change the viewer template set"""
            pt=zptFile(self, 'zpt/changeViewerTemplateSet')
            return pt()

    def getViewerTemplateSets(self,obj_ids=None,RESPONSE=None):
            """Get the ViewerTemplateSet title for configuration"""
            ret=[]
            
            try:
                    viewerTemplateSets=self.ZopeFind(self.viewerTemplateSets,obj_metatypes=['OSAS_viewerTemplateSet'],obj_ids=obj_ids)#assumes viewerTemplateSets folder somewhere in the hierarchie.
            
                    for viewerTemplateSet in viewerTemplateSets:
                            ret.append((viewerTemplateSet[1].title,viewerTemplateSet[0],viewerTemplateSet[1]))

                    return ret
                    
            except:
                    return [('no ViewerTemplateSetfolders','')]

    def getTextToolsField(self,name,default=''):
            """Lese viewerTemplateSet der Collection not implemented yet!"""
            
            return default

            
    def isSelectedViewerTemplateSet(self,obj,id):
        """is ausgewaehlt"""
        
        if self.REQUEST['viewerTemplateSet']==id:
            return 1
        else:
            return None

    def changeViewer(self,newViewer,REQUEST=None,RESPONSE=None):
        """changes all ressources to the newViewer"""
        
        resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1)
        if RESPONSE is not None:
                    RESPONSE.write("<html><body>")

        for resource in resources:
            
            done,msg=resource[1].changeViewer(newViewer)
            if done:
                if RESPONSE is not None:
                    RESPONSE.write("<p>OK: %s"%resource[0])
            else:
                if RESPONSE is not None:
                    RESPONSE.write("<p><a href='%s'>ERROR: %s (%s)</a>"%(resource[1].absolute_url()+'/ECHO_resource_config_main',resource[0],msg))
                

                
        if RESPONSE is not None:
                    RESPONSE.write("<p>Done</p></body></html>")

    def changeViewerTemplateSets(self,project,xslt,thumbtemplate,topbar,digiLibTemplate,digiliburlprefix,RESPONSE=None):
            """change the templates"""

            resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1)

            for resource in resources:

                    resource[1].changeViewerTemplateSet(project,xslt,thumbtemplate,topbar,digiLibTemplate,digiliburlprefix)

            if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')


    def setStartpageFolderForm(self):
            """Form for changing the startpage"""

            
            pt=zptFile(self, 'zpt/ChangeECHO_resourceStartPageFolder.zpt')
            pt.content_type="text/html"
            return pt()
    
    def setStartpageFolder(self,startpage=None,RESPONSE=None):
            """change the templates"""

            resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1)

            for resource in resources:

                    resource[1].setStartPage(startpage)

            if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')

    def copyTitleToInfoXMLFolder(self,RESPONSE=None):
            """copy title into the title field of info.xml
            author and date werden leer!!!
            """

            
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1)
            
            
            for resource in resources:
                    ret,txt=resource[1].copyTitleToInfoXML()
                    if (not ret) and RESPONSE:
                            RESPONSE.write("error: %s\n"%txt)
                    if ret and RESPONSE:
                            RESPONSE.write("ok: %s\n"%txt)
                    
                    #logger("ECHO (copyTitleToInfoXMLFolder)",logging.INFO,txt)   
            if RESPONSE is not None:
                    RESPONSE.write("done!\n")
                    RESPONSE.close()
                    RESPONSE.redirect('manage_main')
                    
    def copySearchFields(self,RESPONSE=None):
            """copys < metadatafields to the object"""
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1)

            for resource in resources:

                    resource[1].copySearchFields()

            if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')
                    
    def reloadMetaDataFromStorageWarning(self,RESPONSE=None):
            """warning"""
            pt=zptFile(self, 'zpt/reloadMetaDataFromStorageWarning.zpt')
            pt.content_type="text/html"
            return pt()

    def reloadMetaDataFromStorage(self,RESPONSE=None):
            """copy metadata from the storage to ECHO"""

            return reloadMetaDataFromStorage(self,RESPONSE)

    
    def getPartnerCopyright(self,name,sonst="generic"):
                """gibt generisches copyright eines partners aus, sonst behalte jetzige einsteillung"""
                #print "hi",name,sonst
                #print getattr(self.partners,name).copyrightType
                try:
                        partner=getattr(self.partners,name)
                        return partner.copyrightType
                except:
                        print "error"
                        return sonst

    def partnerSelector_HTML(self,selected=None):
         """give type selector"""
         if not selected:
                 retStr="<option selected>\n"
         else:
                 retStr="<option>\n"
                 
         try: # erste version copyrightTypes exists
                 for partner in self.getPartners():
                         partnerTitle = partner.title
                         partnerId = partner.__name__
                         if selected and (partnerId==selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(partnerId,partnerTitle)
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(partnerId,partnerTitle)
         except:
                 """nothing"""
         return retStr
 
#    def getViewClassification(self):
#        if hasattr(self,'viewClassification'):
#            return self.viewClassification
#        else:
#            return ""

        
    def createRessourcesFromXMLForm(self):
            """form"""
            pt=zptFile(self, 'zpt/createRessourcesFromXMLForm.zpt')
            return pt()

    def createRessourcesFromXML(self,fileupload):
            """read an XML file for generating resources"""
            dom=xml.dom.minidom.parse(fileupload)
            ret="<h2>Added</h2>"
            for resource in dom.getElementsByTagName('resource'):
                    link=getText(resource.getElementsByTagName('link')[0].childNodes)
                    label=getText(resource.getElementsByTagName('label')[0].childNodes)
                    pageimglink=getText(resource.getElementsByTagName('pageimglink')[0].childNodes)
                    #splitted=link.split("?")[0].split("/")
                    #id=splitted[len(splitted)-1].encode('ascii')
                    id=re.sub(" ","_",label).encode('ascii')
                    
                    ret+="<p>"+label+"</p>"
                    manage_addECHO_resource(self,id,label.encode('ascii'),label.encode('ascii'),"","",pageimglink.encode('ascii'),link.encode('ascii'),"","")
            return ret
        
    security.declarePublic('getImageTag')
    def getImageTag(self):
        """GetTag"""
        try:
            return self.imageTag
        except:
            return ""


    def addMovie(self,id,title,label,description,contentType,responsible,link,thumbUrl,rawFile,lowresFile,metalink,weight,credits=None,coords=None,RESPONSE=None):
        """SSS"""

        #manage_addECHO_movie(self,id,title,label,description,responsible,link,thumbUrl,rawFile,metalink,weight,credits=None,coords=None,RESPONSE=None)
        if not hasattr(self,id):
                try:
                        manage_addECHO_movie(self,id,title,label,description,responsible,link,thumbUrl,rawFile,lowresFile,metalink,weight,credits=None,coords=None,RESPONSE=None)
                        return "done"
                except:
                        return None
        else:
                obj=getattr(self,id)
                obj.changeECHO_movie_main(metalink,link,rawFile,lowresFile,thumbUrl,title,label,description,contentType,weight=weight)

        return "changed"

    def addResource(self,id,title,label,description,contentType,responsible,link,metalink,weight,credits=None,coords=None,RESPONSE=None):
        """SSS"""
        try:
            manage_addECHO_resource(self,id,title,label,description,responsible,link,metalink,weight,credits=None,coords=None,RESPONSE=None)
            return "done"
        except:
        	logging.error("ECHOcollection: (AddResource): %s %s"%sys.exc_info()[0:2])
        	return None

    def addResourceAndCreateLabelAndTitle(self,id,title,label,description,contentType,responsible,link,metalink,weight,credits=None,coords=None,RESPONSE=None):
        """SSS"""
        ret = self.addResource(id,title,label,description,contentType,responsible,link,metalink,weight,credits,coords,RESPONSE)
        if ret is None:
        	return None
        
        try:
        	server2=getattr(self,id)
        	server2.copyIndex_meta2echo_resource()
        	server2.generate_label()
        	server2.generate_title()
    	except:
	    	  logging.error("An Error occured adding the resource A\n %s %s"%sys.exc_info()[0:2])
	    	  return None
        return "done"
    def getSecondaryLink(self):
        """secondary link"""
        try:
            return self.secondaryLink
        except:
            return ""

    def getSecondaryLinkTitle(self):
        """secondary link"""
        try:
            return self.secondaryLinkTitle
        except:
            return ""
        
    def getCollectionTreeXML(self,pwstr=None):
        """Tree as XML"""

        def addPassWd(str,pwstr=None):
                """adds a user/passwd to an url"""
                if pwstr:
                        txt2=re.sub(r"(http://)(.*?)","\g<1>%s@\g<2>"%pwstr,str)
                else:
                        txt2=re.sub(r"(http://)(.*?)","\g<1>www:3333@\g<2>",str)
                return txt2

        def getCollection(object,depth=0,pwstr=None):
            depth+=1
            collections=""
            for entry in object.__dict__.keys():
                element=getattr(object,entry)
                try:
                    if element.meta_type in ["ECHO_collection","ECHO_group"]:
                        collections+="<element name=\""+urllib.quote(element.title)+"\" url=\""+addPassWd(element.absolute_url(),pwstr=pwstr)+"\">"
                        collections+=getCollection(element,depth)+"</element>\n"
                except:
                    """nothing"""
            return collections
        
        ret="""<?xml version="1.0" encoding="utf-8" ?>"""
        return ret+"<collection>"+getCollection(self,pwstr=pwstr)+"</collection>"
    
    def createAllJSAreas(self,mapColTypes=None):
        """create area calls for JavaScript"""
        areas = self.getAllMapAreas(mapColTypes=mapColTypes)
        return self.createJSAreas(areas)

    
    emptyPartner=ECHO_partner("", "","", "", "", "", "", "", "")
    
    security.declarePublic('getCreditObject')
    def getCreditObject(self,name):
        """credit id to credititem"""
        try:
            return getattr(self.partners,name)
        except:
            
            return self.emptyPartner
                
    
    security.declarePublic('ECHO_generateNavBar')
    def ECHO_generateNavBar(self):
        """Erzeuge Navigationsbar"""
        link=""
        object="self"
        ret=[]
        path=self.getPhysicalPath()
        for element in path:
            
           
            if not element=="":
                object+="."+element
                
                label=eval(object).label
                link+="/"+element
                if not label=="":
                    ret.append((label,link))
        return ret
    

    def ECHO_rerenderLinksMDWarning(self):
                """change form"""
                pt=zptFile(self, 'zpt/rerenderLinksWarning')
                pt.content_type="text/html"
                return pt()


    
    security.declarePublic('ECHO_rerenderLinksMD')
    def ECHO_rerenderLinksMD(self,obj=None,types=['title','label']):
        """Rerender all Links"""
        return ECHO_rerenderLinksMD(self,obj,types)

        
    def __init__(self,id,title,label,description="",contentType="",responsible="",credits="",weight="",sortfield="",coords=[],secondaryLinkTitle="",secondaryLink="",imageTag="",bgcolour="",isVisible=True):

        self.id = id
        """Festlegen der ID"""
        self.credits=toList(credits)
        self.label = label
        self.title=title
        self.description=description
        self.contentType=contentType
        self.responsible=responsible
        self.imageTag=imageTag
        self.weight=weight
        self.sortfield=sortfield
        coordsnew=[ string.split(x,",") for x in coords]
        self.coords=coordsnew
        self.secondaryLinkTitle=secondaryLinkTitle
        self.secondaryLink=secondaryLink
        self.bgcolour=bgcolour
        self.isVisible=isVisible
        

    manage_options = Folder.manage_options+ Cacheable.manage_options+(
        {'label':'Main Config','action':'ECHO_collection_config'},
        {'label':'Change Labels','action':'changeLabels'},
        {'label':'Change Titles','action':'changeTitles'},
        {'label':'Localize','action':'localizeObjects'},
        {'label':'Change Weights','action':'changeWeights'},
        {'label':'Rerender Labels and Titles','action':'ECHO_rerenderLinksMDWarning'},
        {'label':'Export/Import Objects','action':'exportImportObjects_html'},
        {'label':'Graphic Coords','action':'ECHO_graphicEntry'},
        {'label':'create resources from XML','action':'createRessourcesFromXMLForm'},
        {'label':'Set Startpage','action':'setStartpageFolderForm'},
        {'label':'Change Viewer Templates and Image Viewer','action':'changeViewerTemplateSetsForm'},
        {'label':'Reload Metadata','action':'reloadMetaDataFromStorageWarning'},
        {'label':'ImportCollection','action':'importCollection'},
        {'label':'Copy MD for indexing and search','action':'copySearchFields'},
        {'label':'Change access rights',  'action':'changeAccessRightsCollectionForm'},
        )

    
    security.declarePublic('ECHO_collection_config')
    def ECHO_collection_config(self):
        """Main configuration"""

        if not hasattr(self,'weight'):
            self.weight=""

        if not hasattr(self,'sortfield'):
            self.sortfield="weight"
  
        pt=zptFile(self, 'zpt/ChangeECHO_collection.zpt')
        return pt()


    def getBgcolour(self):
            """colour"""
            if hasattr(self,'bgcolour') and not (self.bgcolour==""):
                    return self.bgcolour
            else:
                    return "#dddddd"

                
    security.declarePublic('changeECHO_collection')         
    def changeECHO_collection(self,title,label,description,contentType,responsible,weight,secondaryLink,secondaryLinkTitle,credits=None,sortfield="weight",coords=None,RESPONSE=None,imageTag="",bgcolour="",location=None,isAlwaysClickable=None,prefix="",suffix="",isVisible=True):
        """Aenderung der Properties"""

        self.secondaryLink=secondaryLink
        self.secondaryLinkTitle=secondaryLinkTitle
        self.imageTag=imageTag
        self.bgcolour=bgcolour
        self.location=location
        self.isAlwaysClickable=isAlwaysClickable
        self.prefix=prefix[0:]
        self.suffix=suffix[0:]
        self.setIsVisible(isVisible)
        
        setECHO_collectionInformation(self,title,label,description,contentType,responsible,credits,weight)
                
        self.sortfield=sortfield

        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
            
    def setAlwaysClickable(self,flag="yes"):
            """set clickable"""
            if flag=="yes":
                    self.isAlwaysClickable="yes"
            else:
                    self.isAlwaysClickable=None

            return flag
    
    def showOverview(self):
        """overview"""
        if 'ECHO_overview.html' in self.__dict__.keys():
            return getattr(self,'ECHO_overview.html')()
        pt=zptFile(self, 'zpt/ECHO_content_overview.zpt')
        return pt()


    security.declareProtected('View','index_html')
    def index_html(self):
        """standard page"""
        if self.ZCacheable_isCachingEnabled():
            
            result = self.ZCacheable_get()
            if result is not None:
                # Got a cached value.
                return result
        
        if 'index.html' in self.__dict__.keys():
            ret=getattr(self,'index.html')()
        
        elif 'overview' in self.__dict__.keys():
            ret=self.showOverview()
        elif hasattr(self,'collection_index_template'):
            ret=self.collection_index_template()    
        elif hasattr(self,'main_index_template'):

            ret=self.main_index_template.__of__(self)(self.main_template)
        else:
            pt=zptFile(self, 'zpt/ECHO_main_index_template_standard.zpt')
            pt.content_type="text/html"
            ret=pt.render()

        self.ZCacheable_set(ret)
        return ret

    security.declarePublic('content_html')
    def content_html(self,**argv):
        """template fuer content"""
        #print "NN",argv
        ret = ECHO_basis.content_html(self,'collection')
        
        if type(ret) is StringType:
               return ret.decode('utf-8')
        else:
               return ret
        
             
    def getCredits(self):
        """Ausgabe der credits"""
        if self.credits:
            return self.credits
        else:
            return []

    def area_img(self):
        """area image"""
        bt = BrowserCheck(self)
        if bt.isIE or bt.isN4:
            return sendFile(self, 'images/red.gif', 'image/gif')
        else:
            return sendFile(self, 'images/reda.png', 'image/png')

    def trans_img(self):
        """empty image"""
        return sendFile(self, 'images/trans.gif', 'image/gif')

    def hl_lib_js(self):
        """javascript"""
        return sendFile(self, 'js/hl_lib.js', 'text/plain')

    def js_lib_js(self):
        """javascript -- old name"""
        return sendFile(self, 'js/baselib.js', 'text/plain')

    def baselib_js(self):
        """javascript"""
        return sendFile(self, 'js/baselib.js', 'text/plain')

    def hl_add_js(self):
        """javascript"""
        return sendFile(self, 'js/hl_add.js', 'text/plain')

    def getAllMapAreas(self,mapColTypes=None):
        """Give list of coordinates"""
        if mapColTypes is None:
            mapColTypes=['ECHO_collection','ECHO_resource','ECHO_link','ECHO_externalLink']
            
        areas=[]
        for entry in self.getSubCols(subColTypes=mapColTypes):
            object=entry
            areas.extend(object.getMapAreas())
        return areas
    

    def deleteMapAreas(self):
        """deletes all map areas from this object"""
        for obs in self.ZopeFind(self, obj_metatypes=['MapArea'], search_sub=1):
            ob = obs[1]
            id = ob.id
            parent = ob.aq_parent
            print "deleting: ", ob.id
            parent._delObject(id)
        return "Done"


    getSubCols = ECHO_helpers.getSubCols
    
    def getSubcolsXMLRpc(self,searchSimple):
        """simplesearch results suitable for xml rpc, gives back array objid,url"""
        
        return [x.absolute_url() for x in self.getSubCols(searchSimple=searchSimple)]
    
    
Globals.InitializeClass(ECHO_collection)
    
def manage_addECHO_collectionForm(self):
        """Add collection form"""
        pt=zptFile(self, 'zpt/AddECHO_collectionForm.zpt')
        return pt()


def manage_addECHO_collection(self,id,title,label,description="",contentType="",responsible="",weight=0,sortfield="weight",coords="",secondaryLinkTitle="",secondaryLink="",credits=None,RESPONSE=None,imageTag="",bgcolour="",isVisible=True):
    """add a echo collection"""
    

    newObj=ECHO_collection(id,title,label,description,contentType,responsible,credits,weight,sortfield,coords,secondaryLinkTitle=secondaryLinkTitle,secondaryLink=secondaryLink,imageTag=imageTag,bgcolour="",isVisible=isVisible)

    self._setObject(id,newObj)

    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

class ECHO_group(ECHO_collection):
        """ECHO Gruppe"""
        security=ClassSecurityInfo()
        meta_type="ECHO_group"

        manage_options = Folder.manage_options+(
                {'label':'Main Config','action':'ECHO_group_config'},
                {'label':'Rerender Links','action':'ECHO_rerenderLinksMDWarning'},
                {'label':'Graphic Coords','action':'ECHO_graphicEntry'},
                )
        
        security.declareProtected('View','index_html')


        def getRDF(self,urn=None):
            """rdf of the collection"""
            contents=self.ZopeFind(self,obj_metatypes=['ECHO_group','ECHO_resource','ECHO_collection'])

            ret=getRDFDescription(self,self.absolute_url(),urn=urn)

            
            if not urn:
                    urn=self.absolute_url()
            li="""<rdf:li rdf:resource="%s" />\n"""
            
            
            for content in contents:
                    ret+=self.unicodify(content[1].getRDF())+"\n"
                    
            ret+="""<rdf:Seq rdf:about="%s">\n"""%urn
            for content in contents:
                    nurn=content[1].absolute_url()
                    ret+=li%nurn
            return ret+"</rdf:Seq>"
            
        def index_html(self):
                """standard page"""
                displayedObjects=self.ZopeFind(self,obj_metatypes=displayTypes)
                #if (len(displayedObjects)==1) and (displayedObjects[0][1].meta_type=="ECHO_collection"): # nur ein Object dann redirect auf dieses Object
                #       return self.REQUEST.RESPONSE.redirect(displayedObjects[0][1].absolute_url())
                
                if 'index.html' in self.__dict__.keys():
                        return getattr(self,'index.html')()
                
                elif 'overview' in self.__dict__.keys():
                        return self.showOverview()
                elif hasattr(self,'group_index_template'):
                        return self.group_index_template()
                elif hasattr(self,'collection_index_template'):
                        return self.collection_index_template()    
                elif hasattr(self,'main_index_template'):
                    return self.main_index_template()    

                pt=zptFile(self, 'zpt/ECHO_main_index_template_standard.zpt')
                pt.content_type="text/html"
                return pt()

        def ECHO_group_config(self):
                """Main configuration"""
                
                if not hasattr(self,'weight'):
                        self.weight=""
                        
                if not hasattr(self,'sortfield'):
                        self.sortfield="weight"
                                
                if not hasattr(self,'coords'):
                        self.coords=[]

                pt=zptFile(self, 'zpt/ChangeECHO_group.zpt')
                return pt()

        def changeECHO_group(self,title,label,description,contentType,responsible,weight,secondaryLink,secondaryLinkTitle,credits=None,sortfield="weight",coords=None,RESPONSE=None,imageTag="",bgcolour="",logo=""):
                """Aenderung der Properties"""

                self.secondaryLink=secondaryLink
                self.secondaryLinkTitle=secondaryLinkTitle
                self.imageTag=imageTag
                self.bgcolour=bgcolour
                self.logo=logo
                
                setECHO_collectionInformation(self,title,label,description,contentType,responsible,credits,weight)



                self.sortfield=sortfield

                if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')

        def getLogo(self):    
                """logo ausgeben"""
                try:
                    return self.logo
                except:
                    return "ECHO_groups"

        security.declarePublic('content_html')
        def content_html(self):
                """template fuer content"""
                return ECHO_basis.content_html(self,'group')
    


def manage_addECHO_groupForm(self):
        """Add group form"""
        pt=zptFile(self, 'zpt/AddECHO_groupForm.zpt')
        return pt()


def manage_addECHO_group(self,id,title,label,description,contentType,responsible,weight,sortfield,coords="",secondaryLinkTitle="",secondaryLink="",credits=None,RESPONSE=None,imageTag="",bgcolour="",logo=""):
    """add a echo group"""
    

    newObj=ECHO_group(id,title,label,description,contentType,responsible,credits,weight,sortfield,coords,secondaryLinkTitle=secondaryLinkTitle,secondaryLink=secondaryLink,imageTag=imageTag,bgcolour="")

    setattr(newObj,'logo',logo)
    self._setObject(id,newObj)
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

Globals.InitializeClass(ECHO_group)


class ECHO_userFolder(UserFolder):
        """User folder for Intranet"""
        _domain_auth_mode=1 # Identification via domain
        meta_type="ECHO_userFolder"
        
        def authenticate(self, name, password, request):
                emergency = self._emergency_user
                if name is None:
                    return None
                if emergency and name==emergency.getUserName():
                    user = emergency
                else:
                    user = self.getUser(name)
                if user is not None and user.authenticate(password, request):
                    return user
                else:
                    return None

        def domainSpecMatch(self,spec, request):
            host=''
            addr=''

            # Fast exit for the match-all case
            if len(spec) == 1 and spec[0] == '*':
                return 1

            if request.has_key('REMOTE_HOST'):
                host=request['REMOTE_HOST']

            if request.has_key('REMOTE_ADDR'):
                addr=request['REMOTE_ADDR']

            if request.has_key('HTTP_X_FORWARDED_FOR'):
                addr=request['HTTP_X_FORWARDED_FOR']

            
            if not host and not addr:
                return 0

            if not host:
                try:    host=socket.gethostbyaddr(addr)[0]
                except: pass
            if not addr:
                try:    addr=socket.gethostbyname(host)
                except: pass


            _host=host.split('.')
            _addr=addr.split('.')
            _hlen=len(_host)
            _alen=len(_addr)

            for ob in spec:
                sz=len(ob)
                _ob=ob.split('.')
                _sz=len(_ob)

                mo = addr_match(ob)
                if mo is not None:
                    if mo.end(0)==sz:
                        fail=0
                        for i in range(_sz):
                            a=_addr[i]
                            o=_ob[i]
                            if (o != a) and (o != '*'):
                                fail=1
                                break
                        if fail:
                            continue
                        return 1

                mo = host_match(ob)
                if mo is not None:
                    if mo.end(0)==sz:
                        if _hlen < _sz:
                            continue
                        elif _hlen > _sz:
                            _item=_host[-_sz:]
                        else:
                            _item=_host
                        fail=0
                        for i in range(_sz):
                            h=_item[i]
                            o=_ob[i]
                            if (o != h) and (o != '*'):
                                fail=1
                                break
                        if fail:
                            continue
                        return 1
            return 0

# ROC: problem with 2.12
#Globals.default__class_init__(ECHO_userFolder)



def manage_addECHO_userFolder(self,dtself=None,REQUEST=None,**ignored):
    """add a user folder """
    f=ECHO_userFolder()
    self=self.this()
    try:    self._setObject('acl_users', f)
    except: return MessageDialog(
                   title  ='Item Exists',
                   message='This object already contains a User Folder',
                   action ='%s/manage_main' % REQUEST['URL1'])
    self.__allow_groups__=f
    if REQUEST is not None:
        REQUEST['RESPONSE'].redirect(self.absolute_url()+'/manage_main')

def manage_addECHO_userFolderForm(self):
        """add a user folder form"""
        return manage_addECHO_userFolder(self)



def createNode(self,descrs,node):
        name=descrs[node]['name']
        type=descrs[node]['type']
        urn=node
        #print "    will create",node.encode('utf-8')
        id=re.sub('[^a-zA-Z0-9]','',name).encode('ascii','ignore')
        #print "id",id
        #print type
        #self.REQUEST.RESPONSE.write("<p>%s<p>\n"%id)
        if type=="CDLI_group":
                
                try:
                        manage_addECHO_collection(self,id,name,name,"","","","","")
                
                except:
                        self.REQUEST.RESPONSE.write("<p>Error%s</p>\n"%id)
                        
                self.REQUEST.RESPONSE.write("<p>Creates:%s</p>\n"%getattr(self,id).absolute_url())
                
                return type,getattr(self,id),urn

        if type=="CDLI_item":
                try:
                        manage_addECHO_resource(self,id,name,name,"","",urn,"","")
                except:
                        self.REQUEST.RESPONSE.write("<p>Error%s</p>\n"%id)
                self.REQUEST.RESPONSE.write("<p>Creates:%s</p>\n"%getattr(self,id).absolute_url())
                
                
        return "XX"


class ECHO_root(Folder,Persistent,Implicit):
    """ECHO Root Folder"""

    security=ClassSecurityInfo()
    
    meta_type="ECHO_root"
    management_page_charset="utf-8"
    
    getSubCols = ECHO_helpers.getSubCols

    manage_options=Folder.manage_options+(
                {'label':'Main Config','action':'ECHO_copyright_configForm'},
                {'label':'Reload Metadata','action':'reloadMetaDataFromStorageWarning'},
                {'label':'Change Weights','action':'changeWeights'},
                {'label':'Generate from RDF','action':'generateFromRDFForm'},
                {'label':'update Resource Catalog','action':'updateResourceCatalog'},
                {'label':'Copy MD for indexing and search','action':'copySearchFields'},
)


    def getECHORootURL(self):
        return self.absolute_url()
    
    def getECHORoot(self):
        return self
    
    def copySearchFields(self,RESPONSE=None):
            """copys < metadatafields to the object"""
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1)

            for resource in resources:

                    resource[1].copySearchFields()

            if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')
   
    def mod_re_sub(self,pattern,replace,string):
        """re.sub aus mod re zur Verfuegung stellen"""
        return re.sub(pattern,replace,string)
    
    def findObjectFromFulltext(self,existUri):
        '''
        
        @param url:
        @param existUri:
        '''

        if existUri:
            #TODO: at the moment assume that the filename is unique, the whole has to changed to a system of ids.
            filename=existUri.split("/")[-1]
        
        founds=self.resourceCatalog.search({'fullTextUrl':os.path.splitext(filename)[0]})  
        
        ret=[(found.title,found.getObject().absolute_url(),found.getObject().aq_parent.absolute_url()) for found in founds]
        
        return ret
        
    def reindex(self,RESPONSE=None):
        """indiziere alle Objecte neu"""
        
        if RESPONSE:
            RESPONSE.write("<html><body>")
        
        resources=self.ZopeFind(self,obj_metatypes=["ECHO_resource"], search_sub=1)
        
        for resource in resources:
            resource[1].reindex()
            #print "<p> done %s </p>\n"
            if RESPONSE:
                RESPONSE.write("<p> done %s </p>\n"%resource[0])
                
        if RESPONSE:
            RESPONSE.write("<p> ok </p></html></body>\n")
            

    def setLanguage(self,lang):
            """Set language cookie"""
            self.REQUEST.RESPONSE.setCookie('lang_exhibition',lang,path="/")

    def switchLanguage(self):
            """Set language cookie"""
            if self.getLanguage()=="en":
                    lang="de"
            else:
                    lang="en"
            
            self.REQUEST.RESPONSE.setCookie('lang_exhibition',lang,path="/")
            self.REQUEST.RESPONSE.redirect(self.REQUEST['URL1'])
            
    def getLanguage(self):
            """get language cookie"""
            lang= self.REQUEST.cookies.get('lang_exhibition','de')
            if lang == '':
                    lang="de"
            return lang
    
    def getContentOverviewTemplate(self):
        """produces overview template with macro"""
        pt = zptObjectOrFile(self, 'content_overview_template')
        return pt

    def mapstyle_css(self):
        """send mapstyle.css"""
        sendFile(self, 'html/ECHO_mapstyle.css', 'text/css')


    ###Cdli adds -> have to be removed
    def getTablet(self,item):
        #print "getTablet"
        try:
                read=urllib.urlopen("http://enlil.museum.upenn.edu/cgi-bin/cdlget.plx?item=%s&project=ncdl"%item).read()
                read=re.sub("\[search\]","search",read)
                return read[read.find("<body>")+6:read.rfind("</body>")]
        except:
                return "<h1>Sorry no connection to the data server enlil.museum.upenn.edu</h1>"
        #return "http://enlil.museum.upenn.edu/cgi-bin/cdlget.plx?item=%s&project=ncdl"
    ###END CDLI add


    def URLquote(self, text):
        """urllib.quote fuer Michael"""
        return urllib.quote(text)


    def checkResource(self,id):
            """checks if a resource is in the tree, gives back none or list of resources"""
            if not id:
                id=""
            splitted=id.split("/")
            id=splitted[len(splitted)-1]
            if hasattr(self,"_v_checkResource") and self._v_checkResource.has_key(id): #existiert ein cache und id ist bereits drin?
                            return self._v_checkResource[id]

            else:
                    resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'],obj_ids=[id],search_sub=1)

                    if not hasattr(self,"_v_checkResource"): self._v_checkResource={}#lege cache an falls nicht existent
                    if resources:
                            self._v_checkResource[id]=resources[0:] # slicing to be sure that data is stabil
                    else:
                            self._v_checkResource[id]=None
                    
                    return self._v_checkResource[id]
            
    def sendForm(self,fromaddr,content,server='mail.mpiwg-berlin.mpg.de'):
            """sendform"""
            toaddrs=["dwinter@mpiwg-berlin.mpg.de"]
            
            msg = ("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n"
                   % (fromaddr, ", ".join(toaddrs),"testsub"))
            server = smtplib.SMTP(server)
            #server.set_debuglevel(1)
            msg=msg+content
            server.sendmail(fromaddr, toaddrs, msg)
            server.quit()
                 
    def generateFromRDFForm(self):
                """change form"""
                pt=zptFile(self, 'zpt/generateFromRDFForm')
                pt.content_type="text/html"
                return pt()

    def generateFromRDF(self,file,startNode="/Cuneiform Corpus"):

            """generate from RDF"""

            global seqs
            seqs={}
            global descrs
            descrs={}
            global key
            key=""
            global value
            value=""

            def getEdges(seqs,urn):
                    """edges"""
                    ret=[]
                    return seqs[urn]

            def createSubs(self,seqs,descrs,urn,level=0):
                    """create subs"""
                    for edge in getEdges(seqs,urn):
                            cn=createNode(self,descrs,edge)
                            if cn[0]=="CDLI_group":
                                    createSubs(cn[1],seqs,descrs,cn[2],level+1)
                    return

            def start_element(name,attrs):

                    global seqs
                    global descrs
                    global key
                    global value
                    seq=""
                    if name=="rdf:Seq":
                            key=attrs.get('rdf:about')
                            try: # teste ob liste
                                    x=seqs[key][0]
                            except:

                                    seqs[key]=[]

                    
                    elif name=="rdf:Description":
                            key=attrs.get('rdf:about')
                            

                    elif name=="rdf:li":
                            name=attrs.get('rdf:resource')
                            seqs[key].append(name)

                    elif name=="echonavigation:type":
                            value="type"

                    elif name=="echonavigation:name":
                            value="name"
                    elif name=="echonavigation:linkClickable":
                            value="linkClickable"
                            
            def end_element(name):
                            """nothing"""
                            key=""
                            value=""

            def char_data(data):
                    """nothing"""

                    data=re.sub("\n","",data)
                    try:
                            if descrs[key].has_key(value):
                                    descrs[key][value]+=data
                            else:
                                    descrs[key][value]=data
                    except:

                            descrs[key]={}
                            descrs[key][value]=data
                            
            p = xml.parsers.expat.ParserCreate()
            
            p.StartElementHandler = start_element
            p.EndElementHandler = end_element
            p.CharacterDataHandler = char_data
            
            
            p.ParseFile(file)
            self.REQUEST.RESPONSE.write("<html><body><h1>Start</h1>")
            createSubs(self,seqs,descrs,startNode)
            self.REQUEST.RESPONSE.write("<h1>done</h1></body></html>")
            #print "done"

            
            return "done"
    
            

                
    def changeWeightsInCollection(self):
            """change all lables of a collection"""
            ret=""
            argv=self.REQUEST.form
            
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource','ECHO_collection','ECHO_link','ECHO_externalLink'])
            for resource in resources:
                   
                    try:
                            ret+=resource[1].getId()+"   "+argv[resource[1].getId()]+"</br>"
                            resource[1].weight=argv[resource[1].getId()][0:]
                    except:
                            pass
            return ret

    def changeWeights(self):
                """change form"""
                pt=zptFile(self, 'zpt/changeWeightForm')
                pt.content_type="text/html"
                return pt()
  

    def reloadMetaDataFromStorageWarning(self,RESPONSE=None):
            """warning"""
            pt=zptFile(self, 'zpt/reloadMetaDataFromStorageWarning.zpt')
            pt.content_type="text/html"
            return pt()

    def reloadMetaDataFromStorage(self,RESPONSE=None):
            """reload MD from Storage"""

            return reloadMetaDataFromStorage(self,RESPONSE)
    
    def getRDF(self,urn=None):
            """rdf of the collection"""

            contents=self.ZopeFind(self,obj_metatypes=['ECHO_group','ECHO_resource','ECHO_collection'])

            ret=getRDFDescription(self,self.absolute_url(),urn=urn)
            
            li="""<rdf:li rdf:resource="%s" />\n"""

            
            for content in contents:
                    ret+=content[1].getRDF()+"\n"
                    
            ret+="""<rdf:Seq rdf:about="%s">\n"""%urn
            for content in contents:
                    nurn=content[1].absolute_url()
                    ret+=li%nurn
            return ret+"</rdf:Seq>"
            

    def showContent(self,path):
            """return content/html"""
            
            return ECHO_helpers.urlopen(path+"/content_html").read()
    
    def getImageViewers(self):
        """images"""
        viewers=self.ZopeFind(self.standardImageViewer,obj_metatypes=['OSAS_ViewerObject'])
        return viewers
    

    def getBibTag(self,tag,content):
            """get field tag for index-meta-generation"""
            if not content or content=="":
                    return ""
            ret="<%s>"%tag
            #ret+=urllib.quote(content)
            ret+=content

            ret+="</%s>"%tag
            return ret

    def getValueFromClass(self,field,found):
            """ret attribute if existing"""
            try:
                    

                    return getattr(found,field)#.decode('ascii','ignore')



            except:
                    logging.error("can't: decode: %s"%repr(field))
                    logging.error("      %s %s"%(sys.exc_info()[0],sys.exc_info()[1]))
                    return ""

    security.declarePublic('getImageTag')
    def getImageTag(self):
            """needed by main_template"""
            return ""
        
    secondaryLink="" #needed by main_template
    secondaryLinkTitle="" #needed by main_template
    
    def getBgcolour(self):
        """hack"""
        return "#dddddd"

    security.declareProtected('View','contentTypeSelector_HTML')
    def contentTypeSelector_HTML(self,selected=None):
        """give type selector"""
        if not selected:
            retStr="<option selected>\n"
        else:
            retStr="<option>\n"
            
        try: # erste version contentTypes exists
            for contentType in self.ZopeFind(self.contentTypes,obj_metatypes=["ECHO_contentType","OSAS_MetadataMapping"]):
                if selected and (contentType[0]==selected):
                    retStr+="""<option selected value="%s">%s\n"""%(contentType[0],contentType[0])
                else:                
                    retStr+="""<option value="%s">%s\n"""%(contentType[0],contentType[0])
        except:
                try:
                        for contentType in self.ZopeFind(self.standardMD,obj_metatypes=["OSAS_MetadataMapping"]):
                                if selected and (contentType[0]==selected):
                                        retStr+="""<option selected value="%s">%s\n"""%(contentType[0],contentType[0])
                                else:                
                                        retStr+="""<option value="%s">%s\n"""%(contentType[0],contentType[0])
                except:
                        """nothing"""
                            
        return retStr

    def renderingTypeSelector_HTML(self,selected=None):
         """give type selector"""
         if not selected:
                 retStr="<option selected>\n"
         else:
                 retStr="<option>\n"
                 
         try: # erste version renderingTypes exists
                 for renderingType in self.ZopeFind(self.renderingTypes,obj_metatypes=["ECHO_renderingType"]):
                         if selected and (renderingType[0]==selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(renderingType[0],renderingType[0])
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(renderingType[0],renderingType[0])
         except:
                 """nothing"""
         return retStr


    def copyrightTypeSelector_HTML(self, object=None, selected=None,first=None):
         """give type selector"""

         if not first:
                 if not selected:
                         retStr="<option selected>\n"
                 else:
                         retStr="<option>\n"
         else:
                 if not selected:
                         retStr="""<option selected value="%s">%s\n"""%first
                 else:
                         retStr="""<option value="%s">%s\n"""%first

         
         try: # erste version copyrightTypes exists
                 for copyrightType in self.ZopeFind(self.copyrightTypes,obj_metatypes=["ECHO_copyrightType"]):
                         if selected and (copyrightType[0]==selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(copyrightType[0],copyrightType[0])
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(copyrightType[0],copyrightType[0])
                 
                 for copyrightTypeSelf in self.ZopeFind(object,obj_metatypes=["ECHO_copyrightType"],search_sub=1):
                         if selected and (copyrightTypeSelf[0]==selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(copyrightTypeSelf[0],copyrightTypeSelf[0])
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(copyrightTypeSelf[0],copyrightTypeSelf[0])
                                 
         except:
             """nothing"""
                 
         return retStr

    def partnerSelector_HTML(self,selected=None):
         """give type selector"""
         if not selected:
                 retStr="<option selected>\n"
         else:
                 retStr="<option>\n"
                 
         try: # erste version copyrightTypes exists
                 for copyrightType in self.ZopeFind(self.partners,obj_metatypes=["ECHO_partner"]):
                         if selected and (copyrightType[0]==selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(copyrightType[0],copyrightType[0])
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(copyrightType[0],copyrightType[0])
         except:
                 """nothing"""
         return retStr
 
    mediaTypes=['all','text','images','sound','video'] # konfigurierbar in neuer version ueber echoroot??

    def mediaTypeSelector_HTML(self,selected=None):
         """give type selector"""
         if not selected:
                 retStr="<option selected>\n"
         else:
                 retStr="<option>\n"
         
         try: # erste version mediatypesTypes exists
                 for mediaType in self.mediaTypes:
                         if selected and (mediaType in selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(mediaType,mediaType)
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(mediaType,mediaType)
         except:
                 """nothing"""
         return retStr

            
    def patchContentType(self,obj=None):
        """austauschen content_type with contentType (patch bei umstieg von alter Version)"""
    

        if not obj:
            obj = self
            
        entries=obj.ZopeFind(obj,obj_metatypes=['ECHO_resource','ECHO_collection','ECHO_externalLink','ECHO_pageTemplate'])

        for entry in entries:
                setattr(entry[1],'contentType',entry[1].content_type)
                #entry[1].contentType == entry[1].content_type

                if entry[1].meta_type == 'ECHO_collection':
                    entry[1].patchContentType(entry[1])    

                
        return "changed all contenttypes in: "+self.title


    def repairAllCoords(self):
        """updates map coordinates on the same and sublevels"""
        return repairCoords(self)


    def patchViewClassification(self,obj=None):
        """setze viewClassification heuristisch"""

        def checkIfArrow(obj):
                if hasattr(obj,'coords'):
                        for coordtemp in obj.coords:

                                if (len(coordtemp)>4) and not (coordtemp[4]==''):
                                        return 4
                        return None
                return None
        
        if not obj:
            obj = self
            
        entries=obj.ZopeFind(obj,obj_metatypes=['ECHO_resource','ECHO_collection','ECHO_group'])

        for entry in entries:
                
                if checkIfArrow(entry[1]):
                        setattr(entry[1],'viewClassification','view point')
                else:
                        setattr(entry[1],'viewClassification','area')

                #entry[1].contentType == entry[1].content_type

                if entry[1].meta_type in ['ECHO_collection','ECHO_group']:
                    entry[1].patchViewClassification(entry[1])    

                
        return "changed all contenttypes in: "+self.title

    def deleteCache(self,obj=None,RESPONSE=None):
        """setze alle collections auf cache = CacheManager"""
        if not obj:
            obj = self
        entries=obj.ZopeFind(obj,search_sub=1)
        for entry in entries:
                if hasattr(entry[1],'_v_hash'):
                        entry[1]._v_hash=None
        
        return "changed all CM in: "+self.title

    
    security.declarePublic('ECHO_newViewerLink')
    def ECHO_newViewerLink(self,obj=None):
        """change links (:86 faellt weg)"""

        if not obj:
            obj = self
            
        entries=obj.ZopeFind(obj,obj_metatypes=['ECHO_resource','ECHO_collection'])

        for entry in entries:
                
                if entry[1].meta_type == 'ECHO_resource':
                    
                    entry[1].link=re.sub('\:86','',entry[1].link)

                else:
                    
                    entry[1].ECHO_newViewerLink(entry[1])
                
        return "Rerenderd all links to resources in: "+self.title

    def __init__(self,id,title):
        """init"""
        self.id = id
        self.title=title

    def deleteSpace(self,str):
        """delete space at the end of a line"""
        if str[len(str)-1]==" ":
            return str[0:len(str)-1]
        else:
            return str
        
    

    # zusaetliche methoden fuer das vlp muessen in ein eigenes produkt

    def formatAscii(self,str,url=None):
        """ersetze ascii umbrueche durch <br>"""

	if not str: 
            return ""

        if url:
            
            retStr=""
            words=str.split("\n")
            
            for word in words:
                strUrl=url%word

                retStr+="""<a href="%s">%s</a><br/>"""%(strUrl,word)
            str=retStr
        if str:
            str = re.sub(r"[\n]","<br/>",str)
            return unicodify(str)
        else:
            return u""
        
    link2html=vlp_xmlhelpers.link2html
    related2html=vlp_xmlhelpers.related2html

    xml2html=vlp_xmlhelpers.xml2html
 
    checkRef=vlp_xmlhelpers.checkRef
    
#    def checkRef(self,ref):
#            if ref[0:3]=='lit':
#                    if len(self.library_data({ 'id':ref}))>0:
#                            return 1
#            try:
#                    if ref[0:7]=="tec_cat":
#                            return 1
#            except:
#                    """nothing"""
#                    
#            dbs={'vl_technology':'','vl_people':" AND complete='yes'",'vl_sites':''}
#            res=None
#            for db in dbs.keys():
#                    res=res or self.search(var=str("select reference from %s where reference =\'%s\' %s"%(db,ref,dbs[db])))
#            return res
#                                    
#    #Ende Methode fuer vlp

    def PgQuoteString(self,string):
        """Quote string"""

        return libpq.PgQuoteString(string)

    security.declareProtected('View','getPartners')
    def getPartners(self):
        """Get list of Partners. Presently only from a subfolder partners"""
        if hasattr(self,'partners'):            
                ret = [ item[1] for item in self.partners.ZopeFind(self.partners,obj_metatypes=['ECHO_partner'])]
                def sort_title(one, another) : return cmp(one.title, another.title)
                ret.sort(sort_title)
                return ret
        else:
                return []
  
    def getInstitutions(self):
        """Get list of Partners. Presently only from a subfolder partners"""
                    
        return [ item[1] for item in self.institutions.ZopeFind(self.institutions,obj_metatypes=['ECHO_institution'])]

   
    def getPartnersXML(self):
        """partner liste als xml""" 
        partners=self.getPartners()
        ret="""<?xml version="1.0" encoding="utf-8" ?>
        <partners>"""
        
        for partner in partners:
            ret+="""<partner id="%s" title="%s"/>\n"""%(partner.getId(),partner.title)

        return ret+"\n</partners>"
    
    def getCollectionTree(self):
        """get the collection tree (list of triples (parent,child, depth)"""

        def getCollection(object,depth=0):
            depth+=1
            collections=[]
            for entry in object.__dict__.keys():
                element=getattr(object,entry)
                try:
                    if element.meta_type=="ECHO_collection":
                        collections.append((object,element,depth))
                        collections+=getCollection(element,depth)
                except:
                    """nothing"""
            return collections
        

        return getCollection(self)
    
    def getCollectionTreeIds(self):
        """Show the IDs of the Tree"""
        ret=[]
        for collection in self.getCollectionTree():
            ret.append((collection[0].getId(),collection[1].getId(),collection[2]))
        return ret

    def getResourcesHTML(self,viewerType=None,filter=None):
            """gebe all ressourcen aus"""

            def sortHTML(x,y):
                    return cmp(x[1].title,y[1].title)
            
            ret="""<html><body><h2>Resources in ECHO</h3>"""
            
            resources = self.ZopeFind(self,obj_metatypes=['ECHO_resource','VLP_resource'],search_sub=1)
            ret+="""<h3>Found %i resources</h3>"""%len(resources)
            resources.sort(sortHTML)
            for resource in resources:
                    echo_url=resource[1].absolute_url()
                    
                    if hasattr(resource[1],'title'):
                            title=resource[1].title
                    else:
                            title="None"
                    if filter:
                            if re.search(filter,title):
                                    ret+="""\n<p><a href="%s">%s</a></p>"""%(echo_url,title)
                    else:
                            ret+="""\n<p><a href="%s">%s</a></p>"""%(echo_url,title)

            ret +="""\n</body></html>"""
            
            #self.REQUEST.RESPONSE.setHeader("Content-Type", "text/html")
            #self.REQUEST.RESPONSE.write(ret)
            return ret
    
    def getResourcesXML(self,viewerType=None,filter=None):
            """gebe all ressourcen aus"""
            ret="""<?xml version="1.0" ?>
                     <index>"""
            for resource in self.ZopeFind(self,obj_metatypes=['ECHO_resource','VLP_resource'],search_sub=1):

                    echo_url=resource[1].absolute_url()
                    if hasattr(resource[1],'link'):
                            viewer_url=resource[1].link
                    else:
                            viewer_url="NO URL"
                    if filter:
                            if re.search(filter,viewer_url):
                                    ret+="""\n<resource echoLink="%s" viewerLink="%s"/>"""%(urllib.quote(echo_url,safe='/:?'),urllib.quote(viewer_url,safe='/:?'))
                    else:
                            ret+="""\n<resource echoLink="%s" viewerLink="%s"/>"""%(urllib.quote(echo_url,safe='/:?'),urllib.quote(viewer_url,safe='/:?'))
            ret +="""\n</index>"""
            
            self.REQUEST.RESPONSE.setHeader("Content-Type", "text/xml")
            self.REQUEST.RESPONSE.write(ret)

    def getCollectionsXML(self,viewerType=None,filter=None):
            """gebe collections aus"""
            ret="""<?xml version="1.0" ?>
                     <index>"""
            for resource in self.ZopeFind(self,obj_metatypes=['ECHO_collection','ECHO_group'],search_sub=1):

                    echo_url=resource[1].absolute_url()
                 
                    ret+="""\n<collection echoLink="%s"/>"""%urllib.quote(echo_url,safe='/:?')
            ret +="""\n</index>"""
            
            self.REQUEST.RESPONSE.setHeader("Content-Type", "text/xml")
            self.REQUEST.RESPONSE.write(ret)

    def getFullTextsXML(self,viewerType=None,filter=None):
            """gebe all ressourcen aus"""
            ret="""<?xml version="1.0" ?>
                     <index>"""
            for resource in self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1):

                    echo_url=resource[1].absolute_url()
                    if resource[1].getFullTextXML(noredirect="yes"):
                            if hasattr(resource[1],'link'):
                                    viewer_url=echo_url+"/getFullTextXML"
                            else:
                                    viewer_url="NO URL"
                            if filter:
                                    if re.search(filter,viewer_url):
                                            ret+="""\n<resource echoLink="%s" viewerLink="%s"/>"""%(urllib.quote(echo_url,safe='/:?'),urllib.quote(viewer_url,safe='/:?'))
                            else:
                                    ret+="""\n<resource echoLink="%s" viewerLink="%s"/>"""%(urllib.quote(echo_url,safe='/:?'),urllib.quote(viewer_url,safe='/:?'))
            ret +="""\n</index>"""
            
            
            self.REQUEST.RESPONSE.setHeader("Content-Type", "text/xml")
            self.REQUEST.RESPONSE.write(ret)


    def getMetaDatasXML(self,viewerType=None,filter=None):
            """gebe all ressourcen aus"""
            # check if the request's host part was OK
            http_host = self.REQUEST['HTTP_HOST']
            host_port = self.REQUEST['SERVER_PORT']
            fix_host = None
            if http_host and http_host.rfind(host_port) == -1:
                    print "HTTP_HOST needs fixing!"
                    fix_host = http_host + ":" + host_port

            ret="""<?xml version="1.0" ?>
                     <index>"""
            for resource in self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1):

                    echo_url=resource[1].absolute_url()
                    if fix_host:
                            #print "replacing ", http_host, " by ", fix_host
                            echo_url = string.replace(echo_url, http_host, fix_host, 1)
                    
                    if hasattr(resource[1],'link'):
                            meta_url=echo_url+"/getMetaDataXML"
                    else:
                            meta_url="NO_URL"
                            
                    if filter and not re.search(filter,viewer_url):
                            continue

                    #modificationDate=time.strptime(str(resource[1].bobobase_modification_time()),"%Y/%m/%d %H:%M:%S %Z")
                    
                    modificationDate=resource[1].bobobase_modification_time().strftime("%Y/%m/%d %H:%M:%S")

                    ret+="""\n<resource resourceLink="%s" metaLink="%s" modificationDate="%s"/>"""%(urllib.quote(echo_url,safe='/:?'),urllib.quote(meta_url,safe='/:?'),modificationDate)
                    
                            
            ret +="""\n</index>"""

            self.REQUEST.RESPONSE.setHeader("Content-Type", "text/xml")
            self.REQUEST.RESPONSE.write(ret)

            
    def findPartners(self):     
        """find all partners in partners"""
        par=self.ZopeFind(self.partners, obj_metatypes='ECHO_partner')
        return par
        
    def getPartnerFromID(self):
        """find partner form ID return object"""
        pa=self.REQUEST['partner']
        par=self.ZopeFind(self.partners, obj_ids=[pa])
        return par
        
    def getPartnerFromIDParameter(self, id):
        """ find partners from ID"""
        par=self.ZopeFind(self.partners, obj_ids=[id])
        return par
        
    def getInstitutionFromID(self,id):
        """ find institution from id """
        inst=self.ZopeFind(self.institutions, obj_ids=[id])
        return inst

    def updateResourceCatalog(self,REQUEST,RESPONSE):
        """update ressource catalog"""
        catalog=getattr(self,'resourceCatalog',None)
        if not catalog:
            return """<html><body><h3>No resourceCatalog available</h3></body></html>"""
        
        else:
            class element:
                def __init__(self,group,name):
                    self.group=group
                    self.name=name
            catalog.manage_delObjects('author splitter')
            #elements=[element('Stop Words',""" Don't remove stop words' """),element('Word Splitter','ECHO author splitter')]
            elements=[element('Word Splitter','ECHO author splitter')]
            manage_addLexicon(catalog,id="author splitter",elements=elements)
            
            return catalog.manage_catalogReindex(REQUEST,RESPONSE,'manage_main')
            
        
    def checkMetaDataLinks(self,correct=None):
        """check all metadatalinks and if correct is set change the servername to content.mpiwg-berlin.mpg.de"""
        
        self.REQUEST.RESPONSE.write("<html><body>")
        for resource in self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1):
            metalink=resource[1].metalink
        
            try:
                url=urllib.urlopen(metalink)
            except:
                
                self.REQUEST.RESPONSE.write("<p>ERROR (opening %s):"%resource[1].absolute_url() +" -- %s %s <br>"%sys.exc_info()[0:2])
                self.REQUEST.RESPONSE.write("-----------%s"%metalink)
            
                if correct:
                    urlparsed=[x for x in urlparse.urlparse(metalink)]
                    urlparsed[1]="content.mpiwg-berlin.mpg.de"
                    neu=urlparse.urlunparse(urlparsed)
                    resource[1].metalink=neu[0:]
                    self.REQUEST.RESPONSE.write("----------changed to:%s)"%neu)
                                
def manage_addECHO_root(self,id,title,RESPONSE=None):
    """Add an ECHO_root"""
    self._setObject(id,ECHO_root(id,title))
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

def manage_addECHO_rootForm(self):
        """Nothing yet"""
        pt=zptFile(self, 'zpt/AddECHO_root.zpt')
        return pt()

class ECHO_copyrightType(Folder,ECHO_basis):
        """copyright typ"""

        meta_type="ECHO_copyrightType"
        
        def __init__(self,id,title,label,url):
                """init"""
                self.id=id
                self.title=title
                self.label=label
                self.url=url

        manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'ECHO_copyrightType_config_mainForm'},
        )

        def ECHO_copyrightType_config_mainForm(self):
                """change form"""
                pt=zptFile(self, 'zpt/ChangeECHO_copyrightType')
                pt.content_type="text/html"
                return pt()

        def ECHO_copyrightType_config_main(self,title,label,url,RESPONSE=None):
                """change"""
                self.title=title
                self.label=label
                self.url=url

                if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

def manage_addECHO_copyrightTypeForm(self):
        """Form for adding a ressource"""
        pt=zptFile(self, 'zpt/AddECHO_copyrightTypeForm.zpt')
        return pt()


def manage_addECHO_copyrightType(self,id,title,label,url,RESPONSE=None):
    """addaresource"""

    newObj=ECHO_copyrightType(id,title,label,url)

    self._setObject(id,newObj)

    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
 

 

class ECHO_linkList(ZopePageTemplate,ECHO_basis):
    """LinkList Objekt"""
    meta_type="ECHO_linkList"

    _default_content_fn = os.path.join(package_home(globals()), 'html/ECHO_pageTemplateDefault.html')

    manage_options = ZopePageTemplate.manage_options+(
        {'label':'Main Config','action':'changeECHO_linkListWeightForm'},
        )

    def content_html(self):
        """content_html"""
        return ECHO_basis.content_html(self,'pageTemplate')

    def changeECHO_linkListWeightForm(self):
        """change"""
        pt=zptFile(self, 'zpt/ChangeECHO_linkListTemplate.zpt')
        return pt()

    def changeECHO_linkListWeight(self,contentType,label,RESPONSE=None):
        """change"""
        self.contentType=contentType
        self.label=label
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    def getLabel(self):
        if hasattr(self,'label'):
            return self.label.encode('utf-8')
        else:
            return 0
        
    def getcontentType(self):
        """get contentType"""
        if hasattr(self,'contentType'):
            return normalizeCt(self.contentType)
        else:
            return 0

    def __init__(self, id, label, title=None, text=None, contentType=None):
         self.id = str(id)
         self.title=title
         self.label=label
         self.ZBindings_edit(self._default_bindings)
         if text is None:
             text = ''
         if content_type is None:
             content_type = self.content_type
         self.pt_edit(text, contentType)




def manage_addECHO_linkListForm(self):
    """Form for adding"""
    pt=zptFile(self, 'zpt/AddECHO_linkListTemplate.zpt')
    return pt()


def manage_addECHO_linkList(self, id, label,contentType=0,title=None, text=None,
                           REQUEST=None, submit=None):
    """Add a LinkList with optional file content."""
    
    id = str(id)
    if REQUEST is None:
        self._setObject(id, ECHO_linkList(id, label, text))
        ob = getattr(self, id)
        setattr(ob,'contentType',contentType)
        if title:
            ob.pt_setTitle(title)
        return ob
    else:
        file = REQUEST.form.get('file')
        headers = getattr(file, 'headers', None) 
        if (headers is None) or (not file.filename):
            zpt = ECHO_linkList(id, file)
        else:
            zpt = ECHO_linkList(id, label,'', file, headers.get('contentType'))

        self._setObject(id, zpt)
        ob = getattr(self, id)
        if title:
            ob.pt_setTitle(title)
        
        try:
            u = self.DestinationURL()
        except AttributeError:
            u = REQUEST['URL1']

        if submit == " Add and Edit ":
            u = "%s/%s" % (u, urllib.quote(id))
        REQUEST.RESPONSE.redirect(u+'/manage_main')
    return ''
    

class ECHO_support(Folder,ECHO_basis):
        """gefoerdert durch"""
        meta_type="ECHO_support"
        
        
        
        def __init__(self,id,institutions=None):
                """init"""
                self.id=id
                self.title=''
                self.institutions=toList(institutions)

        def getSupporter(self):
                """return institutions"""
                if self.institutions:
                        return self.institutions
                else:
                        return []
                
        manage_options = Folder.manage_options+(
                {'label':'Main Config','action':'ECHO_support_configForm'},
                )

        def ECHO_support_configForm(self):
                """change form"""
                pt=zptFile(self, 'zpt/ChangeECHO_support')
                pt.content_type="text/html"
                return pt()

        def ECHO_support_config(self,institutions=None,RESPONSE=None):
                """change"""
                self.institutions=toList(institutions)

                if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

                
def manage_addECHO_supportForm(self):
    """Form for adding"""
    pt=zptFile(self, 'zpt/AddECHO_support.zpt')
    return pt()

def manage_addECHO_support(self, id,institutions=None,RESPONSE=None):
        """add the copyright"""

        meta_type="ECHO_support"
        
        
        self._setObject(id, ECHO_support(id,institutions))

        if RESPONSE is not None:
                RESPONSE.redirect('manage_main')
                
# ECHO - owner of original
class ECHO_ownerOriginal(Folder,ECHO_basis):
        """besitzer des originals"""
        meta_type="ECHO_ownerOriginal"
        security=ClassSecurityInfo()
        
        
        def __init__(self,id,institutions=None):
                """init"""
                self.id=id
                self.title=''
                self.institutions=toList(institutions)

        security.declarePublic('getOwner')
        def getOwner(self):
                """return institutions"""
                if self.institutions:
                        return self.institutions
                else:
                        return []
                
        manage_options = Folder.manage_options+(
                {'label':'Main Config','action':'ECHO_ownerOriginal_configForm'},
                )

        def ECHO_ownerOriginal_configForm(self):
                """change form"""
                pt=zptFile(self, 'zpt/ChangeECHO_ownerOriginal')
                pt.content_type="text/html"
                return pt()

        def ECHO_ownerOriginal_config(self,institutions=None,RESPONSE=None):
                """change"""
                self.institutions=toList(institutions)

                if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

                
def manage_addECHO_ownerOriginalForm(self):
    """Form for adding"""
    pt=zptFile(self, 'zpt/AddECHO_ownerOriginal.zpt')
    return pt()

def manage_addECHO_ownerOriginal(self, id,institutions=None,RESPONSE=None):
        """add the copyright"""

        meta_type="ECHO_ownerOriginal"
        
        
        self._setObject(id, ECHO_ownerOriginal(id,institutions))

        if RESPONSE is not None:
                RESPONSE.redirect('manage_main')
                
# --------------------------------------------------------------------------------             

# ECHO - digitized copy by
class ECHO_digiCopyBy(Folder,ECHO_basis):
        """besitzer des originals"""
        meta_type="ECHO_digiCopyBy"
        security=ClassSecurityInfo()
        
        
        def __init__(self,id,institutions=None):
                """init"""
                self.id=id
                self.title=''
                self.institutions=toList(institutions)

        security.declarePublic('getDigiCopyBy')
        def getDigiCopyBy(self):
                """return institutions"""
                if self.institutions:
                        return self.institutions
                else:
                        return []
                
        manage_options = Folder.manage_options+(
                {'label':'Main Config','action':'ECHO_digiCopyBy_configForm'},
                )

        def ECHO_digiCopyBy_configForm(self):
                """change form"""
                pt=zptFile(self, 'zpt/ChangeECHO_digiCopyBy')
                pt.content_type="text/html"
                return pt()

        def ECHO_digiCopyBy_config(self,institutions=None,RESPONSE=None):
                """change"""
                self.institutions=toList(institutions)

                if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

                
def manage_addECHO_digiCopyByForm(self):
    """Form for adding"""
    pt=zptFile(self, 'zpt/AddECHO_digiCopyBy.zpt')
    return pt()

def manage_addECHO_digiCopyBy(self, id,institutions=None,RESPONSE=None):
        """add the copyright"""

        meta_type="ECHO_digiCopyBy"
        
        
        self._setObject(id, ECHO_digiCopyBy(id,institutions))

        if RESPONSE is not None:
                RESPONSE.redirect('manage_main')
                
# --------------------------------------------------------------------------------  

class ECHO_institution(Image,Persistent,ECHO_basis):
    """ECHO Institution"""

    meta_type="ECHO_institution"

    def __init__(self, id, title,url, file, person, email, country, content_type='', precondition=''):
        self.__name__=id
        self.title=title
        self.url=url
        self.person=person
        self.email=email
        self.country=country
        self.precondition=precondition
        data, size = self._read_data(file)
        content_type=self._get_content_type(file, data, id, content_type)
        self.update_data(data, content_type, size)

    manage_options = Image.manage_options+(
        {'label':'Institution Information','action':'ECHO_institution_config'},
        )

    
    def changeECHO_institution(self,url,person, email, country,RESPONSE=None):
        """Change main information"""
        self.url=url
        self.person=person
        self.email=email
        self.country=country
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
            
            

    def ECHO_institution_config(self):
        """Main configuration"""
        if not hasattr(self,'url'):
            self.url=""
        pt=zptFile(self, 'zpt/ChangeECHO_institution.zpt')
        return pt()

        
manage_addECHO_institutionForm=DTMLFile('dtml/ECHO_institutionAdd',globals(),
                             Kind='ECHO_institution',kind='ECHO_institution')



def manage_addECHO_institution(self, id, url, person, email, country, file=None, title='', precondition='', content_type='',
                    REQUEST=None):
    """
    Add a new ECHO_institution object.

    Creates a new ECHO_institution object 'id' with the contents of 'file'.
    Based on Image.manage_addImage
    """

    id=str(id)
    title=str(title)
    content_type=str(content_type)
    precondition=str(precondition)

    id, title = OFS.Image.cookId(id, title, file)

    self=self.this()

    # First, we create the image without data:
    self._setObject(id, ECHO_institution(id,title,url,'', person, email, country, content_type, precondition))

    # Now we "upload" the data.  By doing this in two steps, we
    # can use a database trick to make the upload more efficient.
    if file:
        self._getOb(id).manage_upload(file)
    if content_type:
        self._getOb(id).content_type=content_type

    if REQUEST is not None:
        try:    url=self.DestinationURL()
        except: url=REQUEST['URL1']
        REQUEST.RESPONSE.redirect('%s/manage_main' % url)
    return id

def repairCoords(self):
        """updates map coordinates to new MapArea objects"""
        msg = ""
        for cols in self.ZopeFind(self, obj_metatypes=['ECHO_collection','ECHO_resource'], search_sub=1):
            col = cols[1]
            if not hasattr(col, 'coords'):
                continue
            print "fixing: ", col.id
            msg += "fixing: "+col.id+"\n"
            coords = col.coords
            if len(coords) > 0:
                # delete old MapAreas
                for areas in self.ZopeFind(col, obj_metatypes=['MapArea']):
                    #area = areas[1]
                    id = areas[0]
                    print "  deleting: ", id
                    msg += "  deleting: "+id+"\n"
                    col._delObject(id)
                # add new MapAreas
                areacnt = 0
                for coord in coords:
                    if len(coord) < 4:
                        continue
                    type = col.viewClassification
                    if type == 'view point':
                        type = 'arrow'
                    if type is None:
                        if len(coord) > 4:
                            type = 'arrow'
                        else:
                            type = 'area'
                    newid = "a%02d"%areacnt
                    areacnt += 1
                    area = MapArea(newid, coord, type=type)
                    print  "  adding: ", newid
                    msg += "  adding: "+newid+"\n"
                    col.addMapArea(area)
        msg += "\nDone!"
        return msg

from ECHO_Nav import ECHO_navigation

class ECHO_main(ECHO_root,ECHO_navigation):
    """echo main object combines ECHO_root and ECHO_navigation"""
    
    meta_type="ECHO_main"
    
    def __init__(self,id,title):
        """init"""
        self.id=id
        self.title=title
        self.pathToHierarchy="/"+self.id # hierarchie always starts with the object.
        self.displayedMetaTypes=displayTypes
        
        
    manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'ECHO_navigationConfigForm'},
        {'label':'Cache','action':'ECHO_cacheManageForm'},
        {'label':'Main Config','action':'ECHO_copyright_configForm'},
        {'label':'Reload Metadata','action':'reloadMetaDataFromStorageWarning'},
        {'label':'Change Weights','action':'changeWeights'},
        {'label':'Generate from RDF','action':'generateFromRDFForm'},
        {'label':'update Resource Catalog','action':'updateResourceCatalog'},
        {'label':'Copy MD for indexing and search','action':'copySearchFields'},
        )

    
def manage_addECHO_mainForm(self):
    """Form for adding"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddECHO_main.zpt')).__of__(self)
    return pt()

def manage_addECHO_main(self,id,title,RESPONSE=None):
    """Add an ECHO_main"""
    self._setObject(id,ECHO_main(id,title))
    
    obj=self._getOb(id)
    text=file(os.path.join(package_home(globals()),'zpt','main_template')).read()
    
    manage_addPageTemplate(obj,'main_template',text=text)               
    
  
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
