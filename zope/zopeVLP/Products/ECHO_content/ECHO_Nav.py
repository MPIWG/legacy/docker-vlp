# Objekte zur Erzeugung von Navigationselement
from AccessControl import ClassSecurityInfo
from Globals import DTMLFile
from OFS.Image import Image,cookId
from OFS.Folder import Folder
from OFS.SimpleItem import SimpleItem
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
import re
import string
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate
import os.path
from Globals import package_home
import urllib
from ECHO_helpers import *
from ECHO_collection import content_html 
from types import *


from Globals import DTMLFile
from OFS.Image import Image,cookId
from OFS.Folder import Folder
from OFS.SimpleItem import SimpleItem
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
import re
import string
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate
import os.path

import urllib
from ECHO_helpers import displayTypes,checkOnlyOneInGroup,unicodify
from ECHO_collection import content_html
import logging

#def ersetzt logging
def logger(txt,method,txt2):
    """logging"""
    logging.info(txt+ txt2)

STORAGEMANAGER_URL_DEFAULT = "http://storage-web.mpiwg-berlin.mpg.de/storage/storeOnline?path="
try:    
    from Products.ZSQLExtend.ZSQLExtend import ZSQLExtendFolder
except:
    logger('ECHO_Nav',logging.INFO,'ZSQLSimpleSearch not supported')



class ECHO_ZCatalogElement(SimpleItem):
    """Erzeuge navigation aus ZCatalog-abfrage"""
    meta_type="ECHO_ZCatalogElement"
    baseUrl=""

    def __init__(self,id,title,field,catalogId,baseUrl,weight,contentType):
        """init"""
        self.id=id
        self.title=title
        self.catalogId=catalogId
        self.field=field

        self.baseUrl=baseUrl
        self.weight=weight
        self.content=contentType

        
    manage_options= SimpleItem.manage_options+(
        {'label':'Main Config','action':'changeECHO_ZCatalogElementForm'},
        )

        
    def getWeight(self):
        """get weight"""
        if hasattr(self,'weight'):
            return self.weight
        else:
            return 0
        
    def getcontentType(self):
        """get weight"""
        
        if hasattr(self,'contentType'):
            return self.contentType
        else:
            return 0
        

    def changeECHO_ZCatalogElementForm(self):
        """change"""
        
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','changeECHO_ZCatalogElement.zpt')).__of__(self)
        return pt()

    def changeECHO_ZCatalogElement(self,title,field,catalogId,baseUrl,weight,contentType,RESPONSE=None):
        """change"""
        self.title=title
        self.catalogId=catalogId
        self.field=field
        self.baseUrl=baseUrl
        self.weight=weight
        self.contentType=contentType
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
    def getKeywordList(self):
        try:
            return getattr(self,self.catalogId).uniqueValuesFor(self.field)
        except:
            #return getattr(self,self.catalogId).query(self.field)

            list= [x for x in getattr(getattr(self,self.catalogId),self.field).words()]
            list.sort()
            return list
    
    def getNavList(self):
        """ausgabe der treffer list fuer navigation"""
        
        records=self.getKeywordList()
        
        records=[x.encode('utf-8') for x in records]
        records.sort()
        if not records:
            return []
        else:
            ret=[]
            for record in records:
             
                #print "RC",record
                #if record:
                #    ret.append((record,self.baseUrl+urllib.quote(record)))
                try:
                    ret.append((record,self.baseUrl+urllib.quote(record)))
                except:
                    """nothing"""
                    
            return ret
    

def manage_addECHO_ZCatalogElementForm(self):
    """Form for adding"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddECHO_ZCatalogElement.zpt')).__of__(self)
    return pt()

def manage_addECHO_ZCatalogElement(self,id,title,field,catalogId,baseUrl,weight,contentType,RESPONSE=None):
    """Add an ECHO_ZCatalogelement"""

    self._setObject(id,ECHO_ZCatalogElement(id,title,field,catalogId,baseUrl,weight,contentType))
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')



class ECHO_contentType(Image):
    """ContentType Object"""

    meta_type="ECHO_contentType"

    
    manage_options=Image.manage_options+(
        {'label':'Change Description','action':'changeECHO_contentTypeForm'},
       )

    def changeECHO_contentTypeForm(self):
        """Change the description text"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','changeECHO_contentType.zpt')).__of__(self)
        return pt()
 
    def changeECHO_contentType(self,description,RESPONSE=None):
        """Change the Content"""
        self.description=description
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

manage_addECHO_contentTypeForm=DTMLFile('dtml/ECHO_contentTypeAdd',globals(),
                             Kind='ECHO_contentType',kind='ECHO_contentType')

def manage_addECHO_contentType(self, id, file=None, title='', precondition='', content_type='', description='',
                    REQUEST=None):
    """
    Add a new Image object.

    Creates a new Image object 'id' with the contents of 'file'.
    """

    id=str(id)
    title=str(title)
    content_type=str(content_type)
    precondition=str(precondition)

    id, title = cookId(id, title, file)

    self=self.this()

    # First, we create the image without data:
    self._setObject(id, ECHO_contentType(id,title,'',content_type, precondition))

    self._getOb(id).description=description
        
    # Now we "upload" the data.  By doing this in two steps, we
    # can use a database trick to make the upload more efficient.
    if file:
        self._getOb(id).manage_upload(file)
    if content_type:
        self._getOb(id).content_type=content_type

    if REQUEST is not None:
        try:    url=self.DestinationURL()
        except: url=REQUEST['URL1']
        REQUEST.RESPONSE.redirect('%s/manage_main' % url)
    return id


class ECHO_sqlElement(SimpleItem):
    """Erzeuge navigation aus sql-abfrage"""
    meta_type="ECHO_sqlElement"
    baseUrl=""


    
    def __init__(self,id,title,query,field,searchField,baseUrl,weight,contentType):
        """init"""
        self.id=id
        self.title=title
        self.query=query
        self.field=field
        if searchField=="":
            self.searchField=field
        else:
            self.searchField=searchField
        self.baseUrl=baseUrl

        self.content=contentType

    manage_options= SimpleItem.manage_options+(
        {'label':'Main Config','action':'changeECHO_sqlElementForm'},
        )

    def getSearchField(self):
        try:
            return self.searchField
        except:
            return self.field
        
    def getWeight(self):
        """get weight"""
        if hasattr(self,'weight'):
            return self.weight
        else:
            return 0
        
    def getcontentType(self):
        """get weight"""
        
        if hasattr(self,'contentType'):
            return self.contentType
        else:
            return 0
        

    def changeECHO_sqlElementForm(self):
        """change"""
        
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','changeECHO_sqlElement.zpt')).__of__(self)
        return pt()

    def changeECHO_sqlElement(self,title,query,field,searchField,baseUrl,weight,contentType,RESPONSE=None):
        """change"""
        self.title=title
        self.query=query
        self.field=field
        self.searchField=searchField
        self.baseUrl=baseUrl
        self.weight=weight
        self.contentType=contentType
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    try:    
        searchQuery=ZSQLExtendFolder.ZSQLSimpleSearch
    except:
        logger('ECHO_Nav',logging.INFO,'ZSQLSimpleSearch not supported')
            
    def getNavList(self): 
        """ausgabe der treffer list fuer navigation"""
        
        #naechste schritte sind trial and error, warum geht mal das eine mal das andere 2.3 / 2.4 ??
 
        try:
            try:
                records=self.ZSQLSimpleSearch(query=self.query)
            except:
                records=self.searchQuery(self.aq_parent,query=self.query) #python 2.4 version (??)
        except:
            records=self.searchQuery(query=self.query)#python 2.3 version(??)
      
 
        if not records:
            return []
        else:
            ret=[]
            for record in records:
                try:
                    ret.append((getattr(record,self.field),self.baseUrl+urllib.quote(str(getattr(record,self.getSearchField())))))
                except:
                    logger('ECHO_Nav',logging.INFO,"error, search")
                    """notrhing"""
                    
            return ret
    

def manage_addECHO_sqlElementForm(self):
    """Form for adding"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddECHO_sqlElement.zpt')).__of__(self)
    return pt()

def manage_addECHO_sqlElement(self,id,title,query,field,searchField,baseUrl,weight,contentType,RESPONSE=None):
    """Add an ECHO_sqlelement"""
    self._setObject(id,ECHO_sqlElement(id,title,query,field,searchField,baseUrl,weight,contentType))
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')


class ECHO_pageTemplate(ZopePageTemplate,ECHO_basis):
    """pageTemplate Objekt"""
    meta_type="ECHO_pageTemplate"
    security=ClassSecurityInfo()
    
    
    _default_content_fn = os.path.join(package_home(globals()),
                                       'html/ECHO_pageTemplateDefault.html')

    manage_options=ZopePageTemplate.manage_options+ECHO_basis.manage_options+(
        {'label':'Main Config','action':'changeECHO_pageTemplateMainForm'},
        {'label':'Graphic Coords','action':'ECHO_graphicEntry'},
        )

    security.declarePublic('content_html')

    def addChanges(self):
        """do nothing leere methode, notwendig, da bei veerbung 
        von dieser klasse nur ueberschrieben werden kann, 
        falls methode per url aufgerufen werden soll"""
        
        return True
    
    def content_html(self):
        """content_html"""
        return content_html(self,'pageTemplate')

    def changeECHO_pageTemplateMainForm(self):
        """change"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeECHO_pageTemplateMain.zpt')).__of__(self)
        return pt()

    def changeECHO_pageTemplateMain(self,weight,contentType,label,description,RESPONSE=None,isAlwaysClickable=None,suffix=None,prefix=None):
        """change"""
        self.weight=weight
        self.contentType=contentType
        self.label=label
        self.description=description
        self.isAlwaysClickable=isAlwaysClickable
        self.suffix=suffix
        self.prefix=prefix
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
     
    def getWeight(self):
        """get weight"""
        if hasattr(self,'weight'):
            return self.weight
        else:
            return 0
        
    def getcontentType(self):
        """get contentType"""
        if hasattr(self,'contentType'):
            return self.contentType
        else:
            return 0




    ## def __init__(self, id, text=None, contentType=None):
##         self.id = str(id)
##         self.ZBindings_edit(self._default_bindings)
##         if text is None:
##             text = open(self._default_cont).read()
##         self.pt_edit(text, contentType)

def manage_addECHO_pageTemplateForm(self):
    """Form for adding"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddECHO_pageTemplate.zpt')).__of__(self)
    return pt()

from urllib import quote


def manage_addECHO_pageTemplate(self, id, label, weight= 0,contentType=0,title=None, text=None,
                           REQUEST=None, submit=None):
    "Add a Page Template with optional file content."

    
    id = str(id)
    if REQUEST is None:
        self._setObject(id, ECHO_pageTemplate(id, text))
        ob = getattr(self, id)
        setattr(ob,'weight',weight)
        setattr(ob,'label',label)
        setattr(ob,'contentType',contentType)
        if title:
            ob.pt_setTitle(title)
        return ob
    else:
        file = REQUEST.form.get('file')
        headers = getattr(file, 'headers', None)
        if headers is None or not file.filename:
            zpt = ECHO_pageTemplate(id)
        else:
            zpt = ECHO_pageTemplate(id, file, headers.get('contentType'))

        self._setObject(id, zpt)
        ob = getattr(self, id)
        setattr(ob,'weight',weight)
        setattr(ob,'label',label)
        if title:
            ob.pt_setTitle(title)
        
        try:
            u = self.DestinationURL()
        except AttributeError:
            u = REQUEST['URL1']

        if submit == " Add and Edit ":
            u = "%s/%s" % (u, quote(id))
        REQUEST.RESPONSE.redirect(u+'/manage_main')
    return ''


class ECHO_navigation(Folder):
    """Navigations Element"""
    meta_type="ECHO_Navigation"
    management_page_charset="utf-8"
    
    def getUsername(self):
        """get name of user"""
        username=str(self.REQUEST['AUTHENTICATED_USER'])
        if username=='Anonymous User':
            return None
        else:
            return username
        
    def searchExternalCollection(self,url,simpleSearch):
        """search external collection via xmlrpc"""
        server=xmlrpclib.Server(url)
        
        results=server.getSubcolsXMLRpc(simpleSearch)
        
        return results
    
    def getXMLRpcConnection(self,url):
        """getxmlrpc"""
        if not hasattr(self,"_v_server"):
            self._v_server=[]
        
        server=xmlrpclib.Server(url)
        self._v_server.append(server)
        
        return len(self._v_server)-1
    
    def getXMLRpcMethod(self,nr,method,compressed=None):
        """getmethod"""
        
        ret= eval('self._v_server[%i].%s'%(nr,method))
        if compressed:
            ret=decodeRPC(ret)
        return ret
    
    def getNavInfo(self):
        """root informationen"""
        if hasattr(self,'logo_image'):
            if hasattr(self.logo_image,'tag'):
                imageTag=self.logo_image.tag()
        else:
            imageTag=''
        return (self.title,imageTag)

    def linkBar(self,parent):
        """linkbarer balken"""
        str=""

        
        while not(parent.meta_type == "ECHO_root"):
            fragment="""<a href="%s">%s</a>"""%(parent.absolute_url(),parent.label)
            str=fragment+"/"+str
            parent=parent.aq_parent

        return str
    
    def decode(self,str):
        """decoder"""
        return unicodify(str)

    def ECHOFormatListStr(self,list,search="\n",replace="<br>"):
        """formatiert liste im str"""
        if list:
                return re.sub(search,replace,list)
        else:
                return 

    def ECHO_cacheManageForm(self):
            """change form"""
            pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ECHO_cacheManageForm')).__of__(self)
            pt.content_type="text/html"
            return pt()

    def ECHO_cacheManage(self,cacheManagerName=None,deleteCache=None, activateCache=None,RESPONSE=None):
            """cachemanager"""
            if cacheManagerName=="": chacheManagerName=None
            
            self.cacheManagerName=cacheManagerName
            self.setCache(managerName=cacheManagerName)
            
            if deleteCache:
                try:
                    self.deleteCache()
                except:
                    for roots in self.ZopeFind(self,obj_metatypes=['ECHO_root'],search_sub=1):
                        roots[1].deleteCache()
                    

            self.activateCache=activateCache

            if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')
        
    def setCache(self,obj=None,RESPONSE=None,managerName='CacheManager'):
        """setze alle collections auf cache = CacheManager"""


        if not obj:
            obj = self
            
        entries=obj.ZopeFind(obj,obj_metatypes=['ECHO_collection','VLP_resource'],search_sub=1)

        for entry in entries:
            entry[1].ZCacheable_setManagerId(managerName)

        
        return "changed all CM in: "+self.title

    

    displayableMetaTypes=displayTypes
    
    def getImageViewers(self):
        """images"""
        viewers=self.ZopeFind(self.standardImageViewer,obj_metatypes=['OSAS_ViewerObject'])
        return viewers
    
    def getR(self):
        """re"""
        return self.REQUEST

    def __init__(self,id,title,pathToHierarchy):
        """init"""
        self.id=id
        self.title=title
        self.pathToHierarchy=pathToHierarchy
        self.displayedMetaTypes=displayTypes
        
    manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'ECHO_navigationConfigForm'},
        {'label':'Cache','action':'ECHO_cacheManageForm'},

        )
    displayedMetaTypes=displayTypes
    
    def getStorageManagerURL(self):
        """returns the URL to the storagemanager"""
        return getattr(self,"storageManagerURL",STORAGEMANAGER_URL_DEFAULT)
    def ECHO_navigationConfigForm(self):
        """configForm"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeECHO_navigation.zpt')).__of__(self)
        return pt()
    
    def ECHO_navigationConfig(self,title,pathToHierarchy,storageManagerURL="",displayedMetaTypes=displayTypes,RESPONSE=None):
        """config"""
        self.title=title
        self.pathToHierarchy=pathToHierarchy
        self.displayedMetaTypes=toList(displayedMetaTypes)[0:]
        self.storageManagerURL=storageManagerURL

        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def getColls(self,obj,depth,checkOnly="no"):
        nav={}
        #print "HERE",obj.
        for x_objs in obj.ZopeFind(obj,obj_metatypes=self.displayedMetaTypes):
            x=x_objs[1]
            if checkOnly=="yes":
                x = checkOnlyOneInGroup(x)
            if depth>1:
                nav[x.getId()]=(self.getColls(x,depth-1),x)
            else:
                nav[x.getId()]=(None,x)
        return nav
    
    def createNavHash(self,depth,checkOnly="no"):
        """Auslesen des Hierarchiebuames"""
        temp=self.pathToHierarchy[0:]
        temp=re.sub("/",".",temp)
        temp="self.aq_parent"+temp

        
        objtemp=eval(temp)
        nav={}
        
        #check if cash is enabled and if already exists and if not empty
        if getattr(self,'_v_hash',None) and getattr(self,'activateCache',None) and self._v_hash.get(str(depth),None):
           
           return self._v_hash[str(depth)]

        # if not generate new hash
        obj=objtemp      
        
        for keys in obj.ZopeFind(obj,obj_metatypes=self.displayedMetaTypes):

            x=keys[1]
            if depth>1:
                nav[x.getId()]=(self.getColls(x,int(depth)-1,checkOnly),x)
            else:
                nav[x.getId()]=(None,x)
            
        #store in cache if activated
        if getattr(self,'activateCache',None):
            
            try:
                self._v_hash[str(depth)]={}
            except:
                self._v_hash={}
                self._v_hash[str(depth)]={}

            for x in nav.keys():
                self._v_hash[str(depth)][x]=nav[x]

        return nav

    def barFromHash(self,hash,start=None,sortField=None,location=None):
        """Erzeuge Navigations Element"""
                
        listNav=[]
        if not start:
            temp=hash
            sortField="weight"
        else: # liste mit pfad zum hash
            temp=hash
            #print "HI",sortField
            if not sortField:
                tempObj=self.pathToHierarchy[0:]
                tempObj=re.sub("/",".",tempObj)
                tempObj="self.aq_parent"+tempObj
                #print tempObj
                objtemp=eval(tempObj)

                obj=objtemp

                for x in start:
                    #print "OBJ",start,obj,obj.getId(),x

                    obj=getattr(obj,x)

                if hasattr(obj,'sortfield'):
                    sortField=obj.sortfield
                else:
                    sortField='weight'
            #print "START",start
            for x in start:
                #print "N2",x,temp[x]
                try:
                    temp=temp[x][0]
                except:
                    """nothing"""
                    return []
            #print "TEMP",temp,self.tempSorted(temp, field=sortField)

        for x in self.tempSorted(temp, field=sortField):
           
            if not temp[x][1].title == "":
                
                if (temp[x][1].meta_type=="ECHO_sqlElement") or (temp[x][1].meta_type=="ECHO_ZCatalogElement"):

                    for z in temp[x][1].getNavList():
                        listNav.append((z[0],z[1],None,'',''))
            
                else:
                    # add label falls existiert und nicht leer
                    if hasattr(temp[x][1],'label'):
                        
                        if not temp[x][1].getLabel()=='':
                                label=temp[x][1].getLabel()
                        else:
                            try:
                                label=temp[x][1].getTitle()
                            except:
                                logger("ECHO_Nav",logging.ERROR,"%s has neither getLabel nor getTile implemented"%temp[x][1].getId())
                                label=""
                    else:
                        label=temp[x][1].getTitle()

                        
                    if location:

                        loc=getattr(temp[x][1],'location','top')
                        if (loc==location):

                            
                            listNav.append((label,createPath(self,self.absolute_url(),temp[x][1].absolute_url(relative=1))+getattr(temp[x][1],'queryString',''),temp[x][1],getattr(temp[x][1],'prefix',''),getattr(temp[x][1],'suffix','')))
                    else:
                        
                        listNav.append((label,createPath(self,self.absolute_url(),temp[x][1].absolute_url(relative=1))+getattr(temp[x][1],'queryString',''),temp[x][1],getattr(temp[x][1],'prefix',''),getattr(temp[x][1],'suffix','')))
        #print self.REQUEST
        return listNav

    def tempSorted(self,hash,field="weight"):
        ls=[]

        for x in hash.keys():
            #print hash[x][1]
            if hasattr(hash[x][1],field):
                try:
                    weight=int(getattr(hash[x][1],field))
                except:
                    weight=getattr(hash[x][1],field)
                #print weight
            else:
                weight=0

            ls.append((x,weight))

        ls.sort(sortWeight)
        
        return [x[0] for x in ls]

    
        
            
    def isSelectedPath(self,item):
        """test is path is already selected"""

        testStr=re.sub("/index_html",'',self.REQUEST.get('URL',''))
	
	if len(item)<2:
		return 0
        #falls exakte Uebereinstimmung dann immer 1
        if  (testStr==item[1]) and (self.REQUEST.get('QUERY_STRING','')==''):
            
            return 1
           
        found=re.search("\?(.*)",item[1])
        
        if found:
            temp=re.sub(" ","%20",found.group(0))
          
            #print temp+"::"+self.REQUEST['QUERY_STRING']
            if ("?"+self.REQUEST['QUERY_STRING'])==temp:
                if getattr(item[2],'isAlwaysClickable',None): #immer auswaehlbar?
                
                    return 2
                else:
                    return 1

        if (getattr(item[2],'meta_type','')=="ECHO_pageTemplate") and (re.search(item[1],self.REQUEST['URL'])):
            if getattr(item[2],'isAlwaysClickable',None): #immer auswaehlbar?
                
                return 2
            else:
                return 1

        if re.search(item[1]+"/",self.REQUEST['URL']):
            if getattr(item[2],'isAlwaysClickable',None): #immer auswaehlbar?
                
                return 2
            else:
                return 1
        else:
            return 0
    

    def getPathStyle(self, item, style=""):
        """returns a string with the given style + 'sel' if the path is already selected."""

        if self.isSelectedPath(item):
            return style + 'sel'
        else:
            return style
        
    
    def buttonsFromHash(self,hash,start=None,orientation="horizontal"):
        
        """Generate Buttons"""
        if orientation=="horizontal":
            html="<table><tr>"
            startTag="<td>"
            endTag="</td>"
            endHtml="</tr></table>"

        else:
            html="<table>"
            startTag="<tr><td>"
            endTag="</td></tr>"
            endHtml="</table>"
            
        if not start:
            temp=hash
        else:
            temp=hash
            for x in start:
                temp=temp[x][0]
                #print temp
        
        for key in self.tempSorted(temp):
            
            description=temp[x][1].title
            
            
            url=createPath(self,self.absolute_url(),temp[key][1].absolute_url(relative=1))

            if (self.REQUEST['URL']==url) | (self.REQUEST['URL']==url+"/index_html"):
                html=html+startTag+"[%s]" % description+endTag
            else:                
                html=html+startTag+"[<a href='%s'>%s</a>]" % (url,description)+endTag
            

        html=html+endHtml
        
        return html

    def mainNavBar(self):
        """print main navigation bar"""
        keys=""

        try:
            keys=self.buttonsFromHash(self.createNavHash(0))
        except:
            keys=""
            
        return keys

    def mainNavElements(self,location=None):
        """print main navigation bar"""
        keys=""
        #print "NAVELEMENT",self.getId()
        #try:
        
        keys=self.barFromHash(self.createNavHash(0),location=location)
        
        #except:
        #    keys=""
     
        return keys

    def numberOfMainNavElements(self):
        """number of main elements"""

        return len(self.barFromHash(self.createNavHash(0))) 

    def startOfHierarchy(self,list):
        splitted=self.pathToHierarchy.split("/")
        last=splitted[len(splitted)-1]
        #print last,list
        try:
            nr=list.index(last)
        except:
            nr=0
        return nr+1
    
    def subNavStatic(self,obj):
        """subnav" von self"""
        def sortWeight(x,y):
            x1=int(getattr(x[1],'weight','0'))
            y1=int(getattr(y[1],'weight','0'))
            return cmp(x1,y1)
        print "obj",obj
        subs=self.ZopeFind(obj,obj_metatypes=self.displayedMetaTypes)
        subret=[]
        
        for x in subs:
            if not(x[1].title==""):
                subret.append(x)
        subret.sort(sortWeight)
        return subret
    
    def secondNavElements(self):
        """Zweite Ordnung Navigationselemente"""
        
        hash=self.createNavHash(2,checkOnly="no")

        actual_url=self.REQUEST['URL']
        
        ult_temp=re.sub(self.absolute_url(),"",actual_url)
        #print ult_temp

        splitted=ult_temp.split("/")
        
        #print "AU",self.absolute_url(),splitted
        #start=[splitted[2]] #orig
        startNr= self.startOfHierarchy(splitted)
        
        start=[splitted[startNr]]
        
        #print start

        keys=self.barFromHash(hash,start=start)
        
        
        return keys

    def navElements(self,order):
        """n-te Ordnung"""
        
        hash=self.createNavHash(order)
        actual_url=self.REQUEST['URL']
        
        ult_temp=re.sub(self.absolute_url(),"",actual_url)
        #print ult_temp

        splitted=ult_temp.split("/")
        

        ## if splitted[len(splitted)-1]=="index_html":
##             start=[splitted[len(splitted)-2]]
##         else:
##             start=[splitted[len(splitted)-1]]

        #start=splitted[2:order+1] #orig
        startNr= self.startOfHierarchy(splitted)
        #print startNr
        start=splitted[startNr:order+startNr-1] 
        #print start
       
        keys=self.barFromHash(hash,start=start)
        
        
        return keys


    def secondNavBar(self):
        """Zweite Ordnung"""

        hash=self.createNavHash(2)
        actual_url=self.REQUEST['URL']
        
        ult_temp=re.sub(self.absolute_url(),"",actual_url)
        #print ult_temp

        splitted=ult_temp.split("/")
        

        ## if splitted[len(splitted)-1]=="index_html":
##             start=[splitted[len(splitted)-2]]
##         else:
##             start=[splitted[len(splitted)-1]]

        start=[splitted[2]]
        #print start
        
        keys=self.buttonsFromHash(hash,start=start,orientation="vertical")
        
        
        return keys

def manage_addECHO_navigationForm(self):
    """Form for adding"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddECHO_navigation.zpt')).__of__(self)
    return pt()

def manage_addECHO_navigation(self,id,title,pathToHierarchy,RESPONSE=None):
    """Add an ECHO_root"""
    self._setObject(id,ECHO_navigation(id,title,pathToHierarchy))
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')


#helpers



def createPath(self,url1,url2):
    #bugfix in absolute_url, ploetzlich erschein in absolute_url der physikalische pfad.
    
    if self.REQUEST.has_key('VirtualRootPhysicalPath'):
        vp="/".join(self.REQUEST['VirtualRootPhysicalPath'])
        if (len(vp)>0):
            if vp[0]=="/":
            	vp=vp[1:]
        
            url2=url2.replace(vp+"/",'')
    else:
        temp1=url1.split("/")
        temp2=url2.split("/")
        for test in temp1:
          if temp2[0]==test:
            del temp2[0]
    
        url2= string.join(temp2,"/")
        
    return url1+"/"+url2

def sortWeight(x,y):
    #print x[1],y[1]
    return cmp(x[1],y[1])
    
