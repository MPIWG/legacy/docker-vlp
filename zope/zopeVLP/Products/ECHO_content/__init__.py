import ECHO_collection
import ECHO_resource

import ECHO_Nav
import ECHO_xslt
import VLPExtension
import ECHO_movie
#import ECHO_mediathek
try:
	import ECHO_language
except:
	print "no echo_language"

from authorSplitter import authorSplitter


def initialize(context):
    """initialize OSAS"""

    
    context.registerClass(
        ECHO_Nav.ECHO_sqlElement,
        constructors = (
          ECHO_Nav.manage_addECHO_sqlElementForm,
          ECHO_Nav.manage_addECHO_sqlElement
          )
        )

    context.registerClass(
        ECHO_Nav.ECHO_ZCatalogElement,
        constructors = (
          ECHO_Nav.manage_addECHO_ZCatalogElementForm,
          ECHO_Nav.manage_addECHO_ZCatalogElement
          )
        )

    context.registerClass(
        ECHO_Nav.ECHO_contentType,
        constructors = (
          ECHO_Nav.manage_addECHO_contentTypeForm,
          ECHO_Nav.manage_addECHO_contentType
          )
        )
    
    context.registerClass(
        ECHO_Nav.ECHO_pageTemplate,
        constructors = (
          ECHO_Nav.manage_addECHO_pageTemplateForm,
          ECHO_Nav.manage_addECHO_pageTemplate
          )
        )
    
    context.registerClass(
        ECHO_Nav.ECHO_navigation,
        constructors = (
          ECHO_Nav.manage_addECHO_navigationForm,
          ECHO_Nav.manage_addECHO_navigation
          )
        )


    context.registerClass(
        ECHO_collection.ECHO_copyright,
        constructors = (
          ECHO_collection.manage_addECHO_copyrightForm,
          ECHO_collection.manage_addECHO_copyright
          ),
          icon = 'ECHO_copyright.gif'
        )

    context.registerClass(
        ECHO_collection.ECHO_locale,
        constructors = (
          ECHO_collection.manage_addECHO_localeForm,
          ECHO_collection.manage_addECHO_locale
          ),
        )

    context.registerClass(
        ECHO_collection.ECHO_layoutTemplate,
        constructors = (
          ECHO_collection.manage_addECHO_layoutTemplateForm,
          ECHO_collection.manage_addECHO_layoutTemplate
          ),
        icon = 'ECHO_layoutTemplate.gif'
        )
    

    context.registerClass(
        ECHO_collection.ECHO_fullText,
        constructors = (
          ECHO_collection.manage_addECHO_fullTextForm,
          ECHO_collection.manage_addECHO_fullText
          ),
        icon = 'ECHO_layoutTemplate.gif'
        )

    context.registerClass(
        ECHO_collection.ECHO_collection,
        constructors = (
          ECHO_collection.manage_addECHO_collectionForm,
          ECHO_collection.manage_addECHO_collection
          ),
        icon = 'ECHO_collection.gif'
        )

    context.registerClass(
        ECHO_collection.ECHO_group,
        constructors = (
          ECHO_collection.manage_addECHO_groupForm,
          ECHO_collection.manage_addECHO_group
          ),
        icon = 'ECHO_group.gif'
        )
    
    context.registerClass(
        ECHO_collection.ECHO_externalLink,
        constructors = (
          ECHO_collection.manage_addECHO_externalLinkForm,
          ECHO_collection.manage_addECHO_externalLink
          ),
         icon = 'ECHO_externalLink.gif'
        )

    context.registerClass(
        ECHO_collection.ECHO_link,
        constructors = (
          ECHO_collection.manage_addECHO_linkForm,
          ECHO_collection.manage_addECHO_link
          ),
         icon = 'ECHO_externalLink.gif'
        )
    
    context.registerClass(
        ECHO_resource.ECHO_resource,
        constructors = (
          ECHO_resource.manage_addECHO_resourceForm,
          ECHO_resource.manage_addECHO_resource
          ),
         icon = 'ECHO_ressource.gif'
        )
    

    context.registerClass(
        ECHO_collection.ECHO_partner,
        constructors = (
          ECHO_collection.manage_addECHO_partnerForm,
          ECHO_collection.manage_addECHO_partner
          )
        )
    
    context.registerClass(
        ECHO_collection.ECHO_root,
        constructors = (
          ECHO_collection.manage_addECHO_rootForm,
          ECHO_collection.manage_addECHO_root
          )
        )

    context.registerClass(
        ECHO_collection.ECHO_main,
        constructors = (
          ECHO_collection.manage_addECHO_mainForm,
          ECHO_collection.manage_addECHO_main
          )
        )
    
    context.registerClass(
        ECHO_collection.ECHO_copyrightType,
        constructors = (
          ECHO_collection.manage_addECHO_copyrightTypeForm,
          ECHO_collection.manage_addECHO_copyrightType
          )
        )
        
    context.registerClass(
        ECHO_collection.ECHO_support,
        constructors = (
          ECHO_collection.manage_addECHO_supportForm,
          ECHO_collection.manage_addECHO_support
          ),
          icon = 'ECHO_support.gif'
        )
        
    context.registerClass(
        ECHO_collection.ECHO_ownerOriginal,
        constructors = (
          ECHO_collection.manage_addECHO_ownerOriginalForm,
          ECHO_collection.manage_addECHO_ownerOriginal
          ),
          icon = 'ECHO_ownerOriginal.gif'
        )
    
    context.registerClass(
        ECHO_collection.ECHO_digiCopyBy,
        constructors = (
          ECHO_collection.manage_addECHO_digiCopyByForm,
          ECHO_collection.manage_addECHO_digiCopyBy
          ),
          icon = 'ECHO_digiCopyBy.gif'
        )
        
    context.registerClass(
        ECHO_collection.ECHO_institution,
        constructors = (
          ECHO_collection.manage_addECHO_institutionForm,
          ECHO_collection.manage_addECHO_institution
          )
        )
        
    context.registerClass(
        ECHO_collection.ECHO_linkList,
        constructors = (
          ECHO_collection.manage_addECHO_linkListForm,
          ECHO_collection.manage_addECHO_linkList
          )
        )

    context.registerClass(
        VLPExtension.VLP_resource,
        constructors = (
          VLPExtension.manage_addVLP_resourceForm,
          VLPExtension.manage_addVLP_resource
          ),
         icon = 'ECHO_ressource.gif'
        )
    
    context.registerClass(
        VLPExtension.VLP_collection,
        constructors = (
          VLPExtension.manage_addVLP_collectionForm,
          VLPExtension.manage_addVLP_collection
          ),
         icon = 'ECHO_collection.gif'
        )
    
    context.registerClass(
        VLPExtension.VLP_essay,
        constructors = (
          VLPExtension.manage_addVLP_essayForm,
          VLPExtension.manage_addVLP_essay
          )
        )
    
    context.registerClass(
        VLPExtension.VLP_encyclopaedia,
        constructors = (
          VLPExtension.manage_addVLP_encycForm,
          VLPExtension.manage_addVLP_encyc
          )
        )
    
    context.registerClass(
        VLPExtension.sendMailForm,
        constructors = (
          VLPExtension.manage_addSendMailFormForm,
          VLPExtension.manage_addSendMailForm
          )
        )

    context.registerClass(
        ECHO_movie.ECHO_movie,
        constructors = (
          ECHO_movie.manage_addECHO_movieForm,
          ECHO_movie.manage_addECHO_movie
          ),
         icon = 'ECHO_ressource.gif'
        )
    
    context.registerClass(
        ECHO_xslt.ECHO_xslt,
        constructors = (
          ECHO_xslt.manage_addECHO_xsltForm,
          ECHO_xslt.manage_addECHO_xslt
          )
        )
  
    context.registerClass(
        ECHO_helpers.MapText,
        constructors=(
	    ECHO_helpers.manage_addMapTextForm,
	    ECHO_helpers.manage_addMapText
	    )
        )



    try:
     context.registerClass(
        ECHO_language.ECHO_linkCreator,
        constructors=(
        ECHO_language.manage_addECHO_linkCreatorForm,
        ECHO_language.manage_addECHO_linkCreator
        )
        )
    except:
	pass