"""
This module contains extensions which where originally made for the VLP.
"""
from OFS.Cache import Cacheable
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate
from OFS.Image import File
import xml.dom.minidom
import urllib
import xmlrpclib
import vlp_xmlhelpers
import sys
import os
from stat import *
from types import *
from Globals import package_home
import transaction
import Acquisition
import re
import logging

from Products.ECHO_content.ECHO_collection import *
from vlp_xmlhelpers import *
try:
        from Products.zogiLib import zogiLib
except:
        print "Zogilib not installed, VLP_resource will not work"


def lemmatize(str):
        """takes a str and addes links the dictionary service"""
        server = xmlrpclib.ServerProxy("http://archimedes.fas.harvard.edu/cgi-bin/donatus-rpc")
        if server:
                splitted=str.split(" ")
                wordlist=["<w>%s</w>"%split for split in splitted].join()
        return wordlist

                

                
def makeXML(str):
          
            try:
                    dom=xml.dom.minidom.parseString(str)
                    return str
            except:
                    str=str.replace("& ","&amp; ")
                    return """<?xml version="1.0" encoding="utf-8" ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><page>
                              %s
                              </page>"""%str
            

def checkXMLfrag(str):
    """checks and returns str as XML fragment"""
    
    xmltempl = """<?xml version="1.0" encoding="utf-8" ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <txt>%s</txt>"""

    try:
        dom=xml.dom.minidom.parseString(xmltempl%utf8ify(str))
        return str
    except:
        str=str.replace("& ","&amp; ")

    dom=xml.dom.minidom.parseString(xmltempl%utf8ify(str))
    return str
    
                     
            
def getText(nodelist):

    rc = ""
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
           rc = rc + node.data
    return rc

class sendMailForm(ZopePageTemplate):
        """sendMailForm"""
        meta_type="sendMailForm"

        _default_content_fn = os.path.join(package_home(globals()), 'vlp','sendMail_template.zpt')

        manage_options = ZopePageTemplate.manage_options+(
        {'label':'Main Config','action':'main_configForm'},
        )

        main_configForm = PageTemplateFile(
    'vlp/changeSendMailForm', globals())

        def main_config(self, toAddrs,mailServer, subjectAdd=None, RESPONSE=None):
                """main_config"""
                self.toAddrs=toAddrs.split("\n")
                self.mailServer=mailServer
                self.subjectAdd=subjectAdd
                if RESPONSE:
                        RESPONSE.redirect('manage_main')

        def sendForm(self,fromaddr,subject,content,nextPage="index_html",RESPONSE=None):
                """sendform"""
                fromaddr=fromaddr.strip("\r\n\t") # sicherstellen dass keine zusaetzlichen headerzeilen eingefuegt werden
                subject=subject.strip("\r\n\t") # sicherstellen dass keine zusaetzlichen headerzeilen eingefuegt werden
                toaddrs=self.toAddrs

                subject="%s %s"%(self.subjectAdd,subject)
                
                msg = ("From: %s\r\nSubject: %s\r\nTo: %s\r\n\r\n"
                       % (fromaddr, subject, ", ".join(toaddrs)))
                server = smtplib.SMTP(self.mailServer)
                #server.set_debuglevel(1)
                msg=msg+content
                server.sendmail(fromaddr, toaddrs, msg)
                server.quit()

                if RESPONSE:
                        RESPONSE.redirect(nextPage)

manage_addSendMailFormForm = PageTemplateFile(
    'vlp/addSendMailForm', globals(), __name__='manage_addSendMailFormFrom')

from urllib import quote

def manage_addSendMailForm(self, id, toAddrs,mailServer, title=None, text=None, subjectAdd=None,
                           REQUEST=None, submit=None):
    "Add a Page Template with optional file content."
    toAddrsList=toAddrs.split("\n")
    id = str(id)
    if REQUEST is None:
        self._setObject(id, sendMailForm(id, text))
        ob = getattr(self, id)
        if title:
            ob.pt_setTitle(title)
        setattr(ob,'toAddrs',toAddrsList)
        setattr(ob,'mailServer',mailServer)
        setattr(ob,'subjectAdd',subjectAdd)
        return ob
    else:
        file = REQUEST.form.get('file')
        headers = getattr(file, 'headers', None)
        if headers is None or not file.filename:
            zpt = sendMailForm(id)
        else:
            zpt = sendMailForm(id, file, headers.get('content_type'))

        self._setObject(id, zpt)

        try:
            u = self.DestinationURL()
        except AttributeError:
            u = REQUEST['URL1']

        if submit == " Add and Edit ":
            u = "%s/%s" % (u, quote(id))
            
        ob = getattr(self, id)
        if title:
            ob.pt_setTitle(title)
            
        setattr(ob,'toAddrs',toAddrsList)
        setattr(ob,'mailServer',mailServer)
        setattr(ob,'subjectAdd',subjectAdd)
        REQUEST.RESPONSE.redirect(u+'/manage_main')
    return ''

        
class VLP_collectionFolder(Folder):
    """klasse fuer folder innerhalb der collection"""
    meta_type="VLP_collectionFolder"
    
class VLP_essay(Folder):
    """classe fr VLP essays"""

    meta_type="VLP_essay"

    manage_options = Folder.manage_options+(
                {'label':'Main Config','action':'ConfigVLP_essayForm'},
                {'label':'Generate Essay Template','action':'generateEssayTemplateHTML'},
                )

    def content_html(self,type='collection'):
        """template fuer content"""
        #templates = self.ZopeFind(self.aq_parent,obj_ids=[type+"_template"])
        #
        #if templates:
        #    return templates[0][1]()
        
        if hasattr(self,type+"_template"):
            obj=getattr(self,type+"_template")
            return obj()
        else:
            pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ECHO_%s_template_standard.zpt'%type)).__of__(self)
            pt.content_type="text/html"
            return pt()

    def ConfigVLP_essayForm(self):
        """Form for adding"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','ChangeVLP_essay.zpt')).__of__(self)
        return pt()

    def ChangeVLP_essay(self,title,label,description,RESPONSE=None):
        """Change vlp_essay"""
        self.title=title
        self.label=label
        self.description=description

        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    def __init__(self,id,title,label):
        """init"""
        self.id=id
        self.title=title
        self.label=label

    def index_html(self):
        
            """show the rendered file"""
            
            if hasattr(self,'essayTemplate.html'):
                        return getattr(self,'essayTemplate.html')()

            pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','essay_template.zpt')).__of__(self)
            pt.content_type="text/html"
            return pt()

    def generateEssayTemplateHTML(self,RESPONSE=None):
        """lege standard index.html an"""

        if not hasattr(self,'essayTemplate.html'):
            zt=ZopePageTemplate('essayTemplate.html')
            self._setObject('essayTemplate.html',zt)
            default_content_fn = os.path.join(package_home(globals()),
                                               'vlp/essay_template.zpt')
            text = open(default_content_fn).read()
            zt.pt_edit(text, 'text/html')

        else:
            return "already exists!"
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    def getPageList(self):
        """returns sorted list of (id,object) of page objects"""

        def sortFind(x,y):
            return cmp(x[0],y[0])
        
#        pages=[]
#        pagestmp=self.ZopeFind(self,obj_metatypes=['DTML Document','File'])
#        for page in pagestmp:
#            if not (page[1].getId()[0]=="."):
#                pages.append(page)

        pages = self.objectItems(['DTML Document','File'])                        
        pages.sort(sortFind)
        #logging.debug("pageList=%s"%repr(pages))
        return pages
        
        
    def getPageXMLfrag(self, pageNum=None, pageId=None, pageObj=None):
        """returns XML fragment of page with id"""
        xml = None
        
        if pageNum is not None:
            pl = self.getPageList()
            pageObj = pl[int(pageNum)-1][1]
            
        if pageId is not None:
            pageObj = getattr(self, pageId, None)
            
        if pageObj is not None:
            txt = ""
            if pageObj.meta_type == 'File':
                data = pageObj.data
                #logging.debug("data=%s txt=%s"%(repr(data),txt))
                while data is not None:
                    if isinstance(data, str):
                        txt += data
                        data = None
                    else:
                        txt += data.data
                        data = data.next
        
            else:
                # not a File object... perhaps its executable
                txt += pageObj()
                
            xml = checkXMLfrag(txt)

        return unicodify(xml)


    def getPage(self,pagenum):
        """gibt essay page mit num aus"""
        def sortFind(x,y):
            return cmp(x[0],y[0])
        pages=[]
        pagestmp=self.ZopeFind(self,obj_metatypes=['DTML Document','File'])
        for page in pagestmp:
            if not (page[1].getId()[0]=="."):
                pages.append(page)
                        
        pages.sort(sortFind)
        #print str(pages[int(pagenum)][1]())
        
        if pages[int(pagenum)-1][1].meta_type=='File':
            #return makeXML(str(pages[int(pagenum)-1][1]))
            #txt=pages[int(pagenum)-1][1].data.decode('utf-8')
            txt=pages[int(pagenum)-1][1].data
            #print txt.encode('utf-8')
            return self.xml2html(makeXML(txt),quote="no")
        else:
            return self.xml2html(makeXML(pages[int(pagenum)-1][1]()),quote="no")
        
    def nextPage(self,pagenum,url):
        pages=len(self.ZopeFind(self,obj_metatypes=['DTML Document','File'])) # teste ob performance ok, sonst in variable
        if int(pagenum)+1 <= pages:
            retstr=url+"?p="+str(int(pagenum)+1)
            return """<td align="right" <a href="%s">next</a></td>"""%retstr
        
    def previousPage(self,pagenum,url):
        
        if int(pagenum)-1 > 0:
            retstr=url+"?p="+str(int(pagenum)-1)
            return """<td align="left" <a href="%s">previous</a></td>"""%retstr
        
    def nextURL(self,pagenum,url):
        pages=len(self.ZopeFind(self,obj_metatypes=['DTML Document','File'])) # teste ob performance ok, sonst in variable
        if int(pagenum)+1 <= pages:
            retstr=url+"?p="+str(int(pagenum)+1)
            return retstr
        
    def lastURL(self,pagenum,url):
        # teste ob performance ok, sonst in variable
        pages=len(self.ZopeFind(self,obj_metatypes=['DTML Document','File'])) 
        
        return url+"?p="+str(pages)
            
    def previousURL(self,pagenum,url):
        
        if int(pagenum)-1 > 0:
            retstr=url+"?p="+str(int(pagenum)-1)
            return retstr
        
        
def manage_addVLP_essayForm(self):
    """Form for adding"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','AddVLP_essay.zpt')).__of__(self)
    return pt()

def manage_addVLP_essay(self, id,title,label,RESPONSE=None):
        """add the copyright"""

        
        self._setObject(id, VLP_essay(id, title,label))

        if RESPONSE is not None:
                RESPONSE.redirect('manage_main')
        

class VLP_encyclopaedia(VLP_essay):
        """essay"""

        meta_type="VLP_encyclopaedia"
        
        manage_options = VLP_essay.manage_options+(
                {'label':'Load File','action':'loadNewFileForm'},
                )

        
        def loadNewFileForm(self):
                """Neues XML-File einlesen"""
                pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','VLP_newfile.zpt')).__of__(self)
                return pt()

        def loadNewFile(self,RESPONSE=None):
                """einlesen des neuen files"""
                fileupload=self.REQUEST['fileupload']
                if fileupload:
                        file_name=fileupload.filename
                        filedata=fileupload.read()

                        argv=vlp_xmlhelpers.proj2hash(self,filedata)
                        textsStr=string.join(argv['text']).encode('utf-8')
                        
                        texts=textsStr.split("<pb/>")
                        
                        i=0
                        for text in texts:
                                i+=1
                                pageName='page%03d.xml'%i
                                pages=self.ZopeFind(self,obj_ids=[pageName])
                                if pages:
                                        pages[0][1].update_data(text)

                                else:
                                        zt=File(pageName,pageName,text,content_type="text/plain")
                                        self._setObject(pageName,zt)
                                
                if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')
                        
        
def manage_addVLP_encycForm(self):
    """Form for adding"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','AddVLP_encyc.zpt')).__of__(self)
    return pt()

def manage_addVLP_encyc(self, id,title,label,RESPONSE=None):
        """add the copyright"""

        
        self._setObject(id, VLP_encyclopaedia(id, title,label))

        if RESPONSE is not None:
                RESPONSE.redirect('manage_main')
        
class VLP_subcollection(ECHO_collection):
        """ subcollection of elements"""

        meta_type="VLP_subcollection"
        
        def getVLPSubcollectionToc(self,type):
            
            #different ordering depending on the type
            if(type == "(Collection)"):
               sort= "shortreference"
            else:
               sort = "startpages"
           
           #content of journals also displayed if not online
           
            if (type == "(JournalVolume)"):
               online =""
            else:
               online ="AND online = 1"
            sort=self.collectionSort(type)
        
            item = [x for x in self.ZSQLSimpleSearch("""SELECT * FROM vl_literature 
                                 WHERE volumeid ~ '%s\\\\M' """%self.getId()
                                 + """ %s AND not referenceType='%s' """%(online,type)
                                 + """ORDER BY """ + sort)]
        
            tocItem = [x for x in self.ZSQLSimpleSearch("""SELECT * FROM vl_literature 
                                 WHERE reference = '%s' """%self.getId()
                                 + """AND online = 1 AND referenceType='%s' """%type
                                 + """ORDER BY """ + sort)]
            
            

            return tocItem+item
            
        def checkForFrontMatter(self,item):
                """Teste ob Frontmatter exists"""
                if not item==self.getId():
                        return False
                
                if self.ZopeFind(self,obj_ids=[self.getId()]):
                        return True
                else:
                        return False
                
        def generateSubCollectionFromDB(self):
                """erzeuge subcollection"""
                for item in self.ZSQLSimpleSearch("select * from vl_literature where volumeid like '%s'"%self.id):
                        manage_addECHO_link(self,item.reference,item.titlerefdisplay,item.titlerefdisplay)


        def index_html(self):
                """erzeuge darstellung"""
                
                if hasattr(self,'subCollection_template.html'):
                        return getattr(self,'subCollection_template.html')()

                pt=zptFile(self, 'vlp/subCollection_template.zpt')
                return pt()


def manage_addVLP_subCollection(self,id,title,label,RESPONSE=None):

        self._setObject(id,VLP_subcollection(id,title,label))

        if RESPONSE is not None:
                RESPONSE.redirect('manage_main')

from zope.publisher.interfaces import IPublishTraverse
from zope.interface import implements
from zope.publisher.interfaces import NotFound 
#from zope.app import zapi 
from zope.component import queryMultiAdapter
try:
	from ZPublisher.BaseRequest import DefaultPublishTraverse
except:
	pass

class VLP_collection(ECHO_collection,Acquisition.Explicit):
    """VLP spezifische Erweiterung der Collection"""


    
    implements(IPublishTraverse)
    
    
    def foxridgePath2vlp(self,url,mk,RESPONSE=None):
        """convertiert einen pfad zu einer url in vlp"""
        litRe = re.match(r".*lit([0-9]*)",url)
        logging.debug("foxridgePath2vlp URL:"+repr(url))
        lit = "lit"+litRe.group(1)
        splitted=url.split("/")
        path=splitted[-1].split(".")[0]
        newUrl=lit+"?p=%s&mk=%s"%(path,mk)
        
        if RESPONSE:
            RESPONSE.redirect(newUrl)
            return
            
        return newUrl
      
    def findObjFromLitName(self,fileName):
        if not fileName[0:3]=="lit":
            logging.error("getOrCreateFolder wrong filename: %s"%fileName)
            return None
        
        
        restName="%08d"%int(fileName[3:])
        
        fs=(restName[0:2],restName[0:4],restName[0:6])
        
        current =self
        for f in fs:
            obj = getattr(current,f,None)
            if not obj: #subfolder existiert nicht
                return None
            current=obj
            
        return getattr(current,fileName,None)
        
    def publishTraverse(self,request,name):
        """change the traversal if literature object"""
        
        actual_url=request['ACTUAL_URL']
        lastActual=actual_url.split("/")[-1]
        logging.debug("pub_trav:"+actual_url)
   
        if name.startswith("lit"): #umleitung wenn lit aufgerufen wirk
            # umleitung auf den eigentlichen folder nur wenn direkt der Folder litXXX oder dessen index_html methode aufgerufen wird
            if lastActual.startswith("lit") or (lastActual=="index_html") or (lastActual=="index_meta") or   (lastActual=="getTitle") or   (lastActual=="getMetaDataLink") or (lastActual=="hasTranscription") or (lastActual=="transcription"): 
                #obj=self. ZopeFind(self,obj_ids=[name],search_sub=1)
                ob=self.findObjFromLitName(name)
                if not ob:
                    return "LIt not found"
                else:
          
                    logging.debug("request:"+repr(ob))
                    request.response.setStatus(200)
                    #if (lastActual=="index_meta"):
                   #     logging.debug("index_meta requested")
                   #     return ob.index_meta
                    return ob
                
            else: ## andern falls transversiere zum parent.
                obj = self.aq_parent
                return obj
        else: # mache gar nichts falls nicht lit aufgerufen wird
 
           
            tr=DefaultPublishTraverse(self, request)
            ob= tr.publishTraverse(request, name)
       
            return ob
         #raise NotFound(self.context, name, request) 
            #return repr(request)
    
    meta_type="VLP_collection"
    manage_options=ECHO_collection.manage_options+(
        {'label':'Change Path','action':'VLP_path_configForm'},
        {'label':'Update Library','action':'updateCollection'},
        {'label':'Update Metadata','action':'updateCollectionMD'},
        )
    def getAllRessources(self):
        """getallressources"""
        res=self.ZopeFind(self,obj_metatypes=['VLP_resource'],search_sub=1)
        ret=[]
        for r in res:
            ret.append([r[0],r[1].absolute_url()])
        
        return ret
        
    def generateSubCollections(self,errorsTXT="",forceUpdate=False,RESPONSE=None):
        """erzeuge subcollectionen"""
        logging.debug("generateSubCollections")
        founds=self.ZSQLSimpleSearch("select * from vl_literature where referencetype in ('(Edited Book)','(Book)','(Collection)','(JournalVolume)','(Generic Container)')")
        
        if RESPONSE is not None:
            RESPONSE.setHeader('Content-Type','text/html')
            self.REQUEST.RESPONSE.write("<h2>Create Subcollections</h2>\n")
        for found in founds:
            try:
                litid = str(found.reference)
                foundCol=self.findObjFromLitName(litid)
                #foundCol=self.ZopeFind(self,obj_ids=[litid])
                if foundCol:
                    col = foundCol
                    logging.debug("generateSubCollections: subcollection %s exists (%s)"%(repr(col),found.reference))
                    logging.debug(repr([col.title,found.titlerefdisplay,col.label,found.titlerefdisplay]))
                    if (unicodify(col.title) != unicodify(found.titlerefdisplay)) or (unicodify(col.label) != unicodify(found.titlerefdisplay)):
                        # subcollection seems to have changed    
                        logging.debug("generateSubCollections: subcollection has changed, recreating!")
                        col.aq_parent.manage_delObjects([col.getId()])
                        manage_addVLP_subCollection(self.getOrCreateFolderForFile(litid),litid,found.titlerefdisplay,found.titlerefdisplay)
                else:
                    logging.debug("generateSubCollections: creating new subcollection %s"%found.reference)
                    manage_addVLP_subCollection(self.getOrCreateFolderForFile(litid),litid,found.titlerefdisplay,found.titlerefdisplay)
                
                #teste ob es Images auf dem Server gibt mit gleichem Namen (frontmatter)
                if os.path.exists(os.path.join(self.vlp_basis,litid)):
                    logging.debug("generateSubCollections: found frontmatter in %s"%litid)
                    obj=self.findObjFromLitName(litid)
                    if not self.ZopeFind(obj,obj_ids=[litid]):
                        metalink=self.REQUEST['URL1']+"/"+litid+"/"+litid+"/index_meta"
                        newObj=VLP_resource(litid,'',metalink,litid,litid,litid,'generated','book','','','','','','')
                        obj._setObject(litid,newObj)
                        
                    genObj=getattr(obj,litid)
                    genObj.createIndexFile(forceUpdate=forceUpdate)

                if RESPONSE is not None:
                    self.REQUEST.RESPONSE.write("<p>%s</p>\n"%litid)
                logging.debug("<p>%s</p>\n"%litid)

            except:
                error=sys.exc_info()[0:2]
                logging.error("generateSubCollections: ERROR in %s"%litid)
                if RESPONSE is not None:
                    RESPONSE.write("<p>(ERROR  updateII(%s): %s %s  TXT: %s)\n</p>"%(litid,error[0],error[1],sys.exc_info()[2]))
                    RESPONSE.write("<p>(ERROR (%s): %s %s)</p>\n"%(litid,error[0],error[1]))
                logging.error("generateSubCollections: %s %s"%error)
                self.REQUEST.RESPONSE.write("(ERROR (%s): %s %s)\n"%(litid,repr(error[0]),repr(error[1])))
                errorsTXT+="<p>ERROR: No subcollection of %s (ERROR: %s %s)</p>\n"%(litid,error[0],error[1])
                errorsTXT+="<p>No subcollection of %s  "%litid+"(ERROR: %s %s)</p>"%error
                
        return errorsTXT
            
    def VLP_path_configForm(self):
        """change pt"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','ChangeVLPPath.zpt')).__of__(self)
        return pt()

    def VLP_path_config(self,path,RESPONSE=None):
        """config"""
        self.vlp_basis=path
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    
    def getOrCreateFolderForFile(self,fileName):
        
        if not fileName[0:3]=="lit":
            logging.error("getOrCreateFolder wrong filename: %s"%fileName)
            return None
        
        try:
            restName="%08d"%int(fileName[3:])
        except:
            logging.error("getOrCreateFolder wrong filename: %s"%fileName)
            return None
        
        fs=(restName[0:2],restName[0:4],restName[0:6])
        
        current =self
        for f in fs:
            obj = getattr(current,f,None)
            if not obj:
                 newObj=VLP_collectionFolder(f)
                 current._setObject(f,newObj)
                 obj = getattr(current,f)
            current=obj
            
        return current
        
    def changeOrAddResources(self,forceUpdate=False,RESPONSE=None):
        """changes ord updates all resources found in the file system"""
        
        errorsTXT="<h2>Errors</h2>"
        if RESPONSE is not None:
            RESPONSE.setHeader('Content-Type','text/html')
            RESPONSE.write("<html><body>")

        files=os.listdir(self.vlp_basis)
        i=0
        for fileName in files:
            if i<100:
                i+=1
            else:
                i=0
                j=0
                while j<5:
                    try:
                        transaction.get().commit()  
                        j=6
                    except:
                        logging.error("Commit: %s"%j)
                        j+=1
                        
                RESPONSE.write("<p>committed</p>")
            if fileName[0:3]=="lit":
                metalink=self.REQUEST['URL1']+"/"+fileName+"/index_meta"
                
                folder=self.getOrCreateFolderForFile(fileName) #get the folder where fileName lives or should live
                if not folder: #folder gave an error
                    continue
                try:
                    if not hasattr(folder,fileName):
                        # create new resource
                        logging.debug("updateCollection: new %s"%fileName)
                        if RESPONSE is not None:
                            RESPONSE.write("<p>new: %s</p>\n"%fileName)
                        logging.debug("new: %s \n"%fileName)
                        newObj=VLP_resource(fileName,'',metalink,fileName,fileName,fileName,'generated','book','','','','','','')
                        
                        folder._setObject(fileName,newObj)
                
                    if RESPONSE is not None:
                        RESPONSE.write("<p>got: %s "%fileName)

                    genObj=getattr(folder,fileName)
                    logging.debug("got: %s "%fileName)

                    logging.debug("updateCollection: inspecting %s"%fileName)
                    # create index
                    if hasattr(genObj,'createIndexFile'):
                        try:
                            logging.debug("updateCollection: creating index for %s"%fileName)
                            msg = genObj.createIndexFile(forceUpdate=forceUpdate)
                            if msg and RESPONSE is not None:
                                RESPONSE.write(msg)
                        except:
                            error=sys.exc_info()[0:2]
                            if RESPONSE is not None:
                                RESPONSE.write("(ERROR Update(%s): %s %s %s) "%(fileName,error[0],error[1],sys.exc_info()[2]))
                            errorsTXT+="<p>ERROR creating index for %s "%fileName+"(ERROR: %s %s)</p>\n"%error
                    
                    # update pageSizeSum for subcollections
                    if genObj.meta_type=="VLP_subcollection":
                        if RESPONSE is not None:
                            RESPONSE.write(" is subcollection")
                        logging.debug("updateCollection: %s is subcollection"%fileName)
                        genObj.pageSizeSum=getattr(genObj,fileName).getPageSizeSum()
                    else:
                        genObj.pageSizeSum=genObj.getPageSizeSum()

                    if RESPONSE is not None:
                        RESPONSE.write("</p>\n")
                       
                except:
                    error=sys.exc_info()[0:2]
                    logging.error("updateCollection: ERROR in %s (%s %s)"%(fileName,error[0],error[1]))
                    if RESPONSE is not None:
                         RESPONSE.write("(ERROR  updateII(%s): %s %s %s) "%(fileName,error[0],error[1],sys.exc_info()[2]))
                    errorsTXT+="<p>File not created:%s  (ERROR: %s %s)</p>\n"%(fileName,error[0],error[1])
        return errorsTXT
     
    def updateCollection(self,forceUpdate=True,RESPONSE=None):
        """liest verzeichnisse aus dem pfad und legt sie dann als objekte in den ordner"""
        
        # update subcollections 
        errorsTXT=""   
        errorsTXT+=self.changeOrAddResources(forceUpdate, RESPONSE);
        errorsTXT+=self.generateSubCollections(forceUpdate=forceUpdate,RESPONSE=RESPONSE)
        errorsTXT+="\n"
        if RESPONSE is not None:
            RESPONSE.write(errorsTXT)
            RESPONSE.write("</body></html>")

    def updateCollectionMD(self,RESPONSE=None):
        """updateMD"""
        files=os.listdir(self.vlp_basis)
        for fileName in files:
            if fileName[0:3]=="lit":
                genObj=getattr(self,fileName)
                genObj.copyIndex_meta2echo_resource()
                genObj.generate_title()

        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    def copyFullTextsFromDB(self,RESPONSE=None):
            """copy fulltext aus DB"""

            ress=self.ZopeFind(self,obj_metatypes=['VLP_resource'])
            ret=""
            for res in ress:
                    tmp=res[1].copyTranscriptionFromDB()
                    ret+=tmp

                    if RESPONSE:
                            RESPONSE.write(tmp+"\n")
            if not RESPONSE:                
                    return ret
            else:
                    RESPONSE.write("END\n")
                    
            
def manage_addVLP_collectionForm(self):
        """Form for adding a ressource"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','AddVLP_collectionForm.zpt')).__of__(self)
        return pt()



def manage_addVLP_collection(self,id,title,label,description,contentType,responsible,weight,sortfield,path,coords="",secondaryLinkTitle="",secondaryLink="",credits=None,RESPONSE=None,imageTag="",bgcolour=""):
    """add a echo collection"""
    

    newObj=VLP_collection(id,title,label,description,contentType,responsible,credits,weight,sortfield,coords,secondaryLinkTitle=secondaryLinkTitle,secondaryLink=secondaryLink,imageTag=imageTag,bgcolour="")

    self._setObject(id,newObj)
    setattr(newObj,'path',path)

    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')



class VLP_resource(ECHO_resource,Cacheable):
    """VLP spezifische Erweiterung"""

    meta_type="VLP_resource"

    def _p_resolveConflict(self,oldstate,savedstate,newstate):
        logging.debug("updateCollection: Have to resolve conflict!")
        return newstate

    #vlp_basis="/mpiwg/online/permanent/vlp"

    
    
    referencetypes=[]

    manage_options=ECHO_resource.manage_options+Cacheable.manage_options+(
        {'label':'Update Index','action':'createIndexFile'},
        {'label':'Reset MetaLink','action':'resetMetaLink'},
        {'label':'Generate Title','action':'generate_title'},
        {'label':'Generate Library Template','action':'generateLibraryTemplateHTML'},
        )

    def getMetaDataLink(self):
        """link auf die metatdaten"""
        return self.absolute_url()+"/index_meta"


    def getTitle(self):
        """Title der Ressource"""
        title= self.ZSQLSimpleSearch("""select title from vl_literature where reference= \'%s\'  """ %self.getId())[0].title
        logging.debug(title)
        return title
    
 
        
    def getImagePath(self):
            """Pfad zu den Images"""
            path=None
            
            if os.path.isdir(os.path.join(self.vlp_basis,self.resourceID,'pageimg')):
                    path= os.path.join(self.vlp_basis,self.resourceID,'pageimg')
            elif os.path.isdir(os.path.join(self.vlp_basis,self.resourceID,'pages')):
                    path = os.path.join(self.vlp_basis,self.resourceID,'pages')
            # workaround in some cases there seem to be no pictures in pages
            
            if os.path.isdir(os.path.join(self.vlp_basis,self.resourceID,'pagesHi')):

		
		pagesHi = os.listdir(os.path.join(self.vlp_basis,self.resourceID,'pagesHi'))
		if path is None:
			return os.path.join(self.vlp_basis,self.resourceID,'pagesHi')

	        pages = os.listdir(path)

                if len (pagesHi) > len(pages):

		    countHi=0
		    for p in pagesHi:
			dat,ext = os.path.splitext(p)
			if ext.lower() in [".tiff",".tif",".jpg"]:
				countHi+=1
		    count=0
		    for p in pages:
			dat,ext = os.path.splitext(p)
			if ext.lower() in [".tiff",".tif",".jpg"]:
				count+=1
 		    
		    if countHi > count:
                    	path=os.path.join(self.vlp_basis,self.resourceID,'pagesHi')
			
                    	logging.error("pages < pagesHi:  %s"%self.resourceID)
            if path is None:

            
                if len (os.listdir(os.path.join(self.vlp_basis,self.resourceID,'pagesHi'))) > len(os.listdir(path)):
                    
                    
                    
                    path=os.path.join(self.vlp_basis,self.resourceID,'pagesHi')
                    logging.error("pages < pagesHi:  %s"%self.resourceID)
            	else:

                    logging.error("no images:  %s"%self.resourceID)
                    return None   

	    return path 

            
    def transcription(self):
            """show the rendered transcriptiofile"""
            
            if hasattr(self,'libraryTranscriptionTemplate.html'):
                        return getattr(self,'libraryTranscriptionTemplate.html')()

            pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','library_transcriptionTemplate.zpt')).__of__(self)
            pt.content_type="text/html"
            return pt()



    def identifyMediaType(self):
        """identifiziert den Medientype aus der VLP Datenbank"""
        founds=self.ZSQLSimpleSearch('select * from vl_literature where reference=\''+self.resourceID+'\' order by id')
        if (not founds) or (len(founds)==0):
            self.mediaType="generic" # steht nicht in der Datenbank, dann nimm generisch an
            return self.mediaType
        
        
        if founds[0].referencetype =="Audio":
            self.mediaType ="audio"
        elif founds[0].referencetype =="Film":
            self.mediaType ="video"
        else:
            self.mediaType ="image" # nimm an, dass alle anderen Eintrage in der Datenbank auf images verweisen (Buecher oder Fotografien).
        
        return self.mediaType
         
    def getMediaType(self,cached=False):
        """give the Media-type back, if chached False, then get it from the database"""
        
        if (not hasattr(self,'mediaType')) or (self.mediaType=="") or (not cached):
            self.identifyMediaType()
        
        return self.mediaType
    
        
        
        
    def copyTranscriptionFromDB(self):
            """checks if transcription exits in DB"""
            founds=self.ZSQLSimpleSearch('select * from vl_transcript where source=\''+self.resourceID+'\' order by id')


            ret="""<?xml version="1.0" encoding="utf-8"?>
               <text>"""
        
            if founds:
                    
                    for found in founds:
                        
                            text=found.transcript
                            if not text: text=""
                            ret+="<page><pb/>"+text+"</page>"

                    ret+="</text>"
                    re2=ret[0:]
                    re3=self.unicodify(re2)
                    #re3=re2
                    ft=self.ZopeFind(self,obj_metatypes=['ECHO_fullText'])
                    
                    if ft:
                        
                            ft[0][1].pt_edit(re3,'text/xml')
                        
                    else:
        
                            self._setObject('fulltext',ECHO_fullText('fulltext',''))

                            try:
                                getattr(self,'fulltext').pt_edit(re3.encode('utf-8','ignore'),'text/xml')
                            except:
                                getattr(self,'fulltext').pt_edit(re3.encode('latin-1').decode('utf-8'),'text/xml')

                    return "FT: %s"%self.getId()
            else:
                    return "no: %s"%self.getId()
            
    def hasTranscription(self):
        """transcription"""
       
        if not hasattr(self,'fulltext'):
            self.copyTranscriptionFromDB()
        if not hasattr(self,'fulltext'): #still not

            return False

        

        return True 

            
    def resetMetaLink(self,all="no",RESPONSE=None):
        """resets metalink to standard resp. in case of server change it sets to the new server url"""
        if all=="yes":
            self.metalink=self.absolute_url()+"/index_meta"
        else:
            splitted=self.metalink.split("/")
            if (len(splitted)>1) and (splitted[len(splitted)-1]=="index_meta") and (splitted[len(splitted)-2]==self.getId()): # heuristic nur generische Metadatafiles andern
                self.metalink=self.absolute_url()+"/index_meta"

        if RESPONSE:
            return "done"
            
    def show(self):
        """show the rendered file"""

        mt = self.getMediaType()
        
        if (mt in ['video','audio']):
            pt = self.getVLPTemplate('libraryMovieTemplate')
        else: #zur zeit werden alle anderen type auf das libraryTemplate abgebildet.
            
            if self.REQUEST.get('p',None): #make sure that parameter p exists
                #self.REQUEST.set('pn',self.file2page(self.REQUEST.get('p',None)))
                params = "pn=%s"%self.file2page(self.REQUEST.get('p',None))
                mk = self.REQUEST.get('mk',None)
                if mk:
                    params += "&mk=%s"%mk
                self.REQUEST.RESPONSE.redirect(self.REQUEST['URL']+'?'+params)
               
            pt = self.getVLPTemplate('libraryTemplate')
       
        return pt()

    def getVLPTemplate(self,name):
        html=name+".html"
        if hasattr(self,html):
                return getattr(self,html)
        
        pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp',name+'.zpt')).__of__(self)
        pt.content_type="text/html"
        return pt

    def index_html(self):
            """index_html"""
            #return self.REQUEST
            if not self.REQUEST.has_key("ws"):
                    self.REQUEST.form["ws"]="1.5"

            return self.show()

    def generateLibraryTemplateHTML(self,RESPONSE=None):
        """lege standard index.html an"""

        if not hasattr(self,'libraryTemplate.html'):
            zt=ZopePageTemplate('libraryTemplate.html')
            self._setObject('libraryTemplate.html',zt)
            default_content_fn = os.path.join(package_home(globals()),
                                               'vlp/library_template.zpt')
            text = open(default_content_fn).read()
            zt.pt_edit(text, 'text/html')

        else:
            return "already exists!"
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def index_meta(self,RESPONSE=None):
        """index_meta"""
       
        pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','index_meta.zpt')).__of__(self)
        pt.content_type="text/xml"      
        x= pt.pt_render()
        #logging.debug("index_meta 4:"+x)
        if RESPONSE:
            RESPONSE.setHeader("Content-Type","text/xml")
        return x
    def file2page(self,p):
        """converts filename to pagenumber"""
        
        #dom=xml.dom.minidom.parse(ECHO_helpers.urlopen(getattr(self,'index.xml').absolute_url()))
        index=getattr(self,'index.xml')
 
        txt=index.pt_render()
        dom=xml.dom.minidom.parseString(txt)
        
        for page in dom.getElementsByTagName('page'):
                 # pageummer mit lo oder hi sind aequivalent
                 if (page.getAttribute('file')==p) or ("%slo"%page.getAttribute('file')==p) or ("%shi"%page.getAttribute('file')==p):
                     nr=page.getAttribute('nr')
                    
                     return nr
            
        return 0

    def getPageSizeSum(self):
            """generate average page size"""

            #size of hi resolution
            files=[]
            try:
                    pathHi=os.path.join(self.vlp_basis,self.resourceID,'pages')
                    files=os.listdir(pathHi)
                    hi=0
                    for f in files:
                            hi+=os.stat(os.path.join(pathHi,f))[ST_SIZE]

            except:
                    hi=0

            #size of hi resolution
            try:
                    pathHi=os.path.join(self.vlp_basis,self.resourceID,'pagesLo')
                    files=os.listdir(pathHi)
                    lo=0
                    for f in files:
                            lo+=os.stat(os.path.join(pathHi,f))[ST_SIZE]

            except:
                    lo=0

            
            return (hi,lo,len(files))
                    

    def getPageWeights(self):
            """average Page sizes in kBytes"""
    
            res=getattr(self,'pageSizeSum',(0,0,1))
            return (res[0]/(1024*res[2]),res[1]/(1024*res[2]))
                            
            
            
    def dir2index(self):
        """dir to index"""

        excludeNames=['Icon']
                
        
        def calculateName(str):
            name=os.path.splitext(str[1:])[0]
            ret="%s: %s"%(str[0],name)
            return ret

        self.imagePathFull=self.getImagePath();
	self.imagePath=self.imagePathFull.split("/")[-1]
        if (self.imagePath is None):
            return None
        
        temp=[]
        ret = os.listdir(self.imagePathFull)
        
        ret.sort()
        for x in ret:
            if (not (x[0] in ('.',':'))) and (not x[0:4] in excludeNames):
                temp.append((calculateName(x),os.path.splitext(x)[0]))
        return temp

    
    def firstPage(self,url=None):
        """showlink to the first image"""
        if self.REQUEST.get('pn','1')=="1": #erste seinte dann kein a tag
            return """<img src="/images/first.gif" alt="first" width="14" height="14" border="0"><br>first"""

        else:
                ws=self.REQUEST.get('ws','1')
                return """<a href=%s>
                         <img src="/images/first.gif" alt="first" width="14" height="14" border="0"><br>first
                         </a>
                         """%(self.REQUEST['URL']+"?pn=1&ws=%s"%ws)

    def lastPage(self,url=None):
        """showlink to the first image"""
        pages=self.readIndexFile(url)
        if pages:
            ln=len(pages)
        
        if (self.REQUEST.get('pn')==str(ln)) or (ln==1): #letzte seite dann kein a tag oder nur eine Seite
            return """<img src="/images/last.gif" alt="last" width="14" height="14" border="0"><br>last"""

        else:
                ws=self.REQUEST.get('ws','1')
                return """<a href=%s>
                         <img src="/images/last.gif" alt="last" width="14" height="14" border="0"><br>last
                         </a>
                         """%(self.REQUEST['URL']+"?pn="+str(ln)+"&ws="+ws)
            
    def prevPage(self,url=None):
        """showlink to the first image"""
        

        nn = int(self.REQUEST.get('pn','1'))-1
        if nn < 1: #letzte  seite dann kein a tag
            return """<img src="/images/lisePrev.gif" alt="prev" width="14" height="14" border="0"><br>prev"""

        else:
                ws=self.REQUEST.get('ws','1')
                return """<a href=%s>
                         <img src="/images/lisePrev.gif" alt="prev" width="14" height="14" border="0"><br>prev
                         </a>
                         """%(self.REQUEST['URL']+"?pn="+str(nn)+"&ws="+ws)

    def nextPage(self,url=None):
        """showlink to the first image"""
        pages=self.readIndexFile(url)
        if pages:
            ln=len(pages)

        nn = int(self.REQUEST.get('pn','1'))+1
        if (nn>int(ln)) or (ln==1): #letzte  seite dann kein a tag
            return """<img src="/images/liseNext.gif" alt="next" width="14" height="14" border="0"><br>next"""

        else:
                ws=self.REQUEST.get('ws','1')
                return """<a href=%s>
                         <img src="/images/liseNext.gif" alt="next" width="14" height="14" border="0"><br>next
                         </a>
                         """%(self.REQUEST['URL']+"?pn="+str(nn)+"&ws="+ws)
        
    
    def readIndexFile(self,url=None):
        """reads the indexfile. presently url not None isn't implemented. In the future url can be defined."""
        if not url:
            urlTmp=self.absolute_url()+"/index.xml"

        if self.ZCacheable_isCachingEnabled():

            result = self.ZCacheable_get(view_name=urlTmp)
            if result is not None:
                # Got a cached value.
                return result

        pagelist=[]
        
        if not url:

            xmlTxt=getattr(self,'index.xml')()
            dom=xml.dom.minidom.parseString(xmlTxt)
        else:            
            fh=ECHO_helpers.urlopen(urlTmp)
            dom=xml.dom.minidom.parse(fh)
        
        
        for page in dom.getElementsByTagName('page'):
            text=getText(page.childNodes)
            pageNum=page.getAttribute('nr')
            fileName=page.getAttribute('file')
            pagelist.append((pageNum,text,fileName))

        self.ZCacheable_set(pagelist,view_name=urlTmp)
        return pagelist 

    def getFileName(self):
           """getNameOfCurrentFile"""
           try:
            pn=int(self.REQUEST.get('pn','1'))
           except:
            pn=1
           pageinfo=self.readIndexFile()[pn-1]
           return pageinfo[2]
   
    def createIndexFile(self,forceUpdate=False,RESPONSE=None):
        """create index file"""
        logging.debug("createindexfile of %s"%self.id)

        if hasattr(self,'index.xml'):
            # index exists
            zt=getattr(self,'index.xml')
            if not forceUpdate:
                # check if directory is newer (self.imagePath sould be set by now)
                path = self.getImagePath()
                if (path is None):
                    return "CREATE INDEX FILE NOT POSSIBLE"
                imgdir = os.path.join(path)
                dirtime = os.path.getmtime(imgdir)
                zttime = zt.bobobase_modification_time()
                logging.debug("createindexfile: dir %s of %s index of %s"%(imgdir,dirtime,zttime))
                if dirtime < zttime:
                    # nothing to do
                    logging.debug("createindexfile: dir %s older than index"%(imgdir))
                    return "index is up to date"
                
        else:
            # create new index template
            zt=ZopePageTemplate('index.xml')
            self._setObject('index.xml',zt)
        
        # fill index template
        pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','index_template.zpt')).__of__(self)()
        #logging.debug("createindexfile uses %s"%pt)
        zt.pt_edit(pt, 'text/xml')
        transaction.get().commit()

        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

        
def manage_addVLP_resourceForm(self):
        """Form for adding a ressource"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'vlp','AddVLP_resourceForm.zpt')).__of__(self)
        return pt()



def manage_addVLP_resource(self,id,title,label,description,responsible,link,metalink,weight,resourceID=None,contentType=None,renderingType=None,credits=None,coords=None,RESPONSE=None):
    """addaresource"""

    
    if metalink=='':
        metalink=self.absolute_url()+"/"+id+"/index_meta"
            
    newObj=VLP_resource(id,link,metalink,resourceID,title,label,description,contentType,renderingType,responsible,credits,weight,coords)

    self._setObject(id,newObj)

    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
 
