from AccessControl import ClassSecurityInfo
from Globals import package_home
from OFS.Folder import Folder
from OFS.Image import Image
from OFS.Image import File
from OFS.SimpleItem import SimpleItem
from Products.PageTemplates.PageTemplate import PageTemplate
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate
from types import *
from Globals import package_home, ImageFile
from xml_helpers import getUniqueElementText,getText
import cgi
import os
import random
import re
import string
import urllib
import xml.dom.minidom

ZOGIVERSION = "0.10.4b ROC 13.11.2006"

def cropf(f):
    """returns a float with reduced precision"""
    return float(int(f * 10000)/10000.0)


def browserCheck(self):
    """check the browsers request to find out the browser type"""
    bt = {}
    ua = self.REQUEST.get_header("HTTP_USER_AGENT")
    bt['ua'] = ua
    bt['isIE'] = False
    bt['isN4'] = False
    if string.find(ua, 'MSIE') > -1:
        bt['isIE'] = True
    else:
        bt['isN4'] = (string.find(ua, 'Mozilla/4.') > -1)
        
    try:
        nav = ua[string.find(ua, '('):]
        ie = string.split(nav, "; ")[1]
        if string.find(ie, "MSIE") > -1:
            bt['versIE'] = string.split(ie, " ")[1]
    except: pass
    
    bt['isMac'] = string.find(ua, 'Macintosh') > -1
    bt['isWin'] = string.find(ua, 'Windows') > -1
    bt['isIEWin'] = bt['isIE'] and bt['isWin']
    bt['isIEMac'] = bt['isIE'] and bt['isMac']
    bt['staticHTML'] = False

    return bt

    
class zogiImage(SimpleItem):
    """einzelnes Image"""
    meta_type="zogiImage"

    manage_options=(
        {'label':'Main config','action':'changeZogiImageForm'},
       )+SimpleItem.manage_options
    
    
    def __init__(self,id,title,baseUrl,queryString,content_type='',precondition=''):
        """init"""
        self.id=id
        self.title=title
        if baseUrl:
            self.baseUrl=baseUrl
        else:
            self.baseUrl="http://nausikaa.mpiwg-berlin.mpg.de/digitallibrary/servlet/Scaler?"
            
        self.queryString=queryString
        self.content_type=content_type
        self.precondition=precondition

    #def getData(self):
    #    """getUrlData"""
    #    return urllib.urlopen(self.baseUrl+self.queryString)

    changeZogiImageForm = PageTemplateFile('zpt/changeZogiImageForm.zpt', globals())
    
    def index_html(self, REQUEST, RESPONSE):
        """service the request by redirecting to digilib server"""
        # use status 307 = moved temporarily
        RESPONSE.redirect(self.baseUrl+self.queryString, status=307)
        return ''
        

    def rescale(self,width=None,height=None):
        """andere parameter im querystring"""
        qs=cgi.parse_qs(self.queryString)
        for x in qs.keys():
            if type(qs[x]) is ListType:
                qs[x]=qs[x][0]
        
        if width:
            qs['dw']=width
        if height:
            qs['dh']=height
        
        qsneu=urllib.urlencode(qs)
        self.queryString=qsneu
        return "done"
    
    
    def setWithDigilibURL(self,digilibUrl):
        """take all parameters from digilib URL string"""
        base = re.match('(.*?/Scaler\?)(.*)', digilibUrl)
        if base is not None:
            params = base.group(2).split('&')
            newparams = []
            # filter out the parameters we want
            for p in params:
                (key, val) = p.split('=')
                if key in ['fn','pn','dw','dh','ww','wh','wx','wy','mo']:
                    newparams.append(p)
            # set the new parameters
            if len(newparams) > 1:
                self.baseUrl = base.group(1)
                self.queryString = '&'.join(newparams)
                return True
        
        return False
    
    
    def changeZogiImage(self,title,baseUrl, queryString, digilibUrl=None, RESPONSE=None):
        """change it"""
        self.title=title
        self.baseUrl=baseUrl
        self.queryString=queryString
        if digilibUrl is not None and len(digilibUrl) > 0:
            self.setWithDigilibURL(digilibUrl)
            
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    
def manage_addZogiImageForm(self):
    """Form for adding"""
    pt=PageTemplateFile(os.path.join(package_home(globals()), 'zpt/addZogiImage.zpt')).__of__(self)
    return pt()


def manage_addZogiImage(self,id,title,baseUrl,queryString,digilibUrl=None,RESPONSE=None):
    """add zogiimage"""
    newObj=zogiImage(id,title,baseUrl, queryString)
    self.Destination()._setObject(id,newObj)
    if digilibUrl is not None and len(digilibUrl) > 0:
        newObj.setWithDigilibURL(digilibUrl)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')



class zogiLib(Folder):
    """digilib frontend with ZOPE"""

    meta_type="zogiLib"
    #xxxx
    security=ClassSecurityInfo()
    
    manage_options = Folder.manage_options+(
            {'label':'Main Config','action':'changeZogiLibForm'},
            )

    def __init__(self, id, title, dlServerURL, layout="book", basePath="", dlTarget=None, dlToolbarBaseURL=None):
        """init"""

        self.id = id
        self.title = title
        self.dlServerURL = dlServerURL
        self.basePath = basePath
        self.layout = layout
        self.dlTarget = dlTarget

        if dlToolbarBaseURL:
            self.dlToolbarBaseURL = dlToolbarBaseURL
        else:
            self.dlToolbarBaseURL = dlServerURL + "/digimage.jsp?"
            
        self.manage_addFolder('template')


    # form templates
    main_book      = PageTemplateFile('zpt/main_book', globals())
    main_image     = PageTemplateFile('zpt/main_image', globals())
    main_metaData  = PageTemplateFile('zpt/main_metadata', globals())
    #main_static    = PageTemplateFile('zpt/main_static', globals())
    options        = PageTemplateFile('zpt/options', globals())
    #changeForm     = PageTemplateFile('zpt/changeForm', globals())

    # display templates
    aux_divs       = PageTemplateFile('zpt/aux_divs', globals())
    #aux_divsN4     = PageTemplateFile('zpt/aux_divsN4', globals())
    img_div        = PageTemplateFile('zpt/img_div', globals())
    
    # javascripts
    head_js        = PageTemplateFile('zpt/head_js', globals())
    jslib_js       = PageTemplateFile('js/baselib.js', globals())
    dllib_js       = PageTemplateFile('js/dllib.js', globals())
    
    # graphic files
    arr_right = ImageFile('images/right.gif', globals())
    arr_left  = ImageFile('images/left.gif', globals())
    arr_up    = ImageFile('images/up.gif', globals())
    arr_down  = ImageFile('images/down.gif', globals())
    mark1     = ImageFile('images/mark1.gif', globals())
    mark2     = ImageFile('images/mark2.gif', globals())
    mark3     = ImageFile('images/mark3.gif', globals())
    mark4     = ImageFile('images/mark4.gif', globals())
    mark5     = ImageFile('images/mark5.gif', globals())
    mark6     = ImageFile('images/mark6.gif', globals())
    mark7     = ImageFile('images/mark7.gif', globals())
    mark8     = ImageFile('images/mark8.gif', globals())
    corner1   = ImageFile('images/olinks.gif', globals())
    corner2   = ImageFile('images/orechts.gif', globals())
    corner3   = ImageFile('images/ulinks.gif', globals())
    corner4   = ImageFile('images/urechts.gif', globals())    


    security.declareProtected('View','getLayout')
    def getLayout(self):
        """get Layout"""
        return self.layout
    
    def version(self):
        """version information"""
        return ZOGIVERSION

    def getContextStatic(self):
        """get all the contexts which go to static pages"""
        
        try:
            dom=xml.dom.minidom.parse(urllib.urlopen(self.getMetaFileName()))
            contexts=dom.getElementsByTagName("context")

            ret=[]
            for context in contexts:
                name=getUniqueElementText(context.getElementsByTagName("name"))

                link=getUniqueElementText(context.getElementsByTagName("link"))
                if name or link:
                    ret.append((name,link))
            return ret
        except:
            return []

    def getContextDatabases(self):
        """get all dynamic contexts"""
        try:
            dom=xml.dom.minidom.parse(urllib.urlopen(self.getMetaFileName()))
            contexts=dom.getElementsByTagName("context")
            ret=[]
            for context in contexts:
                metaDataLinks=context.getElementsByTagName("meta-datalink")
                for metaDataLink in metaDataLinks:
                    db=metaDataLink.getAttribute("db")
                    link=self.REQUEST['URL1']+"/dl_db?db=%s"%db
                    if db:
                        ret.append((db,link))
                metaDataLinks=context.getElementsByTagName("meta-baselink")

                for metaDataLink in metaDataLinks:
                    db=metaDataLink.getAttribute("db")
                    link=self.REQUEST['URL1']+"/dl_db?db=%s"%db
                    if db:
                        ret.append((db,link))

            return ret
        except:

            return []


    def formatHTML(self,url,label=None,viewUrl=None):

        sets=xml.dom.minidom.parse(urllib.urlopen(url)).getElementsByTagName('dataset')
        ret=""
        #print label
        if label:
            ret+="""<a href="%s">%s</a>"""%(viewUrl,label)
        for set in sets:
            ret+="<table>"
            for node in set.childNodes:
                if hasattr(node,'tagName'):
                    tag=node.tagName
                    label=node.getAttribute("label")
                    if not label:
                        label=tag
                    text=getText(node.childNodes)
                    ret+="""<tr><td><b>%s:</b></td><td>%s</td></tr>"""%(label,text)
            ret+="</table>"
        return ret

    
    def getMetaData(self):
        """getMetaData"""
        try:
            dom=xml.dom.minidom.parse(urllib.urlopen(self.getMetaFileName()))
        except:
            return "error metadata"
        
        contexts=dom.getElementsByTagName("context")
        ret=[]
        db=self.getDLParam("db")
        ob=self.getDLParam("object")
        
        fn=self.getDLParam("fn")
        pn=self.getDLParam("pn")
        if not fn:
            fn=""
        if not pn:
            pn=""
        if not ob:
            ob=""
            
        for context in contexts:
            metaDataLinks=context.getElementsByTagName("meta-datalink")
            for metaDataLink in metaDataLinks:
                 
                if (db==metaDataLink.getAttribute("db")) or (len(metaDataLinks)==1):
                    
                    link=getUniqueElementText(metaDataLink.getElementsByTagName("metadata-url"))
                    label=getUniqueElementText(metaDataLink.getElementsByTagName("label"))
                    url=getUniqueElementText(metaDataLink.getElementsByTagName("url"))

                    return self.formatHTML(link,label,url)

            metaDataLinks=context.getElementsByTagName("meta-baselink")
             
            for metaDataLink in metaDataLinks:
                
                if db==metaDataLink.getAttribute("db") or (len(metaDataLinks)==1):
                    
                    link=getUniqueElementText(metaDataLink.getElementsByTagName("metadata-url"))
                    label=getUniqueElementText(metaDataLink.getElementsByTagName("label"))
                    url=getUniqueElementText(metaDataLink.getElementsByTagName("url"))

                    return self.formatHTML(link+'fn=%s&pn=%s&object=%s'%(fn,pn,ob),label,url)
        return ret


    def getDLInfo(self):
        """get DLInfo from digilib server"""
        paramH={}
        baseUrl=self.getDLBaseUrl()+"/dlInfo-xml.jsp"
        #print "getdlinfo: ", baseUrl
        try:
            url=urllib.urlopen(baseUrl+'?'+self.getAllDLParams())
            dom=xml.dom.minidom.parse(url)
            params=dom.getElementsByTagName('parameter')
            for param in params:
                paramH[param.getAttribute('name')]=param.getAttribute('value')
            return paramH
        except:
            return {}


    def zogilibPathOLD(self, otherbase=None):
        """returns an URL to the zogiLib instance"""
        url = self.REQUEST['URL1']
        # should end with "/"
        if len(url) > 0 and url[-1] != '/':
            url += '/'
        if type(otherbase) is str:
            url += otherbase
        else:
            url += self.basePath
        # should still end with "/"
        if len(url) > 0 and url[-1] != '/':
            url += '/'
        return url

    def zogilibPath(self, otherbase=None):
        """returns an URL to the zogiLib instance"""
        url = self.absolute_url()
        # should end with "/"
        if len(url) > 0 and url[-1] != '/':
            url += '/'
        if type(otherbase) is str:
            url += otherbase
        else:
            url += self.basePath
        # should still end with "/"
        if len(url) > 0 and url[-1] != '/':
            url += '/'
        return url
    def zogilibAction(self, action, otherbase=None):
        """returns a URL with zogilib path and action"""
        url = self.zogilibPath(otherbase)
        url += action
        url += '?' + self.getAllDLParams();
        return url
    
    def getDLBaseUrl(self):
        """returns digilib base URL (sans servlet path)"""
        if self.dlServerURL[-1] == '?':
            # full Servlet URL -- remove last part
            si = self.dlServerURL.rindex('/servlet/')
            if si > 0:
                return self.dlServerURL[:si]
            else:
                # no servlet part :-(
                return "http://nausikaa.mpiwg-berlin.mpg.de/digitallibrary"
        else:
            return self.dlServerURL 
        

    def getScalerUrl(self,requestString=""):
        """send scaler url"""
        if self.dlServerURL[-1] == '?':
            # full Servlet URL
            return self.dlServerURL + requestString
        else:
            return self.dlServerURL+'/servlet/Scaler?'+requestString
    
    def scaledImage(self,requestString=None):
        """scaled Image"""
        
        if not requestString:
            requestString=self.REQUEST['QUERY_STRING']
            
        self.REQUEST.RESPONSE.redirect(self.getScalerUrl(requestString))
        
        return True
    
        
    def createScalerImg(self, requestString=None, bottom=0, side=0, width=500, height=500, options=None):
        """generate Scaler IMG Tag"""
        self.checkQuery()
        bt = self.getBrowserType()
        # override with parameters from session
        if  self.REQUEST.SESSION.has_key('scalerDiv'):
            (requestString, bottom, side, width, height) = self.REQUEST.SESSION['scalerDiv']
        # if not explicitly defined take normal request
        if not requestString:
            requestString = self.getAllDLParams()
        url = self.getScalerUrl(requestString=requestString)
        # take insets from options if present
        if options is not None:
            side = options.get('side', side)
            bottom = options.get('bottom', bottom)
        # construct bottom and side insets
        b_par = ""
        s_par = ""
        if (bottom != 0) or (side != 0):
            b_par = "-" + str(int(bottom))
            s_par = "-" + str(int(side))
        tag = ""
        if bt['staticHTML']:
            tag += '<div id="scaler"><img id="pic" src="%s&dw=%i&dh=%i" /></div>'%(url, int(width-side), int(height-bottom))
        else:
            if bt['isN4']:
                # N4 needs layers
                tag += '<ilayer id="scaler">'
            else:
                tag += '<div id="scaler">'
            tag += '<script type="text/javascript">'
            tag += "var ps = bestPicSize(getElement('scaler'));"
            # write img tag with javascript
            tag += 'document.write(\'<img id="pic" src="%s&dw=\'+(ps.width%s)+\'&dh=\'+(ps.height%s)+\'" />\');'%(url, s_par, b_par)
            tag += '</script>'
            if bt['isN4']:
                tag += '</ilayer>'
            else:
                tag += '</div>'
        return tag

    def createScalerDiv(self, requestString = None, bottom = 0, side = 0, width=500, height=500):
        """generate scaler img and table with navigation arrows"""
        self.checkQuery()
        if requestString != None or bottom != 0 or side != 0:
            self.REQUEST.SESSION['scalerDiv'] = (requestString, bottom, side, width, height)
        else:
            if self.REQUEST.SESSION.has_key('scalerDiv'):
                # make shure to remove unused parameter
                del self.REQUEST.SESSION['scalerDiv']
                
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt/img_div')).__of__(self)
        return pt()

    def index_html(self):
        """main action"""
        self.checkQuery()
        tp = "main_template"
        tpt = self.layout
        
        if not hasattr(self, 'template'):
            # create template folder if it doesn't exist
            print "no template folder -- creating"
            self.manage_addFolder('template')
            
        pt = getattr(self.template, 'main_'+tpt)
        return pt()

    def checkQuery(self):
        """make shure that the query has been saved"""
        if not self.REQUEST.has_key('dlParams'):
            self.storeQuery()

    def storeQuery(self, more=None, withpt=False):
        """parse query parameters into a hash in REQUEST"""
        params = {}
        for fm in self.REQUEST.form.keys():
            params[fm] = self.REQUEST.form[fm]
        # look for more
        if more:
            for fm in more.split('&'):
                try:
                    pv = fm.split('=')
                    params[pv[0]] = pv[1]
                except:
                    pass
                
        # parse digilib mode parameter
        if 'mo' in params:
            if len(params['mo']) > 0:
                modes=params['mo'].split(',')
        else:
            modes = [];

        self.REQUEST.set('dlParams', params)
        self.REQUEST.set('dlModes', modes)
        
        # trigger get pt (from dlInfo) if requested
        if withpt:
            pt = self.getPT()
            
        return params
        

    def getDLParam(self, param, default=None):
        """returns parameter or default"""
        self.checkQuery()
        dlParams = self.REQUEST.get('dlParams')
        try:
            return dlParams[param]
        except:
            return default

    def setDLParam(self, param, value):
        """sets parameter"""
        self.checkQuery()
        dlParams = self.REQUEST.get('dlParams')
        dlParams[param] = value
        return

    def getAllDLParams(self):
        """parameter string for digilib"""
        self.checkQuery()
        dlParams = self.REQUEST.get('dlParams')
        # save modes
        modes = self.REQUEST.get('dlModes')
        if modes:
            dlParams['mo'] = string.join(modes, ',')
        # assemble query string
        ret = ""
        for param in dlParams.keys():
            if dlParams[param] is None: continue
            val = str(dlParams[param])
            if val != "":
                ret += param + "=" + val + "&"

        # omit trailing "&"
        return ret.rstrip('&')
        
    def setDLParams(self,pn=None,ws=None,rot=None,brgt=None,cont=None):
        """setze Parameter"""

        self.setDLParam('brgt', brgt)
        self.setDLParam('cont', cont)
        self.setDLParam('ws', ws)
        self.setDLParam('rot', rot)

        if pn:
            # unmark
            self.setDLParam('mk', None)
            self.setDLParam('pn', pn)
            
        return self.display()

    def getBrowserType(self):
        """get browser type object"""
        if self.REQUEST.SESSION.has_key('browserType'):
            return self.REQUEST.SESSION['browserType']
        else:
            bt = browserCheck(self)
            self.REQUEST.SESSION.set('browserType', bt)                        
            return bt


    def display(self):
        """(re)display page"""
        params = self.getAllDLParams()
            
        if self.basePath:
            self.REQUEST.RESPONSE.redirect(self.REQUEST['URL2']+'?'+params)
        else:
            self.REQUEST.RESPONSE.redirect(self.REQUEST['URL1']+'?'+params)

    def getMetaFileName(self):
        url=self.dlServerURL+'/dlContext-xml.jsp?'+self.getAllDLParams()
        return urlbase

    def getToolbarPageURL(self):
        """returns a toolbar-enabled page URL"""
        url=self.dlToolbarBaseURL+self.getAllDLParams()
        return url
    
    def getDLTarget(self):
        """returns dlTarget"""
        self.checkQuery()
        s = self.dlTarget
        if (s is None) or (s == ""):
#            s = ""
            s = 'dl'
            if self.getDLParam('fn'):
                s += "_" + self.getDLParam('fn')
            if self.getDLParam('pn'):
                s += "_" + self.getDLParam('pn')

        return s

    def getPN(self):
        """pagenumber"""
        pn = int(self.getDLParam('pn', 1))
        return pn
    
    def getPT(self):
        """number of total pages"""
        pt = self.getDLParam('pt', None)
        if pt is None:
            # get pt from dlInfo
            if self.REQUEST.has_key('dlInfo'):
                info = self.REQUEST.get('dlInfo')
            else:
                info = self.getDLInfo()
                self.REQUEST.set('dlInfo', info)
            pt = int(info.get('pt', 1))
            self.setDLParam('pt', pt)
        return int(pt)
    
    def hasMode(self, mode):
        """returns if mode is in the diglib mo parameter"""
        return (mode in self.REQUEST.get('dlModes'))

    def hasNextPage(self):
        """returns if there is a next page"""
        pn = self.getPN()
        pt = self.getPT()
        return (pn < pt)
   
    def hasPrevPage(self):
        """returns if there is a previous page"""
        pn = self.getPN()
        return (pn > 1)

    def canMoveLeft(self):
        """returns if its possible to move left"""
        wx = float(self.getDLParam('wx') or 0)
        return (wx > 0)

    def canMoveRight(self):
        """returns if its possible to move right"""
        wx = float(self.getDLParam('wx') or 0)
        ww = float(self.getDLParam('ww') or 1)
        return (wx + ww < 1)

    def canMoveUp(self):
        """returns if its possible to move up"""
        wy = float(self.getDLParam('wy') or 0)
        return (wy > 0)

    def canMoveDown(self):
        """returns if its possible to move down"""
        wy = float(self.getDLParam('wy') or 0)
        wh = float(self.getDLParam('wh') or 1)
        return (wy + wh < 1)


    def dl_StaticHTML(self):
        """set rendering to static HTML"""
        self.checkQuery()
        self.getBrowserType()['staticHTML'] = True
        return self.display()

    def dl_DynamicHTML(self):
        """set rendering to dynamic HTML"""
        self.checkQuery()
        self.getBrowserType()['staticHTML'] = False
        return self.display()
        
    def dl_HMirror(self):
        """mirror action"""
        modes = self.getSubSession('dlModes')
        if 'hmir' in modes:
            modes.remove('hmir')
        else:
            modes.append('hmir')

        return self.display()
       
    def dl_VMirror(self):
        """mirror action"""
        modes = self.getSubSession('dlModes')
        if 'vmir' in modes:
            modes.remove('vmir')
        else:
            modes.append('vmir')

        return self.display()

    def dl_Zoom(self, z):
        """general zoom action"""
        ww1 = float(self.getDLParam('ww') or 1)
        wh1 = float(self.getDLParam('wh') or 1)
        wx = float(self.getDLParam('wx') or 0)
        wy = float(self.getDLParam('wy') or 0)
        ww2 = ww1 * z
        wh2 = wh1 * z
        wx += (ww1 - ww2) / 2
        wy += (wh1 - wh2) / 2
        ww2 = max(min(ww2, 1), 0)
        wh2 = max(min(wh2, 1), 0)
        wx = max(min(wx, 1), 0)
        wy = max(min(wy, 1), 0)
        self.setDLParam('ww', cropf(ww2))
        self.setDLParam('wh', cropf(wh2))
        self.setDLParam('wx', cropf(wx))
        self.setDLParam('wy', cropf(wy))
        return self.display()
        
    def dl_ZoomIn(self):
        """zoom in action"""
        z = 0.7071
        return self.dl_Zoom(z)

    def dl_ZoomOut(self):
        """zoom out action"""
        z = 1.4142
        return self.dl_Zoom(z)

    def dl_Move(self, dx, dy):
        """general move action"""
        ww = float(self.getDLParam('ww') or 1)
        wh = float(self.getDLParam('wh') or 1)
        wx = float(self.getDLParam('wx') or 0)
        wy = float(self.getDLParam('wy') or 0)
        wx += dx * 0.5 * ww
        wy += dy * 0.5 * wh
        wx = max(min(wx, 1), 0)
        wy = max(min(wy, 1), 0)
        self.setDLParam('wx', cropf(wx))
        self.setDLParam('wy', cropf(wy))
        return self.display()
        
    def dl_MoveLeft(self):
        """move left action"""
        return self.dl_Move(-1, 0)
    
    def dl_MoveRight(self):
        """move left action"""
        return self.dl_Move(1, 0)
    
    def dl_MoveUp(self):
        """move left action"""
        return self.dl_Move(0, -1)
    
    def dl_MoveDown(self):
        """move left action"""
        return self.dl_Move(0, 1)
    
    def dl_WholePage(self):
        """zoom out action"""
        self.setDLParam('ww', 1)
        self.setDLParam('wh', 1)
        self.setDLParam('wx', 0)
        self.setDLParam('wy', 0)
        return self.display()
        
    def dl_PrevPage(self):
        """next page action"""
        pn = self.getPN() - 1
        if pn < 1:
            pn = 1
        self.setDLParam('pn', pn)
        # unmark
        self.setDLParam('mk', None)
        return self.display()
        
    def dl_NextPage(self):
        """next page action"""
        pn = self.getPN() + 1
        pt = self.getPT()
        if pn > pt:
            pn = pt
        self.setDLParam('pn', pn)
        # unmark
        self.setDLParam('mk', None)
        return self.display()

    def dl_FirstPage(self):
        """first page action"""
        self.setDLParam('pn', 1)
        # unmark
        self.setDLParam('mk', None)
        return self.display()
    
    def dl_LastPage(self):
        """last page action"""
        self.setDLParam('pn', self.getPT())
        # unmark
        self.setDLParam('mk', None)
        return self.display()

    def dl_db(self,db):
        """set db"""
        self.setDLParam('db',db)
        self.display()

    def changeZogiLibForm(self):
        """Main configuration"""
        pt=PageTemplateFile(os.path.join(package_home(globals()), 'zpt/changeZogiLibForm.zpt')).__of__(self)
        return pt()
    
    def changeZogiLib(self,title,dlServerURL, version, basePath, dlTarget, dlToolbarBaseURL, RESPONSE=None):
        """change it"""
        self.title=title
        self.dlServerURL=dlServerURL
        self.basePath = basePath
        self.layout=version
        self.dlTarget = dlTarget

        if dlToolbarBaseURL:
            self.dlToolbarBaseURL = dlToolbarBaseURL
        else:
            self.dlToolbarBaseURL = dlServerURL + "/digimage.jsp?"

        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')



    ##
    ## odds and ends
    ##

    def repairZogilib(self, obj=None):
        """change stuff that broke on upgrading"""

        msg = ""

        if not obj:
            obj = self.getPhysicalRoot()

        print "starting in ", obj
        
        entries=obj.ZopeFind(obj,obj_metatypes=['zogiLib'],search_sub=1)

        for entry in entries:
            print "  found ", entry
            #
            # replace digilibBaseUrl by dlServerURL
            if hasattr(entry[1], 'digilibBaseUrl'):
                msg += "  fixing digilibBaseUrl in "+entry[0]+"\n"
                entry[1].dlServerURL = re.sub('/servlet/Scaler\?','',entry[1].digilibBaseUrl)
                del entry[1].digilibBaseUrl
                
            #
            # add dlToolbarBaseURL
            if not hasattr(entry[1], 'dlToolbarBaseURL'):
                msg += "  fixing dlToolbarBaseURL in "+entry[0]+"\n"
                entry[1].dlToolbarBaseURL = entry[1].dlServerURL + "/digimage.jsp?"
                
        return msg+"\n\nfixed all zogilib instances in: "+obj.title

          
def manage_addZogiLibForm(self):
    """interface for adding zogilib"""
    pt=PageTemplateFile(os.path.join(package_home(globals()), 'zpt/addZogiLibForm')).__of__(self)
    return pt()

def manage_addZogiLib(self,id,title,dlServerURL,version="book",basePath="",dlTarget=None,dlToolbarBaseURL=None,RESPONSE=None):
    """add dgilib"""
    newObj=zogiLib(id,title,dlServerURL, version, basePath, dlTarget, dlToolbarBaseURL)
    self.Destination()._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')


class zogiLibPageTemplate(ZopePageTemplate):
    """pageTemplate Objekt"""
    meta_type="zogiLib_pageTemplate"


## def __init__(self, id, text=None, contentType=None):
##         self.id = str(id)
##         self.ZBindings_edit(self._default_bindings)
##         if text is None:
##             text = open(self._default_cont).read()
##         self.pt_edit(text, contentType)

def manage_addZogiLibPageTemplateForm(self):
    """Form for adding"""
    pt=PageTemplateFile(os.path.join(package_home(globals()), 'zpt/addZogiLibPageTemplateForm')).__of__(self)
    return pt()

def manage_addZogiLibPageTemplate(self, title=None, layout=None, text=None,
                           REQUEST=None, submit=None):
    "Add a Page Template with optional file content."

    if not layout: layout = "book"
    id = 'main_%s'%layout
    self._setObject(id, zogiLibPageTemplate(id))
    ob = getattr(self, id)
    ob.pt_edit(open(os.path.join(package_home(globals()),'zpt/main_%s.zpt'%layout)).read(),'text/html')
    if title:
        ob.pt_setTitle(title)
    try:
        url = self.DestinationURL()
    except AttributeError:
        url = REQUEST['URL1']
        
    url = "%s/%s" % (url, urllib.quote(id))
    REQUEST.RESPONSE.redirect(url+'/manage_main')
    return ''

