import zogiLib


def initialize(context):
    """initialize zogilib"""

    context.registerClass(
        zogiLib.zogiImage,
        constructors = (
          zogiLib.manage_addZogiImageForm,
          zogiLib.manage_addZogiImage
          )
        )

    context.registerClass(
        zogiLib.zogiLib,
        constructors = (
          zogiLib.manage_addZogiLibForm,
          zogiLib.manage_addZogiLib
          )
        )

    context.registerClass(
        zogiLib.zogiLibPageTemplate,
        constructors = (
          zogiLib.manage_addZogiLibPageTemplateForm,
          zogiLib.manage_addZogiLibPageTemplate
          )
        )

