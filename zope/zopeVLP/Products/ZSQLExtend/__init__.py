import ZSQLExtend
import ZSQLMetaData
import ZSQLUpdate
import VLMAExtensions
import Index

def initialize(context):
    """initialize OSAS"""
    context.registerClass(
        ZSQLExtend.ZSQLExtendFolder,
        constructors = (
          ZSQLExtend.manage_addZSQLExtendFolderForm,
          ZSQLExtend.manage_addZSQLExtendFolder
          )
        )

    context.registerClass(
        ZSQLExtend.ZSQLBibliography,
        constructors = (
          ZSQLExtend.manage_addZSQLBibliographyForm,
          ZSQLExtend.manage_addZSQLBibliography
          )
        )

    context.registerClass(
        ZSQLMetaData.ZSQLMetadataMappingRoot,
        constructors = (
          ZSQLMetaData.manage_addZSQLMetadataMappingRootForm,
          ZSQLMetaData.manage_addZSQLMetadataMappingRoot
          )
        )

    context.registerClass(
        ZSQLMetaData.ZSQLMetadataMapping,
        constructors = (
          ZSQLMetaData.manage_addZSQLMetadataMappingForm,
          ZSQLMetaData.manage_addZSQLMetadataMapping
          )
        )

    context.registerClass(
        ZSQLUpdate.ZSQLUpdate,
        constructors = (
          ZSQLUpdate.manage_addZSQLUpdateForm,
          ZSQLUpdate.manage_addZSQLUpdate
          )
        )

    context.registerClass(
        VLMAExtensions.VLMATriples,
        constructors = (
          VLMAExtensions.manage_addVLMATriplesForm,
          VLMAExtensions.manage_addVLMATriples
          )
        )

    context.registerClass(
                          Index.IndexManager,
                          constructors =(
                                         Index.manage_addIndexManagerForm,
                                         Index.manage_addIndexManager
                                         )
                          )

methods={
    # We still need this one, at least for now, for both editing and
    # adding.  Ugh.
    'SQLConnectionIDs': ZSQLExtend.showSQLConnectionIDs,

    }
