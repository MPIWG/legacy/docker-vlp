from OFS.Folder import Folder
from Globals import DTMLFile,package_home
from tempfile import mkstemp
from AccessControl import getSecurityManager
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
import urllib
import os.path


import logging

#ersetzt logging
def logger(txt,method,txt2):
    """logging"""
    logging.info(txt+ txt2)



"""Webfrontend fuer das Update der Datenbanken ueber das Web mithilfe von
fm.jar
"""

class ZSQLUpdate(Folder):
    """Klasse zum update, in den Folder werden die verschiedenen Update XML files hineingelegt"""
    meta_type="ZSQLUpdate"

    def update(self,xmlurl=None,xmlobject=None):
        """update by calling fm.jar and using the xmlfile at xmlurl
        @param xmlurl: url auf ein xml file fuer fm.jar
        """


        
        tmphandler,path =mkstemp()
        tmpFile=os.fdopen(tmphandler,"w+b")
        
        if xmlurl:
            xmlf=urllib.urlopen(xmlurl).read()
        if xmlobject:
            try:
                xmlf=getattr(self,xmlobject)()
            except:
                xmlf=getattr(self,xmlobject).data

        
        tmpFile.write(xmlf)
        tmpFile.close()

        #path=tmpFile.name
        
        fmJarPath=os.path.join(package_home(globals()), 'fm.jar')
        
        
        
        logger("MPIWG Web",logging.INFO,"java -classpath %s -Djava.awt.headless=true Convert %s"%(fmJarPath,path))
        ret=os.popen("java -classpath %s -Djava.awt.headless=true Convert %s"%(fmJarPath,path),"r").read()
	logger("MPIWG Web",logging.INFO,"result convert: %s"%ret)

        os.unlink(path)
        return "<html><body><h1>SQL update</h1><p>(notice, everything worked fine if only <q>java.lang.ClassNotFoundException: sun.jdbc.odbc.JdbcOdbcDriver</q> comes out as result)</p><p>Result:<br>%s</p></html></body>"%ret

    def findUpdater(self):
        """find Update, giebt alle Update files als Tupel (id,objekte) zuruck"""
        return self.ZopeFind(self,obj_metatypes=['File','Page Template'])
    def index_html(self):
        """standard ansicht"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt/viewUpdates.zpt')).__of__(self)
        return pt()

manage_addZSQLUpdateForm=DTMLFile('ZSQLUpdateAdd', globals())

def manage_addZSQLUpdate(self, id, title='', label='', description='',
                     createPublic=0,
                     createUserF=0,
                     connection_id=None,
                     REQUEST=None):
    """Add a new Folder object with id *id*.

    If the 'createPublic' and 'createUserF' parameters are set to any true
    value, an 'index_html' and a 'UserFolder' objects are created respectively
    in the new folder.
    """
    
    
    ob=ZSQLUpdate()
    ob.id=str(id)
    ob.title=title
    self._setObject(id, ob)
    ob=self._getOb(id)
    setattr(ob,'label',label)

    
    checkPermission=getSecurityManager().checkPermission


    if REQUEST is not None:
        return self.manage_main(self, REQUEST, update_menu=1)


