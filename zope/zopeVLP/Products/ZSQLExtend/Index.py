from BTrees.OOBTree import OOBTree
from OFS.Folder import Folder
from AccessControl import ClassSecurityInfo
from OFS.SimpleItem import SimpleItem
import re

class OOIndex(SimpleItem):
    """classe fuer indices"""
 
    security=ClassSecurityInfo()
    def __init__(self,id,results,key,value):
        self._index=OOBTree()
        self.id=id
        for result in results:
            ke=getattr(result,key)
            va=getattr(result,value)
            self._index[ke]=va
    
    def update(self,results,key,value):
        del self._index
        
        self.__init__(self.id,results, key, value)
        
    security.declarePublic("getKeys")
    def getKeys(self, pattern):
        """getkeys"""
        #method comes from Lexicon.py (globToWordIds only slightly modified
        # Implement * and ? just as in the shell, except the pattern
        # must not start with either of these
        prefix = ""
        while pattern and pattern[0] not in "*?":
            prefix += pattern[0]
            pattern = pattern[1:]
        if not pattern:
            # There were no globbing characters in the pattern
            wid = self._index.get(prefix, 0)
            if wid:
                return [prefix]
            else:
                return []
        if not prefix:
            # The pattern starts with a globbing character.
            # This is too efficient, so we raise an exception.
            raise QueryError(
                "pattern %r shouldn't start with glob character" % pattern)
        pat = prefix
        for c in pattern:
            if c == "*":
                pat += ".*"
            elif c == "?":
                pat += "."
            else:
                pat += re.escape(c)
        pat += "$"
        prog = re.compile(pat)
        keys = self._index.keys(prefix) # Keys starting at prefix
        wids = []
        for key in keys:
            if not key.startswith(prefix):
                break
            if prog.match(key):
                wids.append(key)
        return wids


class IndexManager(Folder):
    """indexManager"""
    meta_type="IndexManager"
    
    def __init__(self):
        self.id="IndexManager"
        self._indexes={}
    def createIndex(self,id,results,key,value):
        """create an index"""
        index=OOIndex(id,results,key,value)
        self._setOb(id, index)
        ob=self._getOb(id, None)
        ob.id=id
        return index
    
    def updateIndex(self,id,results,key,value):
        """update or create an index"""
        
        index=self._getOb(id, None)
        if not index:
            self.createIndex(id,results,key,value)
        else:
            index.update(results,key,value)
        
        return index
    
    def getKeysFromIndex(self,name,pattern):
        """get keys"""
        index=self._getOb(name, None)
        if not index:
            return []
        else:
            return index.getKeys(pattern)
        
def manage_addIndexManager(self):
    """add an index Manager"""
    obj=IndexManager()
    
    self._setObject("IndexManager",obj)
    
manage_addIndexManagerForm=manage_addIndexManager #no user interaction needed

            
        
        
        