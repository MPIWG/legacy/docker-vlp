###Extensions for VLMA-Triples
from OFS.Folder import Folder
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from OFS.SimpleItem import SimpleItem
from Globals import package_home
import Globals
from AccessControl import ClassSecurityInfo

import os.path

security=ClassSecurityInfo()
security.declarePublic('VLMAObject')

class VLMAObject(SimpleItem):
    """VLMAObject"""

    
    def __init__(self,subject,subjectField,object,objectField,predicate=None):
        self.subject=subject
        self.subjectField=subjectField
        self.object=object
        self.objectField=objectField
        self.predicate=predicate

    def change(self,subject,subjectField,object,objectField,predicate=None):
        self.subject=subject[0:]
        self.subjectField=subjectField[0:]
        self.object=object[0:]
        self.objectField=objectField[0:]
        if predicate:
            self.predicate=predicate[0:]
        else:
            self.predicate=None

    



Globals.InitializeClass(VLMAObject)

class VLMATriples(Folder):
    """Klasse zur Erzeugung der Triplefiles fuer VLMA, muss subobject eine ZSQLExtendFolders sein"""

    meta_type="VLMATriples"

    manage_options=Folder.manage_options+(
        {'label':'Main Config','action':'changeVLMATriplesForm'},
       )

    def __init__(self,id,searchStatement,collectionObject,imageObject=None,thumbObject=None,title=""):
        """init
        @param collectionObject: VLMAObject
        @param imageObject: VLMAObject
        @param thumbObject: VLMAObject"""
        
        self.id=id
        self.title=title
        self._setObject('collectionObject',collectionObject)
        self._setObject('imageObject',imageObject)
        self._setObject('thumbObject',thumbObject)
        self.searchStatement=searchStatement

    def getCollectionObject(self,field):
        """get the obejct"""
        return getattr(self.collectionObject,field)

    def getImageObject(self,field):
        """get the obejct"""
        return getattr(self.imageObject,field,'')

    def getThumbObject(self,field):
        """get the obejct"""
        return getattr(self.thumbObject,field,'')
    
    def createVLMATriples(self):
        """standardview"""

        ret=""

        #collectionobject triple
        
        for found in self.ZSQLSimpleSearch(self.searchStatement):
            
            subject=self.collectionObject.subject%getattr(found,self.collectionObject.subjectField)
            object=self.collectionObject.object%getattr(found,self.collectionObject.objectField)
            predicate="metadata"
            ret+="%s\t%s\t%s\n"%(subject,object,predicate)
            if self.imageObject:
                subject=self.imageObject.subject%getattr(found,self.imageObject.subjectField)
                object=self.imageObject.object%getattr(found,self.imageObject.objectField)
                predicate="image"
                ret+="%s\t%s\t%s\n"%(subject,object,predicate)

            if self.thumbObject:
                subject=self.thumbObject.subject%getattr(found,self.thumbObject.subjectField)
                object=self.thumbObject.object%getattr(found,self.thumbObject.objectField)
                predicate="thumb"
                ret+="%s\t%s\t%s\n"%(subject,object,predicate)
            
        return ret

    index_html=createVLMATriples

    def changeVLMATriplesForm(self):
        """Form for adding a VLMATriplesObject"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','changeVLMATriples.zpt')).__of__(self)
        return pt()

    def changeVLMATriples(self,searchStatement,cS,cSF,cO,cOF,iS,iSF,iO,iOF,tS,tSF,tO,tOF,title="",REQUEST=None):
        """changing triples"""
        self.title=title[0:]
        self.searchStatement=searchStatement[0:]
        self.collectionObject.change(cS,cSF,cO,cOF)
        if iS=="":
            self.imageObject=None
        elif self.imageObject is not None:
            self.imageObject.change(iS,iSF,iO,iOF)
	else:
	    iOb=VLMAObject(iS,iSF,iO,iOF)	
	    self._setObject('imageObject',iOb)
        if tS=="":
            self.thumbObject=None
        elif self.thumbObject is not None:
            self.thumbObject.change(tS,tSF,tO,tOF)
	else:
	     tOb=VLMAObject(tS,tSF,tO,tOF)
	     self._setObject('thumbObject',tOb)
        if REQUEST is not None:
            return self.manage_main(self, REQUEST)

def manage_addVLMATriplesForm(self):
    """Form for adding a VLMATriplesObject"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addVLMATriples.zpt')).__of__(self)
    return pt()

def manage_addVLMATriples(self,id,searchStatement,cS,cSF,cO,cOF,iS,iSF,iO,iOF,tS,tSF,tO,tOF,title="",REQUEST=None):
    """Add a VLMATriples Object"""
    cOb=VLMAObject(cS,cSF,cO,cOF)
    if iS=="":
        iOb=None
    else:
        iOb=VLMAObject(iS,iSF,iO,iOF)
    if tS=="":
        tOb=None
    else:
        tOb=VLMAObject(tS,tSF,tO,tOF)

    ob=VLMATriples(id,searchStatement,cOb,iOb,tOb,title=title)

    self._setObject(id,ob)

    if REQUEST is not None:
        return self.manage_main(self, REQUEST, update_menu=1)

    
