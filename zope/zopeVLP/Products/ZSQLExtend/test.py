from Ft.Xml import Parse
#logger("import xsd",logging.INFO,"called")
#fh=file("/tmp/fmpxml.xml")
import bz2
import base64

filename="/tmp/People.xsd"
elementNameForTable="People"
data=False

ret=""
if data:
  data=bz2.decompress(base64.decodestring(data))

  #logger("import xsd",logging.INFO,"received file")
  doc=Parse(data)
  #logger("import xsd",logging.INFO,"parsed file")

elif filename:
  fh=file(filename)
  txt=fh.read()
  
  doc=Parse(txt)
  #logger("import xsd",logging.INFO,"parsed file")


Nss={'xsd':'http://www.w3.org/2001/XMLSchema'}
definingSequence=doc.xpath("""//xsd:element[@name='%s']/xsd:complexType/xsd:sequence/xsd:element/@name"""%elementNameForTable,explicitNss=Nss)

columns=[x.value for x in definingSequence]


