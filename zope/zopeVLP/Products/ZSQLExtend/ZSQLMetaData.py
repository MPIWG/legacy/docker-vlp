#objekte zur erzeugung und verwaltung von metadaten

from OFS.Folder import Folder
from Globals import Persistent
from Acquisition import Implicit
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
from OFS.SimpleItem import SimpleItem

import string


class ZSQLMetadataMapping(SimpleItem):
    """Einfaches Mapping Object"""

    meta_type="ZSQLMetadataMapping"

    def readFieldsFromParent(self):
        """read all elements from root"""
        return self.ac_parent.fields
    
    def __init__(self,id,title,arglist):
        """init"""
        self.id=id
        self.title=title

        for fieldName in arglist.keys():
            setattr(self,"md_"+fieldName,arglist[fieldName])

    manage_options = SimpleItem.manage_options+(
        {'label':'Main Config','action':'change_ZSQLMappingForm'},
        )

    def getValue(self,fieldName):
        """get md value"""
        try:
            return getattr(self,"md_"+fieldName)
        except:
            return ""
        
    def generateMappingHash(self):
        """erzeugen des Hash"""
        hash={}
        for field in self.fieldList:
            hash[field]=getattr(self,"md_"+field)
        return hash

   
    
    def change_ZSQLMappingForm(self):
        """change"""
        pt=PageTemplateFile('Products/ZSQLExtend/ChangeZSQLMetadataMapping.zpt').__of__(self)
        return pt()
    
    def change_ZSQLMapping(self,titleOfObject,RESPONSE=None):
        """change"""

        self.title=titleOfObject
        arglist=self.REQUEST.form
        for fieldName in arglist.keys():
            setattr(self,"md_"+fieldName,arglist[fieldName])

        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    

def manage_addZSQLMetadataMappingForm(self):
    """interface for adding the OSAS_root"""
    pt=PageTemplateFile('Products/ZSQLExtend/AddZSQLMetadataMapping.zpt').__of__(self)
    return pt()

def manage_addZSQLMetadataMapping(self,idOfObject,titleOfObject,RESPONSE=None):
    """add the OSAS_root"""
    
    argList={}
    for arg in self.REQUEST.form.keys():
        if not (arg in ['idOfObject','titleOfObject']):
            argList[arg]=self.REQUEST.form[arg]
        
    newObj=ZSQLMetadataMapping(idOfObject,titleOfObject,argList)
    self._setObject(idOfObject,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

   
class ZSQLMetadataMappingRoot(Persistent, Implicit, Folder):
    """Root ordner fuer Metadaten set"""

    meta_type='ZSQLMetadataMappingRoot'

    def __init__(self,id,title,fields):
        self.id=id
        self.title=title
        
        self.fieldList=fields.split(",")[0:]

    
    manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'change_ZSQLMappingRootForm'},
        )


    def generateMappingList(self):
        """Erzeuge Mapping"""
        mapping={}
        for dict in self.__dict__:
            #print dict
            obj=getattr(self,dict)
            if hasattr(obj,'meta_type'):
                if obj.meta_type=="ZSQLMetadataMapping":
                    mapping[obj.getId()]=obj.generateMappingHash()
        return mapping
    
    def generateMappingForType(self,type,clean="yes"):
        """erzeuge spezifisches Mapping"""
        hash=self.generateMappingList()
        if hash.has_key(type):
            if clean=="yes":
                temp={}
                for x in hash[type].keys():
                    if not hash[type][x]=="":
                        temp[x]=hash[type][x]
                return temp
            else:
                return hash[type]
        
        else:
            return {}

    def getFieldList(self):
        """erzeuge string aus fields"""
        return string.join(self.fieldList,",")

    def getFields(self):
        """ausgabe der Felder"""
        return self.fieldList

    def getTypeTitle(self,id):
        """Title von ID"""
        try:
            obj=getattr(self,id)
            return obj.title
        except:
            return id
        
    def getStoredTypes(self):
        """Gebe gespeicherte typen zurueck"""
        
        types=[]
        for dict in self.__dict__:
            
            obj=getattr(self,dict)
            if hasattr(obj,'meta_type'):
                if obj.meta_type=="ZSQLMetadataMapping":
                    #print obj
                    if obj.title=="":
                        title=obj.id
                    else:
                        title=obj.title
                    types.append((obj.id,title))
        return types
    
    def change_ZSQLMappingRootForm(self):
        """change"""
        pt=PageTemplateFile('Products/ZSQLExtend/ChangeZSQLMetadataMappingRoot.zpt').__of__(self)
        return pt()


    def change_ZSQLMetadataMapping(self,title,fields,RESPONSE=None):
        """change entries"""
        self.title=title
        self.fieldList=fields.split(",")[0:]

        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    

def manage_addZSQLMetadataMappingRootForm(self):
    """interface for adding the OSAS_root"""
    pt=PageTemplateFile('Products/ZSQLExtend/AddZSQLMetadataMappingRoot.zpt').__of__(self)
    return pt()

def manage_addZSQLMetadataMappingRoot(self,id,title,fields,RESPONSE=None):
    """add the OSAS_root"""
    newObj=ZSQLMetadataMappingRoot(id,title,fields)
    self._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

    
