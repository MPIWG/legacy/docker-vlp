"""Methoden um einen Folder zu generieren"""
import os
import re
import os.path
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate

def addFolder(self,path,folder_name,description,archive_creation_date,creator):
	
	if not os.path.exists(path+"/"+folder_name):		
		os.mkdir(path+"/"+folder_name)
		os.chmod(path+"/"+folder_name,0775)
	add_metadata={}
	
	add_metadata['description']=unicode(description,'latin-1')
	
	add_metadata['archive-creation-date']=archive_creation_date
        add_metadata['folder_name']=folder_name
	add_metadata['archive-path']=path
	add_metadata['creator']=unicode(creator,'latin-1')
	add_metadata['content-type']="folder"
	self.REQUEST.SESSION['add_metadata']=add_metadata
	
	self.metadataprint=""
	
	newtemplate=PageTemplateFile('Products/OSA_system/zpt/index_meta').__of__(self)
	newtemplate.content_type="text/xml"
	renderxml = newtemplate()
	
	
	f=open(add_metadata['archive-path']+"/"+add_metadata['folder_name']+"/index.meta",'w')
	
	f.write(renderxml.encode('utf8'))
	f.close()
	#return renderxml
	os.chmod(add_metadata['archive-path']+"/"+add_metadata['folder_name']+"/index.meta",0644)
	try:
		self.viewpath=re.search(r"/mpiwg/online/(.*)",add_metadata['archive-path']).group(1)
	except:
		self.viewpath=""
	#newtemplate2=PageTemplateFile('/usr/local/mpiwg/Zope/Extensions/saved',"text/html").__of__(self)
	#newtemplate2.content_type="text/html"
	self.REQUEST.response.setHeader('Content-Type','text/html')
	#rval=self.aq_acquire(self.REQUEST['URL'])
	return self.REQUEST.response.redirect(self.REQUEST['URL1']+'?path='+path)
	#return "DONE"
