""" Begin March,17 2004, Class OSAS_viewerTemplateSet contains all layout information for the viewenvironment"""

from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate
from Acquisition import Implicit
from OFS.Folder import Folder
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from urllib import quote
from Products.PageTemplates.PageTemplate import PageTemplate
import os
from Globals import package_home


def copyContent(self,filename):
        """copycontent to path"""
	text= self.document_src({'raw':1})
        #text= self.read()
	#text=getattr(self,'source.html') 
        if not os.path.exists(self.templatePath+"/"+self.aq_parent.getId()):
            os.mkdir(self.templatePath+"/"+self.aq_parent.getId())
	    os.chmod(self.templatePath+"/"+self.aq_parent.getId(),0755)
        path=self.templatePath+"/"+self.aq_parent.getId()+"/"+filename
        fh=file(path,"w")
        
        fh.write(text)
        fh.close()
        os.chmod(path,0664)
        return path

def getPath(self,filename):
    """get path"""
    return self.templatePath+"/"+self.aq_parent.getId()+"/"+filename

class OSAS_thumbTemplate(ZopePageTemplate):
    """pageTemplate Objekt"""
    meta_type="OSAS_thumbTemplate"

    _default_content_fn = os.path.join(package_home(globals()),
                                       'zpt/OSAS_thumbTemplateDefault.zpt')

    manage_options=ZopePageTemplate.manage_options+(
        {'label':'Copy to Filesystem','action':'copyContent'},
       )

    def getPath(self):
        """get path"""
        return getPath(self,'thumbtemplate.templ')
    
    ## def changeECHO_pageTemplateWeightForm(self):
##         """change"""
##         pt=PageTemplateFile('Products/ECHO_content/zpt/ChangeECHO_pageTemplateWeight.zpt').__of__(self)
##         return pt()

##     def changeECHO_pageTemplateWeight(self,weight,content_type,RESPONSE=None):
##         """change"""
##         self.weight=weight
##         self.content_type=content_type

##         if RESPONSE is not None:
##             RESPONSE.redirect('manage_main')
        

    def copyContent(self):
        """copycontent to path"""

        return "copied to:"+copyContent(self,'thumbtemplate.templ')
        
    
def manage_addOSAS_thumbTemplateForm(self):
    """Form for adding"""
    pt=PageTemplateFile('Products/ECHO_content/zpt/AddOSAS_thumbTemplate.zpt').__of__(self)
    return pt()




def manage_addOSAS_thumbTemplate(self, id,title=None, text=None,
                           REQUEST=None, submit=None):
    "Add a Page Template with optional file content."

    
    id = str(id)
    if REQUEST is None:
        self._setObject(id, OSAS_thumbTemplate(id, text))
        ob = getattr(self, id)
       
        if title:
            ob.pt_setTitle(title)
        return ob
    else:
        file = REQUEST.form.get('file')
        headers = getattr(file, 'headers', None)
        if headers is None or not file.filename:
            zpt = OSAS_thumbTemplate(id)
        else:
            zpt = OSAS_thumbTemplate(id, file, headers.get('content_type'))

        self._setObject(id, zpt)
        ob = getattr(self, id)


        try:
            u = self.DestinationURL()
        except AttributeError:
            u = REQUEST['URL1']

        if submit == " Add and Edit ":
            u = "%s/%s" % (u, quote(id))
        REQUEST.RESPONSE.redirect(u+'/manage_main')
    return ''

class OSAS_thumbRuler(ZopePageTemplate):
    """pageTemplate Objekt"""
    meta_type="OSAS_thumbRuler"

    _default_content_fn = os.path.join(package_home(globals()),
                                       'zpt/OSAS_thumbRulerDefault.zpt')

    manage_options=ZopePageTemplate.manage_options+(
        {'label':'Copy to Filesystem','action':'copyContent'},
       )

    def getPath(self):
        """get path"""
        return getPath(self,'thumbruler.tmpl')
    
    ## def changeECHO_pageTemplateWeightForm(self):
##         """change"""
##         pt=PageTemplateFile('Products/ECHO_content/zpt/ChangeECHO_pageTemplateWeight.zpt').__of__(self)
##         return pt()

##     def changeECHO_pageTemplateWeight(self,weight,content_type,RESPONSE=None):
##         """change"""
##         self.weight=weight
##         self.content_type=content_type

##         if RESPONSE is not None:
##             RESPONSE.redirect('manage_main')
        

    def copyContent(self):
        """copycontent to path"""

        return "copied to:"+copyContent(self,'thumbruler.tmpl')

def manage_addOSAS_thumbRulerForm(self):
    """Form for adding"""
    pt=PageTemplateFile('Products/ECHO_content/zpt/AddOSAS_thumbRuler.zpt').__of__(self)
    return pt()




def manage_addOSAS_thumbRuler(self, id,title=None, text=None,
                           REQUEST=None, submit=None):
    "Add a Page Template with optional file content."

    
    id = str(id)
    if REQUEST is None:
        self._setObject(id, OSAS_thumbRuler(id, text))
        ob = getattr(self, id)
       
        if title:
            ob.pt_setTitle(title)
        return ob
    else:
        file = REQUEST.form.get('file')
        headers = getattr(file, 'headers', None)
        if headers is None or not file.filename:
            zpt = OSAS_thumbRuler(id)
        else:
            zpt = OSAS_thumbRuler(id, file, headers.get('content_type'))

        self._setObject(id, zpt)
        ob = getattr(self, id)


        try:
            u = self.DestinationURL()
        except AttributeError:
            u = REQUEST['URL1']

        if submit == " Add and Edit ":
            u = "%s/%s" % (u, quote(id))
        REQUEST.RESPONSE.redirect(u+'/manage_main')
    return ''

class OSAS_digiLibTemplate(ZopePageTemplate):
    """pageTemplate Objekt"""
    meta_type="OSAS_digiLibTemplate"

    _default_content_fn = os.path.join(package_home(globals()),
                                       'zpt/OSAS_digiLibTemplateDefault.zpt')

    manage_options=ZopePageTemplate.manage_options+(
        {'label':'Copy to Filesystem','action':'copyContent'},
       )

    def getPath(self):
        """get path"""
        return getPath(self,'digilibtemplate.templ')
    
    ## def changeECHO_pageTemplateWeightForm(self):
##         """change"""
##         pt=PageTemplateFile('Products/ECHO_content/zpt/ChangeECHO_pageTemplateWeight.zpt').__of__(self)
##         return pt()

##     def changeECHO_pageTemplateWeight(self,weight,content_type,RESPONSE=None):
##         """change"""
##         self.weight=weight
##         self.content_type=content_type

##         if RESPONSE is not None:
##             RESPONSE.redirect('manage_main')
        

    def copyContent(self):
        """copycontent to path"""

        return "copied to:"+copyContent(self,'digilibtemplate.templ')
        
    
def manage_addOSAS_digiLibTemplateForm(self):
    """Form for adding"""
    pt=PageTemplateFile('Products/ECHO_content/zpt/AddOSAS_digiLibTemplate.zpt').__of__(self)
    return pt()




def manage_addOSAS_digiLibTemplate(self, id,title=None, text=None,
                           REQUEST=None, submit=None):
    "Add a Page Template with optional file content."

    
    id = str(id)
    if REQUEST is None:
        self._setObject(id, OSAS_digiLibTemplate(id, text))
        ob = getattr(self, id)
       
        if title:
            ob.pt_setTitle(title)
        return ob
    else:
        file = REQUEST.form.get('file')
        headers = getattr(file, 'headers', None)
        if headers is None or not file.filename:
            zpt = OSAS_digiLibTemplate(id)
        else:
            zpt = OSAS_digiLibTemplate(id, file, headers.get('content_type'))

        self._setObject(id, zpt)
        ob = getattr(self, id)


        try:
            u = self.DestinationURL()
        except AttributeError:
            u = REQUEST['URL1']

        if submit == " Add and Edit ":
            u = "%s/%s" % (u, quote(id))
        REQUEST.RESPONSE.redirect(u+'/manage_main')
    return ''

class OSAS_topTemplate(ZopePageTemplate):
    """pageTemplate Objekt"""
    meta_type="OSAS_topTemplate"

    _default_content_fn = os.path.join(package_home(globals()),
                                       'zpt/OSAS_topTemplateDefault.zpt')
    manage_options=ZopePageTemplate.manage_options+(
        {'label':'Copy to Filesystem','action':'copyContent'},
       )

    def getPath(self):
        """get path"""
        return getPath(self,'toptemplate.templ')

    ## def changeECHO_pageTemplateWeightForm(self):
##         """change"""
##         pt=PageTemplateFile('Products/ECHO_content/zpt/ChangeECHO_pageTemplateWeight.zpt').__of__(self)
##         return pt()

##     def changeECHO_pageTemplateWeight(self,weight,content_type,RESPONSE=None):
##         """change"""
##         self.weight=weight
##         self.content_type=content_type

##         if RESPONSE is not None:
##             RESPONSE.redirect('manage_main')

    def copyContent(self):
        """copycontent to path"""
        return "copied to:"+copyContent(self,'toptemplate.templ')
        

def manage_addOSAS_topTemplateForm(self):
    """Form for adding"""
    pt=PageTemplateFile('Products/ECHO_content/zpt/AddOSAS_topTemplate.zpt').__of__(self)
    return pt()




def manage_addOSAS_topTemplate(self, id,title=None, text=None,
                           REQUEST=None, submit=None):
    "Add a Page Template with optional file content."

    
    id = str(id)
    if REQUEST is None:
        self._setObject(id, OSAS_topTemplate(id, text))
        ob = getattr(self, id)
       
        if title:
            ob.pt_setTitle(title)
        return ob
    else:
        file = REQUEST.form.get('file')
        headers = getattr(file, 'headers', None)
        if headers is None or not file.filename:
            zpt = OSAS_topTemplate(id)
        else:
            zpt = OSAS_topTemplate(id, file, headers.get('content_type'))

        self._setObject(id, zpt)
        ob = getattr(self, id)


        try:
            u = self.DestinationURL()
        except AttributeError:
            u = REQUEST['URL1']

        if submit == " Add and Edit ":
            u = "%s/%s" % (u, quote(id))
        REQUEST.RESPONSE.redirect(u+'/manage_main')
    return ''


class OSAS_viewerTemplateSet(Folder):
    """Main viewerTemplateSet"""

    meta_type="OSAS_viewerTemplateSet"
    
    def __init__(self,id, title, startpage, xsl, templatePath):
        """init"""
        self.id=id
        self.title=title
        self.startpage=startpage
        self.xsl=xsl
        self.templatePath=templatePath
	self._setObject('digiLibTemplate',OSAS_digiLibTemplate(id='digiLibTemplate'))
        self._setObject('thumbTemplate',OSAS_thumbTemplate(id='thumbTemplate'))
        self._setObject('topTemplate',OSAS_topTemplate(id='topTemplate'))
	self._setObject('thumbruler',OSAS_thumbRuler(id='topRuler'))


    manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'configOSAS_viewerTemplateSetForm'},
        {'label':'Update Files at the Server','action':'copyAllFiles'},
        )

    def copyAllFiles(self):
	    """copy all"""
	    fis=self.ZopeFind(self)
	    ret=""
	    for fi in fis:
		    if hasattr(fi[1],'copyContent'):
			    ret=ret+fi[1].copyContent()+"</br>"
	    return ret

    def getField(self,name):
	    """get Field"""
	    try:
		    return(getattr(self,name))
	    except:
		    return ""
    def configOSAS_viewerTemplateSetForm(self):
        """config"""
        pt=PageTemplateFile('Products/OSA_system/zpt/changeOSAS_viewerTemplateSet.zpt').__of__(self)
        return pt()

    def configOSAS_viewerTemplateSet(self,title, startpage, xsl, templatePath,RESPONSE=None):
        """config"""
        self.title=title
        self.startpage=startpage
        self.xsl=xsl
        self.templatePath=templatePath

	
	
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
        
    


    
def manage_addOSAS_viewerTemplateSetForm(self):
    """interface for adding the OSAS_root"""
    pt=PageTemplateFile('Products/OSA_system/zpt/addOSAS_viewerTemplateSet.zpt').__of__(self)
    return pt()

def manage_addOSAS_viewerTemplateSet(self,id,title, startpage, xsl,templatePath,RESPONSE=None):
    """add the OSAS_root"""
    newObj=OSAS_viewerTemplateSet(id,title, startpage, xsl,templatePath)
    self._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')


    

