import OSAS_Root
import OSAS_show
import OSAS_metadata
import OSAS_addfiles
import OSAS_project

try:
	import OSAS_search
except:
   	print "OSAS_search not imported"
try:
    import OSAS_ids
except:
    print "OSAS_ids: not imported"

try:
    import OSAS_archiver
except:
    print "OSAS_archiver: not imported"
    
def initialize(context):
    """initialize OSAS"""

    context.registerClass(
        OSAS_project.OSAS_viewerTemplateSet,
        constructors = (
          OSAS_project.manage_addOSAS_viewerTemplateSetForm,
          OSAS_project.manage_addOSAS_viewerTemplateSet
          )
        )

    try:
        context.registerClass(
            OSAS_archiver.OSAS_archiveInbox,
            constructors = (
              OSAS_archiver.manage_AddOSAS_archiveInboxForm,
              OSAS_archiver.manage_AddOSAS_archiveInbox
              )
            )


        context.registerClass(
            OSAS_archiver.OSAS_metadataOrganizer,
            constructors = (
              OSAS_archiver.manage_AddOSAS_metadataOrganizerForm,
              OSAS_archiver.manage_AddOSAS_metadataOrganizer
              )
            )


        context.registerClass(
            OSAS_archiver.OSAS_processViewer,
            constructors = (
              OSAS_archiver.manage_AddOSAS_processViewerForm,
              OSAS_archiver.manage_AddOSAS_processViewer
              )
            )

        context.registerClass(
            OSAS_archiver.OSAS_producer,
            constructors = (
              OSAS_archiver.manage_AddOSAS_producerForm,
              OSAS_archiver.manage_AddOSAS_producer
              )
            )

        context.registerClass(
            OSAS_archiver.OSAS_archiver,
            constructors = (
              OSAS_archiver.manage_AddOSAS_archiverForm,
              OSAS_archiver.manage_AddOSAS_archiver
              )
            )
    except:
        print "OSAS_archiver.* not implemented!"
        

    try:
        context.registerClass(
            OSAS_ids.OSAS_idGenerator,
            constructors = (
            OSAS_ids.manage_AddOSAS_idGeneratorForm,
            OSAS_ids.manage_AddOSAS_idGenerator
            )
            )
    except:
        print "OSAS_idGenerator not implementet"
    
    context.registerClass(
        OSAS_Root.OSAS_Root,
        constructors = (
          OSAS_Root.manage_AddOSAS_RootForm,
          OSAS_Root.manage_AddOSAS_Root
          )
        )

    context.registerClass(
        OSAS_Root.OSAS_ViewerObject,
        constructors = (
          OSAS_Root.manage_AddOSAS_ViewerObjectForm,
          OSAS_Root.manage_AddOSAS_ViewerObject
          )
        )

    context.registerClass(
        OSAS_show.OSAS_ShowOnline,
        constructors = (
          OSAS_show.manage_AddOSAS_ShowOnlineForm,
          OSAS_show.manage_AddOSAS_ShowOnline
          )
        )
    
    context.registerClass(
        OSAS_show.OSAS_StoreOnline,
        constructors = (
          OSAS_show.manage_AddOSAS_StoreOnlineForm,
          OSAS_show.manage_AddOSAS_StoreOnline
          )
        )
    
    context.registerClass(
        OSAS_metadata.OSAS_MetadataMapping,
        constructors = (
          OSAS_metadata.manage_addOSAS_MetadataMappingForm,
          OSAS_metadata.manage_addOSAS_MetadataMapping
          )
        )


    context.registerClass(
        OSAS_metadata.OSAS_add_Metadata,
        constructors = (
          OSAS_metadata.manage_AddOSAS_add_MetadataForm,
          OSAS_metadata.manage_AddOSAS_add_Metadata
          )
        )

    context.registerClass(
        OSAS_addfiles.OSAS_add_Document,
        constructors = (
          OSAS_addfiles.manage_AddOSAS_add_DocumentForm,
          OSAS_addfiles.manage_AddOSAS_add_Document
          )
        )

    context.registerClass(
        OSAS_addfiles.OSAS_add_Text,
        constructors = (
          OSAS_addfiles.manage_AddOSAS_add_TextForm,
          OSAS_addfiles.manage_AddOSAS_add_Text
          )
        )

    context.registerClass(
        OSAS_addfiles.OSAS_add_Presentation,
        constructors = (
          OSAS_addfiles.manage_AddOSAS_add_PresentationForm,
          OSAS_addfiles.manage_AddOSAS_add_Presentation
          )
        )

    context.registerClass(
        OSAS_addfiles.OSAS_combineTextImage,
        constructors = (
          OSAS_addfiles.manage_AddOSAS_combineTextImageForm,
          OSAS_addfiles.manage_AddOSAS_combineTextImage
          )
        )
    context.registerClass(
        OSAS_addfiles.OSAS_add_contextData,
        constructors = (
          OSAS_addfiles.manage_AddOSAS_add_contextDataForm,
          OSAS_addfiles.manage_AddOSAS_add_contextData
          )
        )
    try:
    	context.registerClass(
        	OSAS_search.OSAS_search,
        	constructors = (
          		OSAS_search.manage_AddOSAS_searchForm,
          		OSAS_search.manage_AddOSAS_search
          		)
        	)
    except:
	"""no"""
