#Neue Version Begin 5.4.2004


"""Methoden zum hinzufuegen von Dokumenten ins Archiv"""
from OSAS_helpers import readArchimedesXML 
try:
        import archive
except:
        print "archive not imported"
        
import os
import os.path
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
import string
import urllib
import logging

#ersetzt logging
def logger(txt,method,txt2):
    """logging"""
    logging.info(txt+ txt2)


import xml.dom.minidom
from time import localtime,strftime
from Globals import package_home
from types import *

import re
def showHelp(helptext):
        """show helptext"""
        return """<html>
        <body>
        %
        </body>
        </html>"""%helptext
def add(self, no_upload=0):
        """ Add metadata or metadata and documents to the repository
        no_upload=0 kein upload sonst upload von documententen"""

        #self.referencetypes=self.ZopeFind(self,obj_metatypes=['OSAS_MetadataMapping'])
        self.referencetypes=self.ZopeFind(self)
        
        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','OSAS_add_new')).__of__(self)
        self.REQUEST.SESSION['path']=self.REQUEST['path']
        if no_upload==0:
                self.REQUEST.SESSION['no_upload']='yes'
        else:
                if self.REQUEST.SESSION.has_key('no_upload'):
                        del self.REQUEST.SESSION['no_upload']
                        
        return newtemplate()



def getISO():
        """ISO"""
        try:
                f=file(os.path.join(package_home(globals()),'iso639-1.inc'),'r').readlines()

                ret={}
                for lineraw in f:
                        line=lineraw.encode('ascii','replace').strip()
                        value=string.split(line,'\t')[0].encode('ascii','replace')
                        key=string.split(line,'\t')[1].encode('ascii','replace')
                        ret[key]=value
        except:
                ret={}
        return ret


def add2(self):
        self.reftype=self.REQUEST['Reference Type']
        self.REQUEST.SESSION['reftype']=self.reftype
        self.bibdata={}
        for referenceType in self.referencetypes:
                #print referenceType
                if referenceType[1].title == self.reftype: 
                        self.bibdata[referenceType[1].title]=referenceType[1].fields
                        self.bibdata['data']=referenceType[1]
                        self.fields=self.bibdata[self.reftype]
        
        self.isolist=getISO()
        tmp=getISO().keys()
        tmp.sort()
        self.isokeys=tmp
        #listed=[ x for x in self.isolist.keys()]
        #print listed
        #sorted=listed.sort()
        #print sorted
        
        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','OSAS_add_bibdata')).__of__(self)
        return newtemplate()
        #return self.fields


def parse_query_string(str):
        queries={}
        key=""
        value=""
        tmp=""
        toggle="key"
        str=urllib.unquote(str)
        for i in str:
                if i=="=":
                        key=tmp
                        toggle="value"                          
                        tmp=""
                elif i=="&":
                        queries[key]=tmp
                        tmp=""
                        toggle="key"
                else:
                        if toggle=="key":
                                if i=="+" : i="-"
                        else:
                                if i=="+" : i=" "
                        tmp=tmp+i
        queries[key]=tmp
        return queries
        
def add3(self):
        """Foldername"""
        metadata=parse_query_string(self.REQUEST['QUERY_STRING'])
        self.REQUEST.SESSION['metadata']=metadata
        vorschlag=[]

        if metadata.has_key('author'):
                vorschlag.append(metadata['author'][:5])
        if metadata.has_key('title'):
                vorschlag.append(metadata['title'][:5])
        if metadata.has_key('year'):
                vorschlag.append(metadata['year'])
        
                
        vorschlag_naming=string.join(vorschlag,"_")
        
                
        self.vorschlag_naming=unicode(vorschlag_naming,'ascii','ignore')
        if self.REQUEST.SESSION.has_key('no_upload'):
                self.REQUEST.SESSION['folder_name']=self.REQUEST.SESSION['path']
                return add5(self)
        else:
                newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','OSAS_add_naming')).__of__(self)
                return newtemplate()
        

def add4(self):

        self.path=re.search(r"/mpiwg(.*)",self.REQUEST.SESSION['path']).group(1)
        
        self.folder_name=self.REQUEST['folder_name']
        # next has to be changed -> error if back button is used!!
        self.REQUEST.SESSION['folder_name']=self.folder_name
        #return self.REQUEST['submit']

        try:    
                #os.popen('mkdir '+self.REQUEST.SESSION['path'])
                os.mkdir(os.path.join(self.REQUEST.SESSION['path'],self.REQUEST['folder_name']))
                os.chmod(os.path.join(self.REQUEST.SESSION['path'],self.REQUEST['folder_name']),0775)

        except:

                """nothing"""

        if self.REQUEST['submit']=="upload images":
                self.REQUEST.SESSION['path']=os.path.join(self.REQUEST.SESSION['path'],self.REQUEST['folder_name'])
                

                self.image_folder_name="pageimg"
                newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','OSAS_upload')).__of__(self)
                
                return newtemplate()
        
        elif self.REQUEST['submit']=="upload pdf":
                os.mkdir(os.path.join(self.REQUEST.SESSION['path'],self.REQUEST['folder_name']))
                return addPdf(self,os.path.join(self.REQUEST.SESSION['path'],self.REQUEST['folder_name']))
        else:
                os.mkdir(os.path.join(self.REQUEST.SESSION['path'],self.REQUEST['folder_name']))
                return addText(self,os.path.join(self.REQUEST.SESSION['path'],self.REQUEST['folder_name']))
        
        
def add5(self):
        """ADD INDEX.META"""
        try:
                os.chmod(self.REQUEST.SESSION['path'],0775)
        except:
                pass

        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','OSAS_add_metadata')).__of__(self)
        return newtemplate()

def add6(self):
        metadata=parse_query_string(self.REQUEST['QUERY_STRING'])
        metadata['archive-path']=os.path.split(self.REQUEST.SESSION['path'])[0]
        #metadata['folder_name']=self.REQUEST.SESSION['folder_name']
        metadata['folder_name']=os.path.split(self.REQUEST.SESSION['path'])[1]
        metadata['content-type']="scanned document"
        self.reftype=self.REQUEST.SESSION['reftype']
        self.REQUEST.SESSION['add_metadata']=metadata   
        self.add_metadata=metadata
        self.metadata=self.REQUEST.SESSION['metadata']
        self.metadataprint=""
        for tag in self.metadata.keys():
                if tag!="":
                        self.metadataprint=self.metadataprint+"<"+tag+">"+self.metadata[tag]+"</"+tag+">\n"

        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','index_meta')).__of__(self)
        newtemplate.content_type="text/plain"
        renderxml = newtemplate(encode='utf-8')
        
                
        if self.REQUEST.SESSION.has_key('no_upload'):
                metapath=self.REQUEST.SESSION['path']+"/index.meta"
        else:
                metapath=self.add_metadata['archive-path']+"/"+self.add_metadata['folder_name']+"/index.meta"
        
        f=open(metapath,'w')
        try:
                f.write(renderxml.encode('utf-8'))
        except:
                f.write(unicode(renderxml,'latin-1').encode('utf-8'))
                #f.write(renderxml)

        f.close()
        os.chmod(metapath,0664)
        os.popen('chmod -R 0775 %s'%self.add_metadata['archive-path']+"/"+self.add_metadata['folder_name']) 
        if self.REQUEST.SESSION.has_key('no_upload'):
                
                #newtemplate2=PageTemplateFile('/usr/local/mpiwg/Zope/Extensions/done',"text/html").__of__(self)
                return self.REQUEST.response.redirect(self.REQUEST['URL2']+"?path="+self.REQUEST.SESSION['path'])
        else:
                #print self.add_metadata['archive-path']
                self.viewpath=re.search(r"/mpiwg/online/(.*)",self.add_metadata['archive-path']).group(1)
        
        if (self.REQUEST.SESSION.has_key('no_upload')) and (self.REQUEST.SESSION['no_upload']=="text"):
                        """text upload"""
                        return 1
        else:   
                        newtemplate2=PageTemplateFile(os.path.join(package_home(globals()),'zpt','OSAS_saved')).__of__(self)
                        newtemplate2.content_type="text/html"
                        self.REQUEST.response.setHeader('Content-Type','text/html')
                        return newtemplate2()
                


def date(self):
        return strftime("%d.%m.%Y",localtime()) 


def addPresentation(self,path):
        """add presentation to the path"""
        
        dom=xml.dom.minidom.parse(path+"/index.meta")
        
        
        try:
                author=archive.getText(dom.getElementsByTagName('author')[0].childNodes)
        except:
                try:
                        author=archive.getText(dom.getElementsByTagName('Author')[0].childNodes)
                except:
                        try:
                                author=archive.getText(dom.getElementsByTagName('Editor')[0].childNodes)
                        except:
                                author=""
        try:
                title=archive.getText(dom.getElementsByTagName('title')[0].childNodes)
        except:
                title=""
                
        try:
                date=archive.getText(dom.getElementsByTagName('year')[0].childNodes)
        except:
                try:
                        date=archive.getText(dom.getElementsByTagName('Year')[0].childNodes)
                except:
                        try:
                                date=archive.getText(dom.getElementsByTagName('date')[0].childNodes)
                        except:
                                date=""
        i=1
        while os.path.exists(path+"/%02d-presentation"%i):
                i+=1
        self.REQUEST.SESSION['presentationname']="%02d-presentation"%i
        self.REQUEST.SESSION['path']=path

        tmpTxt=u"""<?xml version="1.0" encoding="UTF-8"?>
        <info>
        <author>%s</author>
        <title>%s</title>
        <date>%s</date>
        <display>yes</display>
</info>"""%(author,title,date)
        
        self.REQUEST.SESSION['xmlvorschlag']=tmpTxt.encode('utf-8')
        
        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addPresentation')).__of__(self)
        return newtemplate()

def addPresentation2(self):
        """add presentation """
        folder_name=self.REQUEST['folder_name']
        #print self.REQUEST['folder_name']
        content_description=self.REQUEST['content_description']

        path=self.REQUEST.SESSION['path']

        if not self.REQUEST.has_key('fileupload'):
                xmlinfo=self.REQUEST['xmltext']
                file_name="info.xml"

        else:
                file_name=self.REQUEST['fileupload'].filename
                xmlinfo=self.REQUEST.form['fileupload'].read()
                # hack Multipart auswertung funktioniert nicht ausser bei mozilla
                file_name="info.xml"
                xmlinfo=self.REQUEST['xmltext']
        try:
                os.mkdir(path+"/"+folder_name)
        except:
                """nothing"""
        #print "NAME:",file_name
        f=open(path+"/"+folder_name+"/"+file_name,"w")
        f.write(xmlinfo.encode('utf-8'))
        f.close()
        try:
                os.chmod(path+"/"+folder_name,0775)
        except:
                """NO"""

        os.chmod(path+"/"+folder_name+"/"+file_name,0664)
        addDirsToIndexMeta(path,folder_name,content_description,'presentation')
        
        return self.REQUEST.RESPONSE.redirect(self.REQUEST['URL2']+'?path='+path)


def addPdf(self,path,folder=None):
        """add fulltext to the path"""
        self.REQUEST.SESSION['existing_names']=['pageimg'] # to be done generate list of existing text files
        self.REQUEST.SESSION['pathnew']=path
        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addPdf')).__of__(self)
        return newtemplate()

def addPdf2(self):
        """addtext"""
        folder_name="pdf" # foldername fixed
        
        if self.REQUEST['file_name']=="":
                file_name=self.REQUEST['fileupload'].filename
        else:
                file_name=self.REQUEST['file_name']
                
        #print self.REQUEST['folder_name']
        content_description=self.REQUEST['content_description']
        path=self.REQUEST.SESSION['pathnew']

        filedata=self.REQUEST.form['fileupload'].read()
        try:
                os.mkdir(path+"/"+folder_name)
        except:
                """nothing"""
        f=open(path+"/"+folder_name+"/"+file_name,"w")
        f.write(filedata)
        f.close()
        os.chmod(path+"/"+folder_name,0755)
        os.chmod(path+"/"+folder_name+"/"+file_name,0644)
        addDirsToIndexMeta(path,folder_name,content_description,'pdf')

        return self.REQUEST.RESPONSE.redirect(self.REQUEST['URL2']+'?path='+path)

def addText(self,path,folder=None):
        """add fulltext to the path"""
        self.REQUEST.SESSION['existing_names']=['pageimg'] # to be done generate list of existing text files
        self.REQUEST.SESSION['pathnew']=path
        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addText')).__of__(self)
        return newtemplate()

def addText2(self):
        """addtext"""
        folder_name=self.REQUEST['folder_name']
        #print self.REQUEST['folder_name']
        content_description=self.REQUEST['content_description']
        path=self.REQUEST.SESSION['pathnew']
        file_name=self.REQUEST['fileupload'].filename
        filedata=self.REQUEST.form['fileupload'].read()
        os.mkdir(path+"/"+folder_name)
        f=open(path+"/"+folder_name+"/"+file_name,"w")
        f.write(filedata)
        f.close()
        os.chmod(path+"/"+folder_name,0755)
        os.chmod(path+"/"+folder_name+"/"+file_name,0644)
        addDirsToIndexMeta(path,folder_name,content_description,'fulltext')

        return self.REQUEST.RESPONSE.redirect(self.REQUEST['URL2']+'?path='+path)

def addTextExternal(self,path,texturl,version):
        """hinzufuegen eines externen textes"""
        try: #neue text version einlesen
                texttemp=urllib.urlopen(texturl).readlines()
                text=""
                for line in texttemp:
                        text=text+line
        except: #fehler beim lesen des textes
                return "ERROR: cannot read: %s"%texturl
        if TextExternalError(text): #kein xml header
                return "ERROR: cannot read: %s"%texturl, "received:",text 
        textpath=getNewTextPath(path) #erzeuge neuen Ornder fuer den Text
        splitted=string.split(texturl,"/")
        name=splitted[len(splitted)-1] #Name des XML-files
        try:
                writefile=file(path+"/"+textpath+"/"+name,"w")
        except:
                return"ERROR: cannot write: %s"%path+"/"+textpath+"/"+name
        writefile.write(text)
        writefile.close()
        os.chmod(path+"/"+textpath+"/"+name,0644)

        #add new file to XML
        dom=xml.dom.minidom.parse(path+"/index.meta")
        node=dom.getElementsByTagName('resource')[0] #getNode

        subnode=dom.createElement('dir')
        
        namenode=dom.createElement('name')
        namenodetext=dom.createTextNode(textpath)
        namenode.appendChild(namenodetext)
        subnode.appendChild(namenode)
            
        descriptionnode=dom.createElement('description')
        descriptionnodetext=dom.createTextNode('archimedes text:'+version)
        descriptionnode.appendChild(descriptionnodetext)
        subnode.appendChild(descriptionnode)

        contentnode=dom.createElement('content-type')
        contentnodetext=dom.createTextNode('fulltext')
        contentnode.appendChild(contentnodetext)
        subnode.appendChild(contentnode)
        
        node.appendChild(subnode)

        writefile=file(path+"/index.meta","w")
        writefile.write(dom.toxml(encoding="UTF-8"))
        writefile.close()

        #change texttool tag
        dom=xml.dom.minidom.parse(path+"/index.meta")
        node=dom.getElementsByTagName('meta')[0] #getNode

        try: #texttool existiert schon
                subnode=node.getElementsByTagName('texttool')[0]
        except: #wenn nicht Fehler ausgeben
                return "ERROR:no presentation configured yet, user Web Front End to do so!"
        

        try:
                texttoolnodelist=subnode.getElementsByTagName('text')
        
                if not len(texttoolnodelist)==0: #texttool tag existiert schon, dann loeschen
                        subsubnode=subnode.removeChild(texttoolnodelist[0])
                        subsubnode.unlink()
        except:
                """nothing"""
        # text neu anlegen
        textfoldernode=dom.createElement('text')
        textfoldernodetext=dom.createTextNode(textpath+"/"+name)
        textfoldernode.appendChild(textfoldernodetext)
        subnode.appendChild(textfoldernode)

        #index.meta ausgeben
        writefile=file(path+"/index.meta","w")
        writefile.write(dom.toxml(encoding="UTF-8"))
        writefile.close()
        
        #registrieren
        return urllib.urlopen("http://nausikaa2.rz-berlin.mpg.de:86/cgi-bin/toc/admin/reg.cgi?path=%s"%path).readlines()
        
        

def TextExternalError(text):
        firsts=text[0:10]
        #print firsts
        try:
                match=re.search(r".*<?xml.*",firsts)
        except:
                return 1
        return 0

def getNewTextPath(path):
        i=1
        while os.path.exists(path+"/fulltext%i"%i):
                i+=1
        os.mkdir(path+"/fulltext%i"%i)
        os.chmod(path+"/fulltext%i"%i,0755)
        return "fulltext%i"%i

def addImages(self,path):
        """Imagesfolder to the path"""
        self.REQUEST.SESSION['existing_names']=['pageimg'] # to be done generate list of existing pageimages files
        self.REQUEST.SESSION['path']=path
        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','OSAS_addImages')).__of__(self)
        return newtemplate()

def addImages2(self):
        
        self.image_folder_name=self.REQUEST['folder_name']
        #print self.REQUEST['folder_name']
        self.content_description=self.REQUEST['content_description']
        #self.path=self.REQUEST.SESSION['path']
        
        
        self.content_type='images'
        addDirsToIndexMeta(self.REQUEST.SESSION['path'],self.image_folder_name,self.content_description,self.content_type)
        self.REQUEST.SESSION['path']=re.search(r"/mpiwg(.*)",self.REQUEST.SESSION['path']).group(1)
        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','OSAS_upload2')).__of__(self)
        return newtemplate()
        


def addDirsToIndexMeta(path,folder_name,content_description,content_type):
        #f=file(path+"/index.meta",r)
        dom=xml.dom.minidom.parse(path+"/index.meta")
        node=dom.getElementsByTagName('resource')[0] #getNode

        subnode=dom.createElement('dir')
        
        namenode=dom.createElement('name')
        namenodetext=dom.createTextNode(folder_name)
        namenode.appendChild(namenodetext)
        subnode.appendChild(namenode)
            
        descriptionnode=dom.createElement('description')
        descriptionnodetext=dom.createTextNode(content_description)
        descriptionnode.appendChild(descriptionnodetext)
        subnode.appendChild(descriptionnode)

        contentnode=dom.createElement('content-type')
        contentnodetext=dom.createTextNode(content_type)
        contentnode.appendChild(contentnodetext)
        subnode.appendChild(contentnode)
        
        node.appendChild(subnode)

        writefile=file(path+"/index.meta","w")
        writefile.write(dom.toxml(encoding='UTF-8'))
        writefile.close()

def readArchimedesXML(folder):
        """gib URL aus """
        XML=urllib.urlopen("http://archimedes.mpiwg-berlin.mpg.de/cgi-bin/toc/toc.cgi?step=xmlcorpusmanifest").read()
        #print XML
        dom=xml.dom.minidom.parseString(XML)
        items=dom.getElementsByTagName('item')
        dict={}
        
        for item in items:
                #print item.attributes['dir'].value
                try:            
                        dict[item.attributes['dir'].value]=item.attributes['xml'].value
                        #print item.attributes['dir'].value,item.attributes['text'].value
                except:
                        """nothing"""
                
        if dict.has_key(folder):
                return dict[folder]
        else:
                return ""
        

        

def combineTextImage2(self,path):
        """erstellt bzw. aendert texttool meta tag"""
        dom=xml.dom.minidom.parse(path+"/index.meta")
        node=dom.getElementsByTagName('meta')[0] #getNode


        subnodelist=node.getElementsByTagName('texttool')
        if not len(subnodelist)==0: #texttool tag existiert schon, dann loeschen
                subnode=node.removeChild(subnodelist[0])
                subnode.unlink()

        subnode=dom.createElement('texttool') #neu erzeugen

        
        presentfiles=os.listdir(path+"/"+self.REQUEST['presentation'])
        for presentfileTmp in presentfiles:
        	if (presentfileTmp[0]!="."): #schliesse unsichbare DAteien aus.
			    presentfile=presentfileTmp
        

        displaynode=dom.createElement('display')
        displaynodetext=dom.createTextNode('yes')
        displaynode.appendChild(displaynodetext)
        subnode.appendChild(displaynode)
        
        if self.REQUEST.has_key('image'):       
                namenode=dom.createElement('image')
                namenodetext=dom.createTextNode(self.REQUEST['image'])
                namenode.appendChild(namenodetext)
                subnode.appendChild(namenode)
                
        if self.REQUEST.has_key('text'):                    
                textfile=os.listdir(path+"/"+self.REQUEST['text'])[0]
                textfoldernode=dom.createElement('text')
                textfoldernodetext=dom.createTextNode(path+"/"+self.REQUEST['text']+"/"+textfile)
                textfoldernode.appendChild(textfoldernodetext)
                subnode.appendChild(textfoldernode)

        if self.REQUEST.has_key('external'):#USE CVS instead of local text
                textfoldernode=dom.createElement('text')
                textfoldernodetext=dom.createTextNode(self.REQUEST.SESSION['externxml'])
                textfoldernode.appendChild(textfoldernodetext)
                subnode.appendChild(textfoldernode)
                
        if self.REQUEST.has_key('pagebreak'):   
                pagebreaknode=dom.createElement('pagebreak')
                pagebreaknodetext=dom.createTextNode(self.REQUEST['pagebreak'])
                pagebreaknode.appendChild(pagebreaknodetext)
                subnode.appendChild(pagebreaknode)

        if self.REQUEST.has_key('presentation'):        
                presentationnode=dom.createElement('presentation')
                presentationnodetext=dom.createTextNode(self.REQUEST['presentation']+"/"+presentfile)
                presentationnode.appendChild(presentationnodetext)
                subnode.appendChild(presentationnode)
        

        if self.REQUEST.has_key('xslt'):
                if not self.REQUEST['xslt']=="":
                        xsltnode=dom.createElement('xslt')
                        xsltnodetext=dom.createTextNode(self.REQUEST['xslt'])
                        xsltnode.appendChild(xsltnodetext)
                        subnode.appendChild(xsltnode)

        
        if self.REQUEST.has_key('thumbtemplate'):
                if not self.REQUEST['thumbtemplate']=="":
                        xsltnode=dom.createElement('thumbtemplate')
                        xsltnodetext=dom.createTextNode(self.REQUEST['thumbtemplate'])
                        xsltnode.appendChild(xsltnodetext)
                        subnode.appendChild(xsltnode)

        if self.REQUEST.has_key('topbar'):
                if not self.REQUEST['topbar']=="":
                        xsltnode=dom.createElement('toptemplate')
                        xsltnodetext=dom.createTextNode(self.REQUEST['topbar'])
                        xsltnode.appendChild(xsltnodetext)
                        subnode.appendChild(xsltnode)

        if self.REQUEST.has_key('startpage'):
                if not self.REQUEST['startpage']=="":
                        xsltnode=dom.createElement('startpage')
                        xsltnodetext=dom.createTextNode(self.REQUEST['startpage'])
                        xsltnode.appendChild(xsltnodetext)
                        subnode.appendChild(xsltnode)

        if self.REQUEST.has_key('project'):
                if not self.REQUEST['project']=="":
                        xsltnode=dom.createElement('project')
                        xsltnodetext=dom.createTextNode(self.REQUEST['project'])
                        xsltnode.appendChild(xsltnodetext)
                        subnode.appendChild(xsltnode)

        if self.REQUEST.has_key('digiliburlprefix'):
                if not self.REQUEST['digiliburlprefix']=="":
                        xsltnode=dom.createElement('digiliburlprefix')
                        xsltnodetext=dom.createTextNode(self.REQUEST['digiliburlprefix'])
                        xsltnode.appendChild(xsltnodetext)
                        subnode.appendChild(xsltnode)
                        
        node.appendChild(subnode)
        
        try:
                node2=node.getElementsByTagName('bib')[0]
                subs=node2.getElementsByTagName('lang')
                for sub in subs:
                        node2.removeChild(sub)
        except:
                """nothing"""
        try:
                main=dom.getElementsByTagName('bib')[0]
                node=dom.createElement('lang')
                textnode=dom.createTextNode(self.REQUEST['lang'])
                node.appendChild(textnode)
                main.appendChild(node)
        except:
                try:
                        subs=dom.getElementsByTagName('lang')
                        main=dom.getElementsByTagName('resource')[0]
                        for sub in subs:
                                main.removeChild(sub)
                except:
                        """nothing"""
                
                try:
                        main=dom.getElementsByTagName('resource')[0]
                        node=dom.createElement('lang')
                        textnode=dom.createTextNode(self.REQUEST['lang'])
                        #print "LANG:",self.REQUEST['lang']
                        node.appendChild(textnode)
                        main.appendChild(node)
                except:
                        """nothing"""
                        
        writefile=file(path+"/index.meta","w")
        writefile.write(dom.toxml(encoding="UTF-8"))
        writefile.close()
        
        

#        urllib.urlopen("http://nausikaa2.rz-berlin.mpg.de:86/cgi-bin/toc/admin/reg.cgi?path=%s"%path).readlines()
#
#        if self.REQUEST.has_key('image'): # falls bilder
#                path=re.sub('//','/',self.REQUEST['path']) # falls '//' im Pfad
#                dlpath = re.sub('/mpiwg/online/','',path)+"/"+self.REQUEST['image']
#
#                logger('OSas',logging.INFO,"ssh archive@nausikaa2.rz-berlin.mpg.de /usr/local/mpiwg/scripts/scaleomat -src=/mpiwg/online -dest=/mpiwg/temp/online/scaled/thumb -dir=%s -scaleto=90 -sync >> /tmp/sc.out &"%dlpath )
#                ret=os.popen("ssh archive@nausikaa2.rz-berlin.mpg.de /usr/local/mpiwg/scripts/scaleomat -src=/mpiwg/online -dest=/mpiwg/temp/online/scaled/thumb -dir=%s -scaleto=90 -sync >> /tmp/sc.out &"%dlpath ).read()
#                logger('OSAS (combine)',logging.INFO,ret)



        #else: # falls keine Bilder (bug in reg.cgi info file ersetzen)
        #        f=file("/tmp/tmp_info.xml","w")
        #        tmp=patchedInfoXML(self.REQUEST['path'])
        #        f.write(tmp.encode('utf-8'))
        #        f.close()
        #        splitted=path.split("/")
        #        fn=splitted[len(splitted)-1]
        #        remotePath="archive@nausikaa2.rz-berlin.mpg.de:/usr/local/share/archimedes/web/docs/proj/echo/1/docs/"+fn+"/info.xml"
        #        os.popen("scp /tmp/tmp_info.xml %s"%remotePath)

def patchedInfoXML(path):
        dom=xml.dom.minidom.parse(path+"/index.meta")
        
        ret="<info>\n"
        ret+="<remotetext>%s</remotetext>\n"%archive.getText(dom.getElementsByTagName('text')[0].childNodes)
        ret+="<pagebreak>%s</pagebreak>\n"%archive.getText(dom.getElementsByTagName('pagebreak')[0].childNodes)
        ret+="<display>%s</display>\n"%archive.getText(dom.getElementsByTagName('display')[0].childNodes)
        try:
                ret+="<toptemplate>%s</toptemplate>\n"%archive.getText(dom.getElementsByTagName('toptemplate')[0].childNodes)
        except:
                """not"""
        try:
                ret+="<thumbtemplate>%s</thumbtemplate>\n"%archive.getText(dom.getElementsByTagName('thumbtemplate')[0].childNodes)
        except:
                """not"""
        try:
                ret+="<startpage>%s</startpage>\n"%archive.getText(dom.getElementsByTagName('startpage')[0].childNodes)
        except:
                """not"""
                
        ret+="<lang>%s</lang>\n"%archive.getText(dom.getElementsByTagName('lang')[0].childNodes)
        try:
                ret+="<author>%s</author>\n"%archive.getText(dom.getElementsByTagName('author')[0].childNodes)
        except:
                """not"""
        try:
                ret+="<title>%s</title>\n"%archive.getText(dom.getElementsByTagName('title')[0].childNodes)
        except:
                """not"""
                
        ret+="</info>"

        return ret
