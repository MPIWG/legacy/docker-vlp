# Classen zur Produktion von Metataden

# Classes for displaying, browsing and organizing the archive

import addFolder
import OSAS_add # methods for adding metadata and/or files

from Globals import InitializeClass,Persistent,package_home

from OFS.Folder import Folder
from OFS.SimpleItem import SimpleItem
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
from OFS.Traversable import Traversable
from OFS.Cache import Cacheable
from OFS.History import Historical, html_diff
from Shared.DC.Scripts.Script import Script, BindingsUI
from OFS.PropertyManager import PropertyManager
from ExtensionClass import Base
from Acquisition import Implicit
import string
import os
from AccessControl import ClassSecurityInfo


class OSAS_MetadataMapping(SimpleItem):
    """Einfaches Mapping Object"""

    meta_type="OSAS_MetadataMapping"

    def readFieldsFromParent(self):
        """read all elements from root"""
        #print self.getId()
        return self.aq_parent.fieldList
    
    def __init__(self,id,title,arglist,fieldList):
        """init"""
        self.id=id
        self.title=title
        self.fields=[]
        
        for fieldName in fieldList:
            setattr(self,"md_"+fieldName,arglist[fieldName])
            self.fields.append(arglist[fieldName])
            
    manage_options = SimpleItem.manage_options+(
        {'label':'Main Config','action':'change_OSAS_MappingForm'},
        )


    def showSetXML(self,RESPONSE=None):
        """prints out the mapping as XML"""
        ret="""<set name="%s">"""%self.title
        for fieldName in self.readFieldsFromParent():
            entry=getattr(self,"md_"+fieldName)
            if entry[2]=="": # no explanation of this field
                ret+="""<entry genericName="%s" tag="%s" label="%s"/>"""%(fieldName,entry[0],entry[1])
            else:
                ret+="""<entry genericName="%s" tag="%s" label="%s">%s</entry>"""%(fieldName,entry[0],entry[1],entry[2])
        ret+="</set>"

        if not RESPONSE:
            return ret
        else:
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
            return ret

    def getValue(self,fieldName):
        """get md value"""

        try:
            return getattr(self,"md_"+fieldName)
        except:
            return (None,None,None)

    def isEmptyValue(self,fieldName):
        """empty"""
        try:
            field=getattr(self,"md_"+fieldName)
            if field[1]=='':
                return 0
            else:
                return 1
        except:
            return 0
        
    def generateMappingHash(self):
        """erzeugen des Hash"""
        hash={}
        for field in self.fieldList:
            hash[field]=getattr(self,"md_"+field,'')
        return hash

       
    
    def change_OSAS_MappingForm(self):
        """change"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeOSAS_MetadataMapping.zpt')).__of__(self)
        return pt()
    
    def change_OSAS_Mapping(self,titleOfObject,RESPONSE=None):
        """change"""

        self.title=titleOfObject
        arglist=self.REQUEST.form
        self.fields=[]
        
        
        for fieldName in self.readFieldsFromParent():

            setattr(self,"md_"+fieldName,(arglist[fieldName],arglist['label_'+fieldName],arglist['explanation_'+fieldName]))
            self.fields.append(arglist[fieldName])
            
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    

def manage_addOSAS_MetadataMappingForm(self):
    """interface for adding the OSAS_root"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_MetadataMapping.zpt')).__of__(self)
    return pt()

def manage_addOSAS_MetadataMapping(self,idOfObject,titleOfObject,RESPONSE=None):
    """add the OSAS_root"""
    
    argList={}
    for arg in self.fieldList:
        if not (arg in ['idOfObject','titleOfObject']):
            argList[arg]=(self.REQUEST.form[arg],self.REQUEST.form['label_'+arg],self.REQUEST.form['explanation_'+arg])
            
    newObj=OSAS_MetadataMapping(idOfObject,titleOfObject,argList,self.fieldList)
    self._setObject(idOfObject,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

   


class OSAS_add_Metadata(Folder):
    """Foldertype enthaelt methoden zur Halbautomatischen Erstellung von Metadatenfiles"""
    security=ClassSecurityInfo()
    
    def __init__(self,id,linklabel,description,fields):
        """initialize a new instance"""
        self.id = id
        self.linklabel =linklabel #label fuer link auf add page
        self.description=description #description of the method for link page
        self.fieldList=fields.split(",")[0:]
        
    meta_type='OSAS_add_Metadata'

    manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'add_Metadata_config'},
        )

    def showGenericXML(self,RESPONSE=None):
        """show generic fields as XML"""
        ret="""<set name="%s">"""%"generic"
        for field in self.fieldList:
            ret+="""<entry genericName="%s"/>"""%field

        ret+="</set>"
        
        if not RESPONSE:
            return ret
        else:
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
            return ret
    def showOverviewXML(self,RESPONSE):
        """gives an overview over the Metadata stored in this folder"""
        ret="""<metadata name="%s">"""%self.getId()
        ret+=self.showGenericXML()
        for entry in self.ZopeFind(self,obj_metatypes=['OSAS_MetadataMapping']):
            ret+=entry[1].showSetXML()

        ret+="</metadata>"

        if not RESPONSE:
            return ret
        else:
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
            return ret
        
    def generateMappingList(self):
        """Erzeuge Mapping"""
        mapping={}
        for dict in self.__dict__:
            #print dict
            obj=getattr(self,dict)
            if hasattr(obj,'meta_type'):
                if obj.meta_type=="OSAS_MetadataMapping":
                    mapping[obj.getId()]=obj.generateMappingHash()
        return mapping
    
    def generateMappingForType(self,type,clean="yes"):
        """erzeuge spezifisches Mapping"""
        hash=self.generateMappingList()
        if hash.has_key(type):
            if clean=="yes":
                temp={}
                for x in hash[type].keys():
                    if not hash[type][x]=="":
                        temp[x]=hash[type][x]
                return temp
            else:
                return hash[type]
        
        else:
            return {}

    def getFieldList(self):
        """erzeuge string aus fields"""
        try:
            return string.join(self.fieldList,",")
        except:
            return ""
        
    def getFields(self):
        """ausgabe der Felder"""
        return self.fieldList

    def getTypeTitle(self,id):
        """Title von ID"""
        try:
            obj=getattr(self,id)
            return obj.title
        except:
            return id
        
    def getStoredTypes(self):
        """Gebe gespeicherte typen zurueck"""
        
        types=[]
        for dict in self.__dict__:
            
            obj=getattr(self,dict)
            if hasattr(obj,'meta_type'):
                if obj.meta_type=="OSAS_MetadataMapping":
                    #print obj
                    if obj.title=="":
                        title=obj.id
                    else:
                        title=obj.title
                    types.append((obj.id,title))
        return types
    
    security.declarePublic('add_Metadata_config')
    def add_Metadata_config(self):
        """Main configuration"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeOSAS_addMetadata.zpt')).__of__(self)
        return pt()
    
    security.declarePublic('change_OSAS_addMetadata')
    def change_OSAS_addMetadata(self,linklabel,description,fields,RESPONSE=None):
        """Change Metadata"""
        self.linklabel=linklabel
        self.description=description
        self.fieldList=fields.split(",")[0:]
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    security.declarePublic('index_html')
    def index_html(self):
        """standard aufruf"""
        return OSAS_add.add(self,no_upload=0)


    security.declarePublic('add2')
    def add2(self):
        """ anlegen naechster schritt"""
        return OSAS_add.add2(self)

    def add3(self):
        """Foldername"""
        return OSAS_add.add3(self)

    def add6(self):
        """write new index.meta file"""
        return OSAS_add.add6(self)

        
  
    
def manage_AddOSAS_add_MetadataForm(self):
    """interface for adding the OSAS_add_Metadata"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_metadata.zpt')).__of__(self)
    return pt()

def manage_AddOSAS_add_Metadata(self,id,linklabel,description,fields,RESPONSE=None):
    """add the OSAS_root"""
    newObj=OSAS_add_Metadata(id,linklabel,description,fields)
    self.Destination()._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

            
InitializeClass(OSAS_add_Metadata)

