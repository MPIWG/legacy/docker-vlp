""" Methoden und Klassen fuer den MPIWG Archiver
20040303 Zur Zeit nur angepasst and das System des MPIWG
n�chste schritte: Konfigurierbarkeit der Metadaten / Konfigurierbarkeit der Server

"""

from OFS.Image import Image
from OFS.Folder import Folder
from OFS.SimpleItem import SimpleItem
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
from AccessControl import ClassSecurityInfo
from Globals import InitializeClass,Persistent,package_home
from Acquisition import Implicit
from OSAS_show import *
from OSAS_helpers import *
from types import *

import os.path
import os
import OSAS_ids
import archive #Baustelle
import time

class OSAS_archiveInbox(SimpleItem,Persistent,Implicit):
    """Inbox"""

    meta_type="OSAS_archiveInbox"
    pathes=[]
    
    def __init__(self,id,title):
        """init"""
        self.id=id
        self.title=title
        self.pathes=[]

    def addPath(self,path,RESPONSE=None):
        """add a path"""
        today=time.localtime()
        temp=self.pathes[0:]
        temp.append([path,today])
        self.pathes=temp[0:]
        if RESPONSE:
            RESPONSE.redirect("index_html")

    def index_html(self):
        """main"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','OSAS_archiveInboxIndex.zpt')).__of__(self)
        return pt()
    
def manage_AddOSAS_archiveInboxForm(self):
    """interface for adding the OSAS_root"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_archiveInbox.zpt')).__of__(self)
    return pt()


def manage_AddOSAS_archiveInbox(self,id,title="",RESPONSE=None):
    """add the OSAS_root"""
    if title=="":
        title=id
        
    newObj=OSAS_archiveInbox(id, title)
    self._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
    


class OSAS_metadataOrganizer(SimpleItem,Persistent,Implicit):
    """Eingabe von Metadaten"""

    meta_type="OSAS_metadataOrganizer"
    mediaTypes=["image","video","text","audio","data"]
    acquisitionTypes=["Image-Acquisition"]
    mediaToAcquisition={"image":"Image-Acquisition"}
    metaDataSets={'Image-Acquisition': [('device','opt'),('image-type','opt'),('production-comment','opt')]}
    imgData={'image':[('dpi','req')]}
    
    bibDataSets={'Book':[('author','opt'),('year','opt'),('title','opt'),('series editor','opt'),('series title','opt'),('series volume','opt'),('number of pages','opt'),('city','opt'),('publisher','opt'),('edition','opt'),('number of volumes','opt'),('translator','opt'),('ISBN ISSN','opt')],
                  'Journal Article':[('author','opt'),('year','opt'),('title','opt'),('journal','opt'),('volume','opt'),('issue','opt'),('pages','opt'),('alternate journal','opt'),('call number','opt')],
                  'Manuscript':[('author','opt'),('year','opt'),('title','opt'),('location','opt'),('signature','opt'),('pages','opt'),('editorial remarks','opt'),('description','opt'),('keywords','opt')],
                  'Journal Volume':[('title','opt'),('editor','opt'),('year','opt'),('volume','opt'),('number of pages','opt'),('city','opt'),('publisher','opt'),('ISBN ISSN','opt')]
                  }


    referenceTypes=['Book','Journal Article','Manuscript','Journal Volume']

    def __init__(self,id,title):
        """init"""
        self.id=id
        self.title=title
        #self.acquisitionData=['provider_name','provider_address','provider_contact','provider_url','date','description']


    def getName(self):
        """gives name from request session path"""
        path=self.REQUEST.SESSION['path']
        splitted=path.split("/")
        return splitted[len(splitted)-1]
        
    def addRessourceMeta(self,path=None,RESPONSE=None):
        """Metadaten fuer Ressource"""
        if not path:
            path=self.REQUEST.SESSION['path']

        else:
            self.REQUEST.SESSION['path']=path

        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','inputRessourceData.zpt')).__of__(self)
        return pt()

        
    def writeRessourceMetadata(self,name,date,description,creator,RESPONSE=None):
        """schreibe Resourcedata in index.meta"""

        path=self.REQUEST.SESSION['path']

        subnodes={}
        subnodes['name']=name
        subnodes['archive-creation-date']=date
        subnodes['creator']=creator       
        subnodes['description']=description
        
        changeNodesInIndexMeta(path,"",subnodes)
        self.inbox.addPath(self.REQUEST.SESSION['path'])
        RESPONSE.redirect(self.REQUEST['URL2'])
        
        
    def addAcquisition(self,path):
        """Hinzuf�gen von Acquisition Daten"""
        self.REQUEST.SESSION['path']=path
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','inputAcquisitionData.zpt')).__of__(self)
        return pt()


    def writeAcquisitionMetadata(self,date,path,media_type,producer="mpiwg",description=""):
        """Schreibe Acquisiondata in index.meta"""
        
        #schreibe in index.meta
        subnodes={}
        subnodes['media-type']=media_type
        changeNodesInIndexMeta(path,"",subnodes)
        
        subnodes={}
        subnodes['date']=date
        subnodes['description']=description
        
        changeNodesInIndexMeta(path,"acquisition",subnodes,parent="meta")
        #print "HI"

        subnodes={}
        subnodes['provider-id']=producer
        subnodes['url']=getattr(self.producerFolder,producer).url
        subnodes['contact']=getattr(self.producerFolder,producer).contact
        subnodes['address']=getattr(self.producerFolder,producer).address
        
        changeNodesInIndexMeta(path,"provider",subnodes,parent="acquisition")

        
        self.metaDataSet=self.metaDataSets[self.mediaToAcquisition[media_type]]
        self.media_type=self.mediaToAcquisition[media_type]
        
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','inputDocumentMetadata.zpt')).__of__(self)
        return pt()

        
    def writeDocumentMetadata(self,referenceType):
        """write document metadata"""
        form=self.REQUEST.form
        #schreibe in index.meta
        self.bibDataSet=self.bibDataSets[form['referenceType']]
        self.bibdata_type=form['referenceType']

        subnodes={}
        subnodes['device']=form['device']
        subnodes['image-type']=form['image-type']
        subnodes['production-comment']=form['production-comment']
        changeNodesInIndexMeta(self.REQUEST.SESSION['path'],"image-acquisition",subnodes,parent="meta")

        subnodes={}
        subnodes['dpi']=form['dpi']
        changeNodesInIndexMeta(self.REQUEST.SESSION['path'],"img",subnodes,parent="meta")

        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','inputBiblioMetadata.zpt')).__of__(self)
        return pt()


    def writeBiblioMetadata(self,bibdata_type,RESPONSE=None):
        """Write all"""
        #to do write metadata

        subnodes={}
        form=self.REQUEST.form
        #for key in form.keys():
        #    subnodes[key]=form['device']
        subnodes=form
        del subnodes['bibdata_type'] #in form but not metadata
        changeNodesInIndexMeta(self.REQUEST.SESSION['path'],"bib",subnodes,nodeAttributes={'type':bibdata_type},parent="meta")
        #return self.REQUEST
        return self.addRessourceMeta()
        
    

def manage_AddOSAS_metadataOrganizerForm(self):
    """interface for adding the OSAS_root"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_metadataOrganizer.zpt')).__of__(self)
    return pt()


def manage_AddOSAS_metadataOrganizer(self,id,title="",RESPONSE=None):
    """add the OSAS_root"""
    if title=="":
        title=id
        
    newObj=OSAS_metadataOrganizer(id, title)
    self._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
    

class OSAS_processViewer(SimpleItem,Persistent,Implicit):
    """Process viewer for archiving"""

    meta_type="OSAS_processViewer"

    def __init__(self, id, title):
        """init"""
        self.id=id
        self.title=title

    def index_html(self):
        """main page"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','processViewerIndex.zpt')).__of__(self)
        return pt()

    def storeFile(self,something):
        """store info in session"""
        self.REQUEST.SESSION['something']=something
        return 1

    def getFile(self,number):
        """get info from session"""
        check=self.getoverview('/var/tmp/archiver').messages()[number]
        return check

    def getoverview(self,path):
        """get overview"""
        return archive.overview(path)

    def storeerror(self,ret,path,context,i):
        """store an error"""
        session=context.REQUEST.SESSION
        session['error%i'%i]=ret
        session['path%i'%i]=path
     
        return 'error?number=%i'%i

    def geterror(self,str,context):
        session=context.REQUEST.SESSION
        return session[str]


    def readfile(self,path):
        
        ret=""
        f=open(path,'r')
        for g in f.readlines():
            ret=ret+g
        return ret
     
    def writefile(self,path,txt,REQUEST):
        """writefile"""
        f=open(path,'w')
        f.write(txt)
        f.close()
        rval=self.aq_acquire('archive2')
        return rval()


    def view(self,number):
        """view page"""
        self.errnum=number
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','processViewerView.zpt')).__of__(self)
        return pt()

    def error(self):
        """view errors"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','processViewerError.zpt')).__of__(self)
        return pt()
    
def manage_AddOSAS_processViewerForm(self):
    """interface for adding the OSAS_processViewer"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_processViewer.zpt')).__of__(self)
    return pt()


def manage_AddOSAS_processViewer(self,id,title="",RESPONSE=None):
    """add the OSAS_processViewer"""
    if title=="":
        title=id
        
    newObj=OSAS_processViewer(id, title)
    self._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')



class OSAS_archiver(Folder, Persistent,Implicit):
    """Hauptklasse fuer das Archiv"""

    meta_type="OSAS_archiver"

    manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'changeOSAS_archiverForm'},
        )
     
    def unarchive(self,path):
        """unarchive the path"""
        f = os.popen("/usr/local/mpiwg/archive/unarchiver "+path,"r")
        return f.readlines()

    def checkIfUnarchive(self,path):
        if not os.path.exists(path+"/index.meta"):
            return 0
        f=file(path+"/index.meta","r").read()
        dom=xml.dom.minidom.parseString(f)
        archiveStorageDates=dom.getElementsByTagName("archive-storage-date")
        if archiveStorageDates:
            return 1
        else:
            return 0

    ## methoden aus dem alten archive.py

   
    def isArray(self,path):
        """check for array"""
        if type(path) == StringType:
            return 0
        else:
            return 1
        
    def archiver(self,path):
        """archive the documents in path"""
        tmp=archive.archive(path,self.REQUEST.SESSION)
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','archiveStatus.zpt')).__of__(self)
        return pt()

    def archive2(self):
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','archive2.zpt')).__of__(self)
        return pt()


    def metachecker(self,path):
        """check the metadata of the documents in path"""
        self.REQUEST.SESSION['path']=self.REQUEST['path']
        return archive.metacheck(path)

    ## methods  from OSAS_show
    def changeName(self,name):
        return changeName(name)

    def hasMetafile(self,path):
        return hasMetafile(path)

    def getMetafile(self,path):
        return getMetafile(path)

    def toggle_view(self,path,file):
        """Oeffnen bzw. schlie�en der Subfolders"""
        self.tree(path).toggle(path,file)
        return self.REQUEST.RESPONSE.redirect(self.REQUEST['URL1']+"?path="+path)



    def isdigilib2(self,path):
        """check if digilib"""
        return isdigilib2(path)

    def path_to_link_view(self,path):
        """generates navigation bar for viewfiles"""
        return path_to_link_view(self.REQUEST['URL'],path)
    

    def tree(self,start):
	"""get the filetree"""
	k=browse(start)
	return k

    def getfilesystem2(self,start,reload=0):
	"""load filesystem"""
	k=filesystem2(start,1)
	return k

    def getfilesystem(self,start,reload=0):
	"""load filesystem"""

	k=filesystem(start,1)
	return k


    ##init
    def __init__(self, id, title,startPath):
        """init"""
        self.id=id
        self.title=title
        self.startPath=startPath

    def archiver_html(self):
        """archiver"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','OSAS_Archiver.zpt')).__of__(self)
        return pt()

    def index_html(self):
        """main page"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','archiverIndex.zpt')).__of__(self)
        return pt()

    def getDate(self):
        """date"""
        return time.strftime("%Y-%m-%d",time.localtime())
    
    def newFolders_html(self):
        """main page"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','newFolders.zpt')).__of__(self)
        return pt()

    def getProducers(self):
        """Ausgabe der registrierten Benutzer"""
        ret=[]
        #x=7
        id=self.producerFolder.getId()
        for list in self.producerFolder.__dict__:
            obj=getattr(self.producerFolder,list)
            if (hasattr(obj,"meta_type")):
                if (obj.meta_type=="OSAS_producer"):
                    ret.append(obj.getId())
        return ret

    def getProducer(self,id):
        """Gebe ProducerObjekt zur�ck"""
        obj=getattr(self.producerFolder,id)
        return obj
        
        

    def createFoldersForm(self,producer,number):
        """Erzeuge Folder im producer Verzeichnis mit ids"""
        self.REQUEST.SESSION['producer']=producer
        self.REQUEST.SESSION['ids']=self.idGenerator.giveIdsOut(number)
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','createFoldersForm.zpt')).__of__(self)
        return pt()
    
    def createFolders(self,folderList,producer):
        """Erzeug die entsprechenden Folder"""
        #hack
        #producer="library"
        if type(folderList)==StringType:
            folders=[folderList]
        else:
            folders=folderList
        #return producer
        producerFolderName=self.getProducer(producer).producerFolderName
        
        for folder in folders:
            os.mkdir(self.startPath+"/"+producerFolderName+"/"+folder)
            os.chmod(self.startPath+"/"+producerFolderName+"/"+folder,0775)
        self.REQUEST.SESSION['folderList']=folders
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','createFolders.zpt')).__of__(self)
        return pt()

    def storeerror(self,ret,path,context,i):
        """store an error"""
        session=context.REQUEST.SESSION
        session['error%i'%i]=ret
        session['path%i'%i]=path
     
        return 'error?number=%i'%i

    def geterror(self,str,context):
        session=context.REQUEST.SESSION
        return session[str]

    def readfile(self,path):
        
        ret=""
        f=open(path,'r')
        for g in f.readlines():
            ret=ret+g
        return ret
     
    def writefile(self,path,txt,REQUEST):
        """writefile"""
        f=open(path,'w')
        f.write(txt)
        f.close()
        return self.archive2()

    def error(self):
        """view errors"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','processViewerError.zpt')).__of__(self)
        return pt()

    
    def archiveSelected(self):
        """Archiviere ausgewaehlte files"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','archiveSelected.zpt')).__of__(self)
        return pt()

    def enterAcquisitionMetadata(self):
        """Erstelle Metadaten fuer Acquisition"""

    def enterPreliminaryBibMeta(self):
        """Erstelle Metadaten fuer Bibliography"""
        
    def showFilesForArchiving(self):
        """Anzeige der noch zu archivieren Files"""

        
    def changeOSAS_archiverForm(self):
        """change"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeOSAS_archiver.zpt')).__of__(self)
        return pt()

    def changeOSAS_archiver(self,startPath,title="",RESPONSE=None):
        """change"""
        self.startPath=startPath
        self.title=title

        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
    

def manage_AddOSAS_archiverForm(self):
    """interface for adding the OSAS_root"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_archiver.zpt')).__of__(self)
    return pt()


def manage_AddOSAS_archiver(self,id,startPath,title="",RESPONSE=None):
    """add the OSAS_root"""
    if title=="":
        title=id
        
    newObj=OSAS_archiver(id, title,startPath)
    self._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')


class OSAS_producer(SimpleItem,Persistent,Implicit):
    """Klasse fuer Produzenteninformationen
    Metadaten nach  V1.1.1"""

    meta_type="OSAS_producer"

    def __init__(self,shortName,fullName,producerFolderName,address="",url="",contact=""):

        self.id=shortName
        self.title=fullName
        self.address=address
        self.url=url
        self.contact=contact
        self.producerFolderName=producerFolderName
        
    manage_options = SimpleItem.manage_options+(
        {'label':'Main Config','action':'changeOSAS_producerForm'},
        )

    def changeOSAS_producerForm(self):
        """change"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeOSAS_producer.zpt')).__of__(self)
        return pt()

    def changeOSAS_producer(self,title,address,producerFolderName,contact="",url="",RESPONSE=None):
        """change"""
        self.title=title
        self.address=address
        self.url=url
        self.contact=contact
        self.producerFolderName=producerFolderName

        if RESPONSE:
            RESPONSE.redirect("manage_main")
        
def manage_AddOSAS_producerForm(self):
    """interface for adding the OSAS_root"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_producer.zpt')).__of__(self)
    return pt()


def manage_AddOSAS_producer(self,id,producerFolderName,title="",contact="",address="",url="",RESPONSE=None):
    """add the OSAS_root"""
    if title=="":
        title=id
        
    newObj=OSAS_producer(id, title,producerFolderName,address,contact,url)
    self._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

