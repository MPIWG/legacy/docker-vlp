# Methoden und Classen zum Hinzufuegen von Dokumenten


from OSAS_helpers import readArchimedesXML, getText
import os
import xml.dom.minidom
import re
import urllib
import OSAS_add
import OSAS_show
import string
from OFS.Folder import Folder
from AccessControl import ClassSecurityInfo
from Globals import InitializeClass
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
import archive # check if this is necessary
from Globals import package_home

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import sys
import logging
import xmlrpclib
#ersetzt logging
def logger(txt,method,txt2):
    """logging"""
    logging.info(txt+ txt2)



def spaces(depth):
    """needed in XMLtoTree"""
    tmp=""
    k=0
    while k<2*depth:
        k+=1
        tmp=tmp+"&nbsp;"+"&nbsp;"
    return tmp


class OSAS_add_Document(Folder):
    """Hinzufuegen eines Dokumentes zum Storage"""
    security=ClassSecurityInfo()

    def __init__(self,id):
        """initialize a new instance"""
        self.id = id
        
        
    meta_type='OSAS_add_Document'    
    manage_options = Folder.manage_options+(
            {'label':'Main Config','action':'add_Document_config'},
            )
        
     
        
    security.declarePublic('add_Document_config')
    def add_Document_config(self):
        """Main configuration"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeOSAS_addDocument.zpt')).__of__(self)
        return pt()
    
    security.declarePublic('change_OSAS_add_Document')
    def change_OSAS_add_Document(self,RESPONSE=None):
        """Change"""
#        self.RootFolderName=RootFolderName
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    security.declarePublic('index_html')
    def index_html(self):
        """stantard aufruf"""
        return OSAS_add.add(self.standardMD,no_upload=1)


    security.declarePublic('add2')
    def add2(self):
        """ anlegen naechster schritt"""
        return OSAS_add.add2(self.standardMD)

    def add3(self):
        """Foldername"""
        return OSAS_add.add3(self)

    def add4(self):
        """Applet"""
        
        return OSAS_add.add4(self)

    def add5(self):
        """Foldername"""
        return OSAS_add.add5(self)

    def add6(self):
        """write new index.meta file"""
        return OSAS_add.add6(self)

    def addText2(self):
        """add only a text"""
        metadata=OSAS_add.parse_query_string(self.REQUEST['QUERY_STRING'])
        metadata['archive-creation-date']=OSAS_add.date(self)
        metadata['creator']=self.REQUEST['creator']
        metadata['description']=self.REQUEST['content_description']
        metadata['archive-path']=os.path.split(self.REQUEST.SESSION['pathnew'])[0]
        #metadata['folder_name']=self.REQUEST.SESSION['folder_name']
        metadata['folder_name']=os.path.split(self.REQUEST.SESSION['pathnew'])[1]
        metadata['content-type']="fulltext document"
        self.reftype=self.REQUEST.SESSION['reftype']
        self.REQUEST.SESSION['add_metadata']=metadata   
        self.add_metadata=metadata
        self.metadata=self.REQUEST.SESSION['metadata']
        self.metadataprint=""
        for tag in self.metadata.keys():
            
            #print str(self.metadata[tag]).decode('utf-8').encode('utf-8')
            self.metadataprint=self.metadataprint+"<"+tag+">"+self.metadata[tag]+"</"+tag+">\n"

        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','index_meta')).__of__(self)
        newtemplate.content_type="text/plain"
        renderxml = newtemplate(encoding='utf-8')
        #return renderxml
        metapath=self.REQUEST.SESSION['pathnew']+"/index.meta"
        
        f=open(metapath,'w')

        f.write(renderxml)
        f.close()
        os.chmod(metapath,0664)
        os.popen('chmod -R 0775 %s'%self.add_metadata['archive-path']+"/"+self.add_metadata['folder_name']) 
        
        return OSAS_add.addText2(self)

    def addPdf2(self):
        """add only a pdf"""
        metadata=OSAS_add.parse_query_string(self.REQUEST['QUERY_STRING'])
        metadata['archive-creation-date']=OSAS_add.date(self)
        metadata['creator']=self.REQUEST['creator']
        metadata['description']=self.REQUEST['content_description']
        metadata['archive-path']=os.path.split(self.REQUEST.SESSION['pathnew'])[0]
        #metadata['folder_name']=self.REQUEST.SESSION['folder_name']
        metadata['folder_name']=os.path.split(self.REQUEST.SESSION['pathnew'])[1]
        metadata['content-type']="fulltext document"
        self.reftype=self.REQUEST.SESSION['reftype']
        self.REQUEST.SESSION['add_metadata']=metadata   
        self.add_metadata=metadata
        self.metadata=self.REQUEST.SESSION['metadata']
        self.metadataprint=""
        for tag in self.metadata.keys():
                self.metadataprint=self.metadataprint+"<"+tag+">"+self.metadata[tag]+"</"+tag+">\n"

        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','index_meta')).__of__(self)
        newtemplate.content_type="text/plain"
        renderxml = newtemplate(encoding='utf-8')
        metapath=self.REQUEST.SESSION['pathnew']+"/index.meta"
        
        
        f=open(metapath,'w')
        f.write(renderxml)
        f.close()
        os.chmod(metapath,0664)
        os.popen('chmod -R 0775 %s'%self.add_metadata['archive-path']+"/"+self.add_metadata['folder_name']) 
        
        return OSAS_add.addPdf2(self)

    def addImages(self,path):
        """Hinzufuegen eines neuen Imagesfolders"""
        return OSAS_add.addImages(self,path)

    def addImages2(self):
        """Upload des neuen Imagefolders"""
        return OSAS_add.addImages2(self)

    
def manage_AddOSAS_add_DocumentForm(self):
    """interface for adding the OSAS_add_Metadata"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_document.zpt')).__of__(self)
    return pt()

def manage_AddOSAS_add_Document(self,id,RESPONSE=None):
    """add the OSAS_root"""
    newObj=OSAS_add_Document(id)
    self.Destination()._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

            
InitializeClass(OSAS_add_Document)


class OSAS_add_Text(Folder):
    """Hinzufuegen eines Text-Dokumentes zum Storage"""
    security=ClassSecurityInfo()

    def __init__(self,id):
        """initialize a new instance"""
        self.id = id
        
        
    meta_type='OSAS_add_Text'    
    manage_options = Folder.manage_options+(
            {'label':'Main Config','action':'add_Text_config'},
            )
        
     
        
    security.declarePublic('add_Text_config')
    def add_Text_config(self):
        """Main configuration"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeOSAS_addText.zpt')).__of__(self)
        return pt()
    
    security.declarePublic('change_OSAS_add_Text')
    def change_OSAS_add_Text(self,RESPONSE=None):
        """Change"""
#        self.RootFolderName=RootFolderName
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    def addPdf(self,path):
        """Add a pdf"""
        return OSAS_add.addPdf(self,path)

    def addPdf2(self):
        """Read the file and store it"""
        return OSAS_add.addPdf2(self)
    
    def addText(self,path):
        """Add a fulltext"""
        return OSAS_add.addText(self,path)

    def addText2(self):
        """Read the file and store it"""
        return OSAS_add.addText2(self)
    
def manage_AddOSAS_add_TextForm(self):
    """interface for adding the OSAS_add_Metadata"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_text.zpt')).__of__(self)
    return pt()

def manage_AddOSAS_add_Text(self,id,RESPONSE=None):
    """add the OSAS_root"""
    newObj=OSAS_add_Text(id)
    self.Destination()._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

            
InitializeClass(OSAS_add_Text)


class OSAS_add_contextData(Folder):
    """Einfuegen eines Documentes in eine Collection"""
    
    security=ClassSecurityInfo()

            
    meta_type='OSAS_add_contextData'    

    def XmlToTree(self,URL,pwstr=None):
        """Collection XML to Tree"""
        
        class requestHandler(ContentHandler):
            def __init__(self):
                self.depth=0
                self.retStr=""
                
            def startElement(self,name,attrs):
                if name=="element":
                    self.depth+=1
                    begin=""
                    end=""
                    if self.depth==1:
                        begin="<b>"
                        end="</b>"
                        
                    self.retStr+=spaces(self.depth)+"<input type='radio' name='collection' value='%s'>%s</input>"%(attrs.get('url'),begin+urllib.unquote(attrs.get('name'))+end)+"<br>\n"


                    
            def endElement(self,name):
                if name=="element":
                    self.depth-=1

        if pwstr=="":
            pwstr=None
            
        try:
            if pwstr:
                URL+="/getCollectionTreeXML?pwstr=%s"%pwstr    
            else:
                URL+="/getCollectionTreeXML"
            parser=make_parser()
            curHandler=requestHandler()
            parser.setContentHandler(curHandler)

            parser.parse(urllib.urlopen(URL))
            return curHandler.retStr

        except:
            return URL
            return urllib.urlopen(URL).read()
            
      
    def __init__(self,id,collection):
        self.id=id
        self.collection=collection

  
   

    def getPartners(self,URL):
        """Zeige Partnerliste"""
        class requestHandler(ContentHandler):
            def __init__(self):
                self.ret=[]
                
            def startElement(self,name,attrs):
                if name=="partner":
                    self.ret.append((attrs.get('id'),attrs.get('title').encode('utf-8')))


        URL+="/getPartnersXML"
        parser=make_parser()    
        curHandler=requestHandler()
        parser.setContentHandler(curHandler)
        parser.parse(urllib.urlopen(URL))               
        try:    
                  
                return curHandler.ret
        except:
                return [("",URL)]
            
    
    def addContextData(self,path):
        """Hinzufuegen zu einer Sammlung"""
        
        server=getattr(self,'serverUrl',self.REQUEST['SERVER_URL'])
        if path[0]=="/": #strip leading slash
		urlpath=path[1:]
	else:
		urlpath=path
        try:
            urllib.urlopen(server+"/"+urlpath+"/index.meta")
            
        except:
            logger("OSAS_addfiles (addContextData)", logging.INFO,"%s (%s)"%sys.exc_info()[0:2])
            return server+"/"+urlpath+"/index.meta file has to exist!"

        # old version for foxridge storage viewer
        #links=[(path,'storage server view')]
        # libcoll viewer with imagepath: needs pageimg directory
        links=[('http://libcoll.mpiwg-berlin.mpg.de/libview?mode=imagepath&url=%s/pageimg'%path,'libcoll standard viewer (pageimg)')]
        # libcoll viewer with texttool: needs texttool tag
        links+=[('http://libcoll.mpiwg-berlin.mpg.de/libview?mode=texttool&url=%s/index.meta'%path,'libcoll standard viewer (texttool)')]
                
        links+=OSAS_show.readContexts(path) # auslesen von contexten fuer den link
        #print "LINK",links
        #return links
        self.REQUEST.SESSION['links']=links
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','contextDataMain.zpt')).__of__(self)
        return pt()
    
   
    def addContextData2(self,path,collection,link,label,description,content_type,responsible,weight,credits=None):
        """Hinzufuegen der Resource"""
        splitted=path.split("/")
        #print "BLU"
        id=splitted[len(splitted)-1]
        title=splitted[len(splitted)-1]
        server=getattr(self,'serverUrl',self.REQUEST['SERVER_URL'])
        if path[0]=="/": #strip leading slash
		urlpath=path[1:]
	else:
		urlpath=path


        metalink=server+"/"+urlpath+"/index.meta"
        
        #link=TOBEDONE"
        """Hinzufuegen der Ressource"""

        params=urllib.urlencode({'id':id,'title':title,'link':link,'label':label,'description':description,'contentType':content_type,'responsible':responsible,'weight':weight,'credits':credits,'metalink':metalink})

        server=xmlrpclib.ServerProxy(collection,allow_none=True)   
        try:
            #retStr=urllib.urlopen(collection+"/addResource",params).read()
            
            retStr=server.addResource(id,title,label,description,content_type,responsible,link,metalink,weight,credits)
              
        except:
            return "An Error occured adding the resource A\n", collection,params,str(sys.exc_info()[0]),str(sys.exc_info()[1])
        
        #print "added"
        logging.debug("Add an resource:"+repr(retStr))
        if not retStr:
            return "An Error occured adding the resource B\n", collection,params
        
        #print collection+"/"+id+"/copyIndex_meta2echo_resource"
        #TODO: fehler falls generate label oder title nicht funktioniert abfangen, bei ECHO geht das z.B. manchmal nicht.
        #server=xmlrpclib.ServerProxy(collection+"/"+id,allow_none=True)
        #server2=getattr(server,id)
        #server2.copyIndex_meta2echo_resource()
        #urllib.urlopen(collection+"/"+id+"/copyIndex_meta2echo_resource").read()
        #print "copy2res"
	#try:
        	#urllib.urlopen(collection+"/"+id+"/generate_label").read()
   #         server2.generate_label()
      #  except:
    #        logger("OSAS_addfiles (addContextData2,label not generated)", logging.INFO,"%s (%s)"%sys.exc_info()[0:2])
	#try:
        	#urllib.urlopen(collection+"/"+id+"/generate_title").read()
   #         server2.generate_title()
	#except:
	#    logger("OSAS_addfiles (addContextData2,title not generated)", logging.INFO,"%s (%s)"%sys.exc_info()[0:2])
	        

        return self.REQUEST.RESPONSE.redirect(self.REQUEST['URL2']+'?path='+path)
        
    manage_options = Folder.manage_options+(
            {'label':'Main Config','action':'add_contextData_config'},
            )
        
     
    def add_contextData_config(self):
        """Main configuration"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeOSAS_add_contextData.zpt')).__of__(self)
        return pt()
    
    
    def change_OSAS_add_contextData(self,collection,serverUrl,pwstr=None,RESPONSE=None):
        """Change"""
        self.pwstr=pwstr
        self.collection=collection
        self.serverUrl=serverUrl
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
            
def manage_AddOSAS_add_contextDataForm(self):
    """interface for adding the OSAS_add_Metadata"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_contextData.zpt')).__of__(self)
    return pt()

def manage_AddOSAS_add_contextData(self,id,collection,RESPONSE=None):
    """add the OSAS_root"""
    newObj=OSAS_add_contextData(id,collection)
    self.Destination()._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

            
InitializeClass(OSAS_add_contextData)

class OSAS_add_Presentation(Folder):
    """Hinzufuegen der Presentationsinformationen"""
    security=ClassSecurityInfo()

    def __init__(self,id):
        """initialize a new instance"""
        self.id = id
        
        
    meta_type='OSAS_add_Presentation'    
    manage_options = Folder.manage_options+(
            {'label':'Main Config','action':'add_Presentation_config'},
            )
        
     
        
    security.declarePublic('add_Presentation_config')
    def add_Presentation_config(self):
        """Main configuration"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeOSAS_addPresentation.zpt')).__of__(self)
        return pt()
    
    security.declarePublic('change_OSAS_add_Presentation')
    def change_OSAS_add_Presentation(self,RESPONSE=None):
        """Change"""
#        self.RootFolderName=RootFolderName
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    def addPresentation(self,path):
        """Hinzufuegen der Presenationsinformation"""
        return OSAS_add.addPresentation(self,path)

    def addPresentation2(self):
        """Eingabe von Metadateninformationen"""
        return OSAS_add.addPresentation2(self)
    
def manage_AddOSAS_add_PresentationForm(self):
    """interface for adding the OSAS_add_Metadata"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_presentation.zpt')).__of__(self)
    return pt()

def manage_AddOSAS_add_Presentation(self,id,RESPONSE=None):
    """add the OSAS_root"""
    newObj=OSAS_add_Presentation(id)
    self.Destination()._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

            
InitializeClass(OSAS_add_Presentation)

class OSAS_combineTextImage(Folder):
    """Hinzufuege der Combine Text und Image"""
    security=ClassSecurityInfo()

    def __init__(self,id):
        """initialize a new instance"""
        self.id = id
        
        
    meta_type='OSAS_combineTextImage'    
    manage_options = Folder.manage_options+(
            {'label':'Main Config','action':'combineTextImage_config'},
            )
        

        
    security.declarePublic('combineTextImage_config')
    def combineTextImage_config(self):
        """Main configuration"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ChangeOSAS_combineTextImage.zpt')).__of__(self)
        return pt()
    
    security.declarePublic('change_OSAS_combineTextImage')
    def change_OSAS_combineTextImage(self,RESPONSE=None):
        """Change"""
#        self.RootFolderName=RootFolderName
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
            
    security.declarePublic('combineTextImage')
    def combineTextImage(self,path):
        """Hinzufuegen der Presenationsinformation"""
        """gibt input formular zur erstellung des texttools meta tag aus"""
        files = os.listdir(path)
        
        texts=[]
        imagefolders=[]
        presentationfolders=[]

        splitted=path.split("/")
        externxml=readArchimedesXML(splitted[len(splitted)-1])
        
        for filename in files:
                #print "FN",filename
                if archive.isdigilib2(path+"/"+filename):
                        imagefolders.append(filename)
                        
                if archive.isFullText(path,filename):
                        #print "HI"
                        texts.append(filename)
                if archive.isPresentation(path,filename):
                        presentationfolders.append(filename)
        
        dom=xml.dom.minidom.parse(path+"/index.meta")
        try:
                filelanguage=archive.getText(dom.getElementsByTagName('lang')[0].childNodes)
        except:
                filelanguage=""
                
        self.REQUEST.SESSION['isolist']=OSAS_add.getISO()
        
        tmp=self.REQUEST.SESSION['isolist'].keys()
        tmp.sort()
        self.REQUEST.SESSION['isolistsort']=tmp
        self.REQUEST.SESSION['path']=path
        self.REQUEST.SESSION['texts']=texts
        self.REQUEST.SESSION['imagefolders']=imagefolders
        self.REQUEST.SESSION['presentationfolders']=presentationfolders
        self.REQUEST.SESSION['filelanguage']=filelanguage
        self.REQUEST.SESSION['externxml']=externxml

        newtemplate=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ImageandText')).__of__(self)
        return newtemplate()
        
    def getViewerTemplateSets(self,obj_ids=None):
        """Get the ViewerTemplateSet title for configuration"""
        ret=[]
        viewerTemplateSets=self.ZopeFind(self.viewerTemplateSets,obj_metatypes=['OSAS_viewerTemplateSet'],obj_ids=obj_ids)#assumes viewerTemplateSets folder somewhere in the hierarchie.

        try:
            viewerTemplateSets=self.ZopeFind(self.viewerTemplateSets,obj_metatypes=['OSAS_viewerTemplateSet'],obj_ids=obj_ids)#assumes viewerTemplateSets folder somewhere in the hierarchie.
            
            for viewerTemplateSet in viewerTemplateSets:
                ret.append((viewerTemplateSet[1].title,viewerTemplateSet[0],viewerTemplateSet[1]))
            
            return ret
        
        except:
            return [('no ViewerTemplateSetfolders','')]

    def getTextToolsField(self,path,name,default=''):
        """Lese Textoolsfelder aus index.meta im path aus"""
        
        try:
            dom=xml.dom.minidom.parse(path+"/index.meta")
            node=dom.getElementsByTagName('texttool')[0] #getNode
            subnode=node.getElementsByTagName(name)[0]

            # bei text wird nur der Folder gebraucht
            if name=="text":
                splitted=getText(subnode.childNodes).split("/")
                return splitted[len(splitted)-2]
            else:
                return getText(subnode.childNodes)
        except:
            return default
   

    def combineTextImage2(self,path):
        """Eingabe von Metadateninformationen"""
        
        # correct path
        path=re.sub("//","/",path)
        OSAS_add.combineTextImage2(self,path) # Add images
        splitted=path.split("/")
        linkPath=splitted[len(splitted)-1]
        linkViewerEnvironmentImages="http://nausikaa2.mpiwg-berlin.mpg.de/cgi-bin/toc/toc.x.cgi?dir=%s&step=thumb" % linkPath
        linkViewerEnvironmentOnlyText="http://nausikaa2.mpiwg-berlin.mpg.de/cgi-bin/toc/toc.x.cgi?dir=%s&step=textonly" % linkPath

        if self.REQUEST.has_key('image'): # bilder vorhanden
            linkViewerEnvironment=linkViewerEnvironmentImages
        else:
            linkViewerEnvironment=linkViewerEnvironmentOnlyText
            
        self.REQUEST.SESSION['linkViewerEnvironment']=linkViewerEnvironment
        
        writeToContext(path,linkViewerEnvironment,"ECHO standard environment",unique="yes")
        
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_combineTextImageFinal.zpt')).__of__(self)

        return pt()

    def isSelectedViewerTemplateSet(self,obj,id):
        """is ausgewaehlt"""
        
        if self.REQUEST['viewerTemplateSet']==id:
            return 1
        else:
            return None

def writeToContext(path,link,description,unique="no"):
    """Created an additional entry to the index.meta file of path"""
    dom=xml.dom.minidom.parse(path+"/index.meta")
    node=dom.getElementsByTagName('resource')[0]

    if unique=="yes":
        
        contexts=node.getElementsByTagName('context')
        for context in contexts:
            nameTag=getText(context.getElementsByTagName('name')[0].childNodes)
            linkTag=getText(context.getElementsByTagName('link')[0].childNodes)
            
            
            linkTag=re.sub("\:86","",linkTag) # alter port 86 gleich ohne port nummer (hack)
            if (nameTag==description) and (linkTag==link):
                node.removeChild(context).unlink()
                    
    subnode=dom.createElement('context')

    linknode=dom.createElement('link')
    namelinknode=dom.createTextNode(link)
    linknode.appendChild(namelinknode)
    subnode.appendChild(linknode)

    linknode=dom.createElement('name')
    namelinknode=dom.createTextNode(description)
    linknode.appendChild(namelinknode)
    subnode.appendChild(linknode)

    node.appendChild(subnode)
    
    writefile=file(path+"/index.meta","w")
    #print path+"/index.meta"
    writefile.write(dom.toxml(encoding="UTF-8"))
    writefile.close()

def manage_AddOSAS_combineTextImageForm(self):
    """interface for adding the OSAS_add_Metadata"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_combineTextImage.zpt')).__of__(self)
    return pt()

def manage_AddOSAS_combineTextImage(self,id,RESPONSE=None):
    """add the OSAS_root"""
    newObj=OSAS_combineTextImage(id)
    self.Destination()._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

            
InitializeClass(OSAS_combineTextImage)
