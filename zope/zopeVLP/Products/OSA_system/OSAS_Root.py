from OFS.SimpleItem import SimpleItem
from time import localtime,strftime
from Acquisition import Implicit
from OFS.Folder import Folder
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
import urllib
import urlparse
import sys
import logging

#ersetzt logging
def logger(txt,method,txt2):
    """logging"""
    logging.info(txt+ txt2)


import re
import xml.dom.minidom
import os.path
from Globals import package_home

class OSAS_ViewerObject(Folder):
    """Beschreibung eines Viewers"""
    meta_type="OSAS_ViewerObject"
    
    def __init__(self,id,title,prefix):
        """init"""
        self.id=id
        self.title=title
        self.prefix=prefix

    manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'changeViewerObjectForm'},
        )

    def changeViewerObjectForm(self):
        """Main configuration"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','changeViewerObjectForm.zpt')).__of__(self)
        return pt()

    def changeViewerObject(self,title,prefix,RESPONSE=None):
        """Change RootFolderName"""
        self.title=title
        self.prefix=prefix

        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


def manage_AddOSAS_ViewerObjectForm(self):
    """interface for adding the viewer Object"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_ViewerObject.zpt')).__of__(self)
    return pt()

def manage_AddOSAS_ViewerObject(self,id,title,prefix,RESPONSE=None):
    """add the OSAS_root"""
    newObj=OSAS_ViewerObject(id,title,prefix)
    self._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

    
class OSAS_Root(Folder,Implicit):
    """Implicit Folder of the  Storage Systems"""
    def __init__(self,id,RootFolderName,DigILibURL,uploadServletUrl):
        """initialize a new instance"""
        self.id = id
        self.RootFolderName = RootFolderName
        self.DigILibURL=DigILibURL
        self.uploadServletUrl=uploadServletUrl
        
    meta_type="OSAS_Root"

    manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'Root_config'},
        )

    
        
    def setDigILibURL(self):
        """set"""
        self.DigILibURL=""
        
    def Root_config(self):
        """Main configuration"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ConfigOSAS_Root.zpt')).__of__(self)
        return pt()

    def change_OSAS_Root(self,RootFolderName,DigILibURL,uploadServletUrl,RESPONSE=None):
        """Change RootFolderName"""
        self.RootFolderName=RootFolderName
        self.DigILibURL=DigILibURL
        self.uploadServletUrl=uploadServletUrl
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')

    def date(self):
        """gives formatted date"""
	return strftime("%d.%m.%Y",localtime())	

    def getUploadServletUrl(self):
        """get url"""
        try:
            self.uploadServletUrl=uploadServletUrl
        except:
            return "http://foxridge.rz-berlin.mpg.de:8000/upload/up"

    def downloadExternalXML(self,index_meta_url,xml_url):
        """lade xml file"""
        #print "GI"
        #print xml_url
	try:
	        xmlneu=urllib.urlopen(xml_url).read()
	except:
                logger("OSASRoot (downloadExternalXML)", logging.INFO,"%s (%s)"%sys.exc_info()[0:2])
                logger("OSASRoot (downloadExternalXML)", logging.INFO,xml_url)
		return "error"
        try:
            dom=xml.dom.minidom.parseString(xmlneu)
        except:
            print "Error"
            return repr(xml_url)
            return "error"

	
	#TODO: direct access to the file system necessary, fix that also xmlrpc to the server where the index file is stored is possible	
	parsedUrl=urlparse.urlparse(index_meta_url)
	path=parsedUrl[2]

        fh=open(path,'w')
	logger("OSAS",logging.INFO,path)
        fh.write(xmlneu)
        fh.close()
        return "ok"

        
def manage_AddOSAS_RootForm(self):
    """interface for adding the OSAS_root"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddOSAS_Root.zpt')).__of__(self)
    return pt()

def manage_AddOSAS_Root(self,id,RootFolderName,DigILibURL,uploadServletUrl,RESPONSE=None):
    """add the OSAS_root"""
    newObj=OSAS_Root(id,RootFolderName,DigILibURL,uploadServletUrl)
    self._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

    
