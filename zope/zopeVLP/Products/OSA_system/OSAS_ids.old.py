"""Generate and organize the institutes internal IDs
DW 2003, itgroup """

from AccessControl import ClassSecurityInfo
from Globals import InitializeClass
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
from OFS.SimpleItem import SimpleItem
from pyPgSQL import PgSQL
#import pgdb as PgSQL
from Products.PageTemplates.PageTemplate import PageTemplate
from Products.PageTemplates.PageTemplateFile import PageTemplateFile

import random
import types
import time

class OSAS_idGenerator(SimpleItem):
    """Object zum Erzeugen und Registrieren von ID's"""

    def __init__(self, id,title):
        """init"""
        self.id=id
        self.title=title

    meta_type="OSAS_idGenerator"

    def getOSASId(self):
        """erzeuge ID"""
        id=self.generateId()
        print "ID",id
        while self.idExists(id):
            id=self.generateId()
        return id

    def registerID(self,id,date,name):
        """registrieren der id"""

        if self.idExists(id):
            return "ERROR: id - %s - already exists" %id
        

        #conn=PgSQL.connect("127.0.0.1 dbname=osas_ids user=dwinter")


        conn=PgSQL.connect("127.0.0.1:osas_ids:dwinter")

        curs=conn.cursor()

        curs.execute("INSERT INTO institutesIds (id,date,name)  VALUES ('%s','%s','%s')" % (id,date,name))
        
        conn.commit()

        return "OK: id - %s - registered" %id
    
    def idExists(self,id):
        """Existiert eine ID"""
        conn=PgSQL.connect("127.0.0.1:osas_ids:dwinter")
        curs=conn.cursor()

        founds=curs.execute("SELECT id FROM institutesIds WHERE id='%s'"%id) 
        conn.commit()
        
        if not founds==None:
            return True
        else:
            return False
        
    def index_html(self):
        """ID generator"""
        pt=PageTemplateFile("Products/OSA_system/OSAS_ids.zpt").__of__(self)
        return pt()

    def giveIdsOut(self,number,RESPONSE=None):
        """Ausgabe von ID's"""
        number=int(number)
        ids=[]

        for i in range(number):
            ids.append(self.getOSASId())

        if RESPONSE:
            self.REQUEST.SESSION['ids']=ids
            pt=PageTemplateFile("Products/OSA_system/OSAS_printIDs.zpt").__of__(self)
            return pt()
        else:
            return ids

    def registerIdsOut(self,ids,name=None,IdDate=None,RESPONSE=None):
        """Registriere IDS"""
        #print "TYPES",type(ids)
        #import datetime
        
        if not name:
            name="Anon"

        if not IdDate:
            IdDate=time.strftime("%Y%m%d",time.localtime())
            
        if type(ids) is types.ListType:
            
            for id in ids:
                #print "id",id
                self.registerID(id,IdDate,name)
            self.REQUEST.SESSION['ids']=ids
        else:
            self.registerID(ids,IdDate,name)
            self.REQUEST.SESSION['ids']=[ids]

        if RESPONSE:
            pt=PageTemplateFile("Products/OSA_system/OSAS_registrationIdsDone.zpt").__of__(self)
            return pt()
        else:
            return self.REQUEST.SESSION['ids']


    def generateId(self):
        """Zuf�llige ID"""
        driEncode={ 0:'0',
                    1:'1',
                    2:'2',
                    3:'3',
                    4:'4',
                    5:'5',
                    6:'6',
                    7:'7',
                    8:'8',
                    9:'9',
                    10:'A',
                    11:'B',
                    12:'C',
                    13:'D',
                    14:'E',
                    15:'F',
                    16:'G',
                    17:'H',
                    18:'K',
                    19:'M',
                    20:'N',
                    21:'P',
                    22:'Q',
                    23:'R',
                    24:'S',
                    25:'T',
                    26:'U',
                    27:'V',
                    28:'W',
                    29:'X',
                    30:'Y',
                    31:'Z'
                    }
        
        random.seed()
        x=[]
        for i in range(7):
            x.append(random.randint(0,31))

        sum=0
        for i in range(7):
            sum+=(i+1)*x[i]
            print i
        c=sum % 31
        id=""
        for i in range(7):
            id+=driEncode[x[i]]
        id+=driEncode[c]
        return id
        
def manage_AddOSAS_idGeneratorForm(self):
    """Erzeuge idGenerator"""
    pt=PageTemplateFile('Products/OSA_system/AddOSAS_idGenerator.zpt').__of__(self)
    return pt()

def manage_AddOSAS_idGenerator(self,id,title=None,RESPONSE=None):
    """add the OSAS_root"""
    newObj=OSAS_idGenerator(id,title)
    self._setObject(id,newObj)
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')




    
        

