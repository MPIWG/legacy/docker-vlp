"""Methoden fuer Language Technologies"""

from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.ECHO_content.analyseAndTag.analyseAndTag import DonatusFile
from OFS.SimpleItem import SimpleItem
from OFS.Folder import Folder
import xml.parsers
import os.path
import urlparse,urllib
from Globals import package_home

class ECHO_language:
        """language methods"""
        
        
        def tagLex(self,nr="1",id=None):
            """gerateword tags"""
            
           
            df=DonatusFile(txt=self.getPage(_pn=nr,_id=id),baseUri=self.baseUri)
           
            return df.convertedXML()
            #return DonatusFile(txt=self.getPage(_pn=nr)).convertedXML()
        
class Collection(SimpleItem):
         def getCollectionXML(self,RESPONSE=None):
             
             """get collection as xml"""
             return self.aq_parent.getCollectionXML(collection=self.getId(),RESPONSE=RESPONSE)
         
         def __init__(self,id):
             """initialise"""
             self.id=id
             self.entries=[]
             
         
         def getEntries(self):
             """get the entries"""
             entries=self.entries
             for entry in entries: #backward compatibility, cannot be removed a.s.a.p.
                 print entry
                 if entry.has_key('master') and (len(entry['master'])<3):
                     entry['master']=(entry['master'][0],entry['master'][1],'')
                 if entry.has_key('slave') and (len(entry['slave'])<3):
                     entry['slave']=(entry['slave'][0],entry['slave'][1],'')
                 
             return self.entries
         
         def deleteEntry(self,nr):
             """delete an entry"""
             del(self.entries[nr])
           
         def changeEntry(self,nr,slaveUrl,masterID):
             """change an entry, only slaveUrl"""
             tmp=self.entries[nr]
             tm=tmp['master']
             tmp['slave']=(slaveUrl,"","")
             tmp['master']=(tm[0],tm[1],masterID)
             entries=self.entries[0:]
             entries[nr]=tmp 
             self.entries=entries[0:]
             
         def appendEntry(self,fn,id,type,pagelink):
             """append an entry"""
             #check if last entry is complete
             createNew=False
            
             if len(self.entries)==0: #noch gar kein Eintrag
                 createNew=True
             else:
                 entry=self.entries[-1]
                 if entry.get('master',None) and entry.get('slave',None):
                     createNew=True
             if createNew:

                 self.entries.append({})
                 entry=self.entries[-1]
             if type=="master":
                 entry['master']=(fn,id,pagelink)
             elif type=="slave":
                 entry['slave']=(fn,id,pagelink)
             
             entries=self.entries[0:]
             entries[-1]=entry 
             self.entries=entries[0:]
             
class ECHO_linkCreator(Folder):
     """creator for links"""
     
     meta_type="ECHO_linkCreator"
     

                 
                     
     def getCollectionEntries(self,collection):
         col=getattr(self,collection,None)
         if not col:
             return []
       
         return col.getEntries()
     
     def getAllRefIDs(self,collection):
         """return all refids"""
         ret=[]
         entries=self.getCollectionEntries(collection)
         
         for entry in entries:
             ret.append('_pagelink='+entry['master'][2])
         return "&".join(ret)
     
             
     def getCollectionXML(self,collection=None,RESPONSE=None):
         """exports the collection as an XML file"""
         if not collection:
             return "<error>no collection: need parameter collection=COLLECTION_NAME</error>"
       
         i=0
         ret=""
         ret+="""<?xml version="1.0" encoding="UTF-8"?>"""
         ret+="""<linklist xmlns="http://www.mpiwg-berlin.mpg.de/namespace">"""
         ret+="""<linklistname>%s</linklistname>"""%collection
         ret+="""<masterurl ref="%s"/>"""%self.getUrls(collection)[0]
         ret+="""<slaveurl ref="%s"/>"""%self.getUrls(collection)[1]
         
         for entry in self.getCollectionEntries(collection):
             ret+="""<link id="%s">"""%i
             i+=1
             
             if entry.has_key('master'):
                 ms=entry['master']
  
             
                 try:
                     if urlparse.urlparse(ms[0])[0]=="http": # url
                         ret+="""<source filename="%s"/>"""%urllib.quote(ms[0])
                     else:
                         ret+="""<source filename="%s" refid="%s">"""%(ms[0],ms[1])
                         splitted=ms[2].split("/")
                         if (len(splitted)>3):
                             ret+="""<pagelink refid="%s" selectionNodeIndex="%s"/>"""%(splitted[0],splitted[3])
 
                         ret+="""</source>"""
                 except: #ohne pagelink&
                     ret+="""<source filename="%s" refid="%s"/>"""%ms
             if entry.has_key('slave'):
                 ms=entry['slave']
                 try:
                    if urlparse.urlparse(ms[0])[0]=="http": # url
                        ret+="""<target filename="%s"/>"""%urllib.quote(ms[0])
                    else:
                        ret+="""<target filename="%s" refid="%s">"""%(ms[0],ms[1])
                        splitted=ms[2].split("/")
                        if (len(splitted)>3):
                            ret+="""<pagelink refid="%s" selectionNodeIndex="%s"/>"""%(splitted[0],splitted[3])
                        ret+="""</target>"""
                 except: #ohne pagelink
                     ret+="""<target filename="%s" refid="%s"/>"""%ms
                  
             ret+="</link>"
         ret+="""</linklist>"""
         if RESPONSE:
             RESPONSE.setHeader("Content-Type","text/xml")
         return ret
     def index_html(self,collection=None):
         """show create links"""
         if not collection:
             return "no collection: need parameter collection=COLLECTION_NAME"
         
              
         pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ECHO_linkCreator_main')).__of__(self)
         
         col=getattr(self,collection,None)
         if not col:
             return []
       
         masterUrl=getattr(col,'masterUrl','')
         slaveUrl=getattr(col,'slaveUrl','')
         
         return pt(collection=collection,masterUrl=masterUrl,slaveUrl=slaveUrl)
     
     def addEntry(self,collection,fn,id,type,pagelink,fromurl=None,RESPONSE=None,REQUEST=None):
         """add an entry"""
        
         col=getattr(self, collection,None)
         if not col:
             self._setObject(collection,Collection(collection))
             col=getattr(self, collection)
         
         col.appendEntry(fn,id,type,pagelink)
         
         if fromurl and RESPONSE:
     
             RESPONSE.setHeader("Expires",(DateTime()-1).rfc822())
             RESPONSE.setHeader("Cache-Control", "no-cache")
             RESPONSE.redirect(fromurl)
   

     def changeEntry(self,collection,nr,slaveUrl,masterID,RESPONSE=None):
         """change an entry 8only slaveUrl at the moment"""
         col=getattr(self, collection,None)
         col.changeEntry(nr,slaveUrl,masterID)

         if RESPONSE:
             RESPONSE.redirect(self.absolute_url()+"?collection="+collection)
         
     def removeEntry(self,collection,nr,RESPONSE=None):
         """remove an entry"""
         col=getattr(self, collection,None)
         col.deleteEntry(nr)
         
         if RESPONSE:
             RESPONSE.redirect(self.absolute_url()+"?collection="+collection)
         
     def setUrls(self,collection,masterUrl,slaveUrl,RESPONSE=None):
         """set the urls for the document viewer"""
         col=getattr(self, collection,None)
         setattr(col,'masterUrl',masterUrl)
         setattr(col,'slaveUrl',slaveUrl)
         
         if RESPONSE:
             RESPONSE.redirect(self.absolute_url()+"?collection="+collection)
         
     def getUrls(self,collection,RESPONSE=None):
         """set the urls for the document viewer"""
         col=getattr(self, collection,None)
         x=getattr(col,'masterUrl')
         y=getattr(col,'slaveUrl')
         return x,y
         
def manage_addECHO_linkCreatorForm(self,RESPONSE=None):
    """Form for adding"""
    manage_addECHO_linkCreator(self,RESPONSE)

def manage_addECHO_linkCreator(self,RESPONSE=None):
    """Add an ECHO_main"""
    id='linkCreator'
    self._setObject(id,ECHO_linkCreator(id))
    
  
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')      
         
         
         
         
         
         
         
         
         
         
         
         
         
         