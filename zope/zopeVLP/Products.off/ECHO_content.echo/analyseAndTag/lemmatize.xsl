
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://exslt.org/strings" extension-element-prefixes="str"
  exclude-result-prefixes="str" xmlns:mpiwg="http://www.mpiwg-berlin.mpg.de/namespace">
	
<xsl:template match="/" >
<text>
  <xsl:apply-templates />
</text>
</xsl:template>
<xsl:template match="*">
<xsl:apply-templates mode="body"/>
</xsl:template>

<xsl:template match="text()" mode="body">
<xsl:variable name="tokenizedSample" select="str:tokenize(.)"/>
    <xsl:for-each select="$tokenizedSample">
      <mpiwg:w><xsl:attribute name="lang">de</xsl:attribute><xsl:value-of select="."/></mpiwg:w>      
    </xsl:for-each>
</xsl:template>

<xsl:template match="head" mode="body"/>

<xsl:template match="*" mode="body">
<!--<xsl:element name="{local-name()}">-->
<xsl:copy>
<xsl:for-each select="@*"><xsl:copy/></xsl:for-each>
<xsl:apply-templates mode="body"/>
</xsl:copy>
</xsl:template>

</xsl:stylesheet>
