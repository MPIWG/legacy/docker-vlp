<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:str="http://exslt.org/strings" extension-element-prefixes="str"
  exclude-result-prefixes="str">
<xsl:template match="/" >
<wtag locator="xxx"><section lang="de">
  <xsl:apply-templates />
</section></wtag>
</xsl:template>
<xsl:template match="*">
<xsl:apply-templates mode="body"/>
</xsl:template>

<xsl:template match="text()" mode="body">
<xsl:variable name="tokenizedSample" select="str:tokenize(.,'()[].!;, ')"/>
    <xsl:for-each select="$tokenizedSample">
      <w><xsl:value-of select="."/></w>      
    </xsl:for-each>
</xsl:template>

<xsl:template match="*" mode="body">
<xsl:apply-templates mode="body"/>
</xsl:template>
</xsl:stylesheet>
