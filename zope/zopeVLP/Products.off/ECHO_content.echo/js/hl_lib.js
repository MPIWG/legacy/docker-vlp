/* Copyright (C) 2004 itgroup MPIWG
 
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 
Authors: ROC 10.6.2005
*/

var hllibVersion = "1.0.2";
var hlAreas = new Object();
var hlTrafos = new Object();

// Area class
function Area(id, target_id, a_x, a_y, a_w, a_h, type, perm) {
    this.id = id;
    this.target_id = target_id;
    this.rect = new Rectangle(a_x, a_y, a_w, a_h);
    this.type = type;
    this.permanent = (perm.toLowerCase() == 'true');
    this.pxarea = 0;
    return this;
}

function addArea(id, target_id, a_x, a_y, a_w, a_h, type, perm) {
    hlAreas[id] = new Area(id, target_id, a_x, a_y, a_w, a_h, type, perm);
}

function parseTrafo(target) {
    // returns Transform for given target picsize
    var picsize = getElementRect(target);
    var trafo = new Transform();
    // subtract area offset and size
    //trafo.concat(getTranslation(new Position(-dlArea.x, -dlArea.y)));
    //trafo.concat(getScale(new Size(1/dlArea.width, 1/dlArea.height)));
    // scale to screen size
    trafo.concat(getScale(picsize));
    trafo.concat(getTranslation(picsize));
    return trafo;
}

function placeAreas() {
    // positions all area-images
    var maxsize = 0;
    for (var a in hlAreas) {
	var area = hlAreas[a];
	var trafo = hlTrafos[area.target_id];
	if (trafo) {
	    var rect = trafo.transform(area.rect);
	    area.pxarea = rect.getArea();
	    maxsize = Math.max(maxsize, area.pxarea);
	    var img = getElement("i."+area.id);
	    if (area.type == "arrow") {
		var pos = rect.getPosition();
		var isize = getElementSize(img);
		pos.x += rect.width * 0.5 - isize.width * 0.5;
		pos.y += rect.height * 0.5 - isize.height * 0.5;
		moveElement(img, pos);
		if (img.layers) {
		    // N4: grow layer for border
		    img.resizeBy(2,2);
		}
	    } else if (area.type == "text") {
		var txt = getElement("t."+area.id);
		if (img.layers) {
		    // for N4 we size a transparent image as the area
		    var li = img.document.images[0];
		    li.width = rect.width;
		    li.height = rect.height;
		}
		moveElement(img, rect);
		moveElement(txt, rect);
	    } else {
		if (img.layers) {
		    // for N4 we size a transparent image as the area
		    var li = img.document.images[0];
		    li.width = rect.width;
		    li.height = rect.height;
		}
		moveElement(img, rect);
	    }
	    highlightMark(area.id, area.permanent);
	}
    }
    // add z-index (largest value for smallest area)
    for (a in hlAreas) {
	area = hlAreas[a];
	var elem = getElement("i."+area.id);
	if (elem.style) {
	    elem.style.zIndex = getInt(maxsize - area.pxarea);
	} else if (elem.layers) {
	    elem.zIndex = getInt(maxsize - area.pxarea);
	}
    }
}

function highlightLink(id, highlight) {
    // hightlights or unhighlights the link of id
    var link = getElement("a."+id);
    if (highlight) {
	// highlight
	if (link.style) {
	    link.style.backgroundColor = "#f08080";
	} else if (link.layers) {
	    link.bgColor = "#f08080";
	}
    } else {
	// unhighlight
	if (link.style) {
	    link.style.backgroundColor = "";
	} else if (link.layers) {
	    link.bgColor = null;
	}
    }
}

function highlightMark(id, highlight) {
    // hightlights or unhighlights the area/img of id
    var area = hlAreas[id];
    var img = getElement("i."+id);
    if (highlight) {
	// highlight
	if (area.type == "arrow") {
	    if (img.style) {
		img.style.borderStyle = "solid";
	    } else if (img.layers) {
		img.bgColor = "#0000ff";
	    }
	} else if (area.type == "text") {
	    if (img.style) {
		img.style.background = "url(area_img)";
	    } else if (img.layers) {
		img.background.src = "area_img";
	    }
	    var txt = getElement("t."+id);
	    if (txt.style) {
		txt.style.visibility = "visible";
	    }
	} else if (area.type == "area") {
	    if (img.style) {
		img.style.background = "url(area_img)";
	    } else if (img.layers) {
		img.background.src = "area_img";
	    }
	}
    } else {
	// unhighlight
	if (area.type == "arrow") {
	    if (img.style) {
		img.style.borderStyle = "none";
	    } else if (img.layers) {
		img.bgColor = null;
	    }
	} else if (area.type == "text") {
	    var txt = getElement("t."+id);
	    if (img.style) {
		img.style.background = "url(trans_img)";
	    } else if (img.layers) {
		img.background.src = null;
	    }
	    if (txt.style) {
		txt.style.visibility = "hidden";
	    }
	} else if (area.type == "area") {
	    if (img.style) {
		img.style.background = "url(trans_img)";
	    } else if (img.layers) {
		img.background.src = null;
	    }
	}
    }
}

function highlightPair(id, highlight) {
    // hightlights or unhighlights the link and image of id
    //alert("highlightpair: "+id+" "+highlight);
    var area = hlAreas[id];
    highlightLink(id, highlight);
    if (!area.permanent) {
	highlightMark(id, highlight);
    }
}

function init() {
    map_init();
}

var elemScaler;
var dlTrafo;

function map_init() {
    // initialise baselib
    base_init();
    //alert("huhu4!");
    elemScaler = getElement("overview");
    dlTrafo = parseTrafo(elemScaler);
    hlTrafos["overview"] = dlTrafo;
    placeAreas();
    return null;
}

function showcoordsN4() {
    var s = "";
    for (var l in document.layers) {
	if (l == "length") continue;
	e = document.layers[l];
	if (e) {
	    s += " [" + e.name + "]: pageX:" + e.pageX + " pageY:" + e.pageY + " width:" + e.clip.width + " height:" + e.clip.height + " visibility:" + e.visibility + " zindex:" + e.zIndex + "<br>\n";
	} else {
	    s += " {" + l + "}<br>\n";
	}
    }
    return s;
}