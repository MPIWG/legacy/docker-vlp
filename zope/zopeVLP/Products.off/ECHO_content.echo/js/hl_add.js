/* Copyright (C) 2004 itgroup MPIWG
 
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA
 
Authors: ROC 23.11.2004

!! requires hl_lib.js !!
*/

var elemCoords;
var elemRed;

function mapadd_init() {
    map_init();
    // form element for coordinates
    elemCoords = getElement("coordstr");
    // new red area 
    elemRed = getElement("newarea");
    selectArea();
}

function selectArea() {
    var click = 1;
    var pt1, pt2;
    var redrect;
    window.focus();

    function areaClick(evt) {
	// mouse click handler
	if (click == 1) {
	    // first click -- start moving
	    click = 2;
	    pt1 = evtPosition(evt);
	    pt2 = pt1;
	    redrect = new Rectangle(pt1.x, pt1.y, 10, 10);
	    moveElement(elemRed, redrect);
	    // show element
	    if (elemRed.style) {
		elemRed.style.background = "url(area_img)";
	    } else if (elemRed.layers) {
		elemRed.background.src = "area_img";
	    }
	    showElement(elemRed, true);
	    // show moving
	    registerEvent("mousemove", elemScaler, areaMove);
	    registerEvent("mousemove", elemRed, areaMove);
	} else {
	    // second click -- end moving
	    pt2 = evtPosition(evt);
	    unregisterEvent("mousemove", elemScaler, areaMove);
	    unregisterEvent("mousemove", elemRed, areaMove);
	    var p1 = dlTrafo.invtransform(pt1);
	    var p2 = dlTrafo.invtransform(pt2);
	    var ww = p2.x - p1.x;
	    var wh = p2.y - p1.y;
	    if ((ww > 0)&&(wh > 0)) {
		var coords = cropFloat(p1.x)+","+cropFloat(p1.y)+","+cropFloat(ww)+","+cropFloat(wh);
		// fill coords into form
		elemCoords.value = coords;
	    }
	    // start again
	    click = 1;
	}
    }

    function areaMove(evt) {
	// mouse move handler
	pt2 = evtPosition(evt);
	// restrict marks to move right and down
	var w = pt2.x - pt1.x;
	var h = pt2.y - pt1.y;
	redrect.width = (w > 0) ? w : 1;
	redrect.height = (h > 0) ? h : 1;
	moveElement(elemRed, redrect);
    }

    // starting event capture
    registerEvent("mousedown", elemScaler, areaClick);
    registerEvent("mousedown", elemRed, areaClick);
}
