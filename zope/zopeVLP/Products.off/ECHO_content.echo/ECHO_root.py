"""ECHO_root for the central functions for ECHO used by all objects

"""

     
import urlparse
import string
import tempfile
import zipfile
import re
import os,shutil
import OFS.Image
from types import *
from OFS.Cache import Cacheable
from OFS.Image import Image
from Globals import DTMLFile
from OFS.Folder import Folder
from OFS.SimpleItem import SimpleItem
from AccessControl import ClassSecurityInfo
from AccessControl.User import UserFolder
from Globals import InitializeClass
from Globals import DTMLFile
import Globals
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate,manage_addPageTemplate
from Globals import Persistent, package_home
from Acquisition import Implicit
from Products.ZCatalog.CatalogPathAwareness import CatalogAware
from Products.ZCTextIndex.ZCTextIndex import manage_addLexicon
import ECHO_helpers
from Acquisition import aq_parent
try:
    from Products.MetaDataProvider.MetaDataClient import MetaDataClient
except:
    print "no metadataclient"
import urllib
import urllib2
import cgi
import smtplib
import time
#from Ft.Xml.Domlette import NonvalidatingReader
#from Ft.Xml.Domlette import PrettyPrint, Print
#from Ft.Xml import EMPTY_NAMESPACE
#
#import Ft.Xml.XPath
import cStringIO

import sys
import logging

try:
        from psycopg import libpq
except:
        try:
                from pyPgSQL import libpq
        except:
                print "ECHO_collection: Warning - No libpq imported!"
                
import xml.dom.minidom

import urllib
import xml.dom.minidom
import ECHO_helpers
from ECHO_helpers import *



#import vlp_xmlhelpers #TODO: vlp elemente aus echo herausnehmen
import xmlrpclib

import logging

#ersetzt logging.info
def logger(txt,method,txt2):
    """logging"""
    logging.info(txt+ txt2)




class ECHO_root(Folder,Persistent,Implicit):
    """ECHO Root Folder"""

    security=ClassSecurityInfo()
    
    meta_type="ECHO_root"
    management_page_charset="utf-8"
    
    getSubCols = ECHO_helpers.getSubCols

    manage_options=Folder.manage_options+(              
                {'label':'Main Config','action':'mainConfig_html'},
                {'label':'Change Weights','action':'changeWeights'},
                {'label':'Generate from RDF','action':'generateFromRDFForm'},
                {'label':'update Resource Catalog','action':'updateResourceCatalog'},
               )


    def mainConfig_html(self):
        """change form"""
        pt=zptFile(self, 'zpt/ChangeECHO_root.zpt')
        pt.content_type="text/html"
        return pt()
  
  
    def getMetaDataManagerName(self):
        """name of the metadatamanager"""
        return getattr(self,"metaDataManagerName","metadata");
        
    
    def changeECHO_root(self,metaDataManagerName,REQUEST=None):
        """change the main properties"""
        self.metaDataManagerName=metaDataManagerName
        
        if self.getMetaDataManager() is None:
            return """"<html><body>ERROR: Folder: %s does not exist.
            </body></html>
            """%metaDataManagerName
        
        if REQUEST is not None:
             REQUEST.RESPONSE.redirect('manage_main')
        else:
            return ""
        
        
    def getMetaDataManager(self):
         """hole eine Instanz von MetaDataFolder       
         """
    
         mf= getattr(self,self.getMetaDataManagerName(),None)
        
         return mf
         

    def getECHORootURL(self):
        return self.absolute_url()
    
    def getECHORoot(self):
        return self

    def getBrowserType(self):
        """returns browserType object"""
        return browserType(self)
    
    def mod_re_sub(self,pattern,replace,string):
        """re.sub aus mod re zur Verfuegung stellen"""
        return re.sub(pattern,replace,string)
    
    def findObjectFromFulltext(self,existUri):
        '''
        
        @param url:
        @param existUri:
        '''

        if existUri:
            #TODO: at the moment assume that the filename is unique, the whole has to changed to a system of ids.
            filename=existUri.split("/")[-1]
        
        founds=self.resourceCatalog.search({'fullTextUrl':os.path.splitext(filename)[0]})  
        
        ret=[(found.title,found.getObject().absolute_url(),found.getObject().aq_parent.absolute_url()) for found in founds]
        
        return ret
        
    def reindex(self,RESPONSE=None):
        """indiziere alle Objecte neu"""
        
        if RESPONSE:
            RESPONSE.write("<html><body>")
        
        resources=self.ZopeFind(self,obj_metatypes=["ECHO_resource"], search_sub=1)
        
        for resource in resources:
            resource[1].reindex()
            #print "<p> done %s </p>\n"
            if RESPONSE:
                RESPONSE.write("<p> done %s </p>\n"%resource[0])
                
        if RESPONSE:
            RESPONSE.write("<p> ok </p></html></body>\n")
            

    def setLanguage(self,lang):
            """Set language cookie"""
            self.REQUEST.RESPONSE.setCookie('lang_exhibition',lang,path="/")

    def switchLanguage(self):
            """Set language cookie"""
            if self.getLanguage()=="en":
                    lang="de"
            else:
                    lang="en"
            
            self.REQUEST.RESPONSE.setCookie('lang_exhibition',lang,path="/")
            self.REQUEST.RESPONSE.redirect(self.REQUEST['URL1'])
            
    def getLanguage(self):
            """get language cookie"""
            lang= self.REQUEST.cookies.get('lang_exhibition','de')
            if lang == '':
                    lang="de"
            return lang
    
    def getContentOverviewTemplate(self):
        """produces overview template with macro"""
        pt = zptObjectOrFile(self, 'content_overview_template')
        return pt

    def mapstyle_css(self):
        """send mapstyle.css"""
        sendFile(self, 'html/ECHO_mapstyle.css', 'text/css')


    ###Cdli adds -> have to be removed
    def getTablet(self,item):
        #print "getTablet"
        try:
                read=urllib.urlopen("http://enlil.museum.upenn.edu/cgi-bin/cdlget.plx?item=%s&project=ncdl"%item).read()
                read=re.sub("\[search\]","search",read)
                return read[read.find("<body>")+6:read.rfind("</body>")]
        except:
                return "<h1>Sorry no connection to the data server enlil.museum.upenn.edu</h1>"
        #return "http://enlil.museum.upenn.edu/cgi-bin/cdlget.plx?item=%s&project=ncdl"
    ###END CDLI add


    def URLquote(self, text):
        """urllib.quote fuer Michael"""
        return urllib.quote(text)


    def checkResource(self,id):
            """checks if a resource is in the tree, gives back none or list of resources"""
            if not id:
                id=""
            splitted=id.split("/")
            id=splitted[len(splitted)-1]
            if hasattr(self,"_v_checkResource") and self._v_checkResource.has_key(id): #existiert ein cache und id ist bereits drin?
                            return self._v_checkResource[id]

            else:
                    resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource'],obj_ids=[id],search_sub=1)

                    if not hasattr(self,"_v_checkResource"): self._v_checkResource={}#lege cache an falls nicht existent
                    if resources:
                            self._v_checkResource[id]=resources[0:] # slicing to be sure that data is stabil
                    else:
                            self._v_checkResource[id]=None
                    
                    return self._v_checkResource[id]
            
    def sendForm(self,fromaddr,content,server='mail.mpiwg-berlin.mpg.de'):
            """sendform"""
            toaddrs=["dwinter@mpiwg-berlin.mpg.de"]
            
            msg = ("From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n"
                   % (fromaddr, ", ".join(toaddrs),"testsub"))
            server = smtplib.SMTP(server)
            #server.set_debuglevel(1)
            msg=msg+content
            server.sendmail(fromaddr, toaddrs, msg)
            server.quit()
                 
    def generateFromRDFForm(self):
                """change form"""
                pt=zptFile(self, 'zpt/generateFromRDFForm')
                pt.content_type="text/html"
                return pt()

    def generateFromRDF(self,file,startNode="/Cuneiform Corpus"):

            """generate from RDF"""

            global seqs
            seqs={}
            global descrs
            descrs={}
            global key
            key=""
            global value
            value=""

            def getEdges(seqs,urn):
                    """edges"""
                    ret=[]
                    return seqs[urn]

            def createSubs(self,seqs,descrs,urn,level=0):
                    """create subs"""
                    for edge in getEdges(seqs,urn):
                            cn=createNode(self,descrs,edge)
                            if cn[0]=="CDLI_group":
                                    createSubs(cn[1],seqs,descrs,cn[2],level+1)
                    return

            def start_element(name,attrs):

                    global seqs
                    global descrs
                    global key
                    global value
                    seq=""
                    if name=="rdf:Seq":
                            key=attrs.get('rdf:about')
                            try: # teste ob liste
                                    x=seqs[key][0]
                            except:

                                    seqs[key]=[]

                    
                    elif name=="rdf:Description":
                            key=attrs.get('rdf:about')
                            

                    elif name=="rdf:li":
                            name=attrs.get('rdf:resource')
                            seqs[key].append(name)

                    elif name=="echonavigation:type":
                            value="type"

                    elif name=="echonavigation:name":
                            value="name"
                    elif name=="echonavigation:linkClickable":
                            value="linkClickable"
                            
            def end_element(name):
                            """nothing"""
                            key=""
                            value=""

            def char_data(data):
                    """nothing"""

                    data=re.sub("\n","",data)
                    try:
                            if descrs[key].has_key(value):
                                    descrs[key][value]+=data
                            else:
                                    descrs[key][value]=data
                    except:

                            descrs[key]={}
                            descrs[key][value]=data
                            
            p = xml.parsers.expat.ParserCreate()
            
            p.StartElementHandler = start_element
            p.EndElementHandler = end_element
            p.CharacterDataHandler = char_data
            
            
            p.ParseFile(file)
            self.REQUEST.RESPONSE.write("<html><body><h1>Start</h1>")
            createSubs(self,seqs,descrs,startNode)
            self.REQUEST.RESPONSE.write("<h1>done</h1></body></html>")
            #print "done"

            
            return "done"
    
            

                
    def changeWeightsInCollection(self):
            """change all lables of a collection"""
            ret=""
            argv=self.REQUEST.form
            
            resources=self.ZopeFind(self,obj_metatypes=['ECHO_resource','ECHO_collection','ECHO_link','ECHO_externalLink'])
            for resource in resources:
                   
                    try:
                            ret+=resource[1].getId()+"   "+argv[resource[1].getId()]+"</br>"
                            resource[1].weight=argv[resource[1].getId()][0:]
                    except:
                            pass
            return ret

    def changeWeights(self):
                """change form"""
                pt=zptFile(self, 'zpt/changeWeightForm')
                pt.content_type="text/html"
                return pt()
  
    
    def getRDF(self,urn=None):
            """rdf of the collection"""

            contents=self.ZopeFind(self,obj_metatypes=['ECHO_group','ECHO_resource','ECHO_collection'])

            ret=getRDFDescription(self,self.absolute_url(),urn=urn)
            
            li="""<rdf:li rdf:resource="%s" />\n"""

            
            for content in contents:
                    ret+=content[1].getRDF()+"\n"
                    
            ret+="""<rdf:Seq rdf:about="%s">\n"""%urn
            for content in contents:
                    nurn=content[1].absolute_url()
                    ret+=li%nurn
            return ret+"</rdf:Seq>"
            

    def showContent(self,path):
            """return content/html"""
            
            return ECHO_helpers.urlopen(path+"/content_html").read()
    
    

    def getBibTag(self,tag,content):
            """get field tag for index-meta-generation"""
            if not content or content=="":
                    return ""
            ret="<%s>"%tag
            #ret+=urllib.quote(content)
            ret+=content

            ret+="</%s>"%tag
            return ret

    def getValueFromClass(self,field,found):
            """ret attribute if existing"""
            try:
                    

                    return getattr(found,field)#.decode('ascii','ignore')



            except:
                    logging.error("can't: decode: %s"%repr(field))
                    logging.error("      %s %s"%(sys.exc_info()[0],sys.exc_info()[1]))
                    return ""

    security.declarePublic('getImageTag')
    def getImageTag(self):
            """needed by main_template"""
            return ""
        
    secondaryLink="" #needed by main_template
    secondaryLinkTitle="" #needed by main_template
    
    def getBgcolour(self):
        """hack"""
        return "#dddddd"

    security.declareProtected('View','contentTypeSelector_HTML')
    def contentTypeSelector_HTML(self,selected=None):
        """give type selector"""
        if not selected:
            retStr="<option selected>\n"
        else:
            retStr="<option>\n"
            
        try: # erste version contentTypes exists
            for contentType in self.ZopeFind(self.contentTypes,obj_metatypes=["ECHO_contentType"]):
                if selected and (contentType[0]==selected):
                    retStr+="""<option selected value="%s">%s\n"""%(contentType[0],contentType[0])
                else:                
                    retStr+="""<option value="%s">%s\n"""%(contentType[0],contentType[0])
        except:
                
                """nothing"""
                            
        return retStr

    def renderingTypeSelector_HTML(self,selected=None):
         """give type selector"""
         if not selected:
                 retStr="<option selected>\n"
         else:
                 retStr="<option>\n"
                 
         try: # erste version renderingTypes exists
                 for renderingType in self.ZopeFind(self.renderingTypes,obj_metatypes=["ECHO_renderingType"]):
                         if selected and (renderingType[0]==selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(renderingType[0],renderingType[0])
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(renderingType[0],renderingType[0])
         except:
                 """nothing"""
         return retStr


    def copyrightTypeSelector_HTML(self, object=None, selected=None,first=None):
         """give type selector"""

         if not first:
                 if not selected:
                         retStr="<option selected>\n"
                 else:
                         retStr="<option>\n"
         else:
                 if not selected:
                         retStr="""<option selected value="%s">%s\n"""%first
                 else:
                         retStr="""<option value="%s">%s\n"""%first

         
         try: # erste version copyrightTypes exists
                 for copyrightType in self.ZopeFind(self.copyrightTypes,obj_metatypes=["ECHO_copyrightType"]):
                         if selected and (copyrightType[0]==selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(copyrightType[0],copyrightType[0])
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(copyrightType[0],copyrightType[0])
                 
                 for copyrightTypeSelf in self.ZopeFind(object,obj_metatypes=["ECHO_copyrightType"],search_sub=1):
                         if selected and (copyrightTypeSelf[0]==selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(copyrightTypeSelf[0],copyrightTypeSelf[0])
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(copyrightTypeSelf[0],copyrightTypeSelf[0])
                                 
         except:
             """nothing"""
                 
         return retStr

    def partnerSelector_HTML(self,selected=None):
         """give type selector"""
         if not selected:
                 retStr="<option selected>\n"
         else:
                 retStr="<option>\n"
                 
         try: # erste version copyrightTypes exists
                 for copyrightType in self.ZopeFind(self.partners,obj_metatypes=["ECHO_partner"]):
                         if selected and (copyrightType[0]==selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(copyrightType[0],copyrightType[0])
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(copyrightType[0],copyrightType[0])
         except:
                 """nothing"""
         return retStr
 
    mediaTypes=['all','text','images','sound','video'] # konfigurierbar in neuer version ueber echoroot??

    def mediaTypeSelector_HTML(self,selected=None):
         """give type selector"""
         if not selected:
                 retStr="<option selected>\n"
         else:
                 retStr="<option>\n"
         
         try: # erste version mediatypesTypes exists
                 for mediaType in self.mediaTypes:
                         if selected and (mediaType in selected):
                                 retStr+="""<option selected value="%s">%s\n"""%(mediaType,mediaType)
                         else:                
                                 retStr+="""<option value="%s">%s\n"""%(mediaType,mediaType)
         except:
                 """nothing"""
         return retStr

            
    def patchContentType(self,obj=None):
        """austauschen content_type with contentType (patch bei umstieg von alter Version)"""
    

        if not obj:
            obj = self
            
        entries=obj.ZopeFind(obj,obj_metatypes=['ECHO_resource','ECHO_collection','ECHO_externalLink','ECHO_pageTemplate'])

        for entry in entries:
                setattr(entry[1],'contentType',entry[1].content_type)
                #entry[1].contentType == entry[1].content_type

                if entry[1].meta_type == 'ECHO_collection':
                    entry[1].patchContentType(entry[1])    

                
        return "changed all contenttypes in: "+self.title


    def repairAllCoords(self):
        """updates map coordinates on the same and sublevels"""
        return repairCoords(self)


    def patchViewClassification(self,obj=None):
        """setze viewClassification heuristisch"""

        def checkIfArrow(obj):
                if hasattr(obj,'coords'):
                        for coordtemp in obj.coords:

                                if (len(coordtemp)>4) and not (coordtemp[4]==''):
                                        return 4
                        return None
                return None
        
        if not obj:
            obj = self
            
        entries=obj.ZopeFind(obj,obj_metatypes=['ECHO_resource','ECHO_collection','ECHO_group'])

        for entry in entries:
                
                if checkIfArrow(entry[1]):
                        setattr(entry[1],'viewClassification','view point')
                else:
                        setattr(entry[1],'viewClassification','area')

                #entry[1].contentType == entry[1].content_type

                if entry[1].meta_type in ['ECHO_collection','ECHO_group']:
                    entry[1].patchViewClassification(entry[1])    

                
        return "changed all contenttypes in: "+self.title

    def deleteCache(self,obj=None,RESPONSE=None):
        """setze alle collections auf cache = CacheManager"""
        if not obj:
            obj = self
        entries=obj.ZopeFind(obj,search_sub=1)
        for entry in entries:
                if hasattr(entry[1],'_v_hash'):
                        entry[1]._v_hash=None
        
        return "changed all CM in: "+self.title

    
    security.declarePublic('ECHO_newViewerLink')
    def ECHO_newViewerLink(self,obj=None):
        """change links (:86 faellt weg)"""

        if not obj:
            obj = self
            
        entries=obj.ZopeFind(obj,obj_metatypes=['ECHO_resource','ECHO_collection'])

        for entry in entries:
                
                if entry[1].meta_type == 'ECHO_resource':
                    
                    entry[1].link=re.sub('\:86','',entry[1].link)

                else:
                    
                    entry[1].ECHO_newViewerLink(entry[1])
                
        return "Rerenderd all links to resources in: "+self.title

    def __init__(self,id,title):
        """init"""
        self.id = id
        self.title=title

    def deleteSpace(self,str):
        """delete space at the end of a line"""
        if str[len(str)-1]==" ":
            return str[0:len(str)-1]
        else:
            return str
        
    

    # zusaetliche methoden fuer das vlp muessen in ein eigenes produkt

    def formatAscii(self,str,url=None):
        """ersetze ascii umbrueche durch <br>"""

        if not str: 
                return ""
    
        if url:
            
            retStr=""
            words=str.split("\n")
            
            for word in words:
                strUrl=url%word
    
                retStr+="""<a href="%s">%s</a><br/>"""%(strUrl,word)
            str=retStr
        if str:
            str = re.sub(r"[\n]","<br/>",str)
            return unicodify(str)
        else:
            return u""
            
    #link2html=vlp_xmlhelpers.link2html
    #related2html=vlp_xmlhelpers.related2html

    #xml2html=vlp_xmlhelpers.xml2html
 
    #checkRef=vlp_xmlhelpers.checkRef
    
#    def checkRef(self,ref):
#            if ref[0:3]=='lit':
#                    if len(self.library_data({ 'id':ref}))>0:
#                            return 1
#            try:
#                    if ref[0:7]=="tec_cat":
#                            return 1
#            except:
#                    """nothing"""
#                    
#            dbs={'vl_technology':'','vl_people':" AND complete='yes'",'vl_sites':''}
#            res=None
#            for db in dbs.keys():
#                    res=res or self.search(var=str("select reference from %s where reference =\'%s\' %s"%(db,ref,dbs[db])))
#            return res
#                                    
#    #Ende Methode fuer vlp

    def PgQuoteString(self,string):
        """Quote string"""

        return libpq.PgQuoteString(string)

    security.declareProtected('View','getPartners')
    def getPartners(self):
        """Get list of Partners. Presently only from a subfolder partners"""
        if hasattr(self,'partners'):            
                ret = [ item[1] for item in self.partners.ZopeFind(self.partners,obj_metatypes=['ECHO_partner'])]
                def sort_title(one, another) : return cmp(one.title, another.title)
                ret.sort(sort_title)
                return ret
        else:
                return []
  
    def getInstitutions(self):
        """Get list of Partners. Presently only from a subfolder partners"""
                    
        return [ item[1] for item in self.institutions.ZopeFind(self.institutions,obj_metatypes=['ECHO_institution'])]

   
    def getPartnersXML(self):
        """partner liste als xml""" 
        partners=self.getPartners()
        ret="""<?xml version="1.0" encoding="utf-8" ?>
        <partners>"""
        
        for partner in partners:
            ret+="""<partner id="%s" title="%s"/>\n"""%(partner.getId(),partner.title)

        return ret+"\n</partners>"
    
    def getCollectionTree(self):
        """get the collection tree (list of triples (parent,child, depth)"""

        def getCollection(object,depth=0):
            depth+=1
            collections=[]
            for entry in object.__dict__.keys():
                element=getattr(object,entry)
                try:
                    if element.meta_type=="ECHO_collection":
                        collections.append((object,element,depth))
                        collections+=getCollection(element,depth)
                except:
                    """nothing"""
            return collections
        

        return getCollection(self)
    
    def getCollectionTreeIds(self):
        """Show the IDs of the Tree"""
        ret=[]
        for collection in self.getCollectionTree():
            ret.append((collection[0].getId(),collection[1].getId(),collection[2]))
        return ret


    def exportAllCollectionsAsJSON(self,REQUEST=None,exportTypes=['ECHO_resource']):
        "export all collections"
        import json
        collections = self.ZopeFind(self,obj_metatypes=['ECHO_collection','ECHO_group'],search_sub=1)
        #collections = self.ZopeFind(self,obj_metatypes=['ECHO_group'],search_sub=1)
        
        ret={}
        for collection in collections:
            colId = collection[1].absolute_url().replace(self.absolute_url(),'')
            ret[colId]=collection[1].getExport(metatypes=exportTypes);
          
        if REQUEST:
                REQUEST.response.setHeader("Content-type", "text/json; charset=utf-8")
         
        return json.dumps(ret)
    
    def exportAllCollectionsAndSubCollectionAsJSON(self,REQUEST):
        "exportiere alle collectionen und subcolletionen, export nicht die inhalte der collectionen sondern nur die hierarchie"
        return  self.exportAllCollectionsAsJSON(REQUEST=None,exportTypes=['ECHO_collection','ECHO_group'])
      

        
    def getResourcesHTML(self,viewerType=None,filter=None):
            """gebe all ressourcen aus"""

            def sortHTML(x,y):
                    return cmp(x[1].title,y[1].title)
            
            ret="""<html><body><h2>Resources in ECHO</h3>"""
            
          
            resources = self.ZopeFind(self,obj_metatypes=['ECHO_resource','VLP_resource'],search_sub=1)
            ret+="""<h3>Found %i resources</h3>"""%len(resources)
            resources.sort(sortHTML)
            for resource in resources:
                    echo_url=resource[1].absolute_url()
                    
                    if hasattr(resource[1],'title'):
                            title=resource[1].title
                    else:
                            title="None"
                    if filter:
                            if re.search(filter,title):
                                    ret+="""\n<p><a href="%s">%s</a></p>"""%(echo_url,title)
                    else:
                            ret+="""\n<p><a href="%s">%s</a></p>"""%(echo_url,title)

            ret +="""\n</body></html>"""
            
            #self.REQUEST.RESPONSE.setHeader("Content-Type", "text/html")
            #self.REQUEST.RESPONSE.write(ret)
            return ret
    
    def getResourcesXML(self,viewerType=None,filter=None):
            """gebe all ressourcen aus"""
            ret="""<?xml version="1.0" ?>
                     <index>"""
            for resource in self.ZopeFind(self,obj_metatypes=['ECHO_resource','VLP_resource'],search_sub=1):

                    echo_url=resource[1].absolute_url()
                    if hasattr(resource[1],'link'):
                            viewer_url=resource[1].link
                    else:
                            viewer_url="NO URL"
                    if filter:
                            if re.search(filter,viewer_url):
                                    ret+="""\n<resource echoLink="%s" viewerLink="%s"/>"""%(urllib.quote(echo_url,safe='/:?'),urllib.quote(viewer_url,safe='/:?'))
                    else:
                            ret+="""\n<resource echoLink="%s" viewerLink="%s"/>"""%(urllib.quote(echo_url,safe='/:?'),urllib.quote(viewer_url,safe='/:?'))
            ret +="""\n</index>"""
            
            self.REQUEST.RESPONSE.setHeader("Content-Type", "text/xml")
            self.REQUEST.RESPONSE.write(ret)

    def getCollectionsXML(self,viewerType=None,filter=None):
            """gebe collections aus"""
            ret="""<?xml version="1.0" ?>
                     <index>"""
            for resource in self.ZopeFind(self,obj_metatypes=['ECHO_collection','ECHO_group'],search_sub=1):

                    echo_url=resource[1].absolute_url()
                 
                    ret+="""\n<collection echoLink="%s"/>"""%urllib.quote(echo_url,safe='/:?')
            ret +="""\n</index>"""
            
            self.REQUEST.RESPONSE.setHeader("Content-Type", "text/xml")
            self.REQUEST.RESPONSE.write(ret)

    def getFullTextsXML(self,viewerType=None,filter=None):
            """gebe all ressourcen aus"""
            ret="""<?xml version="1.0" ?>
                     <index>"""
            for resource in self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1):

                    echo_url=resource[1].absolute_url()
                    if resource[1].getFullTextXML(noredirect="yes"):
                            if hasattr(resource[1],'link'):
                                    viewer_url=echo_url+"/getFullTextXML"
                            else:
                                    viewer_url="NO URL"
                            if filter:
                                    if re.search(filter,viewer_url):
                                            ret+="""\n<resource echoLink="%s" viewerLink="%s"/>"""%(urllib.quote(echo_url,safe='/:?'),urllib.quote(viewer_url,safe='/:?'))
                            else:
                                    ret+="""\n<resource echoLink="%s" viewerLink="%s"/>"""%(urllib.quote(echo_url,safe='/:?'),urllib.quote(viewer_url,safe='/:?'))
            ret +="""\n</index>"""
            
            
            self.REQUEST.RESPONSE.setHeader("Content-Type", "text/xml")
            self.REQUEST.RESPONSE.write(ret)


    def getMetaDatasXML(self,viewerType=None,filter=None):
            """gebe all ressourcen aus"""
            # check if the request's host part was OK
            http_host = self.REQUEST['HTTP_HOST']
            host_port = self.REQUEST['SERVER_PORT']
            fix_host = None
            if http_host and http_host.rfind(host_port) == -1:
                    print "HTTP_HOST needs fixing!"
                    fix_host = http_host + ":" + host_port

            ret="""<?xml version="1.0" ?>
                     <index>"""
            for resource in self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1):

                    echo_url=resource[1].absolute_url()
                    if fix_host:
                            #print "replacing ", http_host, " by ", fix_host
                            echo_url = string.replace(echo_url, http_host, fix_host, 1)
                    
                    if hasattr(resource[1],'link'):
                            meta_url=echo_url+"/getMetaDataXML"
                    else:
                            meta_url="NO_URL"
                            
                    if filter and not re.search(filter,viewer_url):
                            continue

                    #modificationDate=time.strptime(str(resource[1].bobobase_modification_time()),"%Y/%m/%d %H:%M:%S %Z")
                    
                    modificationDate=resource[1].bobobase_modification_time().strftime("%Y/%m/%d %H:%M:%S")

                    ret+="""\n<resource resourceLink="%s" metaLink="%s" modificationDate="%s"/>"""%(urllib.quote(echo_url,safe='/:?'),urllib.quote(meta_url,safe='/:?'),modificationDate)
                    
                            
            ret +="""\n</index>"""

            self.REQUEST.RESPONSE.setHeader("Content-Type", "text/xml")
            self.REQUEST.RESPONSE.write(ret)

            
    def findPartners(self):     
        """find all partners in partners"""
        par=self.ZopeFind(self.partners, obj_metatypes='ECHO_partner')
        return par
        
    def getPartnerFromID(self):
        """find partner form ID return object"""
        pa=self.REQUEST['partner']
        par=self.ZopeFind(self.partners, obj_ids=[pa])
        return par
        
    def getPartnerFromIDParameter(self, id):
        """ find partners from ID"""
        par=self.ZopeFind(self.partners, obj_ids=[id])
        return par
        
    def getInstitutionFromID(self,id):
        """ find institution from id """
        inst=self.ZopeFind(self.institutions, obj_ids=[id])
        return inst

    def updateResourceCatalog(self,REQUEST,RESPONSE):
        """update ressource catalog"""
        catalog=getattr(self,'resourceCatalog',None)
        if not catalog:
            return """<html><body><h3>No resourceCatalog available</h3></body></html>"""
        
        else:
            class element:
                def __init__(self,group,name):
                    self.group=group
                    self.name=name
            catalog.manage_delObjects('author splitter')
            #elements=[element('Stop Words',""" Don't remove stop words' """),element('Word Splitter','ECHO author splitter')]
            elements=[element('Word Splitter','ECHO author splitter')]
            manage_addLexicon(catalog,id="author splitter",elements=elements)
            
            return catalog.manage_catalogReindex(REQUEST,RESPONSE,'manage_main')
            
        
    def checkMetaDataLinks(self,correct=None):
        """check all metadatalinks and if correct is set change the servername to content.mpiwg-berlin.mpg.de"""
        
        self.REQUEST.RESPONSE.write("<html><body>")
        for resource in self.ZopeFind(self,obj_metatypes=['ECHO_resource'],search_sub=1):
            metalink=resource[1].metalink
        
            try:
                url=urllib.urlopen(metalink)
            except:
                
                self.REQUEST.RESPONSE.write("<p>ERROR (opening %s):"%resource[1].absolute_url() +" -- %s %s <br>"%sys.exc_info()[0:2])
                self.REQUEST.RESPONSE.write("-----------%s"%metalink)
            
                if correct:
                    urlparsed=[x for x in urlparse.urlparse(metalink)]
                    urlparsed[1]="content.mpiwg-berlin.mpg.de"
                    neu=urlparse.urlunparse(urlparsed)
                    resource[1].metalink=neu[0:]
                    self.REQUEST.RESPONSE.write("----------changed to:%s)"%neu)
   
   
   
def manage_addECHO_root(self,id,title,RESPONSE=None):
    """Add an ECHO_root"""
    self._setObject(id,ECHO_root(id,title))
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

def manage_addECHO_rootForm(self):
        """Nothing yet"""
        pt=zptFile(self, 'zpt/AddECHO_root.zpt')
        return pt()
    
    
    
from ECHO_Nav import ECHO_navigation

class ECHO_main(ECHO_root,ECHO_navigation):
    """echo main object combines ECHO_root and ECHO_navigation"""
    
    meta_type="ECHO_main"
    
    def __init__(self,id,title):
        """init"""
        self.id=id
        self.title=title
        self.pathToHierarchy="/"+self.id # hierarchie always starts with the object.
        self.displayedMetaTypes=displayTypes
        
        
    manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'ECHO_navigationConfigForm'},
        {'label':'Cache','action':'ECHO_cacheManageForm'},
        {'label':'Main Config','action':'ECHO_copyright_configForm'},
        {'label':'Change Weights','action':'changeWeights'},
        {'label':'Generate from RDF','action':'generateFromRDFForm'},
        {'label':'update Resource Catalog','action':'updateResourceCatalog'},
        {'label':'Copy MD for indexing and search','action':'copySearchFields'},
        )

    
def manage_addECHO_mainForm(self):
    """Form for adding"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','AddECHO_main.zpt')).__of__(self)
    return pt()

def manage_addECHO_main(self,id,title,RESPONSE=None):
    """Add an ECHO_main"""
    self._setObject(id,ECHO_main(id,title))
    
    obj=self._getOb(id)
    text=file(os.path.join(package_home(globals()),'zpt','main_template')).read()
    
    manage_addPageTemplate(obj,'main_template',text=text)               
    
  
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')

    
                             
