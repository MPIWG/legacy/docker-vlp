import re
import string
import socket
import urllib
import string
import xml.dom.minidom
import types
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate
from OFS.SimpleItem import SimpleItem
from Globals import package_home
import Globals
from AccessControl import ClassSecurityInfo
from Acquisition import aq_parent,aq_chain,aq_base
import os.path
from OFS.Folder import Folder
import ECHO_collection
import base64
import bz2
import xmlrpclib
import sys
import logging
#import amara

#erstzt logging
def logger(txt,method,txt2):
    """logging"""
    logging.info(txt+ txt2)

displayTypes = ['ZSQLExtendFolder','ZSQLBibliography','ECHO_group','ECHO_collection','ECHO_resource','ECHO_link','ECHO_sqlElement','ECHO_pageTemplate','ECHO_externalLink','ImageCollectionIFrame','VLP_resource','VLP_essay','ECHO_ZCatalogElement','ImageCollection','versionedFileFolder','extVersionedFileFolder','ECHO_movie']

def normalizeCt(str):
        """normalizes content_type"""
        #str= str.replace(" ","_")
        return str.replace("-"," ").lower()

#decode and compress for xmlrpc communication 

def encodeRPC(string):
    return base64.encodestring(bz2.compress(string))
def decodeRPC(string):
    return bz2.decompress(base64.decodestring(string))

def unicodify(s):
    """decode str (utf-8 or latin-1 representation) into unicode object"""
    if not s:
        return u""
    if isinstance(s, str):
        try:
            return s.decode('utf-8')
        except:
            return s.decode('latin-1')
    else:
        return s

def utf8ify(s):
    """encode unicode object or string into byte string in utf-8 representation.
       assumes string objects to be utf-8"""
    if not s:
        return ""
    if isinstance(s, str):
        return s
    else:
        return s.encode('utf-8')


def content_html(self,type):
        """template fuer content"""
        #templates = self.ZopeFind(self.aq_parent,obj_ids=[type+"_template"])
        #
        #if templates:
        #    return templates[0][1]()

        if hasattr(self,type+"_template"):

            obj=getattr(self,type+"_template")
            return obj()
        else:
            pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','ECHO_%s_template_standard.zpt'%type)).__of__(self)
            pt.content_type="text/html"
            return pt()
    
    

class ECHO_basis:
    """basis eigenschaften fuer echo objekte"""
    security=ClassSecurityInfo()
    security.declarePublic('getImageTag')
    
    management_page_charset="utf-8"
    isVisible = True
    
    # Managment for the PID
    def setPID(self,pid):
        """set the pid"""
        logging.debug(self.getId()+" PID set to "+pid)
        self.pid=pid
        return True
        
    def getPID(self):
        """get the pid"""

        pid =getattr(self,'pid',None)

        if pid == self.getParentPID():
            return None
        else:
            return pid

        
    def getParentPID(self):
        """get the PID of the parent"""
        parent = self.aq_parent
        
        pidF = getattr(parent,'getPID',None)
        
        if pidF is None:
            return None
            
        else:
            return pidF()

    def unicodify(self, s):
        """return unicode object for string (utf-8 or latin1) or unicode object s"""
        return unicodify(s)
    
    # compatibility of old decode method
    decode = unicodify


    def utf8ify(self, s):
        """return utf-8 encoded string object for string or unicode object s"""
        return utf8ify(s)

    def getBrowserType(self):
        """returns browserType object"""
        return browserType(self)
    
    def manage_addECHO_locale(self,lang,title,label,text=None,content_type=None,RESPONSE=None):
            return ECHO_collection.manage_addECHO_locale(self,lang,title,label)
            
    def getImageTag(self):
            """sollte uerberschrieben werden, falls von der Klasse eine imagetag zurueckkommt"""
            
            return ""
    
    def showRDF(self):
            """showrdf"""
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
            ret="""<?xml version="1.0" encoding="utf-8"?>\n<rdf:rdf xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"  xmlns:echonavigation="http://www.echo.eu/rdf#"
            xmlns:MPIWG="http://www.mpiwg-berlin.mpg.de/ns/mpiwg">\n"""

            ret+=self.getRDF(urn="echo:collectionroot")+"\n"


            ret+="""</rdf:rdf>"""
            return ret 

  
    def RDF(self):
            """showrdf"""
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
            ret="""<?xml version="1.0" encoding="utf-8"?>\n<rdf:rdf xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"  xmlns:echonavigation="http://www.echo.eu/rdf#">\n"""

            ret+=self.getRDF(urn=self.absolute_url())+"\n"


            ret+="""</rdf:rdf>"""
            return ret 

  
    def createSubElementRDF(self,urn=None):
            """rdf list"""
            if not urn:
                    urn=self.absolute_url()
            ret=""

            rettemp="""<rdf:Seq rdf:about="%s">\n"""%urn
            flag=0
            
            li="""<rdf:li rdf:resource="%s" />\n"""
            if not ('<error>' in self.getFullTextXML(noredirect='Yes')):
                    nurn=self.absolute_url()+'/getFullTextXML'
                    rettemp+=li%nurn
                    flag=1
            if not ('<error>' in self.getImageView(noredirect='Yes')):
                    nurn=self.absolute_url()+'/getImageView'
                    rettemp+=li%nurn
                    flag=1


            if not ('<error>' in self.showMetaDataXML()):
                    nurn=self.absolute_url()+'/showMetaDataXML'
                    rettemp+=li%nurn
                    flag=1
                    
            rettemp+="</rdf:Seq>"

            if flag==1:
                    ret+=rettemp
                    
            if not ('<error>' in self.getFullTextXML(noredirect='Yes')):
                    nurn=self.absolute_url()+'/getFullTextXML'  
                    ret+=getRDFDescription(self,self.absolute_url()+'/getFullTextXML',urn=nurn,nameDef="Fulltext",typeName="ECHO_fulltext")

            if not ('<error>' in self.getImageView(noredirect='Yes')):
                    nurn=self.absolute_url()+'/getImageView'
                    ret+=getRDFDescription(self,self.absolute_url()+'/getImageView',urn=nurn,nameDef="Image View",typeName="ECHO_imageview")
                    
            if not ('<error>' in self.showMetaDataXML()):
                    nurn=self.absolute_url()+'/showMetaDataXML'
                    ret+=getRDFDescription(self,self.absolute_url()+'/showMetaDataXML',urn=nurn,nameDef="Metadata",typeName="ECHO_metaData")

       
            return ret
    def content_html_XMLRpc(self):
        """xml_rpc_version"""
        return encodeRPC(self.content_html())
    
    def content_html(self,type="collection"):
            """template fuer content bei einbau und ECHO_Umgebung"""
            
            return content_html(self,type)
    
    def getIsVisible(self):
        """return if object and all parents are visible"""
        
        # if object is not visible return False
        if hasattr(self,'isVisible'):
            if not self.isVisible : return False
            
        # else if parent of object is not ECHO_root, ECHO_basis or None
        if not self.aq_parent.meta_type in ['ECHO_root','ECHO_basis',None] :
            return self.aq_parent.getIsVisible()

        # else return True
        return True

    def setIsVisible(self, isVisible):
        """ set object attribute isVisible"""
        self.isVisible = isVisible

    def getAttributeIsVisible(self):
        """ return object attribute isVisible"""
        if hasattr(self,'isVisible'):
            return self.isVisible
        return True
    
    def getDescription(self):
        """get content of description field"""
        
      
        return self.unicodify(getattr(self,'description',''));
    
    
    def getTitle(self):
        """title"""
        if hasattr(self,'getLanguage'):
            lang=self.getLanguage()    
        else:
            lang=None

        locale=None
        if lang and not (lang =="de"):
                locale=self.ZopeFind(self,obj_ids=["locale_"+lang])

        if locale:
                return unicodify(locale[0][1].title)
                
        return unicodify(self.title)
        
    def getLabel(self):
        """title"""
        if hasattr(self,'getLanguage'):
            lang=self.getLanguage()    
        else:
            lang=None

        locale=None
                
        if lang and not (lang=="de"):
                locale=self.ZopeFind(self,obj_ids=["locale_"+lang])

        if locale:
                return self.decode(locale[0][1].label)
        

        if not hasattr(self,'label'):
                self.label=""
                
        if self.label=="":
                ret=self.getTitle()
                if ret=="":
                        ret=self.getId()
                return ret
        
        return unicodify(self.label)
      
    def changeECHOEntriesForm(self):
        """change Entries for the ECHO Navigation environment"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','changeECHOEntriesForm')).__of__(self)
        return pt()
    
    def changeECHOEntries(self,label,weight,description,queryString,RESPONSE=None):
        """change Entries for the ECHO Navigation environment
        @param label: label fuer die Navigation"""
        self.label=label
        self.weight=weight
        self.description=description
        self.queryString=queryString
        
        if RESPONSE:
            RESPONSE.redirect("manage_main")

    manage_options=({'label':'change ECHO Navigation Entries','action':'changeECHOEntriesForm'},)


    def hasOverview(self):
        """returns if there is an overview graphics"""
        return hasattr(self, 'overview')

    def hasParentOverview(self):
        """returns if the parent has an overview graphics"""
        return hasattr(self.aq_parent, 'overview')

    def getOverview(self):
        """map overview graphics"""
        #return self.aq_parent.ZopeFind(self.aq_parent,obj_ids=['overview'])[0][1]
        return getattr(self, 'overview', None)

    def getMapAreas(self):
        """returns the list of MapAreas"""
        forcetypes = self.REQUEST.get('map_force_types', None)
        if forcetypes:
            typelist = forcetypes.split(',')
        else:
            typelist = None
        areas = []
        # get all contained MapAreas
        for res in self.ZopeFind(self, obj_metatypes=('MapArea')):
            area = res[1]
            # check the allowed types
            if typelist is not None and area.getType() not in typelist:
                continue
            areas.append(area)
        return areas

    def addMapArea(self, newarea):
        """add a map area to this object"""
        if (newarea.id is None):
            # create new id
            ids = [a.id for a in self.getMapAreas()]
            i = len(ids)
            while ("a%02d"%i in ids):
                # if it exists, try the next one
                i += 1
            newarea.id = "a%02d"%i
        self._setObject(newarea.id, newarea)

    def getMapText(self):
        """returns the MapText"""
        # get (the first) contained MapText
        #lang="en"
        lang=self.getLanguage()
        res=None
        if lang and not (lang=="de"):
                resTmp = self.ZopeFind(self, obj_metatypes=('ECHO_mapText'))
                res = self.ZopeFind(resTmp[0][1])

        if not res:
                res = self.ZopeFind(self, obj_metatypes=('ECHO_mapText'))
        if len(res) > 0:
            text = res[0][1]
            return unicodify(text)
        return None

    def ECHO_graphicEntry(self):
        """change map coordinates"""
        if self.hasParentOverview():
            pt=zptFile(self, 'zpt/ECHO_draw.zpt')
            return pt()
        else:
            return "NO OVERVIEW GRAPHICS"

    def ECHO_addCoords(self,RESPONSE=None):
        """change or add MapArea"""
        #return self.REQUEST
        # change existing areas
        for area in self.getMapAreas():
            id = area.getId()
            if self.REQUEST.has_key('del.'+id):
                # delete this area
                self._delObject(id)
                # return to same menu
                if RESPONSE is not None:
                    RESPONSE.redirect('ECHO_graphicEntry')
                return
            # modify this area
            coordstring = self.REQUEST.get('coords.'+id, '')
            coords = string.split(coordstring, ',')
            angle = self.REQUEST.get('angle.'+id, '0')
            type = self.REQUEST.get('type.'+id, 'area')
            permanent = self.REQUEST.get('permanent.'+id, '')
            if len(coords) == 4:
                area.setCoordString(coordstring)
                area.setAngle(angle)
                area.setType(type)
                area.setPermanent(permanent)
        # add the "new" area
        if self.REQUEST.has_key('add'):
            coordstring = self.REQUEST.get('coords.new', '')
            coords = string.split(coordstring, ',')
            angle = self.REQUEST.get('angle.new', '0')
            type = self.REQUEST.get('type.new', 'area')
            permanent = self.REQUEST.get('permanent.new', '')
            if len(coords) == 4:
                coords.append(angle)
                area = MapArea(None, coords, type=type, permanent=permanent)
                self.addMapArea(area)
        # return to edit area menu
        if RESPONSE is not None:
            RESPONSE.redirect('ECHO_graphicEntry')

    def createJSAreas(self, areas):
        """create area calls for JavaScript"""
        forceperm = self.REQUEST.get('map_force_permanent', None)
        js="\n"
        for ob in areas:
            # check for force-permanent
            if forceperm is not None:
                perm = forceperm
            else:
                perm = ob.isPermanent()
            js+="""addArea('%s', 'overview', %s, '%s', '%s');\n"""%(ob.getFullId(),ob.getCoordString(),ob.getType(),perm)
        return js

    def createMapHead(self,mapColTypes=None):
        """create javascript include and script tags for head"""
        pt=zptFile(self, 'zpt/ECHO_content_map_frag_js')
        return pt(mapColTypes=mapColTypes)

    def createMapImg(self):
        """generate img-tag for map"""
        bt = browserType(self)
        tag = ""
        src = self.getOverview().absolute_url()
        if bt['isN4']:
            # N4 needs ilayer to create an id
            tag += '<ilayer id="overview" visibility="show"><img src="%s"></ilayer>'%src
        else:
            # img with id
            tag += '<img id="overview" src="%s" />'%src
        return tag
        
    def createParentMapImg(self):
        """generate img-tag for map of parent"""
        return self.aq_parent.createMapImg()

    def createMapLink(self, ob, text=None, target="_blank"):
        """generate map link"""
        bt = browserType(self)
        id = ob.getFullId()
        url = ob.getTargetUrl()
        if url == "":
            # ECHOResource with empty link -> no link
            href = 'href="#" onclick="return false;"'
        else:
            href = 'href="%s"'%ob.getLinkId()
        
        if target is None:
            targetattr = ""
        else:
            targetattr = 'target="%s"'%target
             
        if text is None:
            text = ob.getLabel()
            
        if text is None:
            text = "link"
            
        tiptext = ob.getTip()

        tag = ""
        if bt['isN4']:
            # N4 needs layer for highlighting
            tag += '<ilayer id="a.%s"><a class="maplink" onmouseover="highlightPair(\'%s\', true)" onmouseout="highlightPair(\'%s\', false)" %s %s'%(id,id,id,href,targetattr)
            tag += ">" + text + "</a></ilayer>"
        else:
            # a-element
            tag = '<a id="a.%s" class="maplink" onmouseover="highlightPair(\'%s\', true)" onmouseout="highlightPair(\'%s\', false)" %s %s'%(id,id,id,href,targetattr)
            if tiptext:
                tag += ' title="%s"'%tiptext

            logging.debug("createMapLink: text=%s"%repr(text))
            tag += ">%s</a>"%text

        return tag

    def createMapAux(self, ob, arrowsrc="http://digilib.mpiwg-berlin.mpg.de/digitallibrary/servlet/Scaler/?dw=15&fn=icons/pfeil", circlesrc="http://digilib.mpiwg-berlin.mpg.de/digitallibrary/servlet/Scaler/?dw=15&fn=icons/kreis", target="_blank",backLink=None,alternativArrowsrc="http://digilib.mpiwg-berlin.mpg.de/digitallibrary/servlet/Scaler/?dw=15&fn=icons/pfeil_blau"):
        """generate map link image, text and other stuff"""
        bt = browserType(self)
        id = ob.getFullId()
        link = ob.getLinkId()
        url = ob.getTargetUrl()
        if url == "":
            # ECHOResource with empty link -> no link
            href = 'href="#" onclick="return false;"'
        else:
            href = 'href="%s"'%ob.getLinkId()
        
        if target is None:
            targetattr = ""
        else:
            targetattr = 'target="%s"'%target
        tiptext = ob.getTip()

        tag = ""
        if bt['isN4']:
            #
            # N4 needs layer and img elements
            #
            tag += '<layer id="i.%s" onmouseover="highlightPair(\'%s\', true)" onmouseout="highlightPair(\'%s\', false)">'%(id,id,id)
            if ob.isTypeArrow():
                # N4 - Arrow
                rot = ob.angle
                
                if hasattr(getattr(self,link),'link'):
                        cLink=getattr(self,link).link
                        # if existing delete virtuell path 
                        cLink=cLink.replace('/'.join(self.REQUEST['VirtualRootPhysicalPath']),'')

                if backLink and hasattr(getattr(self,link),'link') and backLink==self.REQUEST['SERVER_URL']+cLink.lstrip():
                        marksrc=alternativArrowsrc
                else:
                        marksrc = arrowsrc

                
                if float(rot) < 0:
                    marksrc = circlesrc
                tag += '<a %s><img border="0" src="%s&rot=%s" /></a>'%(href,marksrc,rot)
            elif ob.isTypeText():
                # N4 - Text
                tag += '<div id="t.%s" class="maptext"'%(id)
                tag += 'style="visibility:hidden">'
                tag += ob.getText()
                tag += '</div>'
            else:
                # N4 - Area
                tag += '<a %s><img border="0" width="1000" height="1000" src="trans_img"'%(href)
                if tiptext:
                    tag += ' alt="%s"'%tiptext
                tag += ' /></a>'
            tag += '</layer>'
        else:
            #
            # (more or less) DOM capable browser
            #
            tag = '<a id="b.%s" class="mapmarklink" onmouseover="highlightPair(\'%s\', true)" onmouseout="highlightPair(\'%s\', false)" %s %s>'%(id,id,id,href,targetattr)
            if ob.isTypeArrow():
                # DOM - Arrow
                rot = ob.angle
                                
                if hasattr(getattr(self,link),'link'):
                        cLink=getattr(self,link).link
                        # if existing delete virtuell path 
                        if hasattr(self.REQUEST,'VirtualRootPhysicalPath'):
                            cLink=cLink.replace('/'.join(self.REQUEST['VirtualRootPhysicalPath']),'')

                if backLink and hasattr(getattr(self,link),'link') and backLink==self.REQUEST['SERVER_URL']+cLink.lstrip():
                        marksrc=alternativArrowsrc
                else:
                        marksrc = arrowsrc

                try:
                   rot=float(rot)
                except:
                   rot=0

                if float(rot) < 0:
                    marksrc = circlesrc
                if bt['isIEWin'] and bt['versIE'] > 5:
                    # IE/Win 5.5 has "feature" for PNG transparency
                    tag += '<span id="i.%s" style="position:absolute; top:-100px; left:-100px; border-style:none; border-width=1px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'%s&rot=%s\');"><img style="visibility:hidden" src="%s&rot=%s" /></span>'%(id,marksrc,rot,marksrc,rot)
                else:
                    # arrow image
                    tag += '<img id="i.%s" src="%s&rot=%s" border="1" style="position:absolute; top:-100px; left:-100px; border-style:none;" />'%(id,marksrc,rot)
            elif ob.isTypeText():
                # DOM - Text
                tag += '<div id="i.%s" style="position:absolute; top:-100px; left:-100px;"'%(id)
                tag += '><div id="t.%s" class="maptext"'%(id)
                tag += 'style="visibility:hidden">'
                tag += ob.getText()
                tag += '</div></div>'
            else:
                # DOM - Area
                if bt['isIE']:
                    # IE needs transparent img for area
                    tag += '<img id="i.%s" border="0" style="position:absolute; top:-100px; left:-100px;" src="trans_img"'%(id)
                    if tiptext:
                        tag += ' title="%s"'%tiptext
                    tag += " />"
                else:
                    # empty div for area
                    tag += '<div id="i.%s" style="position:absolute; top:-100px; left:-100px;"'%(id)
                    if tiptext:
                        tag += ' title="%s"'%tiptext
                    tag += '> </div>'
            tag += '</a>'
        return tag


    #ende der echo erweiterungen


def toList(field):
    """Einzelfeld in Liste umwandeln"""
    if type(field)==types.StringType:
        return [field]
    else:
        return field

def getText(nodelist):
    rc = ""
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
           rc = rc + node.data
    return rc

def getTextFromNode(nodename):
    nodelist=nodename.childNodes
    rc = ""
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
           rc = rc + node.data
    return rc


def readFieldFromXML(meta_url,parent,field):
        """lesespezifisches metadatum"""
        
        try:
                dom=xml.dom.minidom.parse(meta_url)

        except:
                try:
                        fh=urllib.urlopen(meta_url)
                        dom=xml.dom.minidom.parse(fh)
                except:
                        return None
        if not dom: return None

        parent=dom.getElementsByTagName(parent)
        if not parent: return None

        field=parent[0].getElementsByTagName(field)

        if not field: return None

        return getText(field[0].childNodes)
        
def readFieldFromXML_xpath(meta_url,xpath):
    """xpath auf meta_url"""
    #logging.debug("try to parse:"+meta_url)
    #logging.debug("Xtry to parse xpath:"+xpath)
    doc = amara.parse(meta_url)
    #logging.debug("have:"+doc.xml())
    node= doc.xml_xpath(xpath)
    if node is None:
        return None
    
    return unicode(node[0])
              
def urlopen(url):
        """urlopen mit timeout"""
        socket.setdefaulttimeout(10)
        ret=urllib.urlopen(url)
        socket.setdefaulttimeout(5)
        return ret
#       urlopener = urllib.URLopener()
#
#       try:            
#               con = urlopener.open(url)
#               return con
#       except timeoutsocket.Timeout:
#               return None





def checkOnlyOneInGroup(object): 
        """check if object is a group and if it containt only one element it return this element"""
        displayedObjects=object.ZopeFind(object,obj_metatypes=displayTypes)
        if len(displayedObjects)==1: # nur ein Object dann redirect auf dieses Object

                return displayedObjects[0][1]
        else:           return object



def getCrumb(self):
    """returns breadcrumb for this object"""
    # ignore this object if title is empty
    if not self.title:
        return None
    
    crumb = {'obj': self,
             'url': self.absolute_url(),
             'label': self.getLabel()}
    return crumb        

# roots of the ECHO hierarchy
rootMetaTypes = ['ECHO_root', 'ECHO_main', 'ECHO_nav']

def getHierCrumbs(self):
    """returns a list of hierarchical breadcrumbs from self to the ECHO_root"""
    crumbs = []
    # walk the acquisition chain
    for obj in aq_chain(self):
        #logging.debug("getHiercrumbs: obj=%s"%(obj))
        # test if the object really has a getCrumb method
        if hasattr(aq_base(obj), 'getCrumb'):
            crumb = obj.getCrumb()
            #logging.debug("getHiercrumbs: got %s from %s"%(crumb,obj))
            if crumb is not None:
                crumbs.insert(0, crumb)
                
            if obj.meta_type in rootMetaTypes:
                # stop when we reach the ECHO root
                return crumbs
        
    return crumbs        


def getSubCols(self, sortfield=None, 
               subColTypes=displayTypes, 
               sortFieldMD=None,
               searchMD=None,
               searchMD_modus='search',
               searchSimple=None,
               generic="yes",search_sub=0,
               isVisible=True):
        
        def sort(x,y):
                try:
                        if type(x[0])==type(y[0]):
                                return  cmp(x[0],y[0])
                        else:
                                if type(x[0])==types.StringType:
                                        tmpX=unicodify(x[0])
                                        tmpY=y[0]
                                else:
                                        tmpY=unicodify(y[0])
                                        tmpX=x[0]
                                return cmp(tmpX,tmpY)

                except:
                        logging.error("Error at ECHO_helpers.sort:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
                        logging.error("                         : %s %s"%sys.exc_info()[0:2])
                        logging.error("Error at ECHO_helpers.sort:"+repr(x)+"--"+repr(type(x[0])))
                        logging.error("Error at ECHO_helpers.sort:"+repr(y)+"--"+repr(type(y[0])))
                        logging.error("Error at ECHO_helpers.sort:BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB")
                        return 0
        def sortfnc(sortfield,x,y):
                try:
                        xa=x[1].getMDValue(sortfield,generic=generic)
                except:
                        xa=""
                try:
                        ya=y[1].getMDValue(sortfield,generic=generic)
                except:
                        ya=""
                return cmp(xa,ya)
        
        sortWithMD = lambda sortfield : (lambda x,y : sortfnc(sortfield,x,y))
        ids=[]
        displayedObjects=self.ZopeFind(self,obj_metatypes=subColTypes,search_sub=search_sub)
        
        if searchSimple and (not (searchSimple==""))and hasattr(self,'resourceCatalog'):         
            searchString=" AND ".join(searchSimple.split())         
            ids=[x.getObject() for x in self.resourceCatalog({'getMDValueSimpleSearchField':searchString,'getIsVisible':True})]         
        else:
            for entry in displayedObjects:       
                    object=entry[1]
                    if searchMD:
                         if hasattr(object,'getMDValue'):
                            flag=0
                            for field in searchMD.keys():
                                    #if object.getMDValue(field,generic=generic)==searchMD[field]:
                                    #if re.match(searchMD[field],object.getMDValue(field,generic=generic)):
                                    if searchMD_modus=='match':
                                        searchResult=re.match(searchMD[field],object.getMDValue(field,generic=generic))
                                    else:
                                        searchResult=re.search(searchMD[field],object.getMDValue(field,generic=generic))
                                    if searchResult:
                                            flag=1
                                    else:
                                            flag=0
                                            break
                            if flag==1:
                                    ids.append(object)                            
                    else:
                            ids.append(object)
        if not sortfield:
            sortfield=getattr(self,'sortfield','weight')
        tmplist=[]
        for x in ids:
                if hasattr(x,sortfield):
                        try:
                                tmp=int(getattr(x,sortfield))
                        except:
                                tmp=getattr(x,sortfield)
                                
                else:
                        tmp=10000000
                        
                tmplist.append((tmp,x))

        if not sortFieldMD:
                tmplist.sort(sort)
        else:
                tmplist.sort(sortWithMD(sortFieldMD))
        
        ret=[x for (key,x) in tmplist]
      
        return ret

def ECHO_rerenderLinksMD(self,obj=None,types=['title','label']):
        """Rerender all Links"""
        ret=""
        
        if not obj:
            obj = self
            
        entries=obj.ZopeFind(obj,obj_metatypes=['ECHO_resource'],search_sub=1)

        for entry in entries:
                if entry[1].meta_type == 'ECHO_resource':
                        try:
                                entry[1].ECHO_getResourceMD(template="no",overwrite="yes")
                                logging.debug("ECHO_rerenderLinksMD: gotMD")
                                if "title" in types:
                                    entry[1].generate_title()
                                    logging.debug("ECHO_rerenderLinksMD: generatedTitle")
                                if "label" in types:
                                    entry[1].generate_label()
                                    logging.debug("ECHO_rerenderLinksMD: generatedLabel")
                                ret+="OK:"+entry[0]+"-- "+self.unicodify(entry[1].getTitle())+"-- "+self.unicodify(entry[1].getTitle())+"<br>"
                        except:
                                
                                ret+="Error:"+entry[0]+"<br>"
                                logging.debug("ECHO_rerenderLinksMD: error")
                                logging.debug("%s %s"%(sys.exc_info()[0],sys.exc_info()[1]))
                
                
                
        return "<html><body>"+ret+"Rerenderd all links to resources in: "+self.title+"</html></body>"


    
def getRDFDescription(self,linkURL,urn=None,nameDef=None,typeName=None,ap=""):
            """rdf"""
            
            ret=""
            about="""<rdf:Description rdf:about="%s">"""
            name="""<echonavigation:name>%s</echonavigation:name>"""
            link="""<echonavigation:link xlink:href="%s">%s</echonavigation:link>"""
            clickable="""<echonavigation:linkClickable>%s</echonavigation:linkClickable>"""
            #link="""<echonavigation:link rdf:about="%s"/>"""
            type="""<echonavigation:type>%s</echonavigation:type>"""
            
            #xlink="""<echonavigation:xlink xlink:href="%s"/>"""      
            
            if ap=="":
                ap2=""
            else:
                ap2 = """<mpiwg:archive-path>%s</mpiwg:archive-path>"""%ap
            
            if not urn:
                    #urn="urn:"+re.sub('/',':',self.absolute_url())
                    urn=self.absolute_url()
            about2=about%urn
            if not nameDef:
                    if hasattr(self,'label') and not (self.label==""):
                            name2=name%self.label
                    elif not self.title=="":
                            name2=name%self.title
                    else:
                            name2=name%self.getId()

                    name2=re.sub('&','&amp;',name2)
            else:
                    name2=name%nameDef

            linkURL=re.sub('http:','',linkURL)
            linkURL2=re.sub('&','&amp;',linkURL)
            link2=link%(("http:"+linkURL2),("http:"+urllib.quote(linkURL)))
            clickable2=clickable%"true"

            if not typeName:
                    type2=type%self.meta_type
            else:
                    type2=type%typeName
            
          

            #ret=about2+"\n"+name2+"\n"+link2+"\n"+type2+"\n"+clickable2+"\n</rdf:Description>"
	    try:
            	ret=about2+"\n"+name2+"\n"+type2+"\n"+clickable2+"\n"+ap2+"\n</rdf:Description>"
	    except:
            	ret=self.unicodify(about2)+"\n"+self.unicodify(name2)+"\n"+self.unicodify(type2)+"\n"+self.unicodify(clickable2)+"\n"+self.unicodify(ap2)+"\n</rdf:Description>"

            return ret
    
def getCopyrightsFromForm(self,argv): 
        medias={}
        partners={}
        copyrights={}
        
        copyrightsFinal=[]
        for arg in argv.keys():
                
                if arg[0:5]=='media':
                        nm=int(arg[5:])
                        medias[nm]=argv[arg]
                elif arg[0:5]=='partn':
                        nm=int(arg[5:])
                        partners[nm]=argv[arg]
                elif arg[0:5]=='copyr':
                        nm=int(arg[5:])
                        copyrights[nm]=argv[arg]



        copyrightsList=[(medias[nm],partners[nm],copyrights[nm]) for nm in medias.keys()]
        for copyright in copyrightsList:
                
                if copyright[2]=='institution0000':
                        copyrightsFinal.append((copyright[0],copyright[1],self.getPartnerCopyright(copyright[1],'')))
                else:
                        if not copyright[0]=='':
                                copyrightsFinal.append(copyright)
                
                        
        return copyrightsFinal

#List of different types for the graphical linking viewer
viewClassificationListMaster=['view point','area']


def checkDiffs(self,metadict):
            """check differences"""

            

    
            def NoneToEmpty(obj):
                    if obj:
                            return obj
                    else:
                            return ""

            
            
            diffs={}
            
            tags=self.findTagsFromMapping(self.contentType)
            self.referencetypes=tags[2]
            self.fields=tags[3]
            
    
            for field in tags[1]:
                    try:
                            if (NoneToEmpty(self.getMDValue(self.getFieldTag(tags,field)))==metadict[self.getFieldTag(tags,field)]):
                                    diffs[self.getFieldTag(tags,field)]=1
                            else:
                                    
                                    diffs[self.getFieldTag(tags,field)]=0
                    except:
                            diffs[self.getFieldTag(tags,field)]=0
                            
            return diffs
    

def zptFile(self, path, orphaned=False):
    """returns a page template file from the product"""
    if orphaned:
        # unusual case
        pt=PageTemplateFile(os.path.join(package_home(globals()), path))
    else:
      
            pt=PageTemplateFile(os.path.join(package_home(globals()), path)).__of__(self)
    return pt


def findObjectPath(startobject, filename):
    """returns the object with the name filename starting at startobject"""
    if startobject is None:
        return None
    paths = filename.split('/')
    object = startobject
    for path in paths:
            
        if hasattr(object, path):
            object = getattr(object, path)
        else:
            object = None
            break
    return object


def zptObjectOrFile(self, filename, localdir='zpt', orphaned=False):
    """returns a page template instance or a file from the product"""
    # look for an object called path
    pt = findObjectPath(self, filename)
    # the object should also not be a python function
    if (pt is None) or isinstance(pt, types.FunctionType):
        # get a ZPT file
        pt = zptFile(self, localdir+'/'+filename, orphaned=orphaned)
    return pt


def sendFile(self, filename, type):
    """sends an object or a local file (in the product) as response"""
    # look for an object called filename
    object = findObjectPath(self, filename)
    if object:
        # if the object exists then send it
        return object.index_html(self.REQUEST.REQUEST, self.REQUEST.RESPONSE)
    else:
        # send a local file with the given content-type
        fn = os.path.join(package_home(globals()), filename)
        self.REQUEST.RESPONSE.setHeader("Content-Type", type)
        self.REQUEST.RESPONSE.write(file(fn).read())
    return


def browserType(zope):
    """check the browsers request to find out the browser type"""
    bt = {}
    ua = zope.REQUEST.get_header("HTTP_USER_AGENT")
    bt['ua'] = ua
    bt['isN4'] = False
    bt['isIE'] = False
    if string.find(ua, 'MSIE') > -1:
        bt['isIE'] = True
    else:
        bt['isN4'] = string.find(ua, 'Mozilla/4.') > -1
    try:
        nav = ua[string.find(ua, '('):]
        bt['nav'] = nav
        ie = string.split(nav, "; ")[1]
        if string.find(ie, "MSIE") > -1:
            bt['versIE'] = string.split(ie, " ")[1]
    except: pass
    
    bt['isMac'] = string.find(ua, 'Macintosh') > -1
    bt['isWin'] = string.find(ua, 'Windows') > -1
    bt['isIEWin'] = bt['isIE'] and bt['isWin']
    bt['isIEMac'] = bt['isIE'] and bt['isMac']
    return bt

                
def writeMetadata(url,metadict,project=None,startpage=None,xslt=None,thumbtemplate=None,topbar=None,digiLibTemplate=None,xmlfrag=None,digiliburlprefix=None):
        """Einlesen der Metadaten und und erstellen des geaenderten XML file"""
        
        def updateTextToolNode(tag,value):

                metanode=dom.getElementsByTagName('texttool')[0]
                try:
                        nodeOld=metanode.getElementsByTagName(tag)
                except:
                        nodeOld=None
                        
                if nodeOld:
                        metanode.removeChild(nodeOld[0]).unlink()

                node=dom.createElement(tag)
                nodetext=dom.createTextNode(value)
                node.appendChild(nodetext)
                metanode.appendChild(node)


        if xmlfrag:
                geturl="""<?xml version="1.0" encoding="UTF-8" ?>
                           <resource type="MPIWG">
                           <meta>
                            <bib type="Book">
                            </bib>
                            </meta>
                            </resource>"""
                dom=xml.dom.minidom.parseString(geturl)
        else:
                try:
                        geturl=""
                        for line in urlopen(url).readlines():
                                geturl=geturl+line


                except:
                    logger("ECHO writeMetadata",logging.ERROR,"XCannot open: "+url)
                    return (None,"XCannot open: "+url)

                try:
                        dom=xml.dom.minidom.parseString(geturl)
                except:
                    logger("ECHO writeMetadata",logging.ERROR,"Cannot parse: "+url+"<br>"+geturl)
                    return (None,"Cannot parse: "+url+"<br>"+geturl)

        

        metanodes=dom.getElementsByTagName('bib')

        if not metanodes:
                metanodes=dom.getElementsByTagName('archimedes')

        metanode=metanodes[0]

        for metaData in metadict.keys():
                if metaData: 
                    try:
                        nodeOld=metanode.getElementsByTagName(metaData)
                    except:
                        nodeOld=None
                        
                    if nodeOld:
                        metanode.removeChild(nodeOld[0]).unlink()
                    else:
                        # try also old writing rule - instead of _:
                        try:
                                nodeOld=metanode.getElementsByTagName(re.sub('_','-',metaData))
                        except:
                                nodeOld=None
                                
                        if nodeOld:
                                metanode.removeChild(nodeOld[0]).unlink()


                    metanodeneu=dom.createElement(metaData)
                    try:
                        metanodetext=dom.createTextNode(metadict[metaData].decode('utf-8'))
                    except:
                        metanodetext=dom.createTextNode(metadict[metaData])
                    #try:

                        #metanodetext=dom.createTextNode(unicode(metadict[metaData],"utf-8"))
                    #except:
                        #metanodetext=dom.createTextNode(metadict[metaData].encode('utf-8'))
                    metanodeneu.appendChild(metanodetext)
                    metanode.appendChild(metanodeneu)
                
                
        
        
        
        if project:
                updateTextToolNode('project',project)

        if startpage:
                updateTextToolNode('startpage',startpage)

        if topbar:
                updateTextToolNode('toptemplate',topbar)

        if thumbtemplate:
                updateTextToolNode('thumbtemplate',thumbtemplate)

        if xslt:
                updateTextToolNode('xslt',xslt)

        
        if digiliburlprefix:
                updateTextToolNode('digiliburlprefix',digiliburlprefix)
                
        try:   

         return dom.toxml(encoding='utf-8').encode('utf-8')
        except:
            
            
            return dom.toxml(encoding='utf-8')    
        
    
    

class MapArea(SimpleItem):
    """simple class to hold coordinates"""

    meta_type = 'MapArea'
    # Create a SecurityInfo for this class.
    security = ClassSecurityInfo()
    security.setDefaultAccess("allow")
    # type constants
    TYPE_AREA = 'area'
    TYPE_ARROW = 'arrow'
    TYPE_TEXT = 'text'
        
    def __init__(self, id, coords, label=None, type=None, tip=None, permanent=False):
        """init"""
        self.coords = coords[0:4]
        if len(coords) > 4:
            self.angle = coords[4]
        else:
            self.angle = 0
        self.id = id
        self.label = label
        self.setType(type)
        self.tip = tip
        self.permanent = permanent

    def setCoords(self, coords):
        """sets the coords"""
        self.coords = coords

    def getCoordString(self):
        """returns coordinates as a string"""
        return string.join(self.coords, ',')

    def setCoordString(self, coordstring):
        """sets coordinates from a string"""
        coords = string.split(coordstring, ',')
        self.coords = [c.strip() for c in coords]

    def getFullId(self, prefix=None):
        """returns the id with prefixed parent id"""
        if prefix is None:
            if hasattr(self, 'aq_parent'):
                prefix = self.aq_parent.id
            else:
                prefix = "id"
        fid = prefix + "." + self.id
        return fid

    def getType(self):
        """returns the type"""
        return self.type

    def setType(self, type):
        """sets the type"""
        if type == MapArea.TYPE_ARROW:
            self.type = MapArea.TYPE_ARROW
        elif type == MapArea.TYPE_AREA:
            self.type = MapArea.TYPE_AREA
        elif type == MapArea.TYPE_TEXT:
            self.type = MapArea.TYPE_TEXT
        else:
            self.type = MapArea.TYPE_AREA
            
    def isTypeArea(self):
        """returns if the type is area"""
        return self.type == MapArea.TYPE_AREA

    def isTypeArrow(self):
        """returns if the type is arrow"""
        return self.type == MapArea.TYPE_ARROW

    def isTypeText(self):
        """returns if the type is text"""
        return self.type == MapArea.TYPE_TEXT

    def getAngle(self):
        """returns the angle"""
        return self.angle

    def setAngle(self, angle):
        """sets the angle"""
        self.angle = angle

    def getTip(self):
        """returns the popup text"""
        # patch old version
        if not hasattr(self, 'tip'):
            self.tip = self.text

        if self.tip is None:
            if hasattr(self, 'aq_parent'):
                parent = self.aq_parent
                # text-popup type
                if parent.contentType == 'text-popup':
                    return unicodify(parent.description)

                # use map-text
                text = parent.getMapText()
                if text is not None:
                    return text()

        return unicodify(self.tip)

    def setTip(self, text):
        """sets the text"""
        self.tiptext = text

    def getText(self):
        """returns the text for the area"""
        if hasattr(self, 'aq_parent'):
            parent = self.aq_parent
            text = parent.getMapText()
            if text is not None:
                #return text.document_src()
                return text()
        return ""

    def getLabel(self):
        """returns the link label"""
        if self.label is None:
            if hasattr(self, 'aq_parent'):
                return unicodify(self.aq_parent.label or self.aq_parent.id)
        return unicodify(self.label)

    def getTargetUrl(self):
        """returns the URL of the linked object"""
        if hasattr(self, 'aq_parent'):
            p = self.aq_parent
            # ECHOResource and ECHOLink have 'link' attribute
            if hasattr(p, 'link'):
                if p.link is None:
                    # return empty string for empty link
                    return ""
                else:
                    return p.link
                
        # return None for unknown type
        return None

    def setLabel(self, label):
        """sets the label"""
        self.label = label

    def isPermanent(self):
        """returns the permanent state"""
        # patch old objects
        if not hasattr(self, 'permanent'):
            self.permanent = False
        return self.permanent

    def setPermanent(self, state):
        """sets the permanent state"""
        if state:
            self.permanent = True
        else:
            self.permanent = False

    def getLinkId(self):
        if hasattr(self, 'aq_parent'):
            return self.aq_parent.id
        return self.id

# call this to initialize framework classes, which
# does the right thing with the security assertions.
Globals.InitializeClass(MapArea)


class MapText(Folder,ZopePageTemplate):
    """class to hold text for map areas"""

    meta_type = 'ECHO_mapText'
    # Create a SecurityInfo for this class.
    security = ClassSecurityInfo()
    security.setDefaultAccess("allow")

    
    _default_content_fn = os.path.join(package_home(globals()),
                                       'html', 'ECHO_mapText_default.html')

    def __init__(self, id, text=None, content_type=None):
        self.id = str(id)
        self.ZBindings_edit(self._default_bindings)
        if text is None:
            text = open(self._default_content_fn).read()
        if content_type is None:
            content_type = self.content_type
        self.pt_edit(text, content_type)

    def createLocale(self):
            """create locale"""
            return ECHO_collection.manage_addECHO_localeForm(self)

    manage_main=ZopePageTemplate.pt_editForm

    def manage_addECHO_locale(self,lang,title,label,text=None,content_type=None,RESPONSE=None):
            """manage add echo locale in map"""
            ECHO_collection.manage_addECHO_locale(self,lang,title,label,text,content_type)
            if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')


    def changeLocale(self):
           """change locale"""
           return self.locale_en.change_ECHO_localeForm()

    def change_ECHO_locale(self,lang,title,label,text=None,content_type=None,RESPONSE=None):
                """change echo locale"""

                obj= self.locale_en.change_ECHO_locale(lang,title,label,text,content_type,RESPONSE)
        


                if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

                
# Product registration and Add support
manage_addMapTextForm = PageTemplateFile(
    'zpt/AddECHO_mapText', globals(), __name__='manage_addMapTextForm')

def manage_addMapText(self, id, title=None, text=None,
                           REQUEST=None, submit=None):
    "Add a Map Text with optional file content."

    id = str(id)
    if REQUEST is None:
        self._setObject(id, MapText(id, text))
        ob = getattr(self, id)
        if title:
            ob.pt_setTitle(title)
        return ob
    else:
        file = REQUEST.form.get('file')
        headers = getattr(file, 'headers', None)
        if headers is None or not file.filename:
            zpt = MapText(id, text)
        else:
            zpt = MapText(id, file, headers.get('content_type'))

        self._setObject(id, zpt)
        if title:
            ob = getattr(self, id)
            ob.pt_setTitle(title)

        try:
            u = self.DestinationURL()
        except AttributeError:
            u = REQUEST['URL1']

        if submit == " Add and Edit ":
            u = "%s/%s" % (u, urllib.quote(id))
        REQUEST.RESPONSE.redirect(u+'/manage_main')
    return ''

# call this to initialize framework classes, which
# does the right thing with the security assertions.
Globals.InitializeClass(MapText)
