#TODO: change metaDataHash, jetzt tag -> data(tags aus dem neuen Metadatamain)
#TODO: contentType aus den Metadaten

import urlparse
import string
import tempfile
import zipfile
import re
import os,shutil
import OFS.Image
from types import *
from OFS.Cache import Cacheable
from OFS.Image import Image
from Globals import DTMLFile
from OFS.Folder import Folder
from OFS.SimpleItem import SimpleItem
from AccessControl import ClassSecurityInfo
from AccessControl.User import UserFolder
from Globals import InitializeClass
from Globals import DTMLFile
import Globals
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate,manage_addPageTemplate
from Globals import Persistent, package_home
from Acquisition import Implicit
from Products.ZCatalog.CatalogPathAwareness import CatalogAware
from Products.ZCTextIndex.ZCTextIndex import manage_addLexicon
try:
	from Products.MetaDataProvider.MetaDataClient import MetaDataClient
except:
	print "no metadata services"
import urllib
import urllib2
import cgi
import smtplib
import time
#from Ft.Xml.Domlette import NonvalidatingReader
#from Ft.Xml.Domlette import PrettyPrint, Print
#from Ft.Xml import EMPTY_NAMESPACE
#
#import Ft.Xml.XPath
import cStringIO

import sys

try:
        from psycopg import libpq
except:
        try:
                from pyPgSQL import libpq
        except:
                print "ECHO_collection: Warning - No libpq imported!"
                
import xml.dom.minidom

import urllib
import xml.dom.minidom
import ECHO_helpers
from ECHO_helpers import *


from ECHO_movie import *
#import vlp_xmlhelpers #TODO: vlp elemente aus echo herausnehmen
import xmlrpclib

import logging

class ECHO_resource(CatalogAware,Folder,Persistent,ECHO_basis):
    """ECHO Ressource"""
    security=ClassSecurityInfo()
    meta_type='ECHO_resource'
    default_catalog='resourceCatalog'
    
	#    viewClassificationList=viewClassificationListMaster
	
	
	
		 
    def PrincipiaSearchSource(self):
           """Return cataloguable key for ourselves."""
           return str(self)
          
    def getDCDate(self):
    	"""dc mapped date"""
    	try:
    		bibdata=self.getMetaDataManager().getBibData(path=self.getMetaDataLink())
    		dcMapped = self.getMetaDataManager().getDCMappedData(bibdata)
    		return dcMapped.get('date')
    	except:
	   		 
            logging.error("getDCDate doesn't work, cannot read metadata:"+self.getId())
	   		
		
    getSubCols = ECHO_helpers.getSubCols
    
#    def getMetaDataManager(self):
#		 """hole eine Instanz von MetaDataFolder
#		 TODO: sollte konfigurierbar sein. 
#		 zur Zeit wird enfach nur self.metadata falls vorhanden zurueckggegeben.
#		 """
#	
#		 mf= getattr(self,"metadataFolder",None)
#		 #logging.debug("MetadataManager:"+repr(mf))
#		 return mf
#		 

    def index_meta(self,RESPONSE=None):
        """ gibt das im metalink gespeicher xml-file zurueck"""
        url = self.metalink
        txt=""
        logging.debug("ml:%s"%url)
        try:
            page = urllib.urlopen(url);
            if RESPONSE:
                RESPONSE.setHeader("Content-Type","text/xml")
            
            txt = page.read();
        except:
            logging.error("Cannot read metadata of: %s"%self.getId())
        
        return txt
        
    def reindex(self):
        """generate fields for indexing and reindex"""
        
        #TODO: korrigieren des metalink pfades konfigurierbar machen
        splitted= [x for x in urlparse.urlparse(self.metalink)]
        splitted[1]="foxridge.mpiwg-berlin.mpg.de"[0:]
        
        if splitted[0]=="http":
            self.metalink=urlparse.urlunparse(splitted)
        

        self.fullTextUrl=self.getFullTextXML(noredirect="yes")
        
        #TODO: korrigieren relative url to absoluter url volltexten, konfigurierbar?
        splitted= [x for x in urlparse.urlparse(self.fullTextUrl)]
        if splitted[0]=="":
            splitted[0]="http"
            splitted[1]="foxridge.mpiwg-berlin.mpg.de"[0:]
            
            self.fullTextUrl=urlparse.urlunparse(splitted)
            
        self.imagePath=self.getImagePath()
        
        self.reindex_object()
  
  
 

    def getRDF(self,urn=None):
            """rdf"""
            ap = self.getArchivePathFromMetadata()
         
       
            ret=getRDFDescription(self,self.link,urn=urn,ap=ap)
           
            return ret+self.createSubElementRDF(urn=urn) 

  
    def changeAccessRightForm(self,preselect=None):
        """change the access rights"""
        
        pt=zptFile(self, 'zpt/ChangeECHO_resourceAccessRight.zpt')
        return pt(preselect=preselect)
    
    def changeAccessRight(self):
        """changeRights"""
        argv=self.REQUEST.form
        self.setAccessRightXML(argv["%s_xml"%self.getId()])
        self.accessRight=argv["%s_echo"%self.getId()]
         
        self.REQUEST.RESPONSE.redirect('manage_main')
        
    
    def getAccessRightSelectorHTML(self,outlook="select",xmldominant=None,preselect=None):
            """htmlselector, xmldominant= in der regel wird der wert des xmlfiles ausgegeben ausser er existiert nicht"""
            values=['','free','MPIWG']
            
            if preselect:#set all to the preselected
                ar=(preselect,preselect)
            else:#else set to actual value
                ar=self.getAccessRightMD()
                
            if outlook=="select":
                    ret="""<select name="%s_xml">"""%self.getId()
                    ret1="""<select name="%s_echo">"""%self.getId()
                    for value in values:
                            if value==ar[0]:
                                    ret+="<option selected>%s</option>"%value
                            else:
                                    ret+="<option>%s</option>"%value
                            
                            if value==ar[1]:
                                    ret1+="<option selected>%s</option>"%value
                            else:
                                    ret1+="<option>%s</option>"%value
                    
                    
                    if not xmldominant:
                        return ret+"</select>",ret1+"</select>"
                    else:
                        if ar[0] is not None:
                            return ret+"</select>"
                        else:
                            return "<p>No xml file (only local selection):"+ret1

            else:
                    ret=""
                    ret1=""
                    for value in values:
                            
                            if value==ar[0]:
                                    ret+="""<input type="radio" name="%s_xml" value="%s" checked>%s"""%(self.getId(),value,value)
                            else:
                                    ret+="""<input type="radio" name="%s_xml" value="%s">%s"""%(self.getId(),value,value)
                                    
                            if value==ar[1]:
                                    ret1+="""<input type="radio" name="%s_echo" value="%s" checked>%s"""%(self.getId(),value,value)
                            else:
                                    ret1+="""<input type="radio" name="%s_echo" value="%s">%s"""%(self.getId(),value,value)

                    if not xmldominant:                        
                        return ret,ret1
                    else:                         
                        if ar[0]:
                            return ret
                        else:
                            return "<p>No xml file (only local selection)</p>"+ret1



    def copyAccessRightsFromMD(self):
            """kopiere rechte aus den metadaten"""
            self.accessRight=self.getAccessRightMD()[0]
            
            
            
            
    
    def getAccessRightMD(self):
                """set accessright"""
                url=self.metalink

                try:
                    urllib.urlopen(url)
                except:
                    logger("ECHO_Resource (getAccessRightMD)", logging.INFO,"%s (%s)"%sys.exc_info()[0:2])

                    return None,getattr(self,'accessRight','')
                
            
                try:
                        dom = NonvalidatingReader.parseUri(url)
                except:
                        logger("ECHO_Resource (getAccessRightMD)", logging.INFO,"%s (%s)"%sys.exc_info()[0:2])
                        return (None,"Cannot parse: "+url+"<br>"+"%s (%s)"%sys.exc_info()[0:2])


                accessright=dom.xpath("//meta/access-conditions/access/@type")
                if accessright:
                    accessright=accessright[0].value
                if str(accessright)=='institution':
                    tmp=dom.xpath("//meta/access-conditions/access/name")
                    if tmp:
                        accessright=getTextFromNode(tmp[0])
                    
                if not accessright:
                    accessright=""
                
                return accessright,getattr(self,'accessRight','')             

    def changeAccessRightMD(self,accessright,RESPONSE=None):
            """change the rights - not user anymore"""
            #TODO: check if method still needed
            params="accessright=%s"%accessright


            #print ECHO_helpers.urlopen(self.absolute_url()+'/setAccessRightXML'+'?'+params).read()

            tries=0
            for i in range(10):
                    x=ECHO_helpers.urlopen('http://echo.mpiwg-berlin.mpg.de/echo_nav/storage/downloadExternalXML?index_meta_url=%s&xml_url=%s'%(self.metalink,self.absolute_url()+'/setAccessRightXML'+urllib.quote('?'+params))).read()
                    if x=="ok":
                           
                            break;
                 
            
          
            if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

                
    def setAccessRightXML(self,accessright):
                """set accessright"""
                url=self.metalink
                accessrights=['MPIWG','free']#allowes rights
                
                if accessright =='':
                    """do nothing"""
                    return ""
                    
                    
                try:
                        geturl=""
                        for line in ECHO_helpers.urlopen(url).readlines():
                                geturl=geturl+line 


                except:
                        return (None,"Cannot open: "+url)

                try:
                        dom=xml.dom.minidom.parseString(geturl)
                        root=dom.getElementsByTagName('resource')[0]
                except:
                        return (None,"Cannot parse: "+url+"<br>"+geturl)

                metamains=dom.getElementsByTagName('meta')
                
                if not metamains:
                        nodenew=dom.createElement('meta')
                        root.appendChild(nodenew)
                        metamain=nodenew
                else:
                        metamain=metamains[0]
                
                
                metanodes=metamain.getElementsByTagName('access-conditions')

                if not metanodes:
                        nodenew=dom.createElement('access-conditions')
                        metamain.appendChild(nodenew)
                        metanode=nodenew
                else:
                        metanode=metanodes[0]

                accesses=metanode.getElementsByTagName('access')

                #delete old
                if accesses:
                     metanode.removeChild(accesses[0]).unlink()

                #create new
                    
                nodenew2=dom.createElement('access')
                metanode.appendChild(nodenew2)
                metanode2=nodenew2
                
                attribute=metanode2.getAttribute('type')
                
                if accessright=="free":
                    
                     metanode2.setAttribute('type','free')
                
                elif accessright.upper()=='MPIWG':
                        metanode2.setAttribute('type','institution')
                        nodenew4=dom.createElement('name')
                        metanodetext=dom.createTextNode('MPIWG')
                        nodenew4.appendChild(metanodetext)
                        nodenew2.appendChild(nodenew4)
                #print dom.toxml().encode('utf-8')
                string= encodeRPC(dom.toxml().encode('utf-8'))
                
                #TODO: make server configurable
                server=xmlrpclib.Server("http://foxridge.mpiwg-berlin.mpg.de/server")

                path=urlparse.urlparse(self.metalink)[2]
         
                server.writeMetaDataFile(path,string,"yes")
        

    def createImageUrl(self,pn=1):
            """create ImageUrl"""

            
            resourcepath=readFieldFromXML_xpath(self.metalink,'//resource/archive-path')
            
            digiliburlprefix=readFieldFromXML(self.metalink,'texttool','digiliburlprefix')
            images=readFieldFromXML(self.metalink,'texttool','image')


            if (not resourcepath) or (not digiliburlprefix) or (not images):
                    logger("ECHO (createImageUrl)",logging.ERROR,"Cannot create ImageUrl for %s"%self.absolute_url())
                    return None
            resourcepath=resourcepath.replace('/mpiwg/online','')
            if not digiliburlprefix: digiliburlprefix="http://echo.mpiwg-berlin.mpg.de/zogilib?"

            if (not images) or (not resourcepath): return None

            return "%sfn=%s&pn=%i"%(digiliburlprefix,resourcepath+"/"+images,pn)
    
    def copyTitleToInfoXML(self,RESPONSE=None):
            """copy title from the resource"""
            presentationXML=readFieldFromXML(self.metalink,'texttool','presentation')
            resourcepath=readFieldFromXML_xpath(self.metalink,'//resource/archive-path')
            if (not presentationXML) or (not resourcepath): 
                if RESPONSE:
                        RESPONSE.write("Error: %s\n"%self.getId())
                else:
                        return None,self.absolute_url()

            try:
                    fh=file(os.path.join(resourcepath,presentationXML),'w')
                    fh.write("""<info>
                    <author></author>
                    <title>%s</title>
                    <date></date>
                    <display>yes</display>
                    </info>"""%self.title)
                    fh.close()
                    return 1,self.getId()
            except:
                    if RESPONSE:
                            RESPONSE.write("Error: %s\n"%self.getId())
                    else:
                            return None,self.absolute_url()

            



    def setLink(self, link=None):
        """change link field"""
        if link is not None:
            self.link = link
            
   
    def getTextToolsField(self,name,default=''):
        """Lese Textoolsfelder aus index.meta im path aus"""
        
        try:
            dom=xml.dom.minidom.parse(self.metalink)
            node=dom.getElementsByTagName('texttool')[0] #getNode
            subnode=node.getElementsByTagName(name)[0]

            # bei text wird nur der Folder gebraucht
            if name=="text":
                splitted=getText(subnode.childNodes).split("/")
                return splitted[len(splitted)-2]
            else:
                return getText(subnode.childNodes)
        except:
            return default
   



    security.declarePublic('content_html')      
    def content_html(self):
        """template fuer content"""
        return ECHO_basis.content_html(self,'resource')
    
#    def getViewClassification(self):
#        if hasattr(self,'viewClassification'):
#            return self.viewClassification
#        else:
#            return ""

    def getFullTextXML(self,noredirect=None):
            """getFullTextXML; gives the FullText as an XML Document, and <error></error> if somthing goes wrong."""

            try:
                    #logger("ECHO Fulltext",logging.INFO,"open %s"%self.metalink)
                    fh=ECHO_helpers.urlopen(self.metalink)
                    #logger("ECHO Fulltext",logging.INFO,"opened %s"%self.metalink)
                    dom=xml.dom.minidom.parse(fh)
                    texttools=dom.getElementsByTagName('texttool')
                    text=texttools[0].getElementsByTagName('text')
                    texturl=getText(text[0].childNodes)

                    #logger("ECHO Fulltext",logging.INFO,"found %s"%texturl)
                    fh.close()
                    #logger("ECHO Fulltext",logging.INFO,"closed fh")
                    #keine url
                    if not (texturl.split(":")[0] in ['http','ftp','file']): 
                        if not noredirect:
                            return file(texturl).read()
                        else:
                            return texturl

                    if not noredirect:
                            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')  
                            logger("ECHO Fulltext",logging.INFO,"redirect to:%s"%texturl)
                            self.REQUEST.RESPONSE.redirect(texturl)
                    else:
                            return texturl
            except:

                    if not noredirect:
                            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
                            self.REQUEST.RESPONSE.write("<error>no fulltext available</error>")
                    else:
                            return "<error>no fulltext available</error>"

    def getImagePath(self):
            """gibt pfad zum image aus"""
            return self.getImageView(noredirect="yes",onlyPath="yes")
    
    def getArchivePathFromMetadata(self):
    	try:
    		archivePath=readFieldFromXML_xpath(self.metalink,"//resource/archive-path")
    		return archivePath
    	except:
			return ""
	
    def getImageView(self,noredirect=None,onlyPath=None):
            """getImages; give Imageviewr  and <error></error> if somthing goes wrong."""
            try:
            	    archivePath=readFieldFromXML_xpath(self.metalink,"//resource/archive-path")
                  
                    archivepath=re.sub('/mpiwg/online/','',archivepath) 
                    imageurl="http://echo.mpiwg-berlin.mpg.de/zogilib?fn="+archivepath+"/"+imagetemp
                    fh.close()

                    if not noredirect:
                            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')  
                            self.REQUEST.RESPONSE.redirect(imageurl)
                    else:
                            if not onlyPath:
                                    return imageurl
                            else:
                                    return archivepath+"/"+imagetemp
            except:

                    if not noredirect:
                            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
                            self.REQUEST.RESPONSE.write("<error>no fulltext available</error>")
                    else:
                            return "<error>no images available</error>"

    
    def getCopyrightsHTML(self):
            """gib (link auf copyright link, mediatyp, institution, copyrightType, label von copyrightType) aus"""
            
            if hasattr(self,'copyrightModel'):
                    obj=self.copyrightModel
                    
            else:
                    return "ERROR"
            ret=[]
            
            for copyright in obj.getCopyrights(): #copyright - media / partner / copyrightID
                    
                    
                    try:
                        if hasattr(self.copyrightTypes,copyright[2]):
                             copyrightTypeObj=getattr(self.copyrightTypes,copyright[2])
                             link="copyrightTypes/"+copyright[2]+'/copyright.html'
                        else:
                             copyrightTypeObj=getattr(obj,copyright[2])
                             link="copyrightModel/"+copyright[2]+'/copyright.html'
                             
                        label=copyrightTypeObj.label
                        url=getattr(copyrightTypeObj, 'url', '')
                            
                        if url!='':
                                 ret.append((url,copyright[0],copyright[1],copyright[2],label))
                        else:
                                if hasattr(copyrightTypeObj, 'copyright.html'):
                                     ret.append(("""%s?partner=%s"""%(link,copyright[1]),copyright[0],copyright[1],copyright[2],label))
                                else:
                                     ret.append(('empty',copyright[0],copyright[1],copyright[2],label))
                    except:
                            """nothing"""
                    
            return ret
            
    def getInstitutionsHTML(self):
                """gibt Liste der foerdernden Institutionen aus"""
                
                if hasattr(self,'support'):
                        obj=self.support
                        ret=obj.getSupporter()
                        return ret
                else:
                        return ''
                        
    def getOwnerOriginalsHTML(self):
               """gibt Liste der foerdernden Institutionen aus"""
               
               if hasattr(self,'ownerOriginal'):
                       obj=self.ownerOriginal
                       ret=obj.getOwner()
                       return ret
               else:
                       return ''
    
    def getDigiCopyByHTML(self):
               """gibt Liste der foerdernden Institutionen aus"""
               
               if hasattr(self,'digiCopyBy'):
                       obj=self.digiCopyBy
                       ret=obj.getDigiCopyBy()
                       return ret
               else:
                       return ''                    
    
    def getCredits(self):
        """Ausgabe der credits"""
        if self.credits:
            return self.credits
        else:
            return []


#    def getStorageManagerResourceURL(self):
#        """get the link to storage"""
#        urlbase=self.getStorageManagerURL();
#        
#        #now get the path from the metadatalink
#        
#        path = self.correctPath(self.getMetaDataLink())
#        if path is None:
#            return ""
#        else:
#            path=path.replace("index.meta","")
#            return urlbase+path
#        
        
    def correctPath(self,path):
        #take only the path of the url which starts with /permanent or /experimental
        
        rs= re.search("/permanent/(.*)", path);
        if rs is not None:
            txt="permanent/"+rs.group(1)
        else:
            rs= re.search("/experimental/(.*)", path);
            if rs is not None:
                txt="experimental"+rs.group(1)
            else:
                return None
        
        return txt
        
    def __init__(self,id,link,metalink,resourceID,title,label,description,contentType,renderingType,copyrightType,responsible,credits,weight,coords):

        self.id = id
        """Festlegen der ID"""
        
        self.label = label
        self.link= link
        self.metalink=metalink
        self.title=title
        self.weight=weight
        self.credits=toList(credits)
        self.description=description
        self.contentType=contentType
        self.copyrightType=copyrightType
        self.renderingType=renderingType
        self.responsible=responsible
        self.resourceID=resourceID
        
        if coords:
            coordsnew=[ string.split(x,",") for x in coords]
        else:
            coordsnew=[]
        
        self.coords=coordsnew
#       self.viewClassification=""



    def getContentType(self):
            try:
                    return normalizeCt(self.contentType)
            except:
                    return ""

    def getCopyrightType(self):
            try:
                    return self.copyrightType
            except:
                    return ""

    def getRenderingType(self):
            try:
                    return self.renderingType
            except:
                    return ""

    def ECHO_resource_config(self):
        """Main configuration"""

        if not hasattr(self,'weight'):
            self.weight=""

        pt=zptFile(self, 'zpt/ChangeECHO_resource.zpt')
        return pt()
    

    def ECHO_resource_config_main(self):
        """Main configuration"""
        if not hasattr(self,'weight'):
            self.weight=""
        pt=zptFile(self, 'zpt/ChangeECHO_resource_main.zpt')
        return pt()

    def ECHO_resource_config_coords(self):
        """Coords configuration """
        pt=zptFile(self, 'zpt/ChangeECHO_resource_coords.zpt')
        return pt()

    def ECHO_resource_config_credits(self):
        """Main configuration"""
        pt=zptFile(self, 'zpt/ChangeECHO_resource_credits.zpt')
        return pt()

    
    def changeViewer(self,newViewer):
        """set newViewer to the url of the new viewer
        only if metalink is set, otherwise it gives false
        """
        
        texterUrl="http://digilib.mpiwg-berlin.mpg.de/digitallibrary/servlet/Texter?fn=/"
        
        vl,msg=self.checkValidityOfMetaLink()
        
        if not vl: #ungueltiger link, versuche neuen
            newStr=re.match(".*/mpiwg/online/(.*)",self.metalink) # suche pfad hinter /mpiwg/online 
            oldLink=self.metalink
            
            if newStr:
                self.metalink=texterUrl+newStr.group(1)
                vl,msg=self.checkValidityOfMetaLink()
            else:
                logging.error("change Viewer XX(mew Metadatafile) %s"%self.metalink)
                vl=False
                
            if not vl: # geht immer noch nicht, dann setzte wieder zurueck
                self.metalink=oldLink
            
            logging.info("change Viewer (mew Metadatafile) %s"%self.metalink)
           
        if vl:
            self.newViewer=newViewer
            return vl,msg
        else:
            return vl,msg
    
    def checkValidityOfMetaLink(self):
        """checks if the metalink is valid xml"""
        
        ml=self.metalink
        
        try:
            txt=urllib.urlopen(ml)

        except:
            return False,"Cannot open: %s)"%ml
        
        txt.close()        
        try:
            dom = NonvalidatingReader.parseUri(ml)
        except:
            return False, "Connot parse: %s)"%ml
        
        if len(dom.xpath("//texttool"))==0:
            return False, "No texttools: %s)"%ml
            
        return True,""
        
    def changeECHO_resource_main(self,metalink,link,title,label,description,contentType,renderingType,weight,resourceID,newViewer='',RESPONSE=None):
        """Aenderung der Properties"""
        self.resourceID=resourceID
        self.title=title
        self.label=label
        self.description=description

        self.contentType=contentType
        self.renderingType=renderingType
        self.weight=weight
        
        self.link=link
        self.metalink=metalink
        
        self.newViewer=newViewer
        
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def changeECHO_resource_coords(self,RESPONSE=None):
        """Aenderung der Properties - coords"""
        #return self.REQUEST
        for area in self.getMapAreas():
            id = area.getId()
            if self.REQUEST.has_key('del.'+id):
                # delete this area
                self._delObject(id)
                # return to same menu
                if RESPONSE is not None:
                    RESPONSE.redirect('ECHO_resource_config_coords')
                return
            # modify this area
            coordstring = self.REQUEST.get('coords.'+id, '')
            coords = string.split(coordstring, ',')
            angle = self.REQUEST.get('angle.'+id, '0')
            type = self.REQUEST.get('type.'+id, 'area')
            if len(coords) == 4:
                area.setCoordString(coordstring)
                area.setAngle(angle)
                area.setType(type)
        # return to main menu    
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def changeECHO_resource_credits(self,credits,responsible,copyrightType,RESPONSE=None):
        """Aenderung der Properties"""
        self.credits=credits
        self.responsible=responsible
        self.copyrightType=copyrightType
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


 
    def changeECHO_resource_metadata(self,RESPONSE=None):
            """change metadata"""
            
            self.copyIndex_meta2echo_resource()
            tags=mappedData.keys()
            
          
            for field in tags:
                    try:
                            self.metaDataHash[field]=self.REQUEST.form[field]
                    
                    except:
                            logger("ECHO_collection (changeECHO_resource_metadata)",logging.ERROR,"%s (%s)"%sys.exc_info()[0:2])

            logging.debug("changeECHO_resource_metadata:"+repr(self.newMetaXML()))
            
            
            mc = MetaDataClient("http://content.mpiwg-berlin.mpg.de/metaDataServer") 
            x=mc.writeMetaDataFile(self.getMetaDataLink(),self.newMetaXML(),compressed=None)
                           
            logging.debug("changeECHO_resource_metadata (xlrpc return):"+repr(x))
            

            if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')

    

    def getMDValueSimpleSearchField(self):
            """returns value for simple search aus DC daten"""
            
            try:
			    bibdata=self.getMetaDataManager().getBibData(path=self.getMetaDataLink())
			    dcMapped = self.getMetaDataManager().getDCMappedData(bibdata)
			    logging.debug("getMDValueSimpleSearchField:"+ repr(dcMapped))
			    return " ".join([dcMapped.get('title',''),
			                 dcMapped.get('creator',''),
			                 dcMapped.get('date',''),self.getTitle(),self.getDescription()])
		    
            except:
            	logging.error("getMDValueSimpleSearchField doesn't work cannot read metadata:"+self.getId())
            	return " ".join([self.getDescription(),self.getTitle()])
				

    def newMetaXML(self,project=None,startpage=None,xslt=None,thumbtemplate=None,topbar=None,digiLibTemplate=None,digiliburlprefix=None):
            """new index.meta"""
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')

            if not hasattr(self,'metaDataHash'):
                
                    self.copyIndex_meta2echo_resource()
            
            return writeMetadata(self.metalink,self.metaDataHash,project,startpage,xslt,thumbtemplate,topbar,digiLibTemplate,digiliburlprefix=digiliburlprefix)

    def showMetaDataXML(self,project=None,startpage=None,xslt=None,thumbtemplate=None,topbar=None,digiLibTemplate=None):
            """return bibliographical metadata as stored in the object"""
            
            try:
                    
                    return writeMetadata(self.metalink,self.metaDataHash,project,startpage,xslt,thumbtemplate,topbar,digiLibTemplate,xmlfrag="yes")
            except:
                    return "<error> no metadata stored</error>"
    
    def getXQueryMetaData(self,xquery):
                '''
                execute an xquery on the metadata and returns a list of tuples, 
                each tuple represent the content of the node as text and the whole node as xml-fragment
                @param xquery: xquery auf den metadaten
                '''
                
                try:
                        md=writeMetadata(self.metalink,self.metaDataHash,xmlfrag="yes")
                except:
                        return None

                dom = NonvalidatingReader.parseString(md,"http://www.mpiwg-berlin.mpg.de/")
                
                results=Ft.Xml.XPath.Evaluate(xquery, contextNode=dom)
            
                ret=[]
                for result in results:
                    
                    buf = cStringIO.StringIO()
                    PrettyPrint(result, stream=buf)
                    str = buf.getvalue()[0:]
                    buf.close()
                    ret.append((getTextFromNode(result),str)) 
                        
                return ret
        
    def getMetaDataXML(self,project=None,startpage=None,xslt=None,thumbtemplate=None,topbar=None,digiLibTemplate=None):
            """prints out metadata as stored in the echo environment, format is the index.meta format"""
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
            return writeMetadata(self.metalink,self.metaDataHash)

    def getMetaDataLink(self):
        """get link to the meta data"""
        return self.metalink
    
    def changeECHO_resource(self,metalink,link,title,label,description,contentType,responsible,weight,coords=None,credits=None,RESPONSE=None):
        """Aenderung der Properties"""
        
        setECHO_collectionInformation(self,title,label,description,contentType,responsible,credits,weight)
        self.link=link
        self.metalink=metalink
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
            
            
    manage_options = (
        {'label':'Main Config','action':'ECHO_resource_config_main'},
        {'label':'Edit Metadata','action':'ECHO_getResourceMD'},
        {'label':'Graphic Coords','action':'ECHO_graphicEntry'},
        {'label':'set/change startpage','action':'setStartPageForm'},
        {'label':'Change AccessRights','action':'changeAccessRightForm'}, 
        ) + Folder.manage_options


            


    def getMetaDataHash(self):
            """md hash"""
           
            return getattr(self,'metaDataHash',{})
           
    def getMetaDataValue(self,fieldName):
		
		self.copyIndex_meta2echo_resource()
		return self.metaDataHash.get(fieldName,{}).get('value','')
	   	
    def findLabelsFromMapping(self,referenceType):
        """gib hash mit label -> generic zurueck"""
        #return {},[]
        mo=self.getMetaDataManager().getBibMapping(referenceType)
        
        if mo is None:
			return [],{}     
		  
        return mo.getFieldList(),mo.getFields()

    def findTagsFromMapping(self,referenceType):
        """gib hash mit label -> generic zurueck"""
        
        return self.findLabelsFromMapping(referenceType)

    
   
    def copyIndex_meta2echo_resource(self,RESPONSE=None):
            #copy MD von Index_meta to the echo_resource
            
            logging.debug("copyIndex_meta2echo_resource:"+self.metalink)
            md = self.getMetaDataManager().getBibData(path=self.metalink)
            logging.debug("copyIndex_meta2echo_resource:"+repr(md))
            self.metaDataHash=self.getMetaDataManager().getBibMappedData(md)
            logging.debug("copyIndex_meta2echo_resource:"+repr(self.metaDataHash))
            
            self.contentType=normalizeCt(self.metaDataHash.get('@type',''))
            
            return
           
    def getDRI(self,type="mpiwg"):
    	"""get DRI"""
    	try:
    		dri = self.getMetaDataManager().getDRI(path=self.metalink,type=type)
    	except:
    		return None
    	
    	return dri
        
    def ECHO_getResourceMD(self,template="yes",back=None,overwrite="no"):
        """Einlesen der Metadaten und Anlegen dieser Metadaten als Informationen zur Resource"""
        #(metadict, error)=readMetadata(self.metalink)
        logging.debug("getMetaData:"+self.metalink)
        
        self.copyIndex_meta2echo_resource()
        if back:
            self.REQUEST.SESSION['back']=back

        if (not self.contentType) or (overwrite=="yes"):
                self.contentType=normalizeCt(normalizeCt(self.metaDataHash.get('@type','')).lower())
                
        if not (normalizeCt(self.contentType).lower()==normalizeCt(self.contentType).lower()):
                self.REQUEST.SESSION['contentStorage']=normalizeCt(self.contentType)
                self.REQUEST.SESSION['contentZope']=normalizeCt(self.contentType)

                return zptFile(self, 'zpt/ECHO_getResourceMDErrorContentType.zpt')()

        #self.REQUEST.SESSION['metadict']=metadict
        
        
        if template=="yes":
                #pt=PageTemplateFile(os.path.join(package_home(globals()), 'zpt/ECHO_resourceMD.zpt'),metadict=metadict).__of__(self)
                pt=zptFile(self, 'zpt/ECHO_resourceMD.zpt')
                
                return pt(metadict=self.metaDataHash)

        

    
    
#    def ECHO_getMD(self,item):
#        """Ausgabe der MD"""
#        return getattr(self,item)

    def checkPDF(self,path):
            """check if pdf in the path"""
            try:
                    for fileName in os.listdir(path):
                            if os.path.splitext(fileName)[1]==".pdf":
                                    return os.path.join(path,fileName)
                    return None
            except:
                    return None
            
            
    security.declareProtected('View','index_html')
    def index_html(self):
        """standard page"""
        pdf=self.checkPDF(self.link)
        if pdf:
                fh=file(pdf,'r').read()
                self.REQUEST.RESPONSE.setHeader('Content-Type','application/pdf')
                self.REQUEST.RESPONSE.write(fh)
                self.REQUEST.RESPONSE.close()
                return 
            
        if getattr(self,'newViewer',''): #benutze den neuen viewer
            url=self.newViewer+'url='+self.metalink+"&mode=texttool"
            return self.REQUEST.RESPONSE.redirect(url)
            
        return self.REQUEST.RESPONSE.redirect(self.link)

    def startpage_html(self):
            """prints out a startpage for a resource for use e.g. in the BVE"""

            # suche ob startpage.html in dem Ordner vorhanden ist, dann wir diese angezeigt

            sp=self.ZopeFind(self,obj_ids=['startpage.html'])

            if sp:
                    return sp[1]()

            #pruefen ob irgendwo ein template
            if hasattr(self,'startpage_index_template'): 
                    return self.startpage_index_template()

            #generisches template ausgeben
            
            pt=zptFile(self, 'zpt/ECHO_startpage_index_template_standard.zpt')
            pt.content_type="text/html"
            return pt()

    def toc_html(self):
            
            sp=self.ZopeFind(self,obj_ids=['toc.html'])
                    
            if sp:
                    return sp[0][1]()


    security.declarePublic('generate_label') 

    def generate_label(self):
        """Erzeugt_standard_Label aus Template"""
        self.label=self.getMetaDataManager().getBibFormattedLabel(path=self.metalink)
        return self.label
        

    security.declarePublic('generate_title') 
    def generate_title(self,RESPONSE=None):
        """Erzeugt_standard_Label aus Template"""
        self.title=self.generate_label();
        return self.title

Globals.InitializeClass(ECHO_resource)

def manage_addECHO_resourceForm(self):
        """Form for adding a ressource"""
        pt=zptFile(self, 'zpt/AddECHO_resourceForm.zpt')
        return pt()



def manage_addECHO_resource(self,id,title,label,description="",responsible="",link="",metalink="",weight="",copyrightType=None,resourceID=None,contentType=None,renderingType=None,credits=None,coords=None,RESPONSE=None):
    """addresource"""

    newObj=ECHO_resource(id,link,metalink,resourceID,title,label,description,contentType,renderingType,copyrightType,responsible,credits,weight,coords)

    self._setObject(id,newObj)

    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
 
