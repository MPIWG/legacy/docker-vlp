
"""preliminary object for displaying movies as part of a collection, shall be exchanged by a generalized ressource object"""

import string
import tempfile
import re
import os
import OFS.Image
from types import *
from OFS.Cache import Cacheable
from OFS.Image import Image
from Globals import DTMLFile
from OFS.Folder import Folder
from OFS.SimpleItem import SimpleItem
from AccessControl import ClassSecurityInfo
from AccessControl.User import UserFolder
from Globals import InitializeClass
from Globals import DTMLFile
import Globals
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
from Products.PageTemplates.PageTemplate import PageTemplate
from Products.PageTemplates.ZopePageTemplate import ZopePageTemplate
from Globals import Persistent, package_home
from Acquisition import Implicit

import urllib
import smtplib
import time
#from Ft.Xml.Domlette import NonvalidatingReader
#from Ft.Xml.Domlette import PrettyPrint
#from Ft.Xml import EMPTY_NAMESPACE

#import Ft.Xml.XPath
import cStringIO
import logging

try:
        from psycopg import libpq
except:
        try:
                from pyPgSQL import libpq
        except:
                print "ECHO_collection: Warning - No libpq imported!"
                
import xml.dom.minidom

import urllib
import xml.dom.minidom
import ECHO_helpers
from ECHO_helpers import *

class ECHO_movie(Folder,Persistent,ECHO_basis):
    """ECHO Ressource"""
    security=ClassSecurityInfo()
    meta_type='ECHO_movie'
    contentType='movie'
    
#    viewClassificationList=viewClassificationListMaster

    getSubCols = ECHO_helpers.getSubCols

    security.declareProtected('View','index_html')

    
  
    security.declareProtected('View','createPDF')
  

    def linkHTTP(self):
            """protokoll fuer link html"""
            
            splitted=self.link.split(":")
            if len(splitted)<2:
                    return self.link
            else:
                    splitted[0]="http"

            ret= ":".join(splitted)
            ret=ret.replace("//einst1","//einst3")
            return ret
    
    def linkRTSP(self):
            """protokoll fuer link html"""
            splitted=self.link.split(":")
            if len(splitted)<2:
                    return self.link
            else:
                    splitted[0]="RTSP"

            ret= ":".join(splitted)
            ret=ret.replace("//einst3","//einst1")
            return ret
    
    def lowresHTTP(self):
            """protokoll fuer link html"""
            if getattr(self,"lowresFile","")=="":
                    return None
            
            splitted=self.lowresFile.split(":")
            if len(splitted)<2:
                    return self.lowresFile
            else:
                    splitted[0]="http"

            ret= ":".join(splitted)
            ret=ret.replace("//einst1","//einst3")
            return ret
    
    def lowresRTSP(self):
            """protokoll fuer link html"""
            if getattr(self,"lowresFile","")=="":
                    return None
            
            splitted=self.lowresFile.split(":")
            if len(splitted)<2:
                    return self.lowresFile
            else:
                    splitted[0]="RTSP"

            ret= ":".join(splitted)
            ret=ret.replace("//einst3","//einst1")
            return ret
    

    def getRDF(self,urn=None):
            
            """rdf"""
            #ret=getRDFDescription(self,self.link,urn=urn)
            #return ret+self.createSubElementRDF(urn=urn) 
            if not urn:
                    urn=self.absolute_url()
            ret=""

            rettemp="""<rdf:Seq rdf:about="%s">\n"""%urn
            
            li="""<rdf:li rdf:resource="%s" />\n"""
            rettemp+=li%self.thumbUrl
            rettemp+=li%self.metalink
            rettemp+=li%self.rawFile
            rettemp+=li%getattr(self,'lowresFile','')
            rettemp+=li%self.link
            rettemp+="</rdf:Seq>"

            rettemp+=getRDFDescription(self,self.thumbUrl,nameDef="thumb",typeName="thumb",urn=self.thumbUrl)
            rettemp+=getRDFDescription(self,self.metalink,nameDef="metadata",typeName="ECHO_metaData",urn=self.metalink)
            rettemp+=getRDFDescription(self,self.rawFile,nameDef="raw",typeName="raw_movie",urn=self.rawFile)
            if not (getattr(self,'lowresFile','')==""):
                    rettemp+=getRDFDescription(self,self.lowresFile,nameDef="lores_movie",typeName="lores",urn=self.lowresFile)
            rettemp+=getRDFDescription(self,self.link,nameDef="hires",typeName="hires_movie",urn=self.link)
            return rettemp
    
    def getAccessRightSelectorHTML(self,outlook="select"):
            """htmlselector"""
            values=['free','mpiwg']
            
            if outlook=="select":
                    ret="""<select name="%s">"""%self.getId()

                    for value in values:
                            if value==self.getAccessRightMD():
                                    ret+="<option selected>%s</option>"%value
                            else:
                                    ret+="<option>%s</option>"%value
                    return ret+"</select>"

            else:
                    ret=""
                    for value in values:
                            
                            if value==self.getAccessRightMD():
                                    ret+="""<input type="radio" name="%s" value="%s" checked>%s"""%(self.getId(),value,value)
                            else:
                                    ret+="""<input type="radio" name="%s" value="%s">%s"""%(self.getId(),value,value)
                    return ret


    
    def getAccessRightMD(self):
                """set accessright"""
                url=self.metalink

                try:
                        geturl=""
                        for line in ECHO_helpers.urlopen(url).readlines():
                                geturl=geturl+line


                except:
                        return (None,"Cannot open: "+url)

                try:
                        dom=xml.dom.minidom.parseString(geturl)
                        root=dom.getElementsByTagName('resource')[0]
                except:
                        return (None,"Cannot parse: "+url+"<br>"+geturl)

                internal=dom.getElementsByTagName('internal')
                if internal:
                        institution=dom.getElementsByTagName('institution')
                        return getText(institution[0].childNodes)
                
                free=dom.getElementsByTagName('free')
                if free:
                        return "free"

                return "free" #default free

    def changeAccessRightMD(self,accessright,RESPONSE=None):
            """change the rights"""
            
            params="accessright=%s"%accessright


            #print ECHO_helpers.urlopen(self.absolute_url()+'/setAccessRightXML'+'?'+params).read()

            
            ECHO_helpers.urlopen('http://xserve02.mpiwg-berlin.mpg.de:18880/echo_nav/storage/downloadExternalXML?index_meta_url=%s&xml_url=%s'%(self.metalink,self.absolute_url()+'/setAccessRightXML'+urllib.quote('?'+params))).read()

          
            if RESPONSE is not None:
                        RESPONSE.redirect('manage_main')

                
    def setAccessRightXML(self,accessright):
                """set accessright"""
                url=self.metalink

                try:
                        geturl=""
                        for line in ECHO_helpers.urlopen(url).readlines():
                                geturl=geturl+line 


                except:
                        return (None,"Cannot open: "+url)

                try:
                        dom=xml.dom.minidom.parseString(geturl)
                        root=dom.getElementsByTagName('resource')[0]
                except:
                        return (None,"Cannot parse: "+url+"<br>"+geturl)

                metanodes=dom.getElementsByTagName('access-conditions')

                if not metanodes:
                        nodenew=dom.createElement('access-conditions')
                        root.appendChild(nodenew)
                        metanode=nodenew
                else:
                        metanode=metanodes[0]

                accesses=metanode.getElementsByTagName('access')

                if not accesses:
                        nodenew2=dom.createElement('access')
                        metanode.appendChild(nodenew2)
                        metanode2=nodenew2
                else:
                        metanode2=accesses[0]

                internal=metanode.getElementsByTagName('internal')

                if internal:
                        metanode2.removeChild(internal[0]).unlink()

                free=metanode.getElementsByTagName('free')
                
                if free:
                        metanode2.removeChild(internal[0]).unlink()


                if accessright=='free':
                        nodenew3=dom.createElement('free')
                        metanode2.appendChild(nodenew3)
                elif accessright=='mpiwg':
                        nodenew3=dom.createElement('internal')
                        nodenew4=dom.createElement('institution')
                        metanodetext=dom.createTextNode('mpiwg')
                        nodenew4.appendChild(metanodetext)
                        nodenew3.appendChild(nodenew4)
                        metanode2.appendChild(nodenew3)

                return dom.toxml().encode('utf-8')

  
  
    def copyTitleToInfoXML(self,RESPONSE=None):
            """copy title from the resource"""
            presentationXML=readFieldFromXML(self.metalink,'texttool','presentation')
            resourcepath=readFieldFromXML_xpath(self.metalink,'//resource/archive-path')
            if (not presentationXML) or (not resourcepath): 
                if RESPONSE:
                        RESPONSE.write("Error: %s\n"%self.getId())
                else:
                        return None,self.absolute_url()

            try:
                    fh=file(os.path.join(resourcepath,presentationXML),'w')
                    fh.write("""<info>
                    <author></author>
                    <title>%s</title>
                    <date></date>
                    <display>yes</display>
                    </info>"""%self.title)
                    fh.close()
                    return 1,self.getId()
            except:
                    if RESPONSE:
                            RESPONSE.write("Error: %s\n"%self.getId())
                    else:
                            return None,self.absolute_url()


   

    security.declarePublic('content_html')      
    def content_html(self):
        """template fuer content"""
        return ECHO_basis.content_html(self,'movie')
    
#    def getViewClassification(self):
#        if hasattr(self,'viewClassification'):
#            return self.viewClassification
#        else:
#            return ""

    
    def getCopyrightsHTML(self):
            """gib (link auf copyright link, mediatyp, institution, copyrightType, label von copyrightType) aus"""
            
            if hasattr(self,'copyrightModel'):
                    obj=self.copyrightModel
                    
            else:
                    return "ERROR"
            ret=[]
            
            for copyright in obj.getCopyrights(): #copyright - media / partner / copyrightID
                    
                    
                    try:
                        if hasattr(self.copyrightTypes,copyright[2]):
                             copyrightTypeObj=getattr(self.copyrightTypes,copyright[2])
                             link="copyrightTypes/"+copyright[2]+'/copyright.html'
                        else:
                             copyrightTypeObj=getattr(obj,copyright[2])
                             link="copyrightModel/"+copyright[2]+'/copyright.html'
                             
                        label=copyrightTypeObj.label
                        url=getattr(copyrightTypeObj, 'url', '')
                            
                        if url!='':
                                 ret.append((url,copyright[0],copyright[1],copyright[2],label))
                        else:
                                if hasattr(copyrightTypeObj, 'copyright.html'):
                                     ret.append(("""%s?partner=%s"""%(link,copyright[1]),copyright[0],copyright[1],copyright[2],label))
                                else:
                                     ret.append(('empty',copyright[0],copyright[1],copyright[2],label))
                    except:
                            """nothing"""
                    
            return ret
            
    def getInstitutionsHTML(self):
                """gibt Liste der foerdernden Institutionen aus"""
                
                if hasattr(self,'support'):
                        obj=self.support
                        ret=obj.getSupporter()
                        return ret
                else:
                        return ''
                        
    
    def getCredits(self):
        """Ausgabe der credits"""
        if self.credits:
            return self.credits
        else:
            return []


    
    def __init__(self,id,link,thumbUrl,rawFile,lowresFile,metalink,resourceID,title,label,description,contentType,renderingType,copyrightType,responsible,credits,weight,coords):

        self.id = id
        """Festlegen der ID"""
        
        self.label = label
        self.link= link
        self.metalink=metalink
        self.title=title
        self.weight=weight
        self.credits=toList(credits)
        self.description=description
        self.contentType=contentType
        self.copyrightType=copyrightType
        self.renderingType=renderingType
        self.responsible=responsible
        self.resourceID=resourceID
        self.thumbUrl=thumbUrl
        self.rawFile=rawFile
        self.lowresFile=lowresFile
        
        if coords:
            coordsnew=[ string.split(x,",") for x in coords]
        else:
            coordsnew=[]
        
        self.coords=coordsnew
#       self.viewClassification=""



    def getContentType(self):
            try:
                    return self.contentType
            except:
                    return ""

    def getCopyrightType(self):
            try:
                    return self.copyrightType
            except:
                    return ""

    def getRenderingType(self):
            try:
                    return self.renderingType
            except:
                    return ""

    def ECHO_movie_config(self):
        """Main configuration"""

        if not hasattr(self,'weight'):
            self.weight=""

        pt=zptFile(self, 'zpt/ChangeECHO_movie.zpt')
        return pt()
    

    def ECHO_movie_config_main(self):
        """Main configuration"""
        if not hasattr(self,'weight'):
            self.weight=""
        pt=zptFile(self, 'zpt/ChangeECHO_movie_main.zpt')
        return pt()

    def ECHO_movie_config_coords(self):
        """Coords configuration """
        pt=zptFile(self, 'zpt/ChangeECHO_movie_coords.zpt')
        return pt()

    def ECHO_movie_config_credits(self):
        """Main configuration"""
        pt=zptFile(self, 'zpt/ChangeECHO_movie_credits.zpt')
        return pt()

    def ECHO_resource_config_metadata(self):
        """Main configuration"""
        if (hasattr(self,'metadata')) and not (hasattr(self,'metaDataHash')):
                self.metaDataHash={}
                self.contentType=self.bib_type
                for data in self.metadata:
                        data_neu=re.sub('-','_',data)
                        self.meta
                        DataHash[data_neu]=getattr(self,data)[0:]

        
        pt=zptFile(self, 'zpt/ChangeECHO_resource_metadata.zpt')
        return pt()


    
    def changeECHO_movie_main(self,metalink,link,rawFile,lowresFile,thumbUrl,title,label,description,contentType,renderingType="",weight="",resourceID="",RESPONSE=None):
        """Aenderung der Properties"""
        self.resourceID=resourceID
        self.title=title
        self.label=label
        self.description=description

        self.contentType=contentType
        self.renderingType=renderingType
        self.weight=weight
        
        self.link=link
        self.thumbUrl=thumbUrl
        self.rawFile=rawFile
        self.lowresFile=lowresFile
       
        self.metalink=metalink
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def changeECHO_movie_coords(self,RESPONSE=None):
        """Aenderung der Properties - coords"""
        #return self.REQUEST
        for area in self.getMapAreas():
            id = area.getId()
            if self.REQUEST.has_key('del.'+id):
                # delete this area
                self._delObject(id)
                # return to same menu
                if RESPONSE is not None:
                    RESPONSE.redirect('ECHO_movie_config_coords')
                return
            # modify this area
            coordstring = self.REQUEST.get('coords.'+id, '')
            coords = string.split(coordstring, ',')
            angle = self.REQUEST.get('angle.'+id, '0')
            type = self.REQUEST.get('type.'+id, 'area')
            if len(coords) == 4:
                area.setCoordString(coordstring)
                area.setAngle(angle)
                area.setType(type)
        # return to main menu    
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def changeECHO_movie_credits(self,credits,responsible,copyrightType,RESPONSE=None):
        """Aenderung der Properties"""
        self.credits=credits
        self.responsible=responsible
        self.copyrightType=copyrightType
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')


    def changeECHO_resource_metadata_local(self,RESPONSE=None):
            """change metadata"""
            tags=self.findTagsFromMapping(self.contentType)
            for field in tags[1]:
                    self.metaDataHash[self.getFieldTag(tags,field)]=self.REQUEST.form[self.getFieldTag(tags,field)][0:]
                        

                    
            if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')

    def changeECHO_resource_metadata(self,RESPONSE=None):
            """change metadata"""
            tags=self.findTagsFromMapping(self.contentType)
            self.OSAS_meta={}
            for field in tags[1]:
                    try:
                            self.metaDataHash[self.getFieldTag(tags,field)]=self.REQUEST.form[self.getFieldTag(tags,field)]
                            self.OSAS_meta[self.getFieldTag(tags,field)]=self.REQUEST.form['OSAS_%s'%self.getFieldTag(tags,field)]
                    except:
                            """nothing"""

            ECHO_helpers.urlopen('http://xserve02.mpiwg-berlin.mpg.de:18880/echo_nav/storage/downloadExternalXML?index_meta_url=%s&xml_url=%s'%(self.metalink,self.absolute_url()+'/newMetaXML')).read()


            if RESPONSE is not None:
                    RESPONSE.redirect('manage_main')

    def getMDValue(self,fieldNameTest,empty=None,generic=None):
            """get md values"""
            #TODO: cache mappinghash
            
            fieldName=None
            if generic:
                ct=self.contentType.replace(" ","_").lower()
                
                #caching
                if not hasattr(self,'_v_mapHash'): #noch keine cachevariable
                    self._v_mapHash={}
                    
                tmp=self._v_mapHash.get(ct,None) 
                if tmp: #teste ob type schon existiert
                    fieldName=tmp.get(fieldNameTest,None)
                else:
                    self._v_mapHash[ct]={}
                    
                #noch nicht gecached    
                if not fieldName and hasattr(self.standardMD,ct):    
                    fieldName=getattr(self.standardMD,ct).generateMappingHash()[fieldNameTest][0]
                    self._v_mapHash[ct][fieldNameTest]=fieldName
                    
            if not empty:
                    return self.metaDataHash.get(fieldName,'!!NOT USED HERE in Type: %s'%self.contentType)
            
            else:
                    return self.metaDataHash.get(fieldName,empty)
            
    def newMetaXML(self,project=None,startpage=None,xslt=None,thumbtemplate=None,topbar=None,digiLibTemplate=None,digiliburlprefix=None):
            """new index.meta"""
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')

            if not hasattr(self,'metaDataHash'):
                
                    self.copyIndex_meta2echo_resource()
            try:
                    return writeMetadata(self.metalink,self.OSAS_meta,project,startpage,xslt,thumbtemplate,topbar,digiLibTemplate,digiliburlprefix=digiliburlprefix)
            except:
                    return writeMetadata(self.metalink,self.metaDataHash,project,startpage,xslt,thumbtemplate,topbar,digiLibTemplate,digiliburlprefix=digiliburlprefix)

    def showMetaDataXML(self,project=None,startpage=None,xslt=None,thumbtemplate=None,topbar=None,digiLibTemplate=None):
            """schreibe md"""
            
            try:
                    
                    return writeMetadata(self.metalink,self.metaDataHash,project,startpage,xslt,thumbtemplate,topbar,digiLibTemplate,xmlfrag="yes")
            except:
                    return "<error> no metadata stored</error>"
    
    def getMetaDataXML(self,project=None,startpage=None,xslt=None,thumbtemplate=None,topbar=None,digiLibTemplate=None):
            """prints out metadata as stored in the echo environment, format is the index.meta format"""
            self.REQUEST.RESPONSE.setHeader('Content-Type','text/xml')
            return writeMetadata(self.metalink,self.metaDataHash)

    def changeECHO_movie(self,metalink,link,thumbUrl,rawFile,lowresFile,title,label,description,contentType,responsible,weight,coords=None,credits=None,RESPONSE=None):
        """Aenderung der Properties"""
        
        setECHO_collectionInformation(self,title,label,description,contentType,responsible,credits,weight)
        self.link=link
        self.metalink=metalink
        self.thumbUrl=thumbUrl
        self.rawFile=rawFile
        self.lowresFile=lowresFile
        
        
        if RESPONSE is not None:
            RESPONSE.redirect('manage_main')
            
            
    manage_options = Folder.manage_options+(
        {'label':'Main Config','action':'ECHO_movie_config_main'},
        {'label':'Change Metadata','action':'ECHO_movie_config_metadata'},
        {'label':'Graphic Coords','action':'ECHO_graphicEntry'},
        {'label':'Sync Metadata','action':'ECHO_getResourceMD'},
        {'label':'Change TemplateSets and Image Viewer','action':'changeViewerTemplateSetForm'},
        {'label':'set/change startpage','action':'setStartPageForm'},
        {'label':'Copy MD for indexing and search','action':'copySearchFields'},
        )


    def isDefinedInThisSet(self,fields,field):
            """checks if field is defined in fields"""
            if (fields[0].has_key(field)) and not (fields[0][field]==""):
                    return 1
            else:
                  
                    return 0
            
    def getFieldLabel(self,fields,field):
        """get labels"""
        try:
            ret =fields[0][field]
            if ret == "":
                return field
            else:
                return ret
        except:
            return field



    def getFieldTag(self,fields,field):
        """get labels"""
        try:
            ret =fields[0][field]
            if ret == "":
                return field
            else:
                return ret
        except:
            return field

    

    def getMetaDataHash(self):
            """md hash"""
            return self.metaDataHash
    
    def setFieldValue(self,field,value):
        """get value"""
        
        if not hasattr(self,'metaDataHash'):
                setattr(self,'metaDataHash',{})
        self.metaDataHash[field]=value[0:]
        

    def copySearchFields(self):
            """copys metadatafields to the object"""
            fields=['author','title','year']
            for field in fields:
                    setattr(self,'MD_'+field,self.getMDValue(field))
    

    def findLabelsFromMapping(self,referenceType):
        """gib hash mit label -> generic zurueck"""
        #return {},[]

        temp=self.ZopeFind(self.standardMD)

        if referenceType=="":
                referenceType="book"
        
        
        bibdata={}
        retdata={}
        fields=[]
        fieldlist=self.standardMD.fieldList

        tags=self.findTagsFromMapping(self.contentType)
        self.referencetypes=tags[2]
        for referenceTypeF in self.referencetypes:

                if referenceTypeF[1].title.lower() == referenceType.lower():

                        try:
                                bibdata[referenceTypeF[1].title]=referenceTypeF[1].fields
                                referenceType=referenceTypeF[1].title
                        except:
                                bibdata[referenceType]=referenceTypeF[1].fields
                

                        bibdata['data']=referenceTypeF[1]
                        fields=bibdata[referenceType]
                        for field in fieldlist:
                            retdata[field]=referenceTypeF[1].getValue(field)[1]

        return retdata,fieldlist,temp,fields

    def findTagsFromMapping(self,referenceType):
        """gib hash mit label -> generic zurueck"""
        

        if referenceType=="":
                referenceType="book"
        
        temp =  self.ZopeFind(self.standardMD)[0:]

        
        #self.referencetypes=temp[0:]
        
        
        

        

        bibdata={}
        retdata={}
        fieldlist=self.standardMD.fieldList
        fields=[]
        for referenceTypeF in temp:
                #print referenceType
        
                if referenceTypeF[1].title.lower() == referenceType.lower(): 
                        try:
                                bibdata[referenceTypeF[1].title]=referenceTypeF[1].fields
                                referenceType=referenceTypeF[1].title
                        except:
                                bibdata[referenceType]=referenceTypeF[1].fields
                        bibdata['data']=referenceTypeF[1]
                        fields=bibdata[referenceType]
                        for field in fieldlist:
                            retdata[field]=referenceTypeF[1].getValue(field)[0]
        
        return retdata,fieldlist,temp,fields

    
    security.declarePublic('copyIndex_meta2echo_resource') # has to be change, presentlyset because of OSAS koordination
    def copyIndex_meta2echo_resource(self,RESPONSE=None):
            """copy MD von Index_meta to the echo_resource"""
            
            (metadict, error)=readMetadata(self.metalink)


            
            self.metaDataHash={}
            if not error=="": #Fehler beim Auslesen des Metafiles
                    
                    return "ERROR:",error,self.absolute_url()
            
            self.contentType=metadict['bib_type'][0:]
            fields=self.findTagsFromMapping(self.contentType)
            
            #fields=self.findLabelsFromMapping(self.contentType)
            for field in fields[1]:
                    
                    if self.isDefinedInThisSet(fields,field):
                            #print urllib.unquote(metadict.get(self.getFieldTag(fields,field),''))
                            self.setFieldValue(self.getFieldTag(fields,field),metadict.get(self.getFieldTag(fields,field),''))

            
            
            if RESPONSE:
                    return RESPONSE.redirect('manage_main')
            
    def ECHO_getResourceMD(self,template="yes",back=None):
        """Einlesen der Metadaten und Anlegen dieser Metadaten als Informationen zur Resource"""
        (metadict, error)=readMetadata(self.metalink)

        if back:
            self.REQUEST.SESSION['back']=back

        if not error=="": #Fehler beim Auslesen des Metafiles
                return "ERROR:",error
        

        if not (metadict['bib_type'].lower()==self.contentType.lower()):
                self.REQUEST.SESSION['contentStorage']=metadict['bib_type']
                self.REQUEST.SESSION['contentZope']=self.contentType

                return zptFile(self, 'zpt/ECHO_getResourceMDErrorContentType.zpt')()

        self.REQUEST.SESSION['metadict']=metadict

        
        
        self.REQUEST.SESSION['diffs']=checkDiffs(self,self.REQUEST.SESSION['metadict'])
        

                
        if template=="yes":
                pt=zptFile(self, 'zpt/ECHO_resourceMD.zpt')
                return pt()

        

    
    
    def ECHO_getMD(self,item):
        """Ausgabe der MD"""
        return getattr(self,item)

    def checkRDF(self,path):
            """check if pdf in the path"""
            try:
                    for fileName in os.listdir(path):
                            if os.path.splitext(fileName)[1]==".pdf":
                                    return os.path.join(path,fileName)
                    return None
            except:
                    return None
            
            
    def index_html(self):
        """standard page"""
        pdf=self.checkRDF(self.link)
        if pdf:
                fh=file(pdf,'r').read()
                self.REQUEST.RESPONSE.setHeader('Content-Type','application/pdf')
                self.REQUEST.RESPONSE.write(fh)
                self.REQUEST.RESPONSE.close()
                return 
        return self.REQUEST.RESPONSE.redirect(self.link)


    security.declarePublic('generate_label') # has to be change, presentlyset because of OSAS koordination

    def generate_label(self):
        """Erzeugt_standard_Label aus Template"""
        try:
                pt=getattr(self,"label_template_"+self.contentType.lower())
                self.label=pt()[0:]
                return pt()
        except:
                return self.label

    security.declarePublic('generate_title') # has to be change, presentlyset because of OSAS koordination

    def generate_title(self,RESPONSE=None):
        """Erzeugt_standard_Label aus Template"""
        try:
                pt=getattr(self,"label_template_"+self.contentType.lower())
                self.title=pt()[0:]
                return pt()
        except:
                return self.label
        
Globals.InitializeClass(ECHO_movie)

def manage_addECHO_movieForm(self):
        """Form for adding a ressource"""
        pt=zptFile(self, 'zpt/AddECHO_movieForm.zpt')
        return pt()



def manage_addECHO_movie(self,id,title,label,description="",responsible="",link="",thumbUrl="",rawFile="",lowresFile="",metalink="",weight="",copyrightType=None,resourceID=None,contentType=None,renderingType=None,credits=None,coords=None,RESPONSE=None):
    """addaresource"""

    newObj=ECHO_movie(id,link,thumbUrl,rawFile,lowresFile,metalink,resourceID,title,label,description,contentType,renderingType,copyrightType,responsible,credits,weight,coords)

    self._setObject(id,newObj)

    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
 
