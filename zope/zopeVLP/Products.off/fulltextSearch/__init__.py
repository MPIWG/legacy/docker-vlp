import ZopeFulltextSearch
import ZopeOCRFulltextSearch
import ZopeOCRServerInterface

def initialize(context):
    """xmlrpc server proxy"""

    context.registerClass(
       ZopeFulltextSearch.ZopeFulltextSearch,
        constructors = (
          ZopeFulltextSearch.manage_addZopeFulltextSearchForm,
          ZopeFulltextSearch.manage_addZopeFulltextSearch
          )
        )
    
    context.registerClass(
       ZopeOCRFulltextSearch.ZopeOCRFulltextSearch,
        constructors = (
          ZopeOCRFulltextSearch.manage_addZopeOCRFulltextSearchForm,
          ZopeOCRFulltextSearch.manage_addZopeOCRFulltextSearch
          )
        )
    
    context.registerClass(
       ZopeOCRServerInterface.ZopeOCRServerInterface,
        constructors = (
          ZopeOCRServerInterface.manage_addZopeOCRServerInterfaceForm,
          ZopeOCRServerInterface.manage_addZopeOCRServerInterface
          )
        )