
from OFS.SimpleItem import SimpleItem
import xmlrpclib
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
import os.path
from Globals import package_home
import re
import amara
import logging

class ZopeFulltextSearch(SimpleItem):
    """makes servery proxy available for zope"""
    
    meta_type="ZopeFullTextSearch"
    
    manage_options= (
        {'label':'Main Config','action':'mainConfigHTML'},
        )+ SimpleItem.manage_options

    def __init__(self,id,serverUrl):
        """init server proxy"""
        self.id=id
        self.serverUrl=serverUrl
        
    def getServerProxy(self,serverUrl=None):
        """get the proxy"""
        if not serverUrl:
            serverUrl=self.serverUrl
      
        pr=xmlrpclib.ServerProxy(serverUrl)
       
        return pr
    
    def search(self,string,exactSearch="no"):
        """do proxy request"""
        #only letters are allowed
        ret={}
        pr = self.getServerProxy();
        if exactSearch!="yes":
            string="contentsNormalized:"+string
            
            resultXML= pr.FulltextSearchXML.searchAndAnalyse(string)
        else:
            resultXML= pr.FulltextSearchXML.search(string)
        doc = amara.parse(resultXML);
        #logging.error("xxx:"+resultXML);
        resultsList= doc.searchresult.results
        #logging.error("XXX:"+repr(resultsList))
        for results in resultsList:
            lang=results["lang"]
            
            if hasattr(results,'result'):
                for result in results.result:
                    path=result["path"]
                    term=result["term"]
                    if not ret.has_key(lang):
                        ret[lang]=[]
                    ret[lang].append((term,path))
        return ret
            
    def getTextId(self,string):
        """get numbers behind lit in path"""
        x=re.match(".*lit([0-9]+)",string)
        try:
            nr=x.group(1)
        except:
            nr=""
        return nr
    
    def getPage(self,string):
        """get pageName"""
        pagefile=os.path.split(string)[1]
        return os.path.splitext(pagefile)[0]
        
    def sortPagesById(self,results):
        ret={}
        for resultTuple in results:
            result=resultTuple[1];
            textId = self.getTextId(result)
            if not ret.has_key(textId):
                ret[textId]=[]
            ret[textId].append(self.getPage(result))
            ret[textId].sort();
        return ret
                               
    def getWordsInPage(self,entry,pageNr,searchString):
        """get alls words found in pageNr of Text entry"""
        pr = self.getServerProxy();
                    
        resultXML= pr.FulltextSearchXML.searchForMorph("/*lit"+entry+"/*"+pageNr+"*",searchString);
        #logging.error("rest"+repr(resultXML));
        return resultXML
       
        
    def mainConfigHTML(self):
        """main config form"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','manageZopeFulltextSearch.zpt')).__of__(self)
        return pt()
    
    def mainConfig(self,serverUrl,RESPONSE=None):
        """main config"""
        
        self.serverUrl=serverUrl
        
        
        if RESPONSE:
            RESPONSE.redirect("/manage_main")
            
    def generateImagePath(self,textpath,imageFolderPath,textFolderPath=None,cut=None):
        """erzeuge aus einem Pfad auf den Text mit TExtname xxxx.xml --> bildname xxxxx"""
        if textFolderPath is not None:
            tmp =textpath.replace(textFolderPath,"")
            splitted=tmp.split("/")
            pfad="/".join(splitted[0:-1])
            pfad=os.path.join(pfad,imageFolderPath,splitted[-1])
        else:
            tmp=self.correctPath(textPath, "/Volumes/data/mpiwg/online", "", cut)
            splitted=textPath.split("/")
            pfad=os.path.join(tmp,imageFolderPath,splitted[-1])
            
        return pfad

    def correctPath(self,path,remove=None,prefix=None,cut=0):
        """convinience method um einen pfad zu veraendern"""
        
        if remove is not None:
            path=path.replace(remove,'')
        if prefix is not None:
            path=os.path.join(prefix,path)
        
        if cut>0:
            splitted=path.split("/")
            path="/".join(splitted[0:len(splitted)-cut])
        return path
    
    def unicodify(s):
        """decode str (utf-8 or latin-1 representation) into unicode object"""
        if not s:
            return u""
        if isinstance(s, str):
            try:
                return s.decode('utf-8')
            except:
                return s.decode('latin-1')
        else:
            return s
        
    def utf8ify(s):
        """encode unicode object or string into byte string in utf-8 representation\
    .                                                                               
           assumes string objects to be utf-8"""
        if not s:
            return ""
        if isinstance(s, str):
            return s
        else:
            return s.encode('utf-8')

        
        

def manage_addZopeFulltextSearchForm(self):
    """add the CXMLRpcServerProxy"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addZopeFulltextSearch.zpt')).__of__(self)
    return pt()

def manage_addZopeFulltextSearch(self,id,serverUrl,RESPONSE=None):
    """add the basket"""
    ob=ZopeFulltextSearch(id,serverUrl)
    
    self._setObject(id, ob)
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')