"""This Product is a zope backend which connects to the FulltextSearchServlet 
de.mpiwg.itgroup.fulltect.ocropus.OCRFulltextSeachXML or
de.mpiwg.itgroup.fulltext.harvestet.FulltextSearch
 """

from OFS.SimpleItem import SimpleItem
import xmlrpclib
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
import os.path
from Globals import package_home
import re
import amara
import logging
import urllib
import socket 
import time
import Ft
import types

TEXTERPATH="http://nausikaa2.mpiwg-berlin.mpg.de/digitallibrary/servlet/Texter?" # Url of the text servlet


class ZopeOCRFulltextSearch(SimpleItem):
    """makes Fulltextserver available for zope"""
    
    meta_type="ZopeOCRFullTextSearch"
    
    manage_options= (
        {'label':'Main Config','action':'mainConfigHTML'},
        )+ SimpleItem.manage_options

    def __init__(self,id,serverUrl,searchMethod,indexName):
        """init server proxy"
        @param id: Zope id 
        @param serverUrl:  URL of the FulltextSearchXML xmlrpc server
        @param searchMethod: search method the server shall use, defined in XmlRpcServler.properties of the servlet
        @param indexName: Name of the  Lucene index to be used (as configured in the config file for the servlet)
        """
        self.id=id
        self.serverUrl=serverUrl
        self.searchMethod=searchMethod
        self.indexName=indexName

    
    def createDelayedRedirect(self,newlocation,time):
        """
        Creates the redirection javascript code
        @param newlocation: new location for the redirect
        @param time: time in milliseconds until the redirection should be permformed
        """
        newlocation=newlocation.replace("&amp;","&")
        logging.error("newLocation:"+newlocation)
        ret = """ function redirect () { window.location="%s" } """%newlocation
        ret += """window.setTimeout("redirect()",%s)"""%time
        return ret
        
        
    def analyseSearchResultXML(self, nopages, showpages, resultXML,dcMD=None):
        """analyses the resultXML file from the servlet
        @type param: boolean
        @param nopages: if true page numbers are not displayed
        @param showpages: show the pagenumber of the hints for the file(s) in showpages, contains string/list of strings with the filenames
         (field "path" in lucene),
        @param resultXML: result XML file from the xmlrpcserver
        @dcMD dcMD: if set contains already the hash for the metadata, metadata analyses from resultXML is not performed and the old value is taken
        @return: return  dcMD, numberOfPages, ret i.e. metadata, numberOfPages with hits and list of pages for each language and documents
        structurue is e.g. ret[lang][documentPath] = ...
        """
        ret = {}
        numberOfPages = {}
        
        dcMDTmp=dcMD
        
        if dcMDTmp is not None:
            dcMD = dcMDTmp
        else:
            dcMD = {}
            
        doc = amara.parse(resultXML.encode('utf-8'))
        logging.debug(doc.xml())
        resultsList = doc.searchresult.results
        #logging.error("XXX:"+repr(resultsList))
        for results in resultsList:
            lang = results["lang"]
            if hasattr(results, 'resultsShort'):
                resultsShort = results.resultsShort
                if (dcMDTmp is None):
                    dcmds = self.unpackMD(resultsShort.dcMetaDatas)
                retLang, noOfPages = self.unpack(resultsShort.lines, resultsShort.boxes, resultsShort.dims, nopages=nopages, showpages=showpages)
                #retLang,noOfPages = self.unpack(resultsShort.lines,resultsShort.boxes,resultsShort.dims)
                #logging.error(retLang)
                #logging.error(noOfPages)
                ret[lang] = retLang
                numberOfPages[lang] = noOfPages
                if (dcMDTmp is None):
                    dcMD[lang] = dcmds
            
        
        return dcMD, numberOfPages, ret

        
    def getLineMark(self, line):
        """ creates the line marks for digilib from the bbox information in the ocr file
        @param line: triple (nr,bbox,dim) with nr line number, bbox  bbox form ocr and dim from OCR
        @return: xrel, yrel relatice koordiantes of the line suitable for digilib
        """
        #logging.error("line:" + repr(line))
        nr, bbox, dim = line
        
        #nimm von der box die ersten beide koordinaten al marker
        bSplitted = bbox.split(" ")
        dSplitted = dim.split(" ")
        # erstmal nur die erste Marke anzeigen TODO: mehrerer
        #logging.error("bspl:%s" % repr(bSplitted))
        #logging.error("dspl:%s" % repr(dSplitted))
        xrel = float(bSplitted[0]) / float(dSplitted[0])
        yrel = float(bSplitted[1]) / float(dSplitted[1])
        return xrel, yrel

        
    def getServerProxy(self,serverUrl=None):
        """get the proxy"""
        if not serverUrl:
            serverUrl=self.serverUrl
      
        logging.debug(serverUrl)
        pr=xmlrpclib.ServerProxy(serverUrl)

        return pr
    
#    def isFinishedThread(self,ticket):
#        """ticker finished?"""
#        pr= self.getServerProxy();
#        searchMethod=getattr(self,"searchMethod","OCRFulltextSearch")
#        return getattr(pr,searchMethod).isFinishedThread(ticket)
#       
       
#    def getSearchResultsFromThread(self,ticket):
#        """give the results back"""
#        pr= self.getServerProxy();
#        searchMethod=getattr(self,"searchMethod","OCRFulltextSearch")
#        resultXML=getattr(pr,searchMethod).getResultFromThread(ticket)
#        
#        doc = amara.parse(resultXML);
#        logging.error("xxx:"+resultXML);
#        resultsList= doc.searchresult.results
#        #logging.error("XXX:"+repr(resultsList))
#        for results in resultsList:
#            lang=results["lang"]
#            
#            if hasattr(results,'result'):
#                for result in results.result:
#                    path=result["path"]
#                    term=result["term"]
#                    if not ret.has_key(lang):
#                        ret[lang]=[]
#                        
#                    
#                    linesToPath=[]
#                    
#                    if hasattr(result,'line'):
#                        
#                        for line in result.line:
#                            nr=line["nr"]
#                            bbox=line["bbox"]
#                            dim=line["pageDimension"]
#                            
#                            linesToPath.append((nr,bbox,dim))
#                            
#                        ret[lang].append((term,path,linesToPath))
#        
#    def startSearchThread(self,_searchString,_exactSearch="no",RESPONSE=None,REQUEST=None):
#        """started eine Suchthread auf dem OCR Server"""
#        logging.error("HUHUH")
#        pr= self.getServerProxy();
#        searchMethod=getattr(self,"searchMethod","OCRFulltextSearch")
#        
#        if _exactSearch!="yes":
#            string="contentsNormalized:"+string
#            
#            #resultXML= getattr(pr,searchMethod).searchAndAnalyse(string)
#        else:
#            ticket=  getattr(pr,searchMethod).searchThreaded(_searchString,self.indexName)
#            
#        if RESPONSE:
#            
#            RESPONSE.redirect(REQUEST['HTTP_REFERER']+'?_ticket='+ticket)
#        return ticket
#    
    
    def search(self,string,exactSearch="no"):
        """search for the string, no thread is started on the server, waits for the result
        and makes a full analysis of the searchresult in XML, answering this requestis takes a long time at the
        side of the servlet, therefore the analysis is faster at the zope site.
        @param string: searchString
        @exact search: if not "yes" the field contentsNormalized is used and the searchString is normalized before searching
        """
        #only letters are allowed
        ret={}
        pr = self.getServerProxy();
        
       
        socket.setdefaulttimeout(30)
        searchMethod=getattr(self,"searchMethod","OCRFulltextSearchXML")
        
        if exactSearch!="yes":
            string="contentsNormalized:"+string
            
            resultXML= getattr(pr,searchMethod).searchAndAnalyse(string,self.indexName)
        else:
            resultXML=  getattr(pr,searchMethod).search(string,self.indexName)
        #logging.debug(resultXML)
        doc = amara.parse(resultXML);
        
        
        
        #logging.error("xxx:"+resultXML);
        resultsList= doc.searchresult.results
        #logging.error("XXX:"+repr(resultsList))
        for results in resultsList:
            lang=results["lang"]
            
            if hasattr(results,'result'):
                for result in results.result:
                    path=result["path"]
                    term=result["term"]
                    if not ret.has_key(lang):
                        ret[lang]=[]
                        
                    
                    linesToPath=[]
                    
                    if hasattr(result,'line'):
                        
                        for line in result.line:
                            nr=line["nr"]
                            bbox=line["bbox"]
                            dim=line["pageDimension"]
                            
                            linesToPath.append((nr,bbox,dim))
                            
                        ret[lang].append((term,path,linesToPath))
        
                    
         
                
        #logging.error("search:"+repr(ret))
        return ret
          
    
    def searchShortThread(self,string,exactSearch="no",nopages=False,SESSION=None,showpages='',mdstring="",languages=[]): 
        """search for the string, a thread is started on the server, requests not for a full xml-file but for 
        a short form, where the results are transmitted as a text file (i.e. string representation of java hashmap)
        @param string: searchString
        @exact search: if not "yes" the field contentsNormalized is used and the searchString is normalized before searching
        @param nopages: if true page numbers are not displayed
        @param showpages: show the pagenumber of the hints for the file(s) in showpages, contains string/list of strings with the filenames
         (field "path" in lucene),
        @return: ticketnumber for the server thread
        """
        #logging.debug("showpages:"+showpages)
        #logging.debug("SESSION"+repr(SESSION))
        
        #only letters are allowed
        if type(languages) is not types.ListType:
            languages=[languages]
            
        if (not (showpages == '')) and  SESSION and SESSION.has_key("sresult"):
                ret = SESSION["sresult"]
                resultXML = SESSION["sresultXML"]
                numberOfPages=SESSION["snumOfPages"]
                dcMD=SESSION["sdcMD"]
                
                dcMD,numberOfPages,ret = self.analyseSearchResultXML(nopages, showpages, resultXML,dcMD=dcMD)
                return True,ret,numberOfPages,dcMD
                
     
        else:    
                ret={}
                numberOfPages={}
                dcMD={}
                pr = self.getServerProxy();
                
               
                socket.setdefaulttimeout(30)
                searchMethod=getattr(self,"searchMethod","OCRFulltextSearchXML")
                
                if exactSearch!="yes":
                    string="contentsNormalized:"+string
                    
                    resultXML= getattr(pr,searchMethod).searchAndAnalyse(string,self.indexName)
                  
                else:
                    logging.debug(string)
                    logging.debug(self.indexName)
                    logging.debug(self.searchMethod)
                    if mdstring=="":
                        ticket=  getattr(pr,searchMethod).searchShortThreaded(string,self.indexName,languages)
                    else:
                        logging.debug("langs:"+repr(languages))
                        ticket=  getattr(pr,searchMethod).searchShortMDThreaded(string,mdstring,self.indexName,languages)
                #logging.debug(resultXML)
        
                #logging.error("xxx:"+resultXML);
        return False,ticket,"",""
    
    def getSearchShortThread(self,ticket,SESSION=None,nopages=False,showpages='',languages=[]):
        """
        @param nopages: if true page numbers are not displayed
        @param showpages: show the pagenumber of the hints for the file(s) in showpages, contains string/list of strings with the filenames
         (field "path" in lucene),
        @param ticke: ticket numvber
        gots the results for the ticket form the server, if the threat is not finished the ticket number is returned
        otherwise:
        @return: return  ret, numberOfPages, dcMd i.e. metadata, numberOfPages with hits and list of pages for each language and documents
        structurue is e.g. ret[lang][documentPath] = ...
        """
        
        
        pr = self.getServerProxy();
        logging.debug("getTicket:"+repr(ticket))
       
        socket.setdefaulttimeout(30)
        searchMethod=getattr(self,"searchMethod","OCRFulltextSearchXML")
        
        resultXML=  getattr(pr,searchMethod).getSearchResult(ticket)
        #logging.debug("result:"+repr(resultXML))
      
        if resultXML.startswith("WAIT"):
            splitted=resultXML.split(":")
            
            return False,ticket,splitted[1],splitted[2]
        
        
        dcMD,numberOfPages,ret = self.analyseSearchResultXML(nopages, showpages, resultXML)
                    
        if SESSION:
                    SESSION["sresult"]=ret
                    SESSION["sresultXML"]=resultXML
                    SESSION["snumOfPages"]=numberOfPages
                    SESSION["sdcMD"]=dcMD
                    
        return True,ret,numberOfPages,dcMD
        
    def getSupportedLanguages(self):
        pr = self.getServerProxy();
      
        socket.setdefaulttimeout(30)
        searchMethod=getattr(self,"searchMethod","OCRFulltextSearchXML")
        
        res=  getattr(pr,searchMethod).getSupportedLanguages()
        
        logging.debug("languages:"+repr(res))
        return res
    
    def searchShort(self,string,exactSearch="no",nopages=False,SESSION=None,showpages=''):
        
        """do proxy request"""
        logging.debug("showpages:"+showpages)
        #only letters are allowed
        if not (showpages == ''): # showpage ist gesetzt, gibt es schon ein ergebnis im cache dann nimm das.
           
            if SESSION and SESSION.has_key("sresult"):
                ret = SESSION["sresult"]
                resultXML = SESSION["sresultXML"]
                numberOfPages=SESSION["snumOfPages"]
                dcMD=SESSION["sdcMD"]
               
        else:    
                ret={}
                numberOfPages={}
                dcMD={}
                pr = self.getServerProxy();
                
               
                socket.setdefaulttimeout(30)
                searchMethod=getattr(self,"searchMethod","OCRFulltextSearchXML")
                
                if exactSearch!="yes":
                    string="contentsNormalized:"+string
                    
                    resultXML= getattr(pr,searchMethod).searchAndAnalyse(string,self.indexName)
                else:
                    logging.debug(string)
                    logging.debug(self.indexName)
                    logging.debug(self.searchMethod)
                    resultXML=  getattr(pr,searchMethod).searchShort(string,self.indexName)
                #logging.debug(resultXML)
        
                #logging.error("xxx:"+resultXML);
                dcMD, numberOfPages, ret = self.analyseSearchResultXML(nopages, showpages, resultXML)
                
        if SESSION:
                    SESSION["sresult"]=ret
                    SESSION["sresultXML"]=resultXML
                    SESSION["snumOfPages"]=numberOfPages
                    SESSION["sdcMD"]=dcMD
                    
        return ret,numberOfPages,dcMD
            
    def unpackMD(self,metaDatas):
        """analyses the metadata result transmitted in short form from the servlet and created a hash from this
        @param metaDatas: metadatas transmitted from the servlet
        """
        children = metaDatas.xml_children
        ret ={}
        for i in range(len(children)/2):
            path=unicode(children[i*2])
            path = "/".join(path.split("/")[0:-2])
            #logging.error(path)
            pathMatch =re.match(".*(permanent.*)",path)
            path = pathMatch.group(1)
            md=children[i*2+1]
            #logging.debug(md.xml())
            ret[path]=md.xml()
            
        return ret
        
    def unpack(self,lines,boxes,dims,nopages=False,showpages=''):
        """analyses the lines,boxes, dims result transmitted in short form from the servlet and created a hash from this
        @param lines:
        @param boxes:
        @param dims: values transmitted from the servlet
        @type nopages: boolean
        @param nopages: if true page numbers are not displayed
        @param showpages: show the pagenumber of the hints for the file(s) in showpages, contains string/list of strings with the filenames
         (field "path" in lucene),
        """
        def ts(x,y):
            return cmp(x[0],y[0])
        
        #logging.info("unpack:"+repr(nopages))
        t1 = time.time();
        search = re.compile("(/.*?)=\[([0-9, ]*)\]") # file transmitted has the text form of hashmap "key = value", values can only be numbers
        linesMatches = search.finditer(unicode(lines));
        boxesMatches = search.finditer(unicode(boxes));
        dimsMatches = search.finditer(unicode(dims));
        
        ret = {}
        numOfPages={}
        for linematch in linesMatches:
            txtPath = linematch.group(1)
            lines = linematch.group(2).split(",")
            
            boxes = boxesMatches.next().group(2).split(",")
            dims = dimsMatches.next().group(2).split(",")
            
            path = "/".join(txtPath.split("/")[0:-2])
            pathMatch =re.match(".*(permanent.*)",path)
            path = pathMatch.group(1)
            logging.debug("path:"+path)
       
            
                  
           
            logging.info("unpack:"+path)
            for i in range(len(lines)):
                line, box, dim = lines[i],boxes[i],dims[i]
                
                if not ret.has_key(path):
                    ret[path]={}
                    #logging.error(lines)
                    #logging.error(len(lines))
                    #numOfPages[path]=len(lines);
                    numOfPages[path]=0;
                    
                page = self.getPage(txtPath);
                if not ret[path].has_key(page):
                    ret[path][page]=[]
                if (not nopages) or (path==showpages):
                             
                    ret[path][page].append(self.getLineMark((line.lstrip(),box.lstrip(),dim.lstrip())))
                    numOfPages[path]+=1;  
                else:
                    pass
                    numOfPages[path]+=1;                      
                 
                    
#            
#            if (not nopages) or (path==showpages):
#                logging.info("unpack:"+path)
#                for i in range(len(lines)):
#                    line, box, dim = lines[i],boxes[i],dims[i]
#                    
#                    if not ret.has_key(path):
#                        ret[path]={}
#                        numOfPages[path]=len(lines);
#                        
#                    page = self.getPage(txtPath);
#                    if not ret[path].has_key(page):
#                        ret[path][page]=[]
#                    
#                    ret[path][page].append(self.getLineMark((line.lstrip(),box.lstrip(),dim.lstrip())))
#            else:
#                if not ret.has_key(path):
#                    ret[path]={}
#                    numOfPages[path]=len(lines);
#                     
                #page = self.getPage(txtPath);    
                #if not ret[path].has_key(page):
                #    ret[path][page]=lines
              
        t2= time.time()- t1;
        logging.debug("time for unpack: %s"%t2)
        #logging.debug("RET:"+repr(ret))
        return ret,numOfPages
                
    def getTextId(self,string):
        """get numbers behind lit in path
        conveninience function for the use in the VLP
        """
        x=re.match(".*lit([0-9]+)",string)
        try:
            nr=x.group(1)
        except:
            nr=""
        return nr
    
    def getPage(self,string):
        """get pageName"""
        pagefile=os.path.split(string)[1]
        return os.path.splitext(pagefile)[0]
        
        
    def sortPagesById(self,results):
        def ts(x,y):
            return cmp(x[0],y[0])
        
        ret={}
        for resultTuple in results:
            result=resultTuple[1];
            textId = self.getTextId(result)
            if not ret.has_key(textId):
                ret[textId]=[]
            #logging.error("resTuple:"+repr(resultTuple))
            ret[textId].append((self.getPage(result),self.getLineMarks(resultTuple[2])))
            ret[textId].sort(ts);
            
        return ret
    
    def sortPagesByPath(self,results):
        def ts(x,y):
            return cmp(x[0],y[0])
        
        ret={}
        for resultTuple in results:
            result=resultTuple[1];
        
            if not ret.has_key(result):
                ret[result]=[]
            #logging.error("resTuple:"+repr(resultTuple))
            ret[result].append((self.getPage(result),self.getLineMarks(resultTuple[2])))
            ret[result].sort(ts);
            
        return ret
    
    
    def generateImagePath(self,textpath,imageFolderPath,textFolderPath=None,cut=None):
        """erzeuge aus einem Pfad auf den Text mit TExtname xxxx.xml --> bildname xxxxx"""
        if textFolderPath is not None:
            tmp =textpath.replace(textFolderPath,"")
            splitted=tmp.split("/")
            pfad="/".join(splitted[0:-1])
            pfad=os.path.join(pfad,imageFolderPath,splitted[-1])
        else:
            tmp=self.correctPath(textpath, "/Volumes/data/mpiwg/online", "", cut)
            splitted=textpath.split("/")
            pfad=os.path.join(tmp,imageFolderPath,splitted[-1])
            
        
        pfad=pfad.replace("///","/")
        pfad=pfad.replace("//","/")

        return pfad

    def correctPath(self,path,remove=None,prefix=None,cut=0):
        """convinience method um einen pfad zu veraendern"""
        
        if remove is not None:
            path=path.replace(remove,'')
        if prefix is not None:
            path=os.path.join(prefix,path)
        
        if cut>0:
            splitted=path.split("/")
            path="/".join(splitted[0:len(splitted)-cut])
        return path     
    def getLineMarks(self,lines):
        marks=[]
        
        for line in lines:
            xrel,yrel = self.getLineMark(line)
            
            marks.append((str(xrel),str(yrel)))
        return marks
        
    def generateMarkString(self,marks):
        """erzeugt den Parameterstring fuer mk fuer digilib"""
        ret=""
        #logging.error("marks:"+repr(marks))
        for mark in marks:
            if not (ret==""):
                ret+=","

            ret+=str(mark[0])+'/'+str(mark[1])
            
        return ret
            
    def getWordsInPage(self,entry,pageNr,searchString):
        """get alls words found in pageNr of Text entry"""
        pr = self.getServerProxy();
                    
        resultXML= pr.FulltextSearchXML.searchForMorph("/*lit"+entry+"/*"+pageNr+"*",searchString);
        #logging.error("rest"+repr(resultXML));
        return resultXML
       
        
    def mainConfigHTML(self):
        """main config form"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','manageZopeOCRFulltextSearch.zpt')).__of__(self)
        return pt()
    
    def mainConfig(self,serverUrl,searchMethod,indexName,RESPONSE=None):
        """main config"""
        
        self.serverUrl=serverUrl
        self.searchMethod=searchMethod
        self.indexName=indexName
        
        if RESPONSE:
            RESPONSE.redirect("/manage_main")
            

    def showXMLText(self,path,replaceStr):
        """erzeugt aus dem xml von ocropus ein xml-File zur Ansicht"""
        
        url=path.replace(replaceStr,"")
        
        uo=urllib.urlopen(TEXTERPATH+"fn=%s"%url);
        txt=uo.read();
        self.REQUEST.response.setHeader('Content-Type','text/plain');
        return txt;
        
    def getListOfDocuments(self):
        """get list of all docs"""
        ret={}
        pr = self.getServerProxy();
        
        socket.setdefaulttimeout(30)
        searchMethod=getattr(self,"searchMethod","OCRFulltextSearch")
        searcher=getattr(pr,searchMethod)
        return searcher.getListOfDocuments()
        
    def unicodify(self,s):
        """decode str (utf-8 or latin-1 representation) into unicode object"""
        if not s:
            return u""
        if isinstance(s, str):
            try:
                return s.decode('utf-8')
            except:
                return s.decode('latin-1')
        else:
            return s
        
    def utf8ify(self,s):
        """encode unicode object or string into byte string in utf-8 representation\
    .                                                                               
           assumes string objects to be utf-8"""
        if not s:
            return ""
        if isinstance(s, str):
            return s
        else:
            return s.encode('utf-8')

        
        

def manage_addZopeOCRFulltextSearchForm(self):
    """add the CXMLRpcServerProxy"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addZopeOCRFulltextSearch.zpt')).__of__(self)
    return pt()

def manage_addZopeOCRFulltextSearch(self,id,serverUrl,searchMethod,indexName,RESPONSE=None):
    """add the basket"""
   
    ob=ZopeOCRFulltextSearch(id,serverUrl,searchMethod,indexName)
    
    self._setObject(id, ob)
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')
