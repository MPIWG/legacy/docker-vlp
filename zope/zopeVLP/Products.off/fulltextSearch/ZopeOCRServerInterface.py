import xmlrpclib
from OFS.SimpleItem import SimpleItem
from Products.PageTemplates.PageTemplateFile import PageTemplateFile
import os.path
from Globals import package_home
import base64
import logging

LANGS={"German":"deu",
       "German (fraktur)":"deu-f",
       "English":"eng",
       "French":"fra",
       "Spanish":"spa",
       "Dutch":"nld",
       "Italian":"ita",
       "Latin":"lic",
       "Greek0":"grl0",
       "Greek1":"grl1"}

class ZopeOCRServerInterface(SimpleItem):
    """makes servery proxy available for zope"""
    
    meta_type="ZopeOCRServerInterface"
    
    manage_options= (
        {'label':'Main Config','action':'mainConfigHTML'},
        )+ SimpleItem.manage_options

 
    def getSupportedLanguages(self):
        """get Hash mit den unterstuetzten Sprachen"""
        return LANGS
    
    def __init__(self,id,serverUrl):
        """init server proxy"""
        self.id=id
        self.serverUrl=serverUrl
     

    def getServerProxy(self,serverUrl=None):
        """get the proxy"""
        if not serverUrl:
            serverUrl=self.serverUrl
      
        pr=xmlrpclib.ServerProxy(serverUrl)
       
        return pr.OCRService
    
    
    def OCRFolder(self,fileToStore,language,REQUEST=None,RESPONSE=None):
        """Send the file to the OCR Server"""
        fileTXT=fileToStore.read()
        
        encoded= base64.standard_b64encode(fileTXT);
        
        print len(encoded)
        ticket= self.getServerProxy().OCRFolder(encoded,"folder.zip",language)

        if not RESPONSE:
            return ticket
        else:
       
            RESPONSE.redirect(REQUEST['HTTP_REFERER'].split('?')[0]+"?ticketNr="+ticket)
        
    
    def getLogFile(self,ticket):
        """get the LogFile"""
        logFile=self.getServerProxy().getLogFile(ticket);
        return logFile
        
    def logFile_html(self,checkTicket):
        """display log file"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','logFile_html.zpt')).__of__(self)
        return pt(checkTicket=checkTicket)
    
    def zipExists(self,ticket):
        """existiert das ZipFile"""
        
        return self.getServerProxy().zipExists(ticket);
        
    def getZipFile(self,ticket,RESPONSE=None):
        """download the zip file"""
        pr = self.getServerProxy()
        
       
        file = pr.getZipFile(ticket)
            
        decoded = base64.standard_b64decode(file)

        if not RESPONSE:
            return decoded
        
        RESPONSE.setHeader("Content-Type","application/octet-stream")
        RESPONSE.setHeader("Content-Disposition","""attachement; filename="%s.zip" """%ticket)
        return decoded
        
    def mainConfigHTML(self):
        """main config form"""
        pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','manageZopeOCRServerInterface.zpt')).__of__(self)
        return pt()
    
    def mainConfig(self,serverUrl,RESPONSE=None):
        """main config"""
        
        self.serverUrl=serverUrl
       
        if RESPONSE:
            RESPONSE.redirect("/manage_main")
            


def manage_addZopeOCRServerInterfaceForm(self):
    """add the CXMLRpcServerProxy"""
    pt=PageTemplateFile(os.path.join(package_home(globals()),'zpt','addZopeOCRServerInterface.zpt')).__of__(self)
    return pt()

def manage_addZopeOCRServerInterface(self,id,serverUrl,RESPONSE=None):
    """add the basket"""
    ob=ZopeOCRServerInterface(id,serverUrl)
    
    self._setObject(id, ob)
    
    if RESPONSE is not None:
        RESPONSE.redirect('manage_main')