#!/bin/sh

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin

# save the log file to currentLogs/
# and rename it according to the date
cd /Library/WebServer/VLP/statistics
n=`date +%Y_%m_%d`
mv access_log currentLogs/$n.log

# clear the original log file
# echo '' > access_log

# move files older than 14 days to archive
#find ./currentLogs -mtime +13 -exec mv '{}' ./archivedLogs \;
sudo -u admin find ./currentLogs -mtime +13 -exec zip -m  ./archivedLogs/logs.zip '{}' \;

# filter the error_log, rename and store it
sudo -u admin egrep -v 'notice|robots\.txt|\.exe|\.ida|\.ico|\.cgi|\.pl|\.asp' error_log > ./errorLogs/$n\_error.log
sudo -u admin cp ./errorLogs/$n\_error.log ./errorLogs/index.txt

# clear the error_log
rm error_log

# restart apache to create a new log file
apachectl graceful

# create a new report
# command for version 5.30a:
#open -a /Applications/Analog5.30a/Analog5.30a /Applications/Analog5.30a/analog.cfg
# new: version analog 6, will read analog.cfg (by default)
sudo -u admin /usr/local/analog/analog

# clean up the pdf directory
find /Library/WebServer/VLP/pdf -atime +2 -exec rm -f '{}' \;

# restart ZOPE
#/Library/StartUpItems/Zope/Zope stop
#/Library/StartUpItems/Zope/Zope start
