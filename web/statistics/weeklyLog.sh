# create a log file weekly report using analog

export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin

# analog version 5.30a
#open -a /Applications/Analog5.30a/Analog5.30a /Applications/Analog5.30a/weekly.cfg

# analog version 6
# default config file ommitted by -G option, 
# alternative config file with +gfilename (note: no blank!)
sudo -u admin /usr/local/analog/analog -G +g/usr/local/analog/weekly.cfg
