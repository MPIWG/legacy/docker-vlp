<?php 
/*	TO DO list
	
	xml formar erweiterung: description tag, item, ...
	group attribute
	preview
	bookmarklet ohne login?!
	print formate / settings form
	printDefaultStyles
	printXmlLink
	printBookmarklet


*/

class linkDirectory extends DomDocument {

	public function __construct($path, $logon) {
		parent::__construct();
		
		// load or create a XML linkdirectory file, if it does not exist yet
		if(!file_exists($path)) {
			$dom = $this->createElement('linkdirectory');
			$dom->setAttribute("version", "1.0");
			$this->appendChild($dom);
			$this->save($path); 
		}
		$this->load($path);
		
		$this->admin = $logon;
		
		if($logon) $this->processRequest();
	}	 
	
	// settings
	var $saveDelete = TRUE;
	var $idTags = array('a', 'div'); // list of nodes (tags) with id attribute
	// status
	var $admin = FALSE;
	var $error = FALSE;
	
	
	private function prepareText($txt) {
	/* trims and quotes the textNode texts */
		return htmlentities(trim($txt));
	}
	
	public function getSections($selectedId) {
	/* returns the options (list of sections) for a HTML select elements */
	
		$option = "";
		$titles = $this->getElementsByTagName('title');
		foreach($titles as $title) {
			$id = $title->parentNode->getAttribute('id');
			$selected = ($selectedId == $id) ? "selected" : "";
			$option .= "		<option value=\"" . $id . "\" ";
			$option .= "$selected>";
			$option .= trim($title->textContent) . "</option>\n";
		}
		return $option;
	}
	
	private function nextId() {
	/* returns the next valid node ID */
	
		$nextId = NULL;
		foreach($this->idTags as $tag) {
			$items = $this->getElementsByTagName($tag);
			foreach($items as $item) {
				$nodeId = $item->getAttribute('id');
				$nextId = max($nextId, (int)$nodeId);
			}
		}
		return $nextId + 1;
	}
	

	public function getElement($id) {
	/* finds a node by ID and returns it */
		
		foreach($this->idTags as $tag) {
			$elements = $this->getElementsByTagName($tag);
			foreach($elements as $element) {
				if($element->getAttribute('id') == $id) return $element;
			}
  		}
	}
	
	private function createLinkNode($id, $title, $link) {
	/* creates a new link node and returns it */
		
		$linkId = ($id == 0)? $this->nextId() : $id;
		
		// create a new link node
		$newLink = $this->createElement('a');
		$newLink->appendChild($this->createTextNode(htmlentities($title)));
		$newLink->setAttribute("id", $linkId);
		$newLink->setAttribute("href", $link);
		return $newLink;
	
	}
	
	private function appendLink($newLink, $sectionId) {
	/* appends a link to a given section */
		
		$sections = $this->getElementsByTagName('div');
		foreach($sections as $section) {
			if($section->getAttribute("id") == $sectionId) {
				$section->appendChild($newLink);
				break;
			}
		}
	}
	
	public function newLink($title, $link, $sectionId) {
	/* creates a new link and appends it to a given section */
				
		$newLink = $this->createLinkNode(0, $title, $link);
		$this->appendLink($newLink, $sectionId);
	}
   
	public function newSection($title) {
	/* creates a new empty section, returns the ID */
	
		$id = $this->nextId();
		
		// create a new title node
		$newTitle = $this->createElement('title');
		$newTitle->appendChild($this->createTextNode(htmlentities($title)));
		
		// create the new div node
		$newSection = $this->createElement('div');
		$newSection->appendChild($newTitle);
		$newSection->setAttribute("id", $id);
					
		// append it
		$linkDirectory = $this->getElementsByTagName('linkdirectory');
		$linkDirectory->item(0)->appendChild($newSection);
		
		return $id;
   }	
   
	public function removeLink($id) {
	/* removes a link by ID */
	
		$link = $this->getElement($id);
		if($link) {
			$link->parentNode->removeChild($link);
		}
   
	}
	
	public function removeSection($sectionId) {
	/* removes a section by ID */
	
		$section = NULL;
		$section = $this->getElement($sectionId);
		if($section) {
			if($section->childNodes->item(1) && $this->saveDelete) return -5; // section not empty
			$ok = $section->parentNode->removeChild($section);
			if($ok) return NULL; // OK
			else return -2; // couldn't remove
		} else return -1; // section not found
	}
	
	public function pasteLink($sourceId, $targetId) {
	/* insert a link before another one */
	
		$sourceNode = $this->getElement($sourceId);
		$targetNode = $this->getElement($targetId);
		if($sourceNode && $targetNode) {
			$targetNode->parentNode->insertBefore($sourceNode, $targetNode);
		}
	}
	
	public function insertLink($sourceId, $targetId) {
	/* ungenutzt? paste into...*/
		$sourceNode = $this->getElement($sourceId);
		$targetNode = $this->getElement($targetId);
		if($sourceNode && $targetNode) {
			$targetNode->appendChild($sourceNode);
		}
	}
	
	public function pasteSection($sourceId, $targetId) {
	/* insert a section before another one */
	
		$sourceNode = $this->getElement($sourceId);
		$targetNode = $this->getElement($targetId);
		if($sourceNode && $targetNode) {
			$targetNode->parentNode->insertBefore($sourceNode, $targetNode);
		}
	}

	public function updateSectionTitle($id, $txt) {
	/* change the title of a section by ID */
	
		$section = $this->getElement($id);
		if($section) {
			$titles = $this->getElementsByTagName('title');
			foreach($titles as $_title) {
				if($_title->parentNode === $section) {
					$newTitle = $this->createElement('title');
					$newTitle->appendChild($this->createTextNode(htmlentities($txt)));
					$section->insertBefore($newTitle, $_title);
					$section->removeChild($_title);
				}
			}
		} else return FALSE;
	}
	
	public function updateLink($id, $title, $link, $sectionId) {
	/* update link data by ID */
			
		$oldLink = $this->getElement($id);
		$newLink = $this->createLinkNode($id, $title, $link);
		if($oldLink->parentNode->getAttribute('id') == $sectionId) {
			$oldLink->parentNode->insertBefore($newLink, $oldLink);
		} else {
			$this->appendLink($newLink, $sectionId);
		}
		$oldLink->parentNode->removeChild($oldLink);
	}
	
	public function printForm() {
	/* prints the HTML form to manage the directory */
			
			if(!$this->admin) return;
			
			// default
			$submitText = " Neu ";
			$action = "new";
			
			// prepare an update and display current values
			if(!empty($_GET["edit"])) {
				$submitText = "Aktualisieren";
				$id = $_GET["edit"];
				$element = $this->getElement($id);
				if($element->tagName == "div") {
					$action = "updateSection";
					$titleStatus = $linkStatus = $selectStatus = "disabled";
					$titles = $this->getElementsByTagName('title');
					$section = $element->getAttribute('id');
					foreach($titles as $_title) {
						if($_title->parentNode === $element) 
							$newSection = trim($_title->textContent);
					}
				} elseif ($element->tagName == "a") {
					$action = "update";
					$title = trim($element->textContent);
					$link = $element->getAttribute('href');;
					$section = $element->parentNode->getAttribute('id');
				}
			} elseif ($this->error) {
				// preserve form data on error
				$title = $_POST["title"];
				$link = $_POST["link"];
				$newSection = $_POST["newSection"];
			} else {
				// bookmarklet
				$title = $_GET["title"];
				$link = $_GET["link"];
			}
	
			$form = "<form action=\"{$_SERVER['PHP_SELF']}\" method=\"post\">\n";
			$form .="<input type=\"hidden\" name=\"action\" value=\"$action\" />\n";
			$form .="<input type=\"hidden\" name=\"id\" value=\"$id\" />\n";
			$form .= "<table>\n<tr>\n";
			$form .= "	<td>Titel</td>\n";	
			$form .= "	<td><input type=\"text\" name=\"title\" value=\"$title\" $titleStatus /></td>\n";
			$form .= "</tr>\n<tr>\n";
			$form .= "	<td>Adresse</td> \n";
			$form .= "<td><input type=\"text\" name=\"link\" value=\"$link\" $linkStatus /></td>\n";
			$form .= "</tr>\n<tr>\n";
			$form .= "	<td>Sektion</td>\n";
			$form .= "	<td><select name=\"sectionId\" $selectStatus>\n" . 
							$this->getSections($section) . "></select></td>\n";
			$form .= "</tr>\n<tr>\n";
			$form .= "	<td>... oder neu:</td>\n"; 
			$form .= "	<td><input type=\"text\" name=\"newSection\" value=\"$newSection\" $sectionStatus/></td>\n";
			$form .= "</tr>\n<tr>\n";
			$form .= "	<td>&nbsp;</td>\n";
			$form .= "	<td align=\"right\"><input type=\"submit\" value=\"$submitText\" name=\"Submit\" " .
				"class=\"submit\"/>	</td>\n";
			$form .= "</tr>\n</table>\n<b style='color:red'>" . $this->error . "</b></form>";
			
			echo $form;

	}
	
	public function printDirectory() {
	/* prints out the directory in HTML */
	
		$close = "";

		foreach ($this->getElementsByTagName("div") as $section) {	  
			
			foreach ($section->childNodes as $item) { 
			
				// Auslesen der Werte
				if ($item->nodeType == XML_ELEMENT_NODE && $item->nodeName == 'title') { 
					$title = $item->textContent;
					$nodeId = $item->parentNode->getAttribute('id');
					echo $close;
					$close = "</ul>\n\n";
					echo "<h3 class='sectionTitle'>$title";
					if($this->admin) {
						echo " [<a href='?deleteSection=$nodeId'>del</a>]";
						echo "[<a href='?cutSection=$nodeId'>cut</a>]";
						echo "[<a href='?edit=$nodeId'>edit</a>]";
						// insert
						if(isset($_GET['cutSection']) && $_GET['cutSection'] != $nodeId) 
							echo "[<a href='?insertSection=" . $_GET['cutSection'] . 
								"&amp;before=$nodeId'>paste before</a>]";
					}
					echo "</h3>\n<ul>\n";
				} 
		
				if ($item->nodeType == XML_ELEMENT_NODE && $item->nodeName == 'a') {
					$link = $item->textContent;
					$href = $item->getAttribute('href');
					$nodeId = $item->getAttribute('id');
					echo "\t\t<li class='item'><a href='$href'>$link</a>";
					if($this->admin) {
						echo " [<a href='?delete=$nodeId'>del</a>]";
						echo "[<a href='?cut=$nodeId'>cut</a>]";
						echo "[<a href='?edit=$nodeId'>edit</a>]";
						// insert
						if(isset($_GET['cut']) && $_GET['cut'] != $nodeId) 
							echo "[<a href='?insert=" . $_GET['cut'] . "&amp;before=$nodeId'>paste before</a>]";
					}
					echo "</li>\n";
				}
			} 
		} 
		
		echo $close;
	}
	
	public function processRequest() {
	/* process the HTTP request to manipulate the directory */
	
		$error = FALSE;
		
		// process GET requests
		if(isset($_GET['delete'])) {
			// remove a link
			$error = $this->removeLink($_GET['delete']);
		} elseif(isset($_GET['deleteSection'])) {
			// remove a section
			$error = $this->removeSection($_GET['deleteSection']);
			
			switch($error) {
				case 0: break;
				case -1: {$error = "Sektion nicht gefunden"; break;}
				case -2: {$error = "Loeschen fehlgeschlagen"; break;}
				case -5: {$error = "Sektion nicht leer."; break;}
			}
		
		} elseif(isset($_GET['insertSection'])) {
			// shift a section
			$error = $this->pasteSection($_GET['insertSection'], $_GET['before']);
		
		} elseif(isset($_GET['insert'])) {
			// shift a link
			$this->pasteLink($_GET['insert'], $_GET['before']);
		
		} elseif(isset($_GET['insertLink'])) {
			// append a link
				$this->appendLink($_GET['appendLink'], $_GET['into']);
		}
		
		// process form data
		if (isset($_POST['Submit'])) {
		
			// check for completeness of form data
			if (!empty($_POST['title'])) $title = $_POST['title'];
			else $error .= "Titel fehlt. ";
		
			if (!empty($_POST['link'])) $link = $_POST['link'];
			else $error .= "Kein Link angegeben. ";
			
			if(!$error && !empty($_POST['newSection'])) 
				$sectionId = $this->newSection($_POST['newSection']);
			else 
				$sectionId = $_POST["sectionId"];
				
			if($_POST["action"] == "new") {
				// add a link
				if (!$error) $this->newLink($title, $link, $sectionId);
			} elseif ($_POST["action"] == "updateSection") {
				// update section title (empty title is OK)
				$this->updateSectionTitle($_POST["id"], $_POST["newSection"]);
				$error = FALSE;
			} elseif ($_POST["action"] == "update") {
				// update link
				if(!$error) $this->updateLink($_POST["id"], $title, $link, $sectionId);
			}
			
		}
		
		// save changes to file
		if($fh = fopen (PATH, 'w')) {
			fwrite($fh, $this->saveXML());
			fclose($fh);
		} else $error .= " Fehler beim Sichern: Kein Schreibzugriff.<br>";
		
		$this->error .= $error;

	}
		
}
?>


<?
define (PATH, "data/articles2.xml");
$logon = TRUE;
// Objekt anlegen
$dom = new linkDirectory(PATH, $logon); 
?>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
	<title>DOM test</title>
	<style type="text/css">
	<!--
	body, table, select, input {font-family:verdana;font-size:10px;}
	form {border: 1px solid #999; background-color:#eee;width: 400px;margin-top:25px; padding:5px;}
	input {width:300px;}
	input.submit {width:100px;}
	-->
	</style>
	</head>
<body>	
<h2>Link Directory</h2>

<?php
// Anzeigen des Verzeichnisses
$dom->printDirectory();
// Formular zur Erfassung
$dom->printForm();
?>
<p>	<a href="<?php echo $_SERVER['PHP_SELF'] ?>">reload</a>	| 
	<a href="<?php echo PATH ?>">xml</a> | 
	<a href="javascript:w=window.open('http://vlp/mb/articles2.php?title='+document.title+'&link='+escape(this.location),'dom')">bookmarklet</a> (in die favoritenleiste ziehen)
</p>
</body> 
</html> 
