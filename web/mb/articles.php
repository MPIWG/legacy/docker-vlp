<?php 
class Articles extends DomDocument {

	public function __construct() {
		parent::__construct();
	}	 
	
	public function addArticle($id, $title, $link) {
		
		// create the new node
		$newItem = $this->createElement('item');
		
		$newTitle = $this->createElement('title');
		$newTitle->appendChild($this->createTextNode($title));
		$newTitle->setAttribute("id", $id);
		
		$newLink = $this->createElement('link');
		$newLink->appendChild($this->createTextNode($link));
		
		$newItem->appendChild($newTitle);
		$newItem->appendChild($newLink);
	
		// append it
		$articles = $this->getElementsByTagName('articles');
		$articles->item(0)->appendChild($newItem);
   }	
   
	public function getArticleById($id) {
	
 		$item = NULL;
		$titles = $this->getElementsByTagName('title');
		foreach($titles as $title) {
			if($title->getAttribute('id') == $id) $item = $title->parentNode;
		}
  		return $item;
  		
	}
	
	public function removeArticle($id) {
	
		$item = $this->getArticleById($id);
		if($item) {
			$articles = $this->getElementsByTagName('articles');
			return $articles->item(0)->removeChild($item);
		}
   
	}
	
	public function pasteArticle($sourceId, $targetId) {
	
		$sourceNode = $this->getArticleById($sourceId);
		$targetNode = $this->getArticleById($targetId);
		if($sourceNode && $targetNode) {
			$articles = $this->getElementsByTagName('articles');
			$insert = $articles->item(0)->insertBefore($sourceNode, $targetNode);
		}
	}
	
}

define ('PATH', "data/articles.xml");

// DOM Objekt anlegen und mit Daten f�llen
$dom = new Articles(); 
$dom->load(PATH); 

// L�schen eines Eintrags
if(isset($_GET['delete'])) {

	$dom->removeArticle($_GET['delete']);

}

// Verschieben eines Eintrags
if(isset($_GET['insert'])) {

	$dom->pasteArticle($_GET['insert'], $_GET['before']);

}

// Hinzuf�gen eines neuen Titels
if (isset($_POST['Submit'])) {

	$error = FALSE;
	
	// Test auf Vollst�ndigkeit der Formulardaten
	if (!empty($_POST['id'])) $id = $_POST['id'];
	else $error = "ID fehlt. ";

	if (!empty($_POST['Titel'])) $title = $_POST['Titel'];
	else $error .= "Titel fehlt. ";

	if (!empty($_POST['Link'])) $link = $_POST['Link'];
	else $error .= "Kein Link angegeben. ";

	if (!$error) $dom->addArticle($id, $title, $link);
	
}

// �nderungen sichern
if($fh = fopen (PATH, 'w')) {

	fwrite($fh, $dom->saveXML());
	fclose($fh);
		
} else $error .= " Fehler beim Sichern: Kein Schreibzugriff.<br>";

?>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
	<title>DOM test</title>
	</head>
<body>	
<?php

if($error) echo $error . "<br>\n";

// Ausgabe des (ge�nderten) DomDocuments
echo "\t<p>Vorhandene Titel:</p>\n\t<ul>";

$nextid = NULL;

foreach ($dom->getElementsByTagName("item") as $articles) {	  
	
	// Schleife �ber alle <items>
	foreach ($articles->childNodes as $item) { 
	
		// Auslesen der Werte
		if ($item->nodeType == XML_ELEMENT_NODE && $item->nodeName == 'title') { 
			$nodeId = $item->getAttribute('id');
			$nextid = max($nextid, $nodeId);
			$title = $item->textContent;
		} 

		if ($item->nodeType == XML_ELEMENT_NODE && $item->nodeName == 'link') {
			$link = $item->textContent;
		}
		
	} 
	
	echo "\t\t<li><a href='$link'>$title</a> ";
	echo "[<a href='?delete=$nodeId'>del</a>]";
	echo "[<a href='?cut=$nodeId'>cut</a>]</li>\n";
	if(isset($_GET['cut']) && $_GET['cut'] != $nodeId) 
		echo "[<a href='?insert=" . $_GET['cut'] . "&before=$nodeId'>paste before</a>]";

} 
echo "\t</ul>\n";
// Formular zur Erfassung
?>
	<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post"> 
		<input type="hidden" name="id" value="<?php echo ($nextid + 1); ?>" />
	<table>
		<tr>
			<td>Titel</td>	
			<td><input type="text" name="Titel" /></td>
		</tr>
		<tr>
			<td>Link</td>   
			<td><input type="text" name="Link" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>	 
			<td><input type="submit" value="Senden" name="Submit"/>	</td>
		</tr>	   
	</table>	   
	</form>
	<a href="<?php echo $_SERVER['PHP_SELF'] ?>">reload</a>
</body> 
</html> 
