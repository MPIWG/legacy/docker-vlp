<?php
$header = "<html>
<head>
	<title> </title>
	<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\">
	
	<link rel=\"stylesheet\"
		href=\"http://vlp.mpiwg-berlin.mpg.de/vlp.css\"
		type=\"text/css\">
	<style type=\"text/css\">
	<!--
		@import \"http://vlp.mpiwg-berlin.mpg.de/linkborder.css\";
	// -->
	</style>
	<style type=\"text/css\">
	
		/* background colors */
		.vario { background-color: #B8C0CB; }
		.stage, .stageSpacer, .stageNav  {background-color:#e6e6e6;}
		
		/* navigation in vario area */
		.liseNav td,.menu p, .liseNav a, select {
			font-family: verdana;
			font-weight: normal;
			font-size: 10px;
		}
		.liseNav {color: #777;}
		.liseNav a {color: #333; text-decoration:underline;}
		.liseNav a:hover {color: white;}
		
		select.pageMenu {width:120px; background-color:#d7d7d7}
		
		.surveyForm {padding: 12px; border: 2px solid white; background-color:#eee}
		h4 {margin-bottom:5px;}
		
	</style>
</head>

<body onload=\"init()\">
<div id=\"center1\" name=\"center1\">

<!-- server message 
<table class=\"msg\">
	<tr>
		<td width=34><img src=\"/images/attention.gif\"></td>
		<td align=\"center\"></td>
	</tr>
</table>
-->
<!-- top navigationbar -->
<div class=\"navbar\">
	  
	<a href=\"http://vlp.mpiwg-berlin.mpg.de/essays\">Essays</a>
	<a href=\"http://vlp.mpiwg-berlin.mpg.de/experiments\">Experiments</a>
	<a href=\"http://vlp.mpiwg-berlin.mpg.de/technology\">Technology</a>
	<a href=\"http://vlp.mpiwg-berlin.mpg.de/objects\">&nbsp;Objects&nbsp;</a>
	<a href=\"http://vlp.mpiwg-berlin.mpg.de/sites\">&nbsp;&nbsp;Sites&nbsp;&nbsp;</a>
	<a href=\"http://vlp.mpiwg-berlin.mpg.de/people\">&nbsp;People&nbsp;</a>
	<a href=\"http://vlp.mpiwg-berlin.mpg.de/concepts\">&nbsp;Concepts&nbsp;</a>
	<a class=\"active\" href=\"http://vlp.mpiwg-berlin.mpg.de/library\">Library</a>

</div>
<br clear=\"all\">
	
<table class=\"content\" border=\"0\" cellpadding=\"8\" cellspacing=\"0\" width=\"780\">
<tr>

<!-- vario bereich -->
<td width=\"182\" class=\"vario\" valign=\"top\" rowspan=\"2\">
<img src=\"http://vlp.mpiwg-berlin.mpg.de/images/space.gif\"
  alt=\"spacer\" width=\"182\" height=\"1\">
<div class=\"menu\">

	<!-- new search -->
	<p><a style=\"text-decoration:underline;\"
       href=\"http://vlp.mpiwg-berlin.mpg.de/library/search\">New Search</a><br><br></p>
		
	<!-- bibliographic data -->
	<p>{$_GET['litRef']}</p>

</div>
</td>

<!-- spacer -->
<td class=\"stageSpacer\" width=\"1\" rowspan=\"2\">
	<img src=\"http://vlp.mpiwg-berlin.mpg.de/images/space.gif\"
   height=\"480\" width=\"1\">
</td>

<td class=\"stage\" valign=\"top\"> 
	<h3>Library Download Manager</h3>

	<p>You are going to download this library item in 
	<a href=\"http://www.adobe.com/products/acrobat/readermain.html\">PDF format</a>.</p>";

$footer = '
</td>
</tr>
</table>

<!-- bottom navbar -->
<div class="navbar">
	<a href="http://vlp.mpiwg-berlin.mpg.de/">Home</a>
	<a href="http://vlp.mpiwg-berlin.mpg.de/about">About</a>
	<a href="http://vlp.mpiwg-berlin.mpg.de/sitemap">Sitemap</a>
	<a href="http://vlp.mpiwg-berlin.mpg.de/Search">Search</a>
	<a href="http://vlp.mpiwg-berlin.mpg.de/contact">&nbsp;Contact&nbsp;</a>
	<a href="http://vlp.mpiwg-berlin.mpg.de/tools">&nbsp;&nbsp;Tools&nbsp;&nbsp;</a>
	<a href="http://vlp.mpiwg-berlin.mpg.de/survey">Survey</a>
	<a href="http://vlp.mpiwg-berlin.mpg.de/users"> myLab </a>
	<a href="http://vlp.mpiwg-berlin.mpg.de/intranet">Intranet</a>
</div>
</div>
</body>
</html>';

$report = false;

// takes a file size in kb and formats to kb or mb resp.
function formatSize($size) {
	return ($size > 1024)? sprintf("%.2f mb", (($size)/1024)) : round($size) . " kb";
}

// create the PDF or show the options dialog
		
	$baseURL = "http://vlp.mpiwg-berlin.mpg.de/";
	$serverRoot = "/mpiwg/online/permanent/vlp";
	
	$folder = "$serverRoot/" . $_GET['litID'] . "/pages" . $_GET['res'];
	$res = ($_GET['res'] == "Lo") ? 75 : 150;
	$imageDir = opendir($folder);
	
	echo $header;
	
	if($report) {
		foreach($_GET as $key => $val) {
		echo "<b>$key</b>: $val<br>";
		}
		echo "<br>";
	}
	if($imageDir) {
		
				
		// create the filelist
		$pageNo = 0;
		$size = 0;
		$filelist = array();
				
		while($imageItem = readdir($imageDir)) { 
		
			if($report) echo $imageItem . "<br>";
			if(substr($imageItem, 0, 1) == "." || substr($imageItem, 0, 1) == ":") { // exclude invisibles
				if($report) echo $imageItem . " dropped<br>";
				continue; 
			}
			$filelist[] = $imageItem;
			
		}
		
		if($report) echo "<br>items: ". count($filelist) . "<br><br>";
		sort($filelist);
		
		// selected 'all pages' or all pages selected! :-)
		$all = $_GET['range'] == "all" || count($filelist) == ($_GET['to'] - $_GET['from'] + 1);
		
		if(! $all) $range = $_GET["from"] . "-" . $_GET["to"];
		$fileName = $_GET['litID'] . "_" . $range . $_GET['res'] . ".pdf";
	
		//create a pdf file
		$fd = fopen("/Library/WebServer/VLP/pdf/" . $fileName, "w");
		$pdf = pdf_new();
		pdf_set_parameter($pdf, "license", "m700102-010000-120560-G3U372-GYWWA2");
		pdf_open_file($pdf, "");
		pdf_set_info($pdf, "Creator", "The Virtual Laboratory");
		pdf_set_info($pdf, "Author", "Max-Planck-Institute for the History of Science, Berlin");
		pdf_set_info($pdf, "Title", "Literature Server Printfile");
		
		// prepare link settings: base url and link style
		//pdf_set_parameter($pdf, "base", $baseURL);
		//pdf_set_borderstyle($pdf, "solid", 0)
		
		// title page
		
		pdf_begin_page($pdf, 595, 842);
		// logo
		$image = pdf_open_image_file($pdf, "jpeg", "pdfLogo.jpg", "", 0);
		pdf_place_image($pdf, $image, 80, 700, 72/200);
		pdf_close_image($pdf, $image);
		pdf_add_weblink($pdf, 80, 700, 80 + ((320*72)/200), 700 + ((179*72)/200), $baseURL);
		// draw a box
		pdf_setlinewidth($pdf, 0.5);
		pdf_rect($pdf, 80, 700, 435, (179*72)/200);
		pdf_stroke($pdf);
		// headline
		$font = pdf_findfont($pdf, "Helvetica-Bold", "winansi", 0);
		pdf_setfont($pdf, $font, 24.0);
		pdf_set_text_pos($pdf, 225, 735);
		pdf_show($pdf, "The Virtual Laboratory");
		pdf_set_text_pos($pdf, 225, 720);
		pdf_setfont($pdf, $font, 10.0);
		pdf_show($pdf, "Max-Planck-Institute for the History of Science, Berlin");
		pdf_continue_text($pdf, "    ISSN 1866-4784 - $baseURL");
		// bibliographic data
		if(! $all)
			$intro = "Excerpt from: \r\r";
		$font = pdf_findfont($pdf, "Helvetica", "winansi", 0);
		pdf_setfont($pdf, $font, 11.0);
		// set the textformat parameter to utf8
		pdf_show_boxed($pdf, $intro . mb_convert_encoding($_GET["litRef"], "ISO-8859-1", "UTF-8"), 
			100, 400, 390, 15 * 10, "left", "");
		/* pdf_show_boxed($pdf, $intro . $_GET["litRef"], 
			100, 400, 390, 15 * 10, "left", ""); */
		// link to lise
		pdf_rect($pdf, 80, 50, 435, 22);
		pdf_stroke($pdf);
		pdf_show_boxed($pdf, $baseURL. "references?id=" . $_GET['litID'],  80, 49, 435, 20, "center", "");
		pdf_add_weblink($pdf, 80, 49, 516, 72, $baseURL . "references?id=" . $_GET['litID']);
		
		pdf_end_page($pdf);
						
		for($n = 0; $n < count($filelist); $n++) {
			if($report) echo "reading item " . ($n+1) . ": $filelist[$n]<br>";
		
			if($_GET['range'] == "all" || ($_GET['from'] <= $n +1 && $n +1 <= $_GET['to'])) {
				if(($image = pdf_open_image_file($pdf, "jpeg", $folder . "/" . $filelist[$n], "", 0)) == 0) {
					echo "warning: error on reading $imageItem<br>\n";
					continue; // fehler! ggf. log
				} else {
					if($report) echo "adding page $filelist[$n]<br>";
					pdf_begin_page($pdf, pdf_get_value($pdf, "imagewidth", $image) * 72/$res, 
						pdf_get_value($pdf, "imageheight", $image) * 72/$res);
					pdf_place_image($pdf, $image, 0.0, 0.0, 72/$res);
					pdf_close_image($pdf, $image);
					
					$pageNo++;
					pdf_end_page($pdf);
				}
			} 
			if($n +1 >= $_GET['to'] && $_GET['range'] != "all") break;
		}
	
		// close the pdf
		pdf_close($pdf);
		fwrite($fd, pdf_get_buffer($pdf));
		fclose($fd);
		pdf_delete($pdf);
		
		
		// download link and file information
		$size = filesize("../pdf/$fileName");
		$page = ($_GET['range'] == "selection") ? "/index_html?ws=1.5&pn=" . $_GET['from']: "";
		echo "<br><div style='margin-left:20px'><a href=\"../pdf/$fileName\"><b>download</b></a> (";
		echo "$pageNo page" . (($pageNo == 1)? ", ": "s, ");
		echo (($_GET['res'] == "Lo")? "low ": "high ") . "resolution, ";
		echo formatSize($size/1024) . ")</div><br>";
		echo "<br><a href='/library/data/" . $_GET['litID'] . $page . "'>back to the source</a>\n\n";
		
	} else echo "<br><br>\nError: item " . $_GET['litID'] . " not found!<br>
		Can not connect to the file server, <br>\n
		please retry in about 10 minutes.";
	@closedir($imageDir);
	echo $footer;

?>
