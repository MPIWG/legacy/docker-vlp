<?php

// stop our standard spammer
if (strstr($_POST["content"], "[url=")) die("Error: Security Violation.");

/* fetch the address list from ZOPE */
$fspec = fopen("http://localhost/groupList?vlp", "r");
if ($fspec) {
	$to = "";
	while($token = fread($fspec, 1024)) {
 		$to .= $token;
	}
}
fclose($fspec);

//$to = "behr@mpiwg-berlin.mpg.de";
$to = "permissions@mpiwg-berlin.mpg.de"; // default addresses

/* prepare the data  */
$next = $_POST["nextPage"];
if($next == '') $next = "/contact/thx.html";

$error = 0;
$expTime = 60000; // form expiring time in seconds
if(substr(md5($_POST["sKey"]), -4, 4) != $_POST["sVal"]) $error = 1;
else if (time() - $expTime > $_POST["sKey"]) $error = 2;

if($error == 0) {
	
	// check subject to prevent mail header injection
	$newLines = array("\n", "\r", "%0A");
	$subject = "[vlp contact form] " . str_replace($newLines, '-', $_POST["subj"]);
	$body = wordwrap(eregi_replace("<[^>]*>", "", $_POST["content"]),  70);
	// check address to prevent mail header injection
	$addressCheck = preg_match
		("/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/", $_POST["from"]);
	$from = "From: " . ($addressCheck? 
				$_POST["from"] : "VLP - no reply <no_reply@mpiwg-berlin.mpg.de>" ). 
		"\n";
	$from .= "Content-type: text/plain; charset=utf-8\n";
	$from .= "X-Originating-IP: " . 
		((getenv(HTTP_X_FORWARDED_FOR))? getenv(HTTP_X_FORWARDED_FOR) : getenv(REMOTE_ADDR))
		. "\n";
	$from .= "X-User-Agent: " . $_SERVER['HTTP_USER_AGENT'] . "\n";
	$from .= "X-Referring-Page: " . $_SERVER['HTTP_REFERER'] . "\n";
	
	if(! $addressCheck) {
		$body .= "\n\n........\n" . 
		"NOTE: mail address discarded by script for security reasons:\n   " . 
		$_POST["from"];
	}
	
	/* send the mail */
	$sendError = (mail($to, $subject, $body, $from))? "mail sent" : "error.";
	
	/* debug control output 
	echo $to . "<br>";
	echo $subject . "<br>";
	echo "<pre><b>" . $body . "</b>\n";
	echo $from . "</pre>";
	echo $error;
	*/
	/* redirect */
	header("Location: $next");	
} else {
	/* redirect */
	header("Location: $next?error=" . $error);	
}

?>