<?php

$caption = addslashes($_POST['caption']); // escape '
// on success forward to:
$loc = "http://vlp.mpiwg-berlin.mpg.de/essays/pictureofthemonth" . 
	"?id={$_POST['id']}&caption={$caption}&nick={$_POST['nick']}";

// simple security check: form is only valid for a limited period of time.
// sKey holds the timestamp, sVal the letters [4:14] of the resulting 
// md5(sKey) string, sLimit sets the allowed duration in seconds.
$sLimit = 180;
$error = 0;

$errors = array("OK", "Key Error: sKey/sVal do not match", "Time Error: form expired");
if(substr(md5($_POST["sKey"]), 4, 10) != $_POST["sVal"]) $error = 1;
else if (time() - $sLimit > $_POST["sKey"]) $error = 2;


/* debug output 
echo "Error: " . $error. " - " . $errors[$error];
echo "<br>key: " . $_POST["sKey"] . "; val: " . $_POST["sVal"] . "; val calc: " . 
	substr(md5($_POST["sKey"]), 4, 10);
echo "<br>elapsed time (sec): " . (time() - $_POST["sKey"]) . "; allowed: " . $sLimit;
exit;
*/

if($error == 0) {

	$to  = (isset($_POST["responsible"])) ? $_POST["responsible"] : "behr";
	$to .= "@mpiwg-berlin.mpg.de";
	
	$subject = "[VLP Picture Of The Month] New Caption";
	
	$body  = "A new caption on the picture of the month has been posted:\n«";
	$body .= wordwrap(eregi_replace("<[^>]*>", "", $_POST["caption"]),  70);
	if(isset($_POST["nick"])) $body .= "»\nposted by " . $_POST["nick"] . "\n\n";
	$body .= "The picture of the month: \n";
	$body .= "  <http://vlp.mpiwg-berlin.mpg.de/essays/pictureofthemonth/>\n";
	$body .= "Spam? Remove it here: \n";
	$body .= "  <http://vlp.mpiwg-berlin.mpg.de/intranet/potm2?picid=" . $_POST["id"]. ">\n";
	
	$from  = "From: VLP web site - no reply <no_reply@mpiwg-berlin.mpg.de>\n";
	$from .= "Content-type: text/plain; charset=utf-8\n";
	$from .= "X-Originating-IP: " . 
		((getenv(HTTP_X_FORWARDED_FOR))? getenv(HTTP_X_FORWARDED_FOR) : getenv(REMOTE_ADDR))
		. "\n";
	$from .= "X-User-Agent: " . $_SERVER['HTTP_USER_AGENT'] . "\n";
	$from .= "X-Referring-Page: " . $_SERVER['HTTP_REFERER'] . "\n";
	
	
	/* send the mail */
	$sendError = (mail($to, $subject, $body, $from)) ? "mail sent" : "error.";
	
	/* debug control output 
	echo "<b>Notify.php</b><br><br>";
	echo "<pre>". $to. "\n";
	echo $subject. "\n\n";
	echo $body . "\n";
	echo $from . "</pre>";
	echo "Result: " . $sendError;
	*/
	
	
	$loc .= "&sKey={$_POST['sKey']}&sVal={$_POST['sVal']}";
}

/* redirect */
header("Location: $loc");	

?>