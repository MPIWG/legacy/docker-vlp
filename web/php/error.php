<?php
$nav ='<a href="javascript:history.back()">go back</a> - <a href="/sitemap">sitemap</a>';

if($_SERVER["QUERY_STRING"] == "404") {
	$msg = "<b>The requested resource could not be found.</b><br>\n
		HTTP Error 404";
} else if ($_SERVER["QUERY_STRING"] == "502") {
	$msg = "<b>Server is restarting. <br>\nPlease retry in a few minutes. Thanks.</b><br>\n
		HTTP Error 502";
	$nav ="";
} else {
	$msg = "An unexected error occurred.";
}
?><html>
<head>
<base href="http://vlp.mpiwg-berlin.mpg.de/" />

<title>The Virtual Laboratory - Error Message</title>

<style type="text/css">
<!--
body, td {
	font-family:verdana, arial, helvetica;
	font-size:12px;
	background-color:#d7d7cc;
	color:#333333;}
h1 {font-family: helvetica, arial, verdana;font-size:20px;margin-bottom:0px}
a {text-decoration:none; color: #333333;font-weight:bold;}
-->
</style>
</head>

<body bgcolor="#d7d7dcc" text="#ffffff">

<table border="0" cellspacing="0" cellpadding="0" width="100%" height="90%">
<tr>
<td align="center">

<h1>The Virtual Laboratory</h1>
	
<a href="/essays/"><img src="/media/frogs.gif" height="181" width="320" vspace="4" alt="Frogs" border="0"></a><br><br>
<?php echo $msg; ?><br>
<br>
<br>
<?php echo $nav; ?>
</td>
</tr>
</table>

</body>
</html>