<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<style type="text/css">
<!--
	body {background-color:#e6e6e6;text-align:left;}
	p {font-family:verdana;font-size:9px;width:400px}
	div {width:450px}
	a {font-family:verdana;font-size:9px;color:black}
-->
</style></head>
<body>
<?php
	if (isset($_POST["start"])) {
	// connect or die
	    $connect = pg_connect("dbname=vlp_data user=vlp");
	    if (!$connect) die(pg_errormessage() . "<br>database error: no connection!");
	
		// prepare the request I: multiple search words
	
		if(isset($_POST["grepmode"])) {
			$prefix = "~ '";
			$postfix = "'";
			$searchString = "(LOWER(title) " . $prefix . addslashes($_POST["query"]) . $postfix;;
		} else {
			$prefix = "LIKE '%";
			$postfix = "%'";
			$query = $_POST["query"];
	
			$searchString = "(LOWER(title) " . $prefix . strtolower(strtok($query, " ")) . $postfix;
			
			for($tok = strtok(" "); 
				$tok != false; 
				$tok = strtok(" ")) { $searchString .= " AND LOWER(title) $prefix" . strtolower($tok) . $postfix; }
		}
		$searchString .= ") ";
	
	// prepare the request II: limit to selected journals
	$limitString = "";
	if($_POST["limit"] == "select" && isset($_POST["journalID"])) {
		$limitString = " AND (fullreference ~ '" . $_POST["journalID"][0] . "'";
		
		for($i = 1; $i < count($_POST["journalID"]); $i++) 
			$limitString .= " OR fullreference ~ '" . $_POST["journalID"][$i] . "'";
			
		$limitString .= ") ";
	}

	
	
	// the results
	echo "<div style='margin-left:20px'>\n";
	echo "<p><b>Results from " . $_POST["start"] . (($_POST["incr"] != 1)? " to " . 
	($_POST["start"] + $_POST["incr"] - 1) : "") . "</b></p>\n\n";	

	$query = "SELECT fullreference, reference, online " .
		"FROM vl_literature " . 
		"WHERE referencetype = 'Journal Article' AND authorized = 1" .
			" AND sql_year BETWEEN " . 
				$_POST["start"] . " AND " . ($_POST["start"] + $_POST["incr"] - 1) .
			" AND " . $searchString .
			$limitString . 
			"ORDER BY fullreference ASC";

	$result = pg_exec($connect, $query);
	$num = pg_numrows($result); 

	for ($i=0; $i<$num; $i++) {
		$row = pg_fetch_row($result, $i, PGSQL_BOTH);
		// highlight the search terms, if possible:
		// php & postgreSQL regexp is not fully compatible
		// don't tokenize grep patterns
		if($_POST["grepmode"] != "1") {
			$row[0] = preg_replace("/(" . strtok($_POST["query"], " ") . ")/i", 
					"<b>\\1</b>", 
					$row[0]);
			for($tok = strtok(" "); $tok != false; $tok = strtok(" ")) {
				$row[0] = eregi_replace("(" . $tok . ")", 
					"<b>\\1</b>", 
					$row[0]);
				}
		} else	{
			// change POSIX word borders (not supported by PHP) to PCRE (\y -> \b)
			$_POST["query"] = str_replace("\\y", "\\b", $_POST["query"]); 
			$row[0] = preg_replace("/(" . $_POST["query"] . ")/i", 
					"<b>\\0</b>", 
					$row[0]);
		}
		
		// record online?
		if($row[2] == 1) 
			$row[0] = "<a href='/references?id={$row[1]}' target='_top'>{$row[0]}</a>";
		
		// display the record
		echo  "<p title=\"" . $row[1] . "\">" . ($i+1) . ". " . $row[0] . "</p>\n";
	}
	echo "</p>";
	echo "</div>\n";
	} else echo "Click the years column to see the results in detail."

?>

</body>
</html>